// /**
//  * @format
//  */

// import {AppRegistry, Image} from 'react-native';
// import React from 'react';
// import {Provider} from 'react-redux';
// // import App from './App';
// import 'react-native-gesture-handler';
// import {name as appName} from './app.json';

// import {initialState} from './src/redux/auth';
// import http from './src/services/http';
// import TokenStorage from './src/services/storage/token';
// import homeIcon from './src/assets/images/homeIcon.png';
// import {NavigationContainer} from '@react-navigation/native';
// import {createStackNavigator} from '@react-navigation/stack';
// import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';

// import configureStore from './src/redux/configureStore';

// import MobileInputScreen from './src/screens/MobileInputScreen';
// import RegistrationScreen from './src/screens/RegistrationScreen';
// import DsUserInfoScreen from './src/screens/DsUserInfoScreen';
// import MainScreen from './src/screens/MainScreen';
// import ChatScreen from './src/screens/ChatScreen';
// import LanguageScreen from './src/screens/LanguageScreen';

// import InputScreen from './src/screens/InputScreen';

// // import WebViewPage from './src/screens/WebViewScreen';
// // import ArchiveScreen from './src/screens/ArchiveScreen';

// const Stack = createStackNavigator();
// const Tabs = createBottomTabNavigator();

// export const createStore = async () => {
//   const [token] = await Promise.all([TokenStorage.get()]);
//   const authenticated = !!token;
//   const user = await http.get(`/cabinet/profile/view?access-token=${token}`);

//   const persistedState = {
//     auth: {
//       ...initialState,
//       authenticated,
//       userData: user.data.data.entity,
//       token: token,
//     },
//   };
//   const store = configureStore(persistedState);
//   AppRegistry.registerComponent(appName, () => App(store));
//   // return store;
// };

// // const store = createStore();

// function ModalRoot() {
//   return (
//     <Stack.Navigator mode="modal" headerMode="none">
//       <Stack.Screen name="InputScreen" component={InputScreen} />
//     </Stack.Navigator>
//   );
// }

// function TabApp() {
//   return (
//     <Tabs.Navigator
//       screenOptions={({route}) => ({
//         tabBarIcon: () => {
//           if (true) {
//             return <Image source={homeIcon} />;
//           }
//         },
//       })}>
//       <Tabs.Screen name="MainScreen" component={MainScreen} />
//     </Tabs.Navigator>
//   );
// }
// function App(store) {
//   return (
//     <Provider store={store}>
//       <NavigationContainer>
//         <Stack.Navigator headerMode="none">
//           <Stack.Screen name="LanguageScreen" component={LanguageScreen} />
//           <Stack.Screen name="ChatScreen" component={ChatScreen} />
//           <Stack.Screen name="TabsApp" component={TabApp} />
//           <Stack.Screen name="MobileInput" component={MobileInputScreen} />
//           <Stack.Screen name="Registration" component={RegistrationScreen} />
//           <Stack.Screen name="DsID" component={DsUserInfoScreen} />
//           <Stack.Screen name="ModalRoot" component={ModalRoot} />
//         </Stack.Navigator>
//       </NavigationContainer>
//     </Provider>
//   );
// }

// AppRegistry.registerComponent(appName, () => App);
