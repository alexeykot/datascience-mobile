/**
 * @format
 */

import {AppRegistry, StatusBar} from 'react-native';
import React from 'react';
import {Provider, connect} from 'react-redux';
import messaging, {AuthorizationStatus} from '@react-native-firebase/messaging';
import Orientation from 'react-native-orientation-locker';
import 'react-native-gesture-handler';
import {name as appName} from './app.json';

import {
  initialState as authInitialState,
  saveNewNotificaion,
  optionsMock,
} from './src/redux/auth';
import {initialState as chatInitialState} from './src/redux/chat';
import http from './src/services/http';
import TokenStorage from './src/services/storage/token';
import NotificationsStorage from './src/services/storage/notifications';
import DisplayBlocksStorage from './src/services/storage/displayBlocks';
import DeviceTokenStorage from './src/services/storage/deviceToken';
import FavoritesStorage from './src/services/storage/favorites';
import SearchHistoryStorage from './src/services/storage/searchHistory';
import HidedChatsStorage from './src/services/storage/hidedChats';
import ChatStorage from './src/services/storage/chat';

import {
  NavigationContainer,
  CommonActions,
  useNavigation,
} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';

import configureStore from './src/redux/configureStore';

import PermissionsScreen from './src/screens/PermissionsScreen';
import MainScreen from './src/screens/MainScreen';
import ChatScreen from './src/screens/ChatScreen';
import LanguageScreen from './src/screens/LanguageScreen';

import EventMainScreen from './src/screens/Event/MainScreen';
import DescriptionScreen from './src/screens/Event/DescriptionScreen';
import SpeakersScreen from './src/screens/Event/SpeakersScreen';
import ProgramScreen from './src/screens/Event/ProgramScreen';
import MapScreen from './src/screens/Event/MapScreen';
import PerfomanceScreen from './src/screens/Event/PerfomanceScreen';
import SpeakerScreen from './src/screens/Event/SpeakerScreen';
import NotificationsScreen from './src/screens/NotificationsScreen';
import PieScreen from './src/screens/PieScreen';
import NewsScreen from './src/screens/NewsScreen';
import TabBar from './src/components/TabBar';
import NewsDetailsScreen from './src/screens/NewsDetailsScreen';
import CrossMembersScreen from './src/screens/CrossMembersScreen';
import MemberScreen from './src/screens/MemberScreen';
import PiesScreen from './src/screens/PiesScreen';
import ProfileScreen from './src/screens/ProfileScreen';
import UserInfoScreen from './src/screens/UserInfoScreen';
import RegistrationScreen from './src/screens/Event/RegistrationScreen';
import DialogsScreen from './src/screens/DialogsScreen';
import DirectChat from './src/screens/DirectChat';
import CreateDialog from './src/screens/CreateDialog';
import httpChat from './src/services/http/chat';
import RoleScreen from './src/screens/Event/RoleScreen';
import FavoritesScreen from './src/screens/FavoritesScreen';
import TicketsScreen from './src/screens/TicketsScreen';
import TicketScreen from './src/screens/TicketScreen';
import AllEventsScreen from './src/screens/AllEventsScreen';
import * as sockets from './startSocket';
import SearchScreen from './src/screens/SearchScreen';
import MyEventsScreen from './src/screens/MyEventsScreen';
import MyPiesScreen from './src/screens/MyPiesScreen';
import MyPieScreen from './src/screens/MyPieScreen';

const Stack = createStackNavigator();
const Tabs = createBottomTabNavigator();
console.disableYellowBox = true;
const createStore = async () => {
  const token = await TokenStorage.get();
  // const token = 'Qm9EBc5Ob6kz8R7JuHUKRchWtjx2D2BKBzY4gOEX6wvxyXo1gFszkOtFMD8z9_SdDZMPTs_RJZNIkRgp-PTotlUp0PUkZEVswf1usjmD_w_nQ1-TXmBm5ID8R2Y6z_L_';
  // await TokenStorage.save(token);

  const favs = await FavoritesStorage.get();
  const hidedChats = await HidedChatsStorage.get();
  const searchHistory = await SearchHistoryStorage.get();

  if (searchHistory === null) {
    await SearchHistoryStorage.save([]);
  }
  if (favs === null) {
    await FavoritesStorage.save([]);
  }
  if (hidedChats === null) {
    await HidedChatsStorage.save([]);
  }

  let user = null;
  try {
    user = await http.get(`/cabinet/profile/view?access-token=${token}`);
    console.log('getUser', user);
  } catch (err) {
    console.log('getUserError', err);
  }
  let unreadInfo = null;
  try {
    unreadInfo = await httpChat.get(
      'api/v1/subscriptions.get?updatedSince=2017-11-25T15:08:17.248Z',
    );
    console.log('initial', unreadInfo);
  } catch (err) {
    console.log('unreadInfo', err);
  }
  const authenticated = Boolean(token && user);

  const notifications = await NotificationsStorage.get();
  const displayBlocks = await DisplayBlocksStorage.get();
  let deviceToken = await DeviceTokenStorage.get();
  if (!deviceToken) {
    try {
      deviceToken = await messaging().getToken();
      await DeviceTokenStorage.save(deviceToken);
    } catch (err) {
      console.log('device`Err', err);
    }
  }
  console.log('deviceTolen', deviceToken);
  const persistedState = {
    auth: {
      ...authInitialState,
      authenticated,
      userData: user ? user.data.data.entity : authInitialState.userData,
      token: user ? token : null,
      notifications: notifications ? notifications : [],
      displayBlocks: displayBlocks ? displayBlocks : optionsMock,
      deviceToken: deviceToken,
    },
    chat: {
      ...chatInitialState,
      // socket: socket,
      // unread: unreadInfo.data,
    },
  };
  const store = configureStore(persistedState);
  return store;
};

function TabApp() {
  return (
    <Tabs.Navigator tabBar={props => <TabBar {...props} />}>
      <Tabs.Screen name="MainScreen" component={MainScreen} />
      <Stack.Screen name="AllEventsScreen" component={AllEventsScreen} />
      <Stack.Screen name="PiesScreen" component={PiesScreen} />
      <Stack.Screen name="DialogsScreen" component={DialogsScreen} />
      <Stack.Screen name="FavoritesScreen" component={FavoritesScreen} />
      <Stack.Screen name="EventMainScreen" component={EventMainScreen} />
      <Stack.Screen
        name="EventDescriptionScreen"
        component={DescriptionScreen}
      />
      <Stack.Screen name="EventSpeakersScreen" component={SpeakersScreen} />
      <Stack.Screen name="EventProgramScreen" component={ProgramScreen} />
      <Stack.Screen name="EventMapScreen" component={MapScreen} />
      <Stack.Screen name="EventPerfomanceScreen" component={PerfomanceScreen} />
      <Stack.Screen name="SpeakerScreen" component={SpeakerScreen} />
      <Stack.Screen
        name="NotificationsScreen"
        component={NotificationsScreen}
      />
      <Stack.Screen name="NewsScreen" component={NewsScreen} />
      <Stack.Screen name="PieScreen" component={PieScreen} />
      <Stack.Screen name="NewsDetailsScreen" component={NewsDetailsScreen} />
      <Stack.Screen name="CrossMembersScreen" component={CrossMembersScreen} />
      <Stack.Screen name="MemberScreen" component={MemberScreen} />
      <Stack.Screen name="ProfileScreen" component={ProfileScreen} />
      <Stack.Screen name="UserInfoScreen" component={UserInfoScreen} />
      <Stack.Screen name="RegistrationScreen" component={RegistrationScreen} />
      <Stack.Screen name="RoleScreen" component={RoleScreen} />
      <Stack.Screen name="DirectChat" component={DirectChat} />
      <Stack.Screen name="CreateDialog" component={CreateDialog} />
      <Stack.Screen name="TicketsScreen" component={TicketsScreen} />
      <Stack.Screen name="TicketScreen" component={TicketScreen} />
      <Stack.Screen name="SearchScreen" component={SearchScreen} />
      <Stack.Screen name="MyEventsScreen" component={MyEventsScreen} />
      <Stack.Screen name="MyPiesScreen" component={MyPiesScreen} />
      <Stack.Screen name="MyPieScreen" component={MyPieScreen} />
    </Tabs.Navigator>
  );
}
function App() {
  const [store, createSt] = React.useState(null);
  const [authenticated, setAuthenticated] = React.useState(false);
  async function requestUserPermission() {
    const authStatus = await messaging().requestPermission();
    console.log('authStatus', authStatus);
  }
  React.useEffect(() => {
    Orientation.lockToPortrait();
    requestUserPermission();
    const bootstrapAsync = async () => {
      try {
        const newStore = await createStore();
        createSt(newStore);
        const token = await TokenStorage.get();
        setAuthenticated(!!token);
      } catch (e) {
        // Restoring token failed
      }
    };

    bootstrapAsync();
  }, []);
  return (
    <>
      {store ? (
        <Provider store={store}>
          <StatusBar barStyle="light-content" backgroundColor="#5954f9" />
          <ScreensComp />
        </Provider>
      ) : null}
    </>
  );
}

function Screens(props) {
  React.useEffect(() => {
    const unsubscribe = messaging().onMessage(async remoteMessage => {
      const notifications = await NotificationsStorage.get();
      let newNotifications = [];
      const messageWithSeenStatus = {
        ...remoteMessage,
        seen: false,
      };
      if (notifications !== null) {
        notifications.push(messageWithSeenStatus);
        await NotificationsStorage.save(notifications);
      } else {
        newNotifications.push(messageWithSeenStatus);
        await NotificationsStorage.save(newNotifications);
      }
      props.saveNewNotificaion(messageWithSeenStatus);
    });
    messaging().setBackgroundMessageHandler(async remoteMessage => {
      const notifications = await NotificationsStorage.get();
      let newNotifications = [];
      const messageWithSeenStatus = {
        ...remoteMessage,
        seen: false,
      };
      if (notifications !== null) {
        notifications.push(messageWithSeenStatus);
        await NotificationsStorage.save(notifications);
      } else {
        newNotifications.push(messageWithSeenStatus);
        await NotificationsStorage.save(newNotifications);
      }
      props.saveNewNotificaion(messageWithSeenStatus);
    });
    messaging()
      .getInitialNotification()
      .then(remoteMessage =>
        console.log('getInitialNotification', remoteMessage),
      );
    return unsubscribe;
  }, [props]);
  console.log('orsrs', props);
  return (
    <NavigationContainer>
      <Stack.Navigator
        initialRouteName={props.authenticated ? 'TabsApp' : 'LanguageScreen'}
        headerMode="none"
        mode="modal">
        <Stack.Screen name="LanguageScreen" component={LanguageScreen} />
        <Stack.Screen name="PermissionsScreen" component={PermissionsScreen} />
        <Stack.Screen name="ChatScreen" component={ChatScreen} />
        <Stack.Screen name="TabsApp" component={TabApp} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

const mapStateToProps = ({auth}) => ({
  notifications: auth.notifications,
  authenticated: auth.authenticated,
});

const mapDispatchToProps = {
  saveNewNotificaion,
};

const ScreensComp = connect(
  mapStateToProps,
  mapDispatchToProps,
)(Screens);

const bgMessaging = async remoteMessage => {
  const notifications = await NotificationsStorage.get();
  notifications.push(remoteMessage);
  await NotificationsStorage.save(notifications);
};

AppRegistry.registerComponent(appName, () => App);
AppRegistry.registerHeadlessTask(
  'ReactNativeFirebaseMessagingHeadlessTask',
  () => bgMessaging,
);
