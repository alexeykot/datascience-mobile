/* eslint-disable no-alert */
export const sendJsonParams = (socket, params) =>
  socket.send(JSON.stringify(params));

// export const createSocket = () => {
//   const socket = new WebSocket('wss://chat-ai-id.dev-vps.ru/websocket');
//   console.log('socket', socket);
//   return socket;
// };

export const connect = socket => {
  try {
    sendJsonParams(socket, {
      msg: 'connect',
      version: '1',
      support: ['1'],
    });
  } catch (err) {
    alert('Ошибка соединения с чатом!');
  }
  console.log('WebSocket-connected');
};

export const login = (socket, chatUserToken) => {
  try {
    sendJsonParams(socket, {
      msg: 'method',
      method: 'login',
      id: chatUserToken.userId,
      params: [{resume: chatUserToken.authToken}],
    });
    console.log('WebSocket-login');
  } catch (err) {
    alert('Ошибка логина в чат!');
  }
};

export const getRooms = socket => {
  sendJsonParams(socket, {
    msg: 'method',
    method: 'rooms/get',
    id: 'getRooms',
    params: [{$date: 1480377601}],
  });
  console.log('WebSocket-getRooms');
};

export const subscribe = socket => {
  sendJsonParams(socket, {
    msg: 'sub',
    name: 'stream-room-messages',
    id: 'subRoom',
    params: ['__my_messages__', false],
  });
  console.log('WebSocket-subscribed');
};

export const test = socket => {
  console.log('sendTest');
  sendJsonParams(socket, {
    msg: 'method',
    method: 'subscriptions/get',
    id: 'status',
    params: [{$date: 1480377601}],
  });
};

export const pong = socket => {
  sendJsonParams(socket, {
    msg: 'pong',
  });
};
