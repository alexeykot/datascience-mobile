export const registrationForm = [
  {
    name: 'surname',
    title: 'Фамилия',
  },
  {
    name: 'name',
    title: 'Имя',
  },
  {
    name: 'thirdname',
    title: 'Отчество',
  },
  {
    name: 'sex',
    title: 'Пол',
  },
  {
    name: 'date',
    title: 'Дата рождения',
  },
  {
    name: 'city',
    title: 'Город',
    isCity: true,
  },
  {
    name: 'email',
    title: 'E-mail',
  },
];
