import styled from 'styled-components';

export const Container = styled.ScrollView`
  /* flex: 1; */
  /* height: 1900px; */
  background-color: white;
`;

export const InfoCard = styled.View`
  width: 100%;
  height: 146px;
  background-color: rgba(98, 94, 249, 0.4);
  /* opacity: 0.38; */
  border-radius: 20px;
  /* box-shadow: 0 5px 48px 3px rgba(19, 57, 110, 0.6); */
  margin-top: 50px;
  flex-direction: column;
  align-items: center;
  padding-top: 77px;
`;

export const RankContainer = styled.View`
  width: 80px;
  height: 24px;
  border-radius: 12px;
  background-color: #ffffff;
  margin-top: 8px;
`;

export const Work = styled.Text`
  color: #ffffff;
  font-family: Roboto;
  font-size: 14px;
  font-weight: 400;
  margin-top: 17px;
`;

export const StatusContainer = styled.View`
  width: 100%;
  height: 48px;
  border-radius: 24px;
  border-width: 2px;
  border-color: rgba(130, 100, 252, 0.38);
  background-color: transparent;
  /* opacity: 0.38; */
  margin-top: 15px;
  justify-content: center;
  align-items: center;
`;

export const StatusText = styled.Text`
  color: #ffffff;
  font-family: Roboto;
  font-size: 14px;
  font-weight: 500;
`;

export const CardContainer = styled.View`
  width: 100%;
  padding: 0 15px;
  background-color: #443fd9;
`;

export const UserName = styled.Text`
  color: #ffffff;
  font-family: Roboto;
  font-size: 20px;
  font-weight: 700;
`;

export const UserAvatar = styled.Image`
  width: 120px;
  height: 120px;
  border-radius: 120px;
  position: absolute;
  /* left: 30%; */
  align-self: center;
  top: -50px;
  background-color: #d7f5ff;
`;

export const EventsContainer = styled.View`
  width: 100%;
  background-color: #443fd9;
  padding-bottom: ${({noPagination}) => (noPagination ? 0 : 44)}px;
`;

export const Title = styled.Text`
  color: #ffffff;
  font-family: Roboto;
  font-size: 18px;
  font-weight: 700;
  margin-left: 15px;
  margin-top: 34px;
  margin-bottom: 10px;
`;

export const BioContainer = styled.View`
  padding-top: 34px;
  padding-left: 38px;
  width: 100%;
`;

export const InfoBlock = styled.View`
  /* height: 144px; */
  justify-content: space-between;
  margin-top: 19px;
  margin-bottom: 39px;
`;

export const BioTitle = styled.Text`
  color: #1b041e;
  font-family: Roboto;
  font-size: 18px;
  font-weight: 700;
`;

export const Row = styled.View``;

export const SubTitle = styled.Text`
  color: #8b8d94;
  font-family: Roboto;
  font-size: 14px;
  font-weight: 400;
  line-height: 18px;
`;

export const SubName = styled.Text`
  color: #1b041e;
  font-family: Roboto;
  font-size: 14px;
  font-weight: 400;
  line-height: 18px;
`;

export const Bio = styled.Text`
  color: #1b041e;
  font-family: Roboto;
  font-size: 14px;
  font-weight: 400;
  padding-right: 40px;
`;

export const MoreButton = styled.TouchableOpacity`
  margin-bottom: 31px;
`;

export const MoreText = styled.Text`
  font-family: Roboto;
  font-size: 14px;
  font-weight: 400;
  color: #5954f9;
`;

export const SocialBlock = styled.View`
  flex-direction: row;
`;

export const SocialButton = styled.TouchableOpacity`
  width: 48px;
  height: 48px;
  background-color: #5954f9;
  margin-left: ${({isMargined}) => (isMargined ? 10 : 0)}px;
  border-radius: 48px;
  margin-top: 20px;
  margin-bottom: 20px;
  justify-content: center;
  align-items: center;
`;

export const Icon = styled.Image``;
