import React from 'react';

import * as S from './styled';
import Header from '../Event/components/Header';
import facebook from '../../assets/images/facebook.png';
import vk from '../../assets/images/vk.png';
import Carousel, {Pagination} from 'react-native-snap-carousel';
import {Dimensions} from 'react-native';
import EventCard from '../MainScreen/components/EventCard';
import FavoritesStorage from '../../services/storage/favorites';
import http from '../../services/http';
import {connect} from 'react-redux';

const data = [
  {
    id: 0,
    name: '123',
  },
  {
    id: 2,
    name: '1234',
  },
  {
    id: 4,
    name: '1235',
  },
  {
    id: 5,
    name: '23123',
  },
];
const info = [
  {
    title: 'Город',
    name: 'Москва',
  },
  {
    title: 'Город',
    name: 'Москва',
  },
  {
    title: 'Город',
    name: 'Москва',
  },
];

const MemberScreen = props => {
  const [activeIndex, setActiveIndex] = React.useState(0);
  const [viewAllText, setViewAllText] = React.useState(false);
  const [events, setEvents] = React.useState([]);
  const [isFavorite, setFavorite] = React.useState(false);
  const {
    route: {
      params: {member},
    },
  } = props;
  React.useEffect(() => {
    async function fetchEvents() {
      const ids = await http.get(`/event/client/index?id=${member.id}`);
      console.log('ids', ids);
      setEvents(ids.data.data.entities);
    }
    fetchEvents();
  }, [member.id]);

  React.useEffect(() => {
    async function isFav() {
      const is = await isAddedToFavorite();
      setFavorite(is);
    }
    isFav();
  }, [isAddedToFavorite, member.id]);

  const deleteFromFavorite = async () => {
    const {
      route: {
        params: {member: memb},
      },
    } = props;
    const favorites = await FavoritesStorage.get();
    const filteredFavorites = favorites.filter(f => f.id !== memb.id);
    setFavorite(false);
    await FavoritesStorage.save(filteredFavorites);
  };

  const addToFavorite = async () => {
    const eventWithType = {
      ...props.route.params.member,
      type: 'Люди',
    };
    setFavorite(true);
    const favorites = await FavoritesStorage.get();
    favorites.push(eventWithType);
    await FavoritesStorage.save([...favorites]);
  };

  const isAddedToFavorite = async () => {
    const {
      route: {
        params: {member: memb},
      },
    } = props;
    const favorites = await FavoritesStorage.get();
    if (!favorites) {
      return false;
    }
    const favoriteEvents = favorites.filter(f => f.type === 'Люди');
    if (!favoriteEvents.length) {
      return false;
    }
    const favEvent = favoriteEvents.find(f => f.id === memb.id);
    return !!favEvent;
  };

  const onLikePress = async () => {
    const isFav = await isAddedToFavorite();
    if (isFav) {
      deleteFromFavorite();
    } else {
      addToFavorite();
    }
  };

  const pagination = () => {
    return (
      <>
        {events.length ? (
          <Pagination
            dotsLength={events.length}
            activeDotIndex={activeIndex}
            dotStyle={{
              width: 14,
              height: 4,
              borderRadius: 2,
              backgroundColor: 'white',
            }}
            inactiveDotStyle={{
              width: 4,
              height: 4,
              borderRadius: 5,
              backgroundColor: 'white',
            }}
            containerStyle={{
              marginTop: -17,
              // marginBottom: -25,
            }}
            inactiveDotOpacity={1}
            inactiveDotScale={1}
          />
        ) : null}
      </>
    );
  };
  console.log('member', member);
  return (
    <S.Container>
      <Header
        props={props}
        color="#443fd9"
        title="Мои знакомые"
        onLikePress={onLikePress}
        isFavorite={isFavorite}
      />
      <S.CardContainer>
        <S.InfoCard>
          <S.UserAvatar source={{uri: member.photo.url}} />
          <S.UserName>{member.fullname}</S.UserName>
          {/* <S.RankContainer />
          <S.Work>{member.fields.activity}</S.Work> */}
        </S.InfoCard>
        <S.StatusContainer>
          <S.StatusText>Участник у вас в знакомых</S.StatusText>
        </S.StatusContainer>
      </S.CardContainer>
      <S.EventsContainer noPagination={events.length > 1}>
        <S.Title>Участник мероприятий</S.Title>
        <Carousel
          layout={'default'}
          data={events}
          containerCustomStyle={{
            paddingLeft: events.length === 1 ? 0 : 15,
          }}
          renderItem={({item, index}) => (
            <EventCard
              isOne={events.length === 1}
              key={item.id}
              event={item}
              onCardPress={() =>
                props.navigation.navigate('EventMainScreen', {
                  event: item,
                })
              }
            />
          )}
          inactiveSlideScale={1}
          inactiveSlideOpacity={1}
          activeSlideAlignment={events.length === 1 ? 'center' : 'start'}
          sliderWidth={Dimensions.get('window').width}
          itemWidth={events.length === 1 ? 330 : 338}
          onSnapToItem={index => setActiveIndex(index)}
        />
        {pagination()}
      </S.EventsContainer>
      <S.BioContainer>
        <S.BioTitle>Общая информация</S.BioTitle>
        <S.InfoBlock>
          {member.city ? (
            <S.Row>
              <S.SubTitle>Город</S.SubTitle>
              <S.SubName>{member.city}</S.SubName>
            </S.Row>
          ) : null}
          <S.SubTitle> </S.SubTitle>
          {member.country ? (
            <S.Row>
              <S.SubTitle>Страна</S.SubTitle>
              <S.SubName>{member.country}</S.SubName>
            </S.Row>
          ) : null}
        </S.InfoBlock>
        <S.BioTitle>Биография</S.BioTitle>
        <S.Bio numberOfLines={viewAllText ? 100 : 7}>
          Президент Высшей школы методологиии, основатель интеллектуального
          кластера «Игры разума», создатель современной модели психотерапии,
          руководитель одного из лучших психотерапевтических центров России,
          автор более сотни научных работ и двенадцати монографий. Читать далее
          ... Научный руководитель лаборатории нейронаук и поведения человека
          Сбербанка, президент Высшей школы методологии, основатель
          интеллектуального кластера «Игры разума» в Санкт-Петербурге. Андрей
          Курпатов создал новое научно-философское направление «методология
          мышления», которое существует на стыке нейрофизиологии, социальной
          психологии, когнитивистики и современной философии сознания. Знания
          методологии мышления лежат в основе запущенного им проекта Академия
          смысла, где участники обучаются технологиям эффективного мышления.
          Андрей Курпатов написал более 100 научных работ и монографий в области
          психотерапии, пограничной психиатрии, психодиагностики,
          психофизиологии, психосоматики. Создал психотерапевтическое
          направление «системная поведенческая психотерапия».
        </S.Bio>
        <S.MoreButton onPress={() => setViewAllText(!viewAllText)}>
          <S.MoreText>
            {!viewAllText ? 'Читать далее ...' : 'Скрыть'}
          </S.MoreText>
        </S.MoreButton>
        {!member.connectFb && !member.connectVk ? null : (
          <>
            <S.BioTitle>Как связаться</S.BioTitle>
            <S.SocialBlock>
              {member.connectVk ? (
                <S.SocialButton>
                  <S.Icon source={vk} />
                </S.SocialButton>
              ) : null}
              {member.connectFb ? (
                <S.SocialButton isMargined>
                  <S.Icon source={facebook} />
                </S.SocialButton>
              ) : null}
            </S.SocialBlock>
          </>
        )}
      </S.BioContainer>
    </S.Container>
  );
};

const mapStateToProps = ({auth}) => ({
  token: auth.token,
});

export default connect(
  mapStateToProps,
  null,
)(MemberScreen);
