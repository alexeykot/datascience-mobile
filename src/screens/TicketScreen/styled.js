import styled from 'styled-components';
import {Dimensions} from 'react-native';

export const Container = styled.ScrollView`
  background-color: #5954f9;
  flex: 1;
  display: flex;
`;

export const Background = styled.ImageBackground`
  width: 100%;
  height: 120px;
  border-top-right-radius: 15px;
  border-top-left-radius: 15px;
  justify-content: center;
  /* align-items: center; */
`;

export const MainContainer = styled.View`
  width: 100%;
  min-height: 40%;
  padding-bottom: 30px;
  background-color: white;
`;

export const AdditionalContainer = styled.View`
  width: 100%;
  height: ${(Dimensions.get('window').height - 120) / 2}px;
  background-color: white;
`;

export const Field = styled.View`
  margin-top: 28px;
  padding-left: 30px;
`;

export const FieldPlaceholder = styled.Text`
  color: #8b8d94;
  font-family: Roboto;
  font-size: 12px;
  font-weight: 400;
`;

export const FieldValue = styled.Text`
  margin-top: 10px;
  color: #1b041e;
  font-family: Roboto;
  font-size: 14px;
  font-weight: 400;
`;

export const SaveButton = styled.TouchableOpacity`
  width: 300px;
  height: 48px;
  border-radius: 24px;
  border: 1px solid #c9ccd5;
  align-self: center;
  justify-content: center;
  align-items: center;
  position: absolute;
  bottom: 10px;
  background-color: white;
`;

export const ButtonText = styled.Text`
  color: #1b041e;
  font-family: Roboto;
  font-size: 14px;
  font-weight: 700;
`;

export const QRCodeWrapper = styled.TouchableOpacity`
  width: 100px;
  height: 100px;
  align-self: center;
  padding: 20px;
  background-color: white;
  justify-content: center;
  align-items: center;
`;

export const LargeContainer = styled.View`
  width: 220px;
  height: 220px;
  padding: 20px;
  background-color: white;
  align-self: center;
  justify-content: center;
  align-items: center;
`;
