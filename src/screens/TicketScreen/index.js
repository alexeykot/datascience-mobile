import React from 'react';
import {PermissionsAndroid, Platform} from 'react-native';
import Modal from 'react-native-modal';
import Dash from 'react-native-dash';
import ViewShot from 'react-native-view-shot';
import moment from 'moment';
import CameraRoll from '@react-native-community/cameraroll';
import QRCode from 'react-native-qrcode-svg';

import * as S from './styled';
import Header from './components/Header';
import Round from './components/Round';
import http from '../../services/http';

const TicketScreen = props => {
  let view = React.useRef();
  const [isFullQr, setFullQr] = React.useState(false);
  const [ticketInfo, setTicketInfo] = React.useState(null);
  React.useEffect(() => {
    async function getTicket() {
      const {data} = await http.get(
        `/event/event/ticket?id=${props.route.params.ticket.id}`,
      );
      setTicketInfo(data.data.entity);
    }
    getTicket();
  }, [ticketInfo, props.route.params.ticket.id]);
  const hasAndroidPermission = async () => {
    const permission = PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE;
    const hasPermission = await PermissionsAndroid.check(permission);
    if (hasPermission) {
      return true;
    }
    const status = await PermissionsAndroid.request(permission);
    return status === 'granted';
  };

  const onPress = async () => {
    if (Platform.OS === 'android' && !(await hasAndroidPermission())) {
      alert('No Permissions!');
      return;
    }
    const uri = await view.capture();
    await CameraRoll.save(uri);
  };

  const openLargeQr = () => setFullQr(true);
  const closeLargeQr = () => setFullQr(false);
  return (
    <>
      <S.Container>
        <Header props={props} title="Мои билеты" />
        <ViewShot
          ref={r => {
            view = r;
          }}
          options={{format: 'png', quality: 1}}>
          <S.Background
            imageStyle={{borderTopLeftRadius: 25, borderTopRightRadius: 25}}
            source={{uri: props.route.params.ticket.src.url}}>
            <S.QRCodeWrapper onPress={openLargeQr}>
              <QRCode
                value={JSON.stringify(props.route.params.ticket.id)}
                size={80}
              />
            </S.QRCodeWrapper>
            <Modal
              onSwipeComplete={closeLargeQr}
              swipeDirection={['down']}
              onBackdropPress={closeLargeQr}
              // style={styles.view}
              isVisible={isFullQr}>
              <S.LargeContainer>
                <QRCode
                  value={JSON.stringify(props.route.params.ticket.id)}
                  size={200}
                />
              </S.LargeContainer>
            </Modal>
            <Round position="bottom-left" />
            <Round position="bottom-right" />
          </S.Background>
          <S.MainContainer>
            <Round position="top-left" />
            <Round position="top-right" />
            <Round position="bottom-left" />
            <Round position="bottom-right" />
            {/* {fields.map(field => (
              <S.Field>
                <S.FieldPlaceholder>{field.title}</S.FieldPlaceholder>
                <S.FieldValue>{field.value}</S.FieldValue>
              </S.Field>
            ))} */}
            {ticketInfo ? (
              <>
                <S.Field>
                  <S.FieldPlaceholder>ФИО</S.FieldPlaceholder>
                  <S.FieldValue>{ticketInfo.ticket.clientName}</S.FieldValue>
                </S.Field>
                <S.Field>
                  <S.FieldPlaceholder>Мероприятие</S.FieldPlaceholder>
                  <S.FieldValue>{ticketInfo.title}</S.FieldValue>
                </S.Field>
                <S.Field>
                  <S.FieldPlaceholder>Дата</S.FieldPlaceholder>
                  <S.FieldValue>
                    {moment(ticketInfo.beginDate).format('D MMMM YYYY')}
                  </S.FieldValue>
                </S.Field>
                <S.Field>
                  <S.FieldPlaceholder>Место проведения</S.FieldPlaceholder>
                  <S.FieldValue>{ticketInfo.place}</S.FieldValue>
                </S.Field>
              </>
            ) : null}
          </S.MainContainer>
          <S.AdditionalContainer>
            <Dash
              style={{width: '100%', height: 1, opacity: 0.2}}
              dashColor="#5954f9"
            />
            <Round position="top-left" />
            <Round position="top-right" />
            {ticketInfo ? (
              <>
                <S.Field>
                  <S.FieldPlaceholder>Номер билета</S.FieldPlaceholder>
                  <S.FieldValue>#{ticketInfo.ticket.id}</S.FieldValue>
                </S.Field>
                <S.Field>
                  <S.FieldPlaceholder>Организатор</S.FieldPlaceholder>
                  <S.FieldValue>{ticketInfo.partner.title}</S.FieldValue>
                </S.Field>
              </>
            ) : null}
          </S.AdditionalContainer>
        </ViewShot>
      </S.Container>
      <S.SaveButton onPress={onPress}>
        <S.ButtonText>Сохранить билет картинкой</S.ButtonText>
      </S.SaveButton>
    </>
  );
};

export default TicketScreen;
