/* eslint-disable react-native/no-inline-styles */
import React from 'react';

import {View} from 'react-native';

const defaultChildStyles = {
  backgroundColor: '#5954f9',
  height: 10,
  width: 10,
};

const defaultViewStyles = {
  width: 20,
  height: 20,
  backgroundColor: 'transparent',
  alignItems: 'center',
};

const styles = {
  'bottom-left': {
    child: {
      ...defaultChildStyles,
      alignSelf: 'flex-start',
      borderTopRightRadius: 50,
    },
    parent: {
      position: 'absolute',
      bottom: 0,
    },
    view: {
      ...defaultViewStyles,
      justifyContent: 'flex-end',
    },
  },
  'bottom-right': {
    child: {
      ...defaultChildStyles,
      alignSelf: 'flex-end',
      borderTopLeftRadius: 50,
    },
    parent: {
      position: 'absolute',
      bottom: 0,
      right: 0,
    },
    view: {
      ...defaultViewStyles,
      justifyContent: 'flex-end',
    },
  },
  'top-left': {
    child: {
      ...defaultChildStyles,
      alignSelf: 'flex-start',
      borderBottomRightRadius: 50,
    },
    parent: {
      position: 'absolute',
      top: 0,
    },
    view: {
      ...defaultViewStyles,
      justifyContent: 'flex-start',
    },
  },
  'top-right': {
    child: {
      ...defaultChildStyles,
      alignSelf: 'flex-end',
      borderBottomLeftRadius: 50,
    },
    parent: {
      position: 'absolute',
      top: 0,
      right: 0,
    },
    view: {
      ...defaultViewStyles,
      justifyContent: 'flex-start',
    },
  },
};

const Round = ({position}) => {
  return (
    <View style={styles[position].parent}>
      <View style={styles[position].view}>
        <View style={styles[position].child} />
      </View>
    </View>
  );
};

export default Round;
