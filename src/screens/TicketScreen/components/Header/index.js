import React from 'react';

import heart from '../../../../assets/images/upload.png';
import arrow from '../../../../assets/images/arrow.png';

import * as S from './styled';

const Header = ({props, title, color, onLikePress}) => {
  return (
    <S.Container color={color}>
      <S.LeftButton onPress={() => props.navigation.goBack()}>
        <S.Icon source={arrow} />
      </S.LeftButton>
      <S.Title>{title}</S.Title>
      <S.RightButton onPress={onLikePress}>
        <S.Icon source={heart} />
      </S.RightButton>
    </S.Container>
  );
};

export default Header;
