import React from 'react';

import * as S from './styled';

const OptionBlock = ({icon, name, onPress, disabled}) => {
  return (
    <S.Container disabled={disabled} onPress={onPress}>
      {icon}
      <S.Title disabled={disabled}>{name}</S.Title>
    </S.Container>
  );
};

export default OptionBlock;
