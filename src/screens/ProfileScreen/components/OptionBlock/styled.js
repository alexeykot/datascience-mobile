import styled from 'styled-components';

export const Container = styled.TouchableOpacity`
  width: 160px;
  height: 98px;
  border-radius: 10px;
  background-color: #ffffff;
  elevation: 20;
  opacity: ${({disabled}) => (disabled ? 0.1 : 1)};
  justify-content: center;
  align-items: center;
`;

export const Icon = styled.Image``;

export const Title = styled.Text`
  /* color: #140c09; */
  color: ${({disabled}) => (disabled ? '#ffff' : '#140c09')};
  font-family: Roboto;
  font-size: 14px;
  margin-top: 10px;
`;
