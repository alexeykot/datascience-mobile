import React from 'react';
import ImagePicker from 'react-native-image-picker';

import * as S from './styled';
import ava from '../../assets/images/ava.png';

import Close from '../../assets/icons/close-01.svg';
import Exit from '../../assets/icons/exit-01.svg';
import Lightning from '../../assets/icons/lightning.svg';
import EventsWhiteIcon from '../../assets/icons/calendar-white.svg';
import EventsIcon from '../../assets/icons/calendar-01.svg';
import InfoIcon from '../../assets/icons/profile-line-01.svg';
import CrossWhiteIcon from '../../assets/icons/user-add-white.svg';
import CrossIcon from '../../assets/icons/user-add-line-01.svg';
import TicketWhiteIcon from '../../assets/icons/ticket-white.svg';
import TicketIcon from '../../assets/icons/ticket-line-01.svg';
import HeartIcon from '../../assets/icons/heart-01.svg';
import HeartWhiteIcon from '../../assets/icons/heart-white.svg';
import ShopIcon from '../../assets/icons/shopping-basket-2-line-01.svg';

import Bonus from '../../assets/icons/bonus.svg';
import {StatusBar} from 'react-native';
import OptionBlock from './components/OptionBlock';
import {connect} from 'react-redux';
import {requestLogOut, requestUpdateAvatar} from '../../redux/auth';
import FavoritesStorage from '../../services/storage/favorites';
import http from '../../services/http';
import StatusModal from '../MainScreen/components/StatusModal';

const ProfileScreen = props => {
  const [clientEvents, setClientEvents] = React.useState([]);
  const [isStatusModalVisible, setStatusModalVisible] = React.useState(false);
  const [cross, setCross] = React.useState([]);
  const [favorites, setFavorites] = React.useState([]);

  React.useEffect(() => {
    props.navigation.addListener('focus', async () => {
      const {data: clientEvs} = await http.get('/event/client/index');
      setClientEvents(clientEvs.data.entities);
      const {data: crossMembers} = await http.get('/event/client/environment');
      setCross(crossMembers.data.entities);
      const favoritesFromStorage = await FavoritesStorage.get();
      setFavorites(favoritesFromStorage);
    });
  });
  const {user: userInfo} = props;
  const logOut = () => {
    props.navigation.navigate('LanguageScreen');
    props.requestLogOut();
  };
  const chooseFile = () => {
    const options = {
      title: 'Выбрать фотографию',
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };
    ImagePicker.showImagePicker(options, response => {
      console.log('Response = ', response);
      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else {
        let source = response;
        props.requestUpdateAvatar(source.data);
      }
    });
  };

  const toggleModal = () => {
    setStatusModalVisible(!isStatusModalVisible);
  };

  return (
    <S.Container>
      <StatusBar barStyle="dark-content" backgroundColor="white" />
      <S.Header>
        <S.HeaderButton onPress={() => props.navigation.navigate('MainScreen')}>
          <Close />
        </S.HeaderButton>
        <S.HeaderButton onPress={logOut}>
          <Exit />
        </S.HeaderButton>
      </S.Header>
      <S.MainBlock>
        <S.Name>
          {userInfo.name} {userInfo.surname}
        </S.Name>
        <S.StatusContainer onPress={toggleModal}>
          <StatusModal
            onPress={() => props.navigation.navigate('AllEventsScreen')}
            isVisible={isStatusModalVisible}
            onClose={toggleModal}
          />
          <S.StatusBlock>
            <S.Status>Соучастник</S.Status>
            <S.StatusHint>Как повысить статус?</S.StatusHint>
          </S.StatusBlock>
          <Lightning />
        </S.StatusContainer>
        <S.Row>
          <OptionBlock
            onPress={() => props.navigation.navigate('UserInfoScreen')}
            icon={<InfoIcon />}
            name="Мои данные"
          />
          <OptionBlock
            disabled={!clientEvents.length}
            // onPress={() => {
            //   props.navigation.navigate('NewsScreen', {
            //     items: clientEvents,
            //     title: 'Мои ближайшие мероприятия',
            //     onCardPress: item =>
            //       props.navigation.navigate('EventMainScreen', {
            //         event: item,
            //       }),
            //   });
            // }}
            onPress={() =>
              props.navigation.navigate('MyEventsScreen', {
                events: clientEvents,
              })
            }
            icon={clientEvents.length ? <EventsIcon /> : <EventsWhiteIcon />}
            name="Мои мероприятия"
          />
        </S.Row>
        <S.Row>
          <OptionBlock
            disabled={!cross.length}
            onPress={() => props.navigation.navigate('CrossMembersScreen')}
            icon={cross.length ? <CrossIcon /> : <CrossWhiteIcon />}
            name="С кем пересекался"
          />
          <OptionBlock
            disabled={!clientEvents.length}
            onPress={() => props.navigation.navigate('TicketsScreen')}
            icon={clientEvents.length ? <TicketIcon /> : <TicketWhiteIcon />}
            name="Мои билеты"
          />
        </S.Row>
        <S.Row>
          <OptionBlock
            disabled={!favorites.length}
            onPress={() => props.navigation.navigate('FavoritesScreen')}
            icon={favorites.length ? <HeartIcon /> : <HeartWhiteIcon />}
            name="Избранное"
          />
          <OptionBlock
            onPress={() => props.navigation.navigate('MyPiesScreen')}
            disabled={false}
            icon={<ShopIcon />}
            name="Мои покупки"
          />
        </S.Row>
        <S.BonusContainer>
          <S.BonusTitle>У вас накоплено</S.BonusTitle>
          <S.Wrapper>
            <S.Bonus>{props.route.params.bonus}</S.Bonus>
            <Bonus fill="white" />
          </S.Wrapper>
        </S.BonusContainer>
        {/* <S.InviteBlock>
          <S.InviteCode>WQXR45ZTYR5</S.InviteCode>
          <S.InviteHint>
            Пригласите друга и получите 100 баллов за его первое посещение
            мероприятия
          </S.InviteHint>
          <S.ButtonsContainer>
            <S.Button>
              <S.ButtonText>Поделиться</S.ButtonText>
            </S.Button>
            <S.SecondaryButton>
              <S.SecondaryButtonText>Копировать</S.SecondaryButtonText>
            </S.SecondaryButton>
          </S.ButtonsContainer>
          <S.InviteLogo source={layers} />
        </S.InviteBlock> */}
        <S.SupportText>Поддержка</S.SupportText>
        <S.SupportHint>
          Дайте нам знать, если у Вас возникли проблемы с сервисом
          <S.SupportButtonText> help@ds-id.ru</S.SupportButtonText>
        </S.SupportHint>
        {/* <S.SupportButton>
          <S.SupportButtonText>Напишите нам</S.SupportButtonText>
        </S.SupportButton> */}
        {!userInfo.photo.url ? (
          <S.AvatarMock onPress={chooseFile}>
            <S.Icon source={ava} />
          </S.AvatarMock>
        ) : (
          <S.Avatar onPress={chooseFile} source={{uri: userInfo.photo.url}}>
            <S.AvatarImage source={{uri: userInfo.photo.url}} />
          </S.Avatar>
        )}
      </S.MainBlock>
    </S.Container>
  );
};

const mapStateToProps = ({auth}) => ({
  user: auth.userData,
  token: auth.token,
});

const mapDispatchToProps = {
  requestLogOut,
  requestUpdateAvatar,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ProfileScreen);
