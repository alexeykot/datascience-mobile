import styled from 'styled-components';

export const Container = styled.ScrollView`
  /* flex: 1; */
  background-color: white;
`;

export const Header = styled.View`
  width: 100%;
  height: 70px;
  background-color: white;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`;

export const HeaderButton = styled.TouchableOpacity`
  width: 50px;
  height: 50px;
  justify-content: center;
  align-items: center;
`;

export const Icon = styled.Image``;

export const MainBlock = styled.View`
  background-color: #5954f9;
  width: 100%;
  margin-top: 45px;
  padding: 0 15px;
  z-index: 1;
`;

export const UpdateAvatar = styled.TouchableOpacity`
  z-index: 111111;
`;

export const AvatarMock = styled.TouchableOpacity`
  width: 120px;
  height: 120px;
  border-radius: 120px;
  background-color: #f2f2ff;
  justify-content: center;
  align-items: center;
  elevation: 20;
  position: absolute;
  top: -51px;
  align-self: center;
`;

export const Avatar = styled.TouchableOpacity`
  width: 120px;
  height: 120px;
  border-radius: 120px;
  position: absolute;
  top: -51px;
  align-self: center;
  z-index: 1111111;
  background-color: transparent;
`;

export const AvatarImage = styled.Image`
  width: 120px;
  height: 120px;
  border-radius: 120px;
`;

export const Name = styled.Text`
  color: #ffffff;
  font-family: Roboto;
  font-size: 20px;
  font-weight: 700;
  margin-top: 85px;
  align-self: center;
`;

export const StatusContainer = styled.TouchableOpacity`
  border-radius: 20px;
  background-color: #625ef9;
  width: 100%;
  height: 78px;
  margin-top: 29px;
  padding: 15px 20px 18px 19px;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`;

export const Status = styled.Text`
  color: #ffffff;
  font-family: Roboto;
  font-size: 18px;
  font-weight: 500;
`;

export const StatusHint = styled.Text`
  opacity: 0.5;
  color: #ffffff;
  font-family: Roboto;
  font-size: 12px;
`;

export const StatusBlock = styled.View`
  height: 100%;
  justify-content: space-between;
`;

export const Row = styled.View`
  flex-direction: row;
  justify-content: space-between;
  margin-top: 10px;
`;

export const BonusContainer = styled.View`
  border-radius: 20px;
  background-color: #625ef9;
  width: 100%;
  height: 58px;
  margin-top: 20px;
  padding: 0 20px 0 23px;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`;

export const Wrapper = styled.View`
  flex-direction: row;
  align-items: center;
`;

export const BonusTitle = styled.Text`
  color: #ffffff;
  font-family: Roboto;
  font-size: 16px;
  font-weight: 700;
`;

export const Bonus = styled.Text`
  color: #ffffff;
  font-family: Roboto;
  font-size: 24px;
  font-weight: 700;
  margin-right: 10px;
`;

export const InviteBlock = styled.View`
  height: 164px;
  width: 100%;
  elevation: 20;
  border-radius: 10px;
  background-color: #ffffff;
  margin-top: 20px;
  padding: 20px;
`;

export const InviteLogo = styled.Image`
  position: absolute;
  top: 7px;
  right: 7px;
`;

export const InviteCode = styled.Text`
  color: #1b041e;
  font-family: Roboto;
  font-size: 18px;
  font-weight: 700;
  text-transform: uppercase;
`;

export const InviteHint = styled.Text`
  color: #1b041e;
  font-family: Roboto;
  font-size: 14px;
  font-weight: 400;
  margin-top: 9px;
  padding-right: 80px;
`;

export const ButtonsContainer = styled.View`
  width: 100%;
  flex-direction: row;
  justify-content: space-between;
  margin-top: 10px;
`;

export const Button = styled.TouchableOpacity`
  width: 140px;
  height: 40px;
  border-radius: 20px;
  background-color: #5954f9;
  justify-content: center;
  align-items: center;
`;

export const ButtonText = styled.Text`
  color: #ffffff;
  font-family: Roboto;
  font-size: 14px;
  font-weight: 500;
`;

export const SecondaryButton = styled.TouchableOpacity`
  width: 140px;
  height: 40px;
  border-radius: 20px;
  border: 1px solid #c9ccd5;
  justify-content: center;
  align-items: center;
`;

export const SecondaryButtonText = styled.Text`
  color: black;
  font-family: Roboto;
  font-size: 14px;
  font-weight: 500;
`;

export const SupportText = styled.Text`
  color: #ffffff;
  font-family: Roboto;
  font-size: 18px;
  font-weight: 700;
  margin-top: 32px;
`;

export const SupportHint = styled.Text`
  margin-top: 8px;
  color: #ffffff;
  font-family: Roboto;
  font-size: 14px;
  font-weight: 400;
  /* padding-right: 50px; */
  margin-bottom: 50px;
`;

export const SupportButton = styled.TouchableOpacity`
  width: 100%;
  height: 40px;
  border-radius: 20px;
  border: 1px solid #c9ccd5;
  /* background-color: #f7f5fa; */
  /* opacity: 0.5; */
  justify-content: center;
  align-items: center;
  margin-top: 10px;
  margin-bottom: 50px;
`;

export const SupportButtonText = styled.Text`
  color: #ffffff;
  font-family: Roboto;
  font-size: 14px;
  font-weight: 700;
`;
