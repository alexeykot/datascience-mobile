import styled from 'styled-components';

export const Container = styled.View`
  width: 100%;
  background-color: #fff;
  align-items: center;
  flex-direction: row;
  padding: 15px 0;
  margin-top: 5px;
  justify-content: center;
`;

export const InputArea = styled.TextInput`
  width: 100%;
  height: 42px;
  padding-left: 0;
  padding-top: 1px;
  border-color: ${({isFocused}) => (isFocused ? '#4eadfe' : '#979797')};
`;

export const Placeholder = styled.Text`
  position: absolute;
  left: 0;
  color: #000;
  font-weight: ${({isFocused}) => (isFocused ? '600' : '100')};
  padding-right: 6;
  padding-left: 0;
`;

export const ClearButton = styled.TouchableOpacity`
  width: 14px;
  height: 14px;
  position: absolute;
  right: 0;
`;

export const ClearIcon = styled.Image`
  width: 100%;
  height: 100%;
`;
