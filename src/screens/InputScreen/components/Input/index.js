import React from 'react';

import {Animated} from 'react-native';
import {connect} from 'react-redux';
import {requestFormData} from '../../../../redux/auth';

import clear from '../../../../assets/images/close.png';
import {animatedTimingTransform} from './helpers';
import * as S from './styled';

const APlaceholder = Animated.createAnimatedComponent(S.Placeholder);
const AInputArea = Animated.createAnimatedComponent(S.InputArea);

class Input extends React.Component {
  state = {
    showPassword: this.props.secureTextEntry,
    isFocused: false,
    showPhoneMask: false,
    ATop: new Animated.Value(21),
    AFontSize: new Animated.Value(16),
    ABorder: new Animated.Value(1),
    value: '',
  };

  componentDidUpdate(prevProps) {
    if (this.props.field.name !== prevProps.field.name) {
      this.setState({value: ''});
    }
  }

  componentDidMount() {
    if (this.props.fieldData) {
      this.setState({ value: this.props.fieldData });
    }
  }
  handleInputFocus = () => {
    if (this.state.value) {
      animatedTimingTransform(this.state.ABorder, 2, 300);
      animatedTimingTransform(this.state.ATop, 0, 300);
      animatedTimingTransform(this.state.AFontSize, 12, 300);
      return;
    }
    this.setState({isFocused: true});
    animatedTimingTransform(this.state.ABorder, 2, 300);
    animatedTimingTransform(this.state.ATop, 0, 300);
    animatedTimingTransform(this.state.AFontSize, 12, 300);
  };

  handleInputBlur = () => {
    if (this.state.value) {
      this.props.saveData(this.props.field.name, this.state.value);
      animatedTimingTransform(this.state.ABorder, 1, 300);
      return;
    }
    this.setState({isFocused: false});
    animatedTimingTransform(this.state.ABorder, 1, 300);
    animatedTimingTransform(this.state.ATop, 21, 300);
    animatedTimingTransform(this.state.AFontSize, 16, 300);
  };

  render() {
    const {field} = this.props;
    return (
      <S.Container>
        <APlaceholder
          style={{top: this.state.ATop, fontSize: this.state.AFontSize}}
          isFocused={this.state.isFocused}>
          {field.title}
        </APlaceholder>
        <AInputArea
          autoFocus={true}
          onChangeText={text => this.setState({value: text})}
          style={{borderBottomWidth: this.state.ABorder}}
          isFocused={this.state.isFocused}
          onFocus={this.handleInputFocus}
          onBlur={this.handleInputBlur}
          value={this.state.value}
          underlineColorAndroid="transparent"
        />
        {this.state.value ? (
          <S.ClearButton onPress={() => this.setState({value: ''})}>
            <S.ClearIcon source={clear} />
          </S.ClearButton>
        ) : null}
      </S.Container>
    );
  }
}

export default Input;
