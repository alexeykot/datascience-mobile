import React from 'react';

import Input from './components/Input';
import {registrationForm} from '../../configs/registation';
import DissmissKeyboard from '../../components/HOC/DismissKeyboard';

import * as S from './styled';

const DismissKeyboardView = DissmissKeyboard(S.Container);
class InputScreen extends React.Component {
  // state = {
  //   name: '',
  //   surname: '',
  //   thirdname: '',
  //   sex: '',
  //   date: '',
  //   city: '',
  //   email: '',
  // };

  render() {
    const {
      route: {
        params: {field, index, saveData, fieldData},
      },
    } = this.props;
    return (
      <DismissKeyboardView>
        <S.Header>
          <S.DiscardButton onPress={() => this.props.navigation.goBack()}>
            <S.DiscardText>Отменить</S.DiscardText>
          </S.DiscardButton>
        </S.Header>
        <Input saveData={saveData} field={field} fieldData={fieldData} />
        <S.ActionsContainer>
          {index ? (
            <S.DiscardButton
              onPress={() =>
                this.props.navigation.navigate('ModalRoot', {
                  screen: 'InputScreen',
                  params: {
                    field: registrationForm[index - 1],
                    index: index - 1,
                  },
                })
              }>
              <S.DiscardText>Назад</S.DiscardText>
            </S.DiscardButton>
          ) : null}
          {registrationForm.length !== index + 1 ? (
            <S.Button
              onPress={() =>
                this.props.navigation.navigate('ModalRoot', {
                  screen: 'InputScreen',
                  params: {
                    field: registrationForm[index + 1],
                    index: index + 1,
                  },
                })
              }>
              <S.ButtonText>Далее</S.ButtonText>
            </S.Button>
          ) : (
            <S.Button>
              <S.ButtonText>Готово</S.ButtonText>
            </S.Button>
          )}
        </S.ActionsContainer>
      </DismissKeyboardView>
    );
  }
}

// const mapDispatchToProps = {
//   requestFormData,
// };

// export default connect(
//   null,
//   mapDispatchToProps,
// )(InputScreen);

export default InputScreen;
