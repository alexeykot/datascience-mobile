import styled from 'styled-components';

export const Container = styled.View`
  flex: 1;
  background-color: white;
  padding: 0 15px;
`;

export const Button = styled.TouchableOpacity`
  width: 100px;
  height: 40px;
  background-color: #4eadfe;
  border-radius: 10px;
  justify-content: center;
  align-items: center;
  margin-left: auto;
`;

export const ButtonText = styled.Text`
  font-size: 14px;
  color: white;
`;

export const Header = styled.View`
  height: 40px;
  width: 100%;
  background-color: white;
  align-items: flex-end;
  justify-content: center;
`;

export const ActionsContainer = styled.View`
  width: 100%;
  flex-direction: row;
  margin-top: 100px;
  align-items: center;
  justify-content: space-between;
`;

export const DiscardButton = styled.TouchableOpacity``;

export const DiscardText = styled.Text`
  color: #4eadfe;
`;

export const StyledInput = styled.TextInput`
  width: 100%;
  height: 40px;
  border-bottom-width: 1px;
  border-color: #c9ccd5;
`;
