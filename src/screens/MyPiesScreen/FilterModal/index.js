import React from 'react';
import {StyleSheet, Switch} from 'react-native';

import Modal from 'react-native-modal';

import mark from '../../../assets/images/mark.png';
import Close from '../../../assets/icons/close-01.svg';

import * as S from './styled';

const filters = [
  'Сначала популярное',
  'Новинки',
  'Сначала дороже',
  'Сначала дешевле',
];

const FilterModal = ({
  isVisible,
  onClose,
  piesLength,
  minBorder,
  maxBorder,
  pieFilter,
  fitBalance,
  bonus,
  onChangeBorder,
  onFilterChange,
  onFitBalanceChange,
  discardAllFilters,
}) => {
  const close = () => {
    onClose();
  };
  return (
    <Modal
      onSwipeComplete={close}
      swipeDirection={['down']}
      onBackdropPress={close}
      style={styles.view}
      isVisible={isVisible}>
      <S.Container>
        <S.Title>Фильтр</S.Title>
        <S.CloseButton onPress={close}>
          <Close />
        </S.CloseButton>
        <S.SubTitle>Показать</S.SubTitle>
        <S.OptionsContainer>
          {filters.map(filter => (
            <>
              <S.Option onPress={() => onFilterChange(filter)}>
                <S.OptionText>{filter}</S.OptionText>
                {filter === pieFilter ? (
                  <S.CheckBox>
                    <S.Icon source={mark} />
                  </S.CheckBox>
                ) : null}
              </S.Option>
              <S.Separator />
            </>
          ))}
        </S.OptionsContainer>
        <S.SubTitle>Количество баллов</S.SubTitle>
        <S.InputsContainer>
          <S.Input
            onChangeText={val => onChangeBorder('minBorder', val)}
            keyboardType="decimal-pad"
            value={minBorder}
          />
          <S.Input
            onChangeText={val => onChangeBorder('maxBorder', val)}
            keyboardType="decimal-pad"
            value={maxBorder}
          />
        </S.InputsContainer>
        <S.FitBalanceContainer>
          <S.OptionText>Соответствует моему балансу</S.OptionText>
          <Switch value={fitBalance} onValueChange={onFitBalanceChange} />
        </S.FitBalanceContainer>
        <S.Hint>Ваш баланс: {bonus} баллов</S.Hint>
        <S.DefaultButton onPress={close}>
          <S.ButtonText>Показать {piesLength} предложения</S.ButtonText>
        </S.DefaultButton>
        <S.SecondaryButton onPress={discardAllFilters}>
          <S.SecondaryButtonText>Сбросить все</S.SecondaryButtonText>
        </S.SecondaryButton>
      </S.Container>
    </Modal>
  );
};

const styles = StyleSheet.create({
  view: {
    justifyContent: 'flex-end',
    margin: 0,
  },
  wrapper: {
    flex: 1,
  },
});

export default FilterModal;
