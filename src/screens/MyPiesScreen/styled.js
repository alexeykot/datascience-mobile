import styled from 'styled-components';

export const PiesContainer = styled.ScrollView``;

export const Row = styled.View`
  width: 100%;
  flex-direction: row;
  justify-content: space-evenly;
`;

export const CountContainer = styled.View`
  width: 330px;
  height: 75px;
  border-radius: 20px;
  background-color: #f2f2ff;
  align-self: center;
  margin-top: 10px;
  padding: 15px;
  justify-content: space-between;
`;

export const CountText = styled.Text`
  color: #1b041e;
  font-family: Roboto;
  font-size: 14px;
  font-weight: 400;
  text-align: left;
`;

export const FakeBlock = styled.View`
  width: 100%;
  height: 200px;
`;
