import styled from 'styled-components';

export const ShopCard = styled.TouchableOpacity`
  width: 330px;
  height: 130px;
  border-radius: 20px;
  background-color: #f2f2ff;
  padding: 15px;
  flex-direction: row;
  align-self: center;
  margin-top: 10px;
`;

export const StatusContainer = styled.View`
  width: 90px;
  height: 24px;
  border-radius: 12px;
  justify-content: center;
  align-items: center;
  background-color: ${({color}) => color};
`;

export const StatusText = styled.Text`
  color: #ffffff;
  font-family: Roboto;
  font-size: 12px;
  font-weight: 400;
`;

export const ShopImage = styled.Image`
  width: 120px;
  height: 100px;
  border-radius: 20px;
`;

export const RankContainer = styled.View`
  flex-direction: row;
`;

export const InfoContainer = styled.View`
  margin-left: 10px;
  flex-direction: column;
  justify-content: space-between;
`;

export const Title = styled.Text`
  width: 130px;
  color: #1b041e;
  font-family: Roboto;
  font-size: 14px;
  font-weight: 700;
  text-align: left;
`;

export const Rank = styled.Text`
  color: #1b041e;
  font-family: Roboto;
  font-size: 16px;
  font-weight: 700;
  margin-right: 5px;
`;

export const CountContainer = styled.View`
  width: 24px;
  height: 24px;
  border-radius: 24px;
  background-color: #5954f9;
  justify-content: center;
  align-items: center;
  position: absolute;
  top: 15px;
  right: 15px;
`;

export const Count = styled.Text`
  color: #ffffff;
  font-family: Roboto;
  font-size: 12px;
  font-weight: 700;
`;
