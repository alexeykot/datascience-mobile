/* eslint-disable react-native/no-inline-styles */
import React from 'react';

import Bonus from '../../../assets/icons/bonus.svg';

import * as S from './styled';

const colorsEnum = {
  1: '#f99635',
  2: '#c9ccd5',
  3: '#3dd576',
};

const PieCard = ({index, pie, onPress, isFirst, status, quantity}) => {
  return (
    <S.ShopCard onPress={onPress} activeOpacity={0.8} isFirst={isFirst}>
      <S.CountContainer>
        <S.Count>{quantity}</S.Count>
      </S.CountContainer>
      <S.ShopImage source={{uri: pie.src.url}} />
      <S.InfoContainer>
        <S.StatusContainer color={colorsEnum[status.id]}>
          <S.StatusText>{status.label}</S.StatusText>
        </S.StatusContainer>
        <S.Title>{pie.title}</S.Title>
        <S.RankContainer>
          <S.Rank>{pie.rank}</S.Rank>
          <Bonus fill="#5954f9" />
        </S.RankContainer>
      </S.InfoContainer>
    </S.ShopCard>
  );
};

export default PieCard;
