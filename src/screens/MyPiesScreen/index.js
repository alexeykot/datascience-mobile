/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {StyleSheet} from 'react-native';

import BaseScreen from '../../components/BaseScreen';
import Bonus from '../../assets/icons/bonus.svg';

import PieCard from './PieCard';

import * as S from './styled';
import http from '../../services/http';
import {ActivityIndicator} from 'react-native';
import {Rank, RankContainer} from './PieCard/styled';

const tabs = ['Ожидает', 'Принят', 'Отменен'];

class MyPiesScreen extends React.Component {
  state = {
    selectedTab: tabs[1],
    filterText: '',
    borderX: 27.3,
    borderW: 58,
    visibleModal: false,
    pieFilter: null,
    minBorder: 0,
    maxBorder: 0,
    fitBalance: false,
    bonus: null,
    pies: [],
    loading: false,
  };

  componentDidMount() {
    this.props.navigation.addListener('focus', async () => {
      this.setState({loading: true});
      const {data} = await http.get('/pie/order/index');
      this.setState({pies: data.data.entities, loading: false});
    });
  }

  toggleCategory = category => {
    const categories = [...this.state.categories];
    const index = categories.findIndex(c => c.id === category.id);
    categories[index].isActive = !categories[index].isActive;
    this.setState({categories: categories});
  };

  filterBySelected = items => {
    console.log('items', items);
    // let arr = [];
    // if (this.state.categories.every(c => !c.isActive)) {
    //   return items;
    // }
    // for (let i = 0; i < this.state.categories.length; i++) {
    //   items.forEach(item => {
    //     if (
    //       item.status.id === this.state.categories[i].id &&
    //       this.state.categories[i].isActive
    //     ) {
    //       arr.push(item);
    //     }
    //   });
    // }
    const arr = items.filter(i => i.status.label === this.state.selectedTab);
    return arr;
  };

  countCost = items => {
    console.log('items12312313', items);
    if (items.length === 1) {
      return items[0].cost;
    }
    console.log(items.reduce((n, {cost}) => n + cost, 0));
    const count = items.reduce((n, {cost}) => n + cost, 0);
    return count;
  };

  // devidePiesToRow = pies => {
  //   let obj = {};
  //   for (let i = 0; i < pies.length; i += 2) {
  //     obj[i] = [pies[i], pies[i + 1]].filter(a => a !== undefined);
  //   }
  //   return obj;
  // };

  render() {
    const {selectedTab} = this.state;
    const filteredItems = this.state.pies.filter(pie =>
      pie.pie.title
        .toLocaleLowerCase()
        .includes(this.state.filterText.toLocaleLowerCase()),
    );
    // const formattedByRow = this.devidePiesToRow(filteredItems);
    return (
      <BaseScreen
        title="Мои плюшки"
        scrollableOutsideContainer
        withSearch
        searchProps={{
          withFilter: true,
          isFiltered: () => false,
          filterText: this.state.filterText,
          onInputChange: text =>
            this.setState({filterText: text.toLocaleLowerCase()}),
          onResetPress: () => this.setState({filterText: ''}),
        }}
        withTabs
        tabsProps={{
          tabs: tabs,
          selectedTab: this.state.selectedTab,
          onTabPress: t => this.setState({selectedTab: t}),
        }}
        content={
          <>
            {/* {selectedTab === 'Ожидает' ? (
              <S.CountContainer>
                <RankContainer>
                  <Rank>
                    {this.countCost(this.filterBySelected(this.state.pies))}
                  </Rank>
                  <Bonus fill="#5954f9" />
                </RankContainer>
                <S.CountText>Общая сумма обмена на плюшки</S.CountText>
              </S.CountContainer>
            ) : null} */}
            <S.PiesContainer contentContainerStyle={style.piesScroll}>
              {this.state.loading && !this.state.pies.length ? (
                <ActivityIndicator
                  style={{marginLeft: '45%'}}
                  color="#5954f9"
                  size="large"
                />
              ) : null}
              {this.filterBySelected(filteredItems).map((pie, index) => (
                <PieCard
                  isFirst={!index}
                  pie={pie.pie}
                  quantity={pie.quantity}
                  status={pie.status}
                  onPress={() =>
                    this.props.navigation.navigate('MyPieScreen', {
                      pie: pie,
                    })
                  }
                />
              ))}
            </S.PiesContainer>
          </>
        }
      />
    );
  }
}

const style = StyleSheet.create({
  piesScroll: {
    // flexDirection: 'row',
    // flexWrap: 'wrap',
    // width: '100%',
    // paddingTop: 10,
    // zIndex: 1,
    // paddingBottom: 50,
  },
});

export default MyPiesScreen;
