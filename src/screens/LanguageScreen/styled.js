import styled from 'styled-components';

export const Container = styled.View`
  flex: 1;
  background-color: white;
  justify-content: flex-end;
  align-items: center;
  padding-bottom: 20px;
`;

export const ButtonsContainer = styled.View`
  flex-direction: row;
`;

export const Button = styled.TouchableOpacity``;

export const Logo = styled.Image`
  width: 180px;
  height: 48px;
`;

export const Icon = styled.Image`
  height: 100px;
  width: 100px;
`;

export const Title = styled.Text`
  color: #8b8d94;
  font-size: 14px;
  font-weight: 500;
  margin-top: 160px;
`;
