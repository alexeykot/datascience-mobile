import React from 'react';
import bigLogo from '../../assets/images/bigLogo.png';
import rus from '../../assets/images/rus.png';
import eng from '../../assets/images/eng.png';
import {requestSignUp} from '../../redux/auth';
import {connect} from 'react-redux';

import * as S from './styled';

const LanguageScreen = props => {
  // React.useEffect(() => {
  //   if (props.authenticated) {
  //     props.navigation.navigate('TabsApp');
  //   }
  // });
  return (
    <S.Container>
      <S.Logo source={bigLogo} />
      <S.Title>Выберите язык / Select a language</S.Title>
      <S.ButtonsContainer>
        <S.Button
          onPress={() => props.navigation.navigate('PermissionsScreen')}>
          <S.Icon source={rus} />
        </S.Button>
        <S.Button>
          <S.Icon source={eng} />
        </S.Button>
      </S.ButtonsContainer>
    </S.Container>
  );
};

const mapDispatchToProps = {
  requestSignUp,
};

const mapStateToProps = ({auth}) => ({
  user: auth.userData,
  authenticated: auth.authenticated,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(LanguageScreen);
