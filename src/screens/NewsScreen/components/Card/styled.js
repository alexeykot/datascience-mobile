import styled from 'styled-components';
import {Dimensions} from 'react-native';

export const Container = styled.TouchableOpacity`
  width: 100%;
  margin-top: 26px;
`;

export const Image = styled.Image`
  width: 100%;
  height: 160px;
  border-radius: 15px;
  background-color: #f7f5fa;
  align-self: center;
`;

export const Title = styled.Text`
  color: #1b041e;
  font-family: Roboto;
  font-size: 14px;
  font-weight: 700;
  margin-top: 8px;
  text-align: left;
  /* padding-horizontal: ${(Dimensions.get('window').width - 284) / 2}; */
`;
