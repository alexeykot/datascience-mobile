import React from 'react';

import * as S from './styled';

const Card = ({item, onCardPress}) => {
  return (
    <S.Container onPress={onCardPress}>
      <S.Image source={{uri: item.src.url}} />
      <S.Title>{item.title}</S.Title>
    </S.Container>
  );
};

export default Card;
