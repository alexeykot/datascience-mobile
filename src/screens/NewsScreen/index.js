import React from 'react';

import * as S from './styled';

import Card from './components/Card';
import BaseScreen from '../../components/BaseScreen';

const defaultTitle = 'Пресса';

const NewsScreen = props => {
  const {items, title, onCardPress} = props.route.params;
  return (
    <BaseScreen
      title={title ? title : defaultTitle}
      scrollableOutsideContainer
      containerPadding={0}
      content={
        <S.NewsContainer
          contentContainerStyle={{paddingBottom: 400, paddingHorizontal: 20}}>
          {items.map(item => (
            <Card
              onCardPress={() => onCardPress(item)}
              key={item.id}
              item={item}
            />
          ))}
        </S.NewsContainer>
      }
    />
  );
};

export default NewsScreen;
