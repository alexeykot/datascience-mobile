import styled from 'styled-components';

export const Container = styled.View`
  background-color: #5954f9;
  flex: 1;
`;

export const NewsContainer = styled.ScrollView`
  width: 100%;
  border-top-left-radius: 30px;
  border-top-right-radius: 30px;
  background-color: #ffffff;
`;
