/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {getTime} from '../../../../helpers/dateHelpers';

import * as S from './styled';

const EventCard = ({event, onPress}) => {
  return (
    <S.Container onPress={onPress}>
      <S.Rounds isRight />
      <S.Rounds />
      <S.Background source={{uri: event.src.url}} />
      <S.InfoContainer>
        <S.Title>{event.title}</S.Title>
        <S.Date>{getTime(event)}</S.Date>
      </S.InfoContainer>
    </S.Container>
  );
};

export default EventCard;
