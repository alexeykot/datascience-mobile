import styled from 'styled-components';

export const Container = styled.TouchableOpacity`
  background-color: white;
  width: 300px;
  height: 140px;
  border-radius: 15px;
  margin-top: 15px;
  z-index: 1;
  elevation: 3;
  flex-direction: row;
`;

export const Rounds = styled.View`
  width: 15px;
  height: 15px;
  border-radius: 15px;
  background-color: white;
  z-index: 1111;
  position: absolute;
  bottom: 40px;
  left: ${({isRight}) => (isRight ? -7.5 : 292.5)}px;
`;

export const Background = styled.Image`
  width: 122px;
  height: 100%;
  border-top-left-radius: 15px;
  border-bottom-left-radius: 15px;
`;

export const Title = styled.Text`
  color: #1b041e;
  font-family: Roboto;
  font-size: 14px;
  font-weight: 700;
`;

export const Date = styled.Text`
  color: #8b8d94;
  font-family: Roboto;
  font-size: 12px;
  font-weight: 400;
`;

export const InfoContainer = styled.View`
  height: 100%;
  width: ${300 - 122}px;
  padding-left: 10px;
  padding-top: 20px;
  padding-bottom: 20px;
  justify-content: space-between;
  border-top-right-radius: 15px;
  border-bottom-right-radius: 15px;
`;
