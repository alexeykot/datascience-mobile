import React from 'react';
import moment from 'moment';

import * as S from './styled';
import EventCard from './components/EventCard';

import http from '../../services/http';
import {connect} from 'react-redux';
import BaseScreen from '../../components/BaseScreen';

const tabs = ['Ближайшие', 'Прошедшие'];

const TicketsScreen = props => {
  const [selectedTab, setSelectedTab] = React.useState(tabs[0]);
  const [events, setEvents] = React.useState([]);
  React.useEffect(() => {
    async function fetchEvents() {
      const clientEvents = await http.get(
        `/event/client/index?access-token=${props.token}`,
      );
      console.log(clientEvents);
      setEvents(clientEvents.data.data.entities);
    }
    fetchEvents();
  }, [props.token, events.length]);

  const filterByDate = evs => {
    const formatted = evs.filter(e =>
      selectedTab === 'Прошедшие'
        ? moment(e.finishDate).isBefore(moment())
        : !moment(e.finishDate).isBefore(moment()),
    );
    return formatted;
  };
  return (
    <BaseScreen
      title="Мои билеты"
      scrollableOutsideContainer
      withTabs
      tabsProps={{
        tabs: tabs,
        onTabPress: tab => setSelectedTab(tab),
        selectedTab: selectedTab,
      }}
      content={
        <>
          <S.VerticalScroll contentContainerStyle={{alignItems: 'center'}}>
            {filterByDate(events).map(e => (
              <EventCard
                onPress={() =>
                  props.navigation.navigate('TicketScreen', {
                    ticket: e,
                  })
                }
                event={e}
              />
            ))}
            <S.FakeBlock />
          </S.VerticalScroll>
        </>
      }
    />
  );
};

const mapStateToProps = ({auth}) => ({
  token: auth.token,
});

export default connect(
  mapStateToProps,
  null,
)(TicketsScreen);
