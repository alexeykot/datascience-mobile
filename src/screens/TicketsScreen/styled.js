import styled from 'styled-components';

export const VerticalScroll = styled.ScrollView`
  align-self: center;
  width: 100%;
`;

export const FakeBlock = styled.View`
  width: 100%;
  height: 400px;
`;
