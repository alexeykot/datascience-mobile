/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {Animated, StyleSheet} from 'react-native';

import Header from '../Event/components/Header';

import * as S from './styled';
import {formatDescription} from '../Event/DescriptionScreen/helpers';
// import {formatEventTime, formatDescription} from './helpers';

class NewsDetailsScreen extends React.Component {
  componentWillMount() {
    this._animatedValue = new Animated.Value(0);
  }
  render() {
    const {event: eventInfo} = this.props.route.params;
    console.log('news', eventInfo);
    const interpolatedOpacity = this._animatedValue.interpolate({
      inputRange: [0, 150],
      outputRange: [1, 0.5],
      extrapolate: 'clamp',
    });

    const event = Animated.event([
      {
        nativeEvent: {
          contentOffset: {
            y: this._animatedValue,
          },
        },
      },
    ]);
    return (
      <>
        <S.Container stickyHeaderIndices={[0]} onScroll={event}>
          <Header props={this.props} title="Описание" withoutRightButton />
          <S.OptionsContainer>
            <Animated.Image
              source={{uri: eventInfo.src.url}}
              style={[
                style.eventImage,
                {
                  width: interpolatedOpacity.interpolate({
                    inputRange: [0.5, 1],
                    outputRange: [250, 330],
                  }),
                  opacity: interpolatedOpacity.interpolate({
                    inputRange: [0.7, 1],
                    outputRange: [0, 1],
                  }),
                  transform: [
                    {
                      rotateX: interpolatedOpacity.interpolate({
                        inputRange: [0.7, 1],
                        outputRange: ['80deg', '0deg'],
                      }),
                    },
                  ],
                },
              ]}
            />
            <Animated.View
              style={{
                marginTop: interpolatedOpacity.interpolate({
                  inputRange: [0.5, 1],
                  outputRange: [50, 100],
                  extrapolate: 'clamp',
                }),
                paddingHorizontal: 15,
                marginBottom: 10,
              }}>
              <S.EventTitle>{eventInfo.title}</S.EventTitle>
              {/* <S.EventDescription>{eventInfo.description}</S.EventDescription> */}
              <S.EventDescription>
                {formatDescription(eventInfo.announce)}
              </S.EventDescription>
              {/* <S.EventDescription>29&nbsp;августа 2020 года в Москве состоится XXI Всероссийская банковская конференция &laquo;Банковская система России-2020: пропорциональное регулирование</S.EventDescription> */}
            </Animated.View>
          </S.OptionsContainer>
        </S.Container>
      </>
    );
  }
}

const style = StyleSheet.create({
  eventImage: {
    height: 180,
    position: 'absolute',
    borderRadius: 15,
    alignSelf: 'center',
    top: -130,
    backgroundColor: 'red',
  },
});

export default NewsDetailsScreen;
