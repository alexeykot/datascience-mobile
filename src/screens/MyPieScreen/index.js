/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {Animated, View} from 'react-native';
import http from '../../services/http';

import Header from '../Event/components/Header';
import {formatDescription} from '../Event/DescriptionScreen/helpers';

import * as S from './styled';

const colorsEnum = {
  1: '#f99635',
  2: '#c9ccd5',
  3: '#3dd576',
};

class MyPieScreen extends React.Component {
  state = {
    event: null,
    enableScrollViewScroll: true,
    showFullText: false,
    isFavorite: false,
    bonus: 0,
    showMoreButton: true,
    showPurchaseModal: false,
    showSuccessModal: false,
    showHowModal: false,
    pieInfo: {
      condition: '',
    },
  };

  componentWillMount() {
    this.props.navigation.addListener('focus', async () => {
      await this.getPieInfo();
    });
  }

  getPieInfo = async () => {
    const {pie} = this.props.route.params;
    const {data: pieInfo} = await http.get(
      `/pie/default/view?id=${pie.pie.id}`,
    );
    this.setState({pieInfo: pieInfo.data.entity});
  };

  onTextLayout = e => {
    if (this.state.showMoreButton) {
      this.setState({
        showFullText: e.nativeEvent.lines.length < 4,
      });
    }
  };

  render() {
    const {pie, status} = this.props.route.params.pie;
    const {showFullText} = this.state;

    return (
      <View
        onStartShouldSetResponderCapture={() => {
          this.setState({enableScrollViewScroll: true});
        }}>
        <S.Container stickyHeaderIndices={[0]}>
          <Header withoutRightButton props={this.props} />
          <S.ImageBackground source={{uri: pie.src.url}} />
          <S.OptionsContainer>
            <Animated.View
              style={{
                marginTop: 30,
                paddingHorizontal: 15,
                marginBottom: 10,
              }}>
              <S.StatusContainer color={colorsEnum[status.id]}>
                <S.StatusText>{status.label}</S.StatusText>
              </S.StatusContainer>
              <S.EventTitle>{pie.title}</S.EventTitle>
              <S.SubTitle>Описание</S.SubTitle>
              <S.EventDescription
                onTextLayout={this.onTextLayout}
                numberOfLines={showFullText ? 5 : 1}>
                {pie.announce}
              </S.EventDescription>
              {!showFullText && (
                <S.MoreButton
                  onPress={() =>
                    this.setState({showFullText: true, showMoreButton: false})
                  }>
                  <S.MoreText>Читать далее..</S.MoreText>
                </S.MoreButton>
              )}
              {this.state.pieInfo.condition ? (
                <>
                  <S.SubTitle>Условия покупки</S.SubTitle>
                  <S.EventDescription>
                    {formatDescription(this.state.pieInfo.condition)}
                  </S.EventDescription>
                </>
              ) : null}
            </Animated.View>
          </S.OptionsContainer>
        </S.Container>
      </View>
    );
  }
}

export default MyPieScreen;
