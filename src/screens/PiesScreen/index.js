/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import moment from 'moment';
import {StyleSheet, View} from 'react-native';

import Header from '../Event/components/Header';

import Search from '../../assets/icons/search.svg';
import Filter from '../../assets/icons/filter.svg';
import * as S from './styled';
import PieCard from './components/PieCard';
import FilterModal from './components/FilterModal';
import http from '../../services/http';
import BaseScreen from '../../components/BaseScreen';

const allCategory = {
  title: 'Все',
  id: 'Все',
};

class PiesScreen extends React.Component {
  state = {
    filterText: '',
    borderX: 10,
    borderW: 58,
    selectedTab: allCategory,
    visibleModal: false,
    pieFilter: null,
    minBorder: 0,
    maxBorder: 0,
    fitBalance: false,
    bonus: null,
  };

  componentWillMount() {
    this.props.navigation.addListener('focus', async () => {
      const {data: bonus} = await http.get('/score/balance/index');
      this.setState({bonus: bonus.data.entity.value});
    });
  }

  titleRefs = [];
  devidePiesToRow = pies => {
    let obj = {};
    for (let i = 0; i < pies.length; i += 2) {
      obj[i] = [pies[i], pies[i + 1]].filter(a => a !== undefined);
    }
    return obj;
  };

  addAllCategory = pies => {
    pies.forEach(pie => {
      const newCat = [...pie.categories, allCategory];
      pie.categories = [...newCat];
      return pie;
    });
    return pies;
  };

  filterByPieFilter = items => {
    let filtered = [];
    if (this.state.pieFilter === 'Сначала дороже') {
      filtered = items.sort((a, b) => b.rank - a.rank);
      return filtered;
    }
    if (this.state.pieFilter === 'Сначала дешевле') {
      filtered = items.sort((a, b) => a.rank - b.rank);
      return filtered;
    }
    if (this.state.pieFilter === 'Новинки') {
      filtered = items.sort((a, b) =>
        moment(b.createdAt, 'DD MM YYYY').diff(
          moment(a.createdAt, 'DD MM YYYY'),
        ),
      );
      return filtered;
    }
    return items;
  };

  filterByRankBorders = items => {
    const {maxBorder, minBorder, fitBalance} = this.state;
    let filtered = [];
    if (maxBorder && minBorder) {
      filtered = items.filter(i => i.rank >= minBorder && i.rank <= maxBorder);
      return filtered;
    }
    if (maxBorder) {
      filtered = items.filter(i => i.rank <= maxBorder);
      return filtered;
    }
    if (minBorder) {
      filtered = items.filter(i => i.rank >= minBorder);
      return filtered;
    }
    if (fitBalance) {
      filtered = items.filter(i => i.rank <= this.state.bonus);
      return filtered;
    }
    return items;
  };

  discardAllFilters = () => {
    this.setState({
      minBorder: 0,
      maxBorder: 0,
      pieFilter: null,
      fitBalance: false,
    });
  };

  onChangeBorder = (type, value) => {
    this.setState({[type]: value, fitBalance: false});
  };

  onFitBalanceChange = () => {
    this.setState({
      fitBalance: !this.state.fitBalance,
      minBorder: 0,
      maxBorder: 0,
    });
  };

  onFilterChange = filter => {
    this.setState({pieFilter: this.state.pieFilter === filter ? null : filter});
  };

  isFiltered = () => {
    const {pieFilter, maxBorder, minBorder, fitBalance} = this.state;
    return pieFilter || maxBorder || minBorder || fitBalance;
  };

  render() {
    const {items, categories} = this.props.route.params;
    const itemsWithAllCategory = this.addAllCategory(items);
    const filteredItemsByCategory = itemsWithAllCategory.filter(item =>
      item.categories.some(cat => cat.title === this.state.selectedTab.title),
    );
    const filteredItems = filteredItemsByCategory.filter(pie =>
      pie.title.toLocaleLowerCase().includes(this.state.filterText),
    );

    const itemsToDisplay = this.state.filterText
      ? filteredItems
      : filteredItemsByCategory;

    const filteredByPieFilter = this.filterByPieFilter(itemsToDisplay);
    const filteredByRank = this.filterByRankBorders(filteredByPieFilter);

    const formattedByRow = this.devidePiesToRow(filteredByRank);
    return (
      <BaseScreen
        title="Приятные плюшки"
        scrollableOutsideContainer
        containerPadding="0 0"
        withSearch
        searchProps={{
          withFilter: true,
          onInputChange: text =>
            this.setState({filterText: text.toLocaleLowerCase()}),
          onFilterPress: () => this.setState({visibleModal: true}),
          isFiltered: () => this.isFiltered(),
          filterText: this.state.filterText,
          onResetPress: () => this.setState({filterText: ''}),
        }}
        content={
          <>
            <S.TabsContainer>
              {[allCategory, ...categories].map((t, index) => (
                <S.Tab
                  onPress={() => {
                    this.setState({selectedTab: t});
                    this.titleRefs[index].measure(
                      (fx, fy, width, height, px, py) => {
                        this.setState({borderW: width, borderX: px});
                      },
                    );
                  }}>
                  <S.TabTitle
                    ref={view => {
                      this.titleRefs[index] = view;
                    }}
                    selected={t === this.state.selectedTab}>
                    {t.title}
                  </S.TabTitle>
                </S.Tab>
              ))}
            </S.TabsContainer>
            <S.BorderContainer>
              <S.SelectedBorder
                borderW={this.state.borderW}
                borderX={this.state.borderX}
              />
            </S.BorderContainer>
            <S.PiesContainer contentContainerStyle={style.piesScroll}>
              {Object.keys(formattedByRow).map(rowKey => (
                <S.Row>
                  {formattedByRow[rowKey].map((pie, index) => (
                    <PieCard
                      isFirst={!index}
                      pie={pie}
                      onPress={() =>
                        this.props.navigation.navigate('PieScreen', {
                          event: pie,
                        })
                      }
                    />
                  ))}
                </S.Row>
              ))}
            </S.PiesContainer>
            <FilterModal
              isVisible={this.state.visibleModal}
              onClose={() => this.setState({visibleModal: false})}
              pieFilter={this.state.pieFilter}
              minBorder={this.state.minBorder}
              maxBorder={this.state.maxBorder}
              onChangeBorder={this.onChangeBorder}
              onFilterChange={this.onFilterChange}
              discardAllFilters={this.discardAllFilters}
              piesLength={filteredByRank.length}
              fitBalance={this.state.fitBalance}
              bonus={this.state.bonus}
              onFitBalanceChange={this.onFitBalanceChange}
            />
          </>
        }
      />
    );
  }
}

const style = StyleSheet.create({
  piesScroll: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    width: '100%',
    paddingTop: 20,
    paddingBottom: 80,
    zIndex: 1,
  },
});

export default PiesScreen;
