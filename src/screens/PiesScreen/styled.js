import styled from 'styled-components';

export const Container = styled.View`
  background-color: #5954f9;
`;

export const OptionsContainer = styled.View`
  width: 100%;
  height: 700px;
  border-top-left-radius: 30px;
  border-top-right-radius: 30px;
  background-color: white;
  margin-top: 20px;
  /* padding: 0 15px; */
  padding: 15px 0;
`;

export const SearchIcon = styled.View`
  position: absolute;
  left: 16px;
  z-index: 1111;
  opacity: 0.5;
`;

export const FilterIcon = styled.TouchableOpacity`
  position: absolute;
  right: 0px;
  border-top-right-radius: 30px;
  padding-top: 5px;
  border-bottom-right-radius: 30px;
  height: 100%;
  width: 50px;
  justify-content: center;
  background-color: white;
  align-items: center;
  z-index: 1111;
`;

export const SearchInput = styled.TextInput`
  width: 100%;
  height: 100%;
  /* padding-left: 15px; */
  border-radius: 20px;
  /* opacity: 0.38; */
`;

export const PiesContainer = styled.ScrollView``;

export const Row = styled.View`
  width: 100%;
  flex-direction: row;
  justify-content: space-evenly;
`;

export const TabsContainer = styled.View`
  width: 100%;
  height: 50px;
  margin-top: 20px;
  flex-direction: row;
  justify-content: space-evenly;
`;

export const Tab = styled.TouchableOpacity`
  height: 100%;
  justify-content: center;
  align-items: center;
`;

export const TabTitle = styled.Text`
  color: ${({selected}) => (selected ? '#5954f9' : '#1b041e')};
  font-family: Roboto;
  font-size: 14px;
  font-weight: 700;
  text-align: center;
`;

export const BorderContainer = styled.View`
  width: 100%;
`;

export const SelectedBorder = styled.View`
  height: 3px;
  border-radius: 2px;
  background-color: #5954f9;
  /* width: 58px; */
  width: ${({borderW}) => borderW}px;
  position: absolute;
  left: ${({borderX}) => borderX}px;
  /* top: -1px; */
  margin-top: 2px;
  z-index: 1111;
`;

export const PaddingBlock = styled.View`
  height: 100px;
  width: 100%;
`;
