import styled from 'styled-components';

export const Container = styled.View`
  /* height: 400px; */
  width: 100%;
  background-color: white;
  border-top-left-radius: 30px;
  border-top-right-radius: 30px;
  padding: 28px 30px 10px 30px;
`;

export const Row = styled.View`
  flex-direction: row;
  justify-content: space-between;
`;

export const Title = styled.Text`
  color: #1b041e;
  font-family: Roboto;
  font-size: 18px;
  font-weight: 700;
`;

export const SubTitle = styled.Text`
  color: #1b041e;
  font-family: Roboto;
  font-size: 16px;
  font-weight: 700;
  margin-top: 36px;
`;

export const Hint = styled.Text`
  color: #8b8d94;
  font-family: Roboto;
  font-size: 14px;
  font-weight: 400;
`;

export const OptionsContainer = styled.View`
  margin-top: 32px;
`;

export const Option = styled.TouchableOpacity`
  flex-direction: row;
  justify-content: space-between;
  height: 40px;
`;

export const Icon = styled.Image``;

export const CheckBox = styled.View`
  width: 24px;
  height: 24px;
  background-color: #5954f9;
  border-radius: 12px;
  justify-content: center;
  align-items: center;
`;

export const Separator = styled.View`
  height: 1px;
  background-color: #c9ccd5;
  opacity: 0.5;
  width: 100%;
  margin-bottom: 19px;
`;

export const OptionText = styled.Text`
  color: #1b041e;
  font-family: Roboto;
  font-size: 14px;
  font-weight: 700;
`;

export const DefaultButton = styled.TouchableOpacity`
  height: 48px;
  border-radius: 24px;
  background-color: #5954f9;
  width: 100%;
  justify-content: center;
  align-items: center;
  margin-top: 32px;
`;

export const SecondaryButton = styled.TouchableOpacity`
  height: 48px;
  border-radius: 24px;
  background-color: white;
  border: 1px solid rgba(0, 0, 0, 0.2);
  width: 100%;
  justify-content: center;
  align-items: center;
  margin-top: 10px;
`;

export const ButtonText = styled.Text`
  color: #ffffff;
  font-family: Roboto;
  font-size: 14px;
  font-weight: 700;
`;

export const SecondaryButtonText = styled.Text`
  color: black;
  font-family: Roboto;
  font-size: 14px;
  font-weight: 700;
`;

export const Input = styled.TextInput`
  width: 140px;
  height: 48px;
  border-radius: 10px;
  border: 1px solid rgba(0, 0, 0, 0.2);
  justify-content: center;
  align-items: center;
  color: black;
  font-weight: 700;
  text-align: center;
`;

export const InputsContainer = styled.View`
  width: 100%;
  flex-direction: row;
  justify-content: space-between;
  margin-top: 20px;
`;

export const FitBalanceContainer = styled.View`
  flex-direction: row;
  width: 100%;
  justify-content: space-between;
  margin-top: 17px;
`;

export const CloseButton = styled.TouchableOpacity`
  position: absolute;
  top: 30px;
  right: 30px;
  width: 30px;
  height: 30px;
  justify-content: center;
  align-items: flex-end;
`;
