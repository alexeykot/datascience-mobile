import styled from 'styled-components';
import {Dimensions} from 'react-native';

export const Container = styled.View`
  background-color: #5954f9;
`;

export const OptionsContainer = styled.View`
  width: 100%;
  height: ${({keyboardHeight}) =>
    Dimensions.get('window').height - 93 - keyboardHeight}px;
  padding-top: 10px;
  border-top-left-radius: 30px;
  border-top-right-radius: 30px;
  background-color: white;
  margin-top: 10px;
`;

export const SearchIcon = styled.View`
  position: absolute;
  right: 14px;
`;

export const EventTime = styled.TextInput`
  /* color: #5754f4;
  font-family: Roboto;
  font-size: 14px;
  font-weight: 700;
  line-height: 14px; */
  /* text-transform: uppercase; */
  /* background-color: red; */
  width: 100%;
  height: 100%;
  padding-left: 15px;
  border-radius: 20px;
  opacity: 0.38;
`;

export const PlaceHolder = styled.Text`
  opacity: 0.38;
  color: #8b8d94;
  font-family: Roboto;
  font-size: 14px;
  font-weight: 500;
  position: absolute;
`;

export const InfoView = styled.View`
  margin-top: 100px;
  padding: 0 15px;
  margin-bottom: 10px;
`;

export const EventTitle = styled.Text`
  color: #1b041e;
  font-family: Roboto;
  font-size: 18px;
  font-weight: 700;
  line-height: 20px;
  text-align: center;
`;

export const EventDescription = styled.Text`
  color: #1b041e;
  font-family: Roboto;
  font-size: 14px;
  font-weight: 400;
  line-height: 22px;
`;

export const Wrapper = styled.ScrollView``;

export const FakeBlock = styled.View`
  width: 80px;
  height: 50px;
  background-color: transparent;
  justify-content: center;
  align-items: flex-start;
`;

export const FakeBlock1 = styled.View`
  width: 50px;
  height: 50px;
  background-color: transparent;
`;

export const MessageRow = styled.View`
  /* width: 100%; */
  width: ${Dimensions.get('window').width}px;
  flex-direction: row;
  justify-content: ${({self}) => (!self ? 'flex-start' : 'flex-end')};
  padding: 0 8px;
  margin-bottom: 15px;
`;

export const BlankView = styled.View`
  width: 100%;
  height: 100px;
`;

export const ReplyContainer = styled.View`
  position: absolute;
  bottom: 60px;
  left: 0px;
  right: 0px;
  background-color: white;
  padding: 0 40px;
  height: 40px;
  align-items: center;
  flex-direction: row;
  justify-content: space-between;
`;

export const TextContainer = styled.View`
  flex-direction: row;
  align-items: center;
  height: 100%;
`;

export const ReplySeparator = styled.View`
  height: 80%;
  width: 2px;
  background-color: #5954f9;
  margin-right: 15px;
`;

export const ResetReplyButton = styled.TouchableOpacity`
  width: 20px;
  height: 20px;
  background-color: white;
  justify-content: center;
  align-items: center;
`;

export const ResetReplyButtonText = styled.Text`
  font-size: 15px;
  color: black;
`;

export const ReplyMessage = styled.Text`
  max-width: 200px;
`;

export const InputContainer = styled.View`
  position: absolute;
  bottom: 0;
  right: 0;
  left: 0;
  width: 100%;
  /* height: 60px; */
  flex-direction: row;
  border: 0px solid #e0e0e7;
  border-top-width: 1px;
  align-items: center;
  background-color: white;
`;

export const Input = styled.TextInput`
  width: 80%;
  height: 60px;
  padding-left: 13px;
`;

export const Button = styled.TouchableOpacity`
  width: 48px;
  height: 48px;
  border-radius: 48px;
  background-color: #5954f9;
  justify-content: center;
  align-items: center;
`;

export const SaveButton = styled.TouchableOpacity`
  width: 90px;
  height: 40px;
  border-radius: 48px;
  background-color: #5954f9;
  justify-content: center;
  align-items: center;
  position: absolute;
  right: 10px;
`;

export const SaveButtonText = styled.Text`
  color: white;
`;

export const SeparatorContainer = styled.View`
  width: 100%;
  margin-bottom: 10px;
`;

export const Separator = styled.View`
  width: 85px;
  height: 20px;
  border-radius: 10px;
  background-color: #c9ccd5;
  justify-content: center;
  align-items: center;
  align-self: center;
`;

export const SeparatorText = styled.Text`
  color: #ffffff;
  font-family: Roboto;
  font-size: 10px;
  font-weight: 500;
`;
