/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {
  FlatList,
  Image,
  Dimensions,
  ActivityIndicator,
  Keyboard,
} from 'react-native';
import moment from 'moment';
import _ from 'lodash';
import * as S from './styled';
import sendIcon from '../../assets/images/sendIcon.png';
import Reply from '../../assets/icons/reply.svg';
import http from '../../services/http';
import httpChat from '../../services/http/chat';
import {connect} from 'react-redux';
import {requestGetMyself} from '../../redux/chat';
import SelfMessage from './components/SelfMessage';
import OpponentMessage from './components/OpponentMessage';
import Header from './components/Header';
import {
  addDateSeparators,
  splitRepliedMessage,
  isRepliedMessage,
} from './helpers';
import {AutoGrowingTextInput} from 'react-native-autogrow-textinput';
import * as sockets from '../../../startSocket';

class DirectChat extends React.Component {
  state = {
    messages: [],
    message: '',
    presence: null,
    optionVisible: '',
    editing: false,
    editingMessage: {},
    replying: false,
    replyMessage: null,
    keyBoardHeight: 0,
    lastSeen: null,
    scrollOffSet: 0,
    showReplyIcons: false,
  };
  inputRef;
  scrollRef;

  onKeyboardDidShow = e => {
    this.setState({keyBoardHeight: e.endCoordinates.height});
  };

  onKeyboardDidHide = e => {
    this.setState({keyBoardHeight: 0});
  };

  componentWillMount() {
    Keyboard.addListener('keyboardDidShow', this.onKeyboardDidShow);
    Keyboard.addListener('keyboardDidHide', this.onKeyboardDidHide);
    let interval = null;
    this.props.navigation.addListener('blur', () => {
      clearInterval(interval);
    });
    this.props.navigation.addListener('focus', async () => {
      const {socket, roomId} = this.props.route.params;
      sockets.test(socket);
      interval = setInterval(this.getUserPresence, 1000);
      socket.onmessage = async ({data}) => {
        const formatted = JSON.parse(data);
        console.log('DirectChat - data', formatted);
        if (formatted.msg === 'ping') {
          sockets.pong(socket);
        }
        if (formatted.id === 'getHistory' && formatted.msg === 'result') {
          this.setState({
            messages: formatted.result.messages,
          });
        }
        if (formatted.msg === 'changed') {
          socket.send(
            JSON.stringify({
              msg: 'method',
              method: 'loadHistory',
              id: 'getHistory',
              params: [roomId, null, 50, {$date: 1480377601}],
            }),
          );
          this.setRead(roomId);
        }
        if (formatted.id === 'delete-msg') {
          socket.send(
            JSON.stringify({
              msg: 'method',
              method: 'loadHistory',
              id: 'getHistory',
              params: [roomId, null, 50, {$date: 1480377601}],
            }),
          );
          this.setRead(roomId);
        }
        if (formatted.id === 'status') {
          console.log('changedStatus', formatted.result);
          const room = formatted.result.update.find(i => i.rid === roomId);
          console.log('chanaaaa', room);
          this.setState({lastSeen: room.ls.$date});
        }
      };
      socket.send(
        JSON.stringify({
          msg: 'method',
          method: 'loadHistory',
          id: 'getHistory',
          params: [roomId, null, 50, {$date: 1480377601}],
        }),
      );
    });
  }

  setRead = async rid => {
    await httpChat.post('/api/v1/subscriptions.read', {
      rid: rid,
    });
  };

  getUserPresence = async () => {
    const {opponentName} = this.props.route.params;
    const {data} = await httpChat.get(
      `/api/v1/users.getPresence?username=${opponentName}`,
    );
    this.setState({presence: data});
  };

  sendMessage = () => {
    const {socket, roomId} = this.props.route.params;
    let {message} = this.state;
    if (this.state.replying) {
      message = `[ ](http://chat-ai-id.dev-vps.ru/direct/wwbormetest34?msg=${
        this.state.replyMessage._id
      }) ${message}`;
    }
    socket.send(
      JSON.stringify({
        msg: 'method',
        method: 'sendMessage',
        id: 'sendMessage',
        params: [
          {
            rid: roomId,
            msg: message,
          },
        ],
      }),
    );
    socket.send(
      JSON.stringify({
        msg: 'method',
        method: 'loadHistory',
        id: 'getHistory',
        params: [roomId, null, 50, {$date: 1480377601}],
      }),
    );
    this.setState({message: ''});
    this.resetReply();
    this.inputRef.blur();
  };

  deleteMessage = async message => {
    const {socket, roomId} = this.props.route.params;
    socket.send(
      JSON.stringify({
        msg: 'method',
        method: 'deleteMessage',
        id: 'delete-msg',
        params: [{_id: message._id}],
      }),
    );
    socket.send(
      JSON.stringify({
        msg: 'method',
        method: 'loadHistory',
        id: 'getHistory',
        params: [roomId, null, 50, {$date: 1480377601}],
      }),
    );
    this.setState({optionVisible: ''});
  };

  replyMessage = message => {
    console.log('reply-message', message);
    this.inputRef.focus();
    this.setState({replying: true, replyMessage: message});
  };

  setOptions = id => this.setState({optionVisible: id});

  editMessage = item => {
    this.setState({
      message: item.msg,
      editingMessage: item,
      editing: true,
      optionVisible: '',
    });
    this.inputRef.focus();
  };

  saveMessage = async () => {
    const {socket, roomId} = this.props.route.params;
    const message = this.state.editingMessage;
    await httpChat.post('/api/v1/chat.update', {
      roomId: message.rid,
      msgId: message._id,
      text: this.state.message,
    });
    // try {
    //   const {data: msg} = await httpChat.get(`/api/v1/chat.getMessage?msgId=${message._id}`);
    //   console.log('msg', msg);
    // } catch (err) {
    //   console.log('er', err.response);
    // }

    socket.send(
      JSON.stringify({
        msg: 'method',
        method: 'loadHistory',
        id: 'getHistory',
        params: [roomId, null, 50, {$date: 1480377601}],
      }),
    );
    this.setState({editing: false, message: ''});
    this.inputRef.blur();
  };

  resetReply = () => this.setState({replying: false, replyMessage: null});
  scrollRefs = [];
  render() {
    return (
      <>
        <S.Container>
          <Header
            props={this.props}
            title={this.props.route.params.fullName}
            presence={this.state.presence}
            lastSeen={this.state.lastSeen}
          />
          <S.OptionsContainer keyboardHeight={this.state.keyBoardHeight}>
            <FlatList
              // data={addDateSeparators(this.state.messages).reverse()}
              data={addDateSeparators(this.state.messages)}
              inverted
              contentContainerStyle={{paddingTop: 100}}
              keyExtractor={item => item._id}
              renderItem={({item, index}) => {
                return (
                  <>
                    {index === 0 ? <S.FakeBlock1 /> : null}
                    <S.Wrapper
                      snapToAlignment="center"
                      horizontal
                      ref={r => {
                        this.scrollRefs[index] = r;
                      }}
                      snapToInterval={200}
                      onMomentumScrollEnd={() => {
                        if (this.state.scrollOffSet > 40) {
                          this.replyMessage(this.state.messages[index]);
                        }
                        this.scrollRefs[index].scrollTo({
                          x: 0,
                          y: 0,
                        });
                      }}
                      onScroll={e => {
                        this.setState({
                          showReplyIcons: e.nativeEvent.contentOffset.x > 40,
                        });
                        this.setState({
                          scrollOffSet: e.nativeEvent.contentOffset.x,
                        });
                      }}
                      showsHorizontalScrollIndicator={false}>
                      <S.MessageRow
                        self={item.u.username === this.props.me.username}>
                        {item.u.username === this.props.me.username ? (
                          <SelfMessage
                            splitRepliedMessage={splitRepliedMessage(
                              item.msg,
                              this.state.messages,
                            )}
                            isRepliedMessage={isRepliedMessage(item.msg)}
                            options={this.state.optionVisible}
                            item={item}
                            msg={item.msg}
                            time={item.ts}
                            replyMessage={this.replyMessage}
                            setOptions={this.setOptions}
                            editMessage={this.editMessage}
                            deleteMessage={this.deleteMessage}
                          />
                        ) : (
                          <OpponentMessage
                            splitRepliedMessage={splitRepliedMessage(
                              item.msg,
                              this.state.messages,
                            )}
                            isRepliedMessage={isRepliedMessage(item.msg)}
                            item={item}
                            options={this.state.optionVisible}
                            setOptions={this.setOptions}
                            replyMessage={this.replyMessage}
                            msg={item.msg}
                            time={item.ts}
                          />
                        )}
                      </S.MessageRow>
                      <S.FakeBlock>
                        {this.state.showReplyIcons ? (
                          <Reply fill="#c9ccd5" width={30} height={30} />
                        ) : null}
                      </S.FakeBlock>
                    </S.Wrapper>
                    {item.separator ? (
                      <S.SeparatorContainer>
                        <S.Separator>
                          <S.SeparatorText>{item.date}</S.SeparatorText>
                        </S.Separator>
                      </S.SeparatorContainer>
                    ) : null}
                  </>
                );
              }}
            />
          </S.OptionsContainer>
        </S.Container>
        {this.state.replying ? (
          <S.ReplyContainer>
            <S.TextContainer>
              <S.ReplySeparator />
              <S.ReplyMessage numberOfLines={1}>
                {this.state.replyMessage.msg}
              </S.ReplyMessage>
            </S.TextContainer>
            <S.ResetReplyButton onPress={this.resetReply}>
              <S.ResetReplyButtonText>X</S.ResetReplyButtonText>
            </S.ResetReplyButton>
          </S.ReplyContainer>
        ) : null}
        <S.InputContainer>
          <AutoGrowingTextInput
            ref={ref => {
              this.inputRef = ref;
            }}
            style={{
              width: '80%',
              paddingLeft: 13,
            }}
            minHeight={60}
            maxHeight={120}
            value={this.state.message}
            onChangeText={text => this.setState({message: text})}
            placeholder="Напишите сообщение"
          />
          {this.state.message && !this.state.editing ? (
            <S.Button onPress={this.sendMessage}>
              <Image source={sendIcon} style={{width: 20, height: 22}} />
            </S.Button>
          ) : null}
          {this.state.message && this.state.editing ? (
            <>
              <S.SaveButton onPress={this.saveMessage}>
                <S.SaveButtonText>Сохранить</S.SaveButtonText>
              </S.SaveButton>
            </>
          ) : null}
        </S.InputContainer>
      </>
    );
  }
}

const mapDispatchToProps = {
  requestGetMyself,
};

const mapStateToProps = ({chat}) => ({
  me: chat.me,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(DirectChat);
