import React from 'react';
import check from '../../../../assets/images/check.png';
import * as S from './styled';
import {Clipboard} from 'react-native';
import ModalWindow from '../EditingModal';

const SelfMessage = ({
  msg,
  time,
  item,
  setOptions,
  options,
  editMessage,
  deleteMessage,
  splitRepliedMessage,
  isRepliedMessage,
  replyMessage,
}) => {
  const [isModalOpen, setModalOpen] = React.useState(false);

  const onMessagePress = ms => {
    if (options === ms._id) {
      setOptions('');
    } else {
      setOptions(ms._id);
    }
    setModalOpen(true);
  };

  const copyMessage = message => {
    setOptions('');
    Clipboard.setString(message);
  };
  return (
    <>
      <S.SelfMessageContainer onPress={() => onMessagePress(item)}>
        <>
          <S.SelfMessage>
            {isRepliedMessage ? (
              <S.RepliedMessageContainer>
                <S.RepliedSeparator />
                <S.RepliedMessage numberOfLines={1}>
                  {splitRepliedMessage.replied}
                </S.RepliedMessage>
              </S.RepliedMessageContainer>
            ) : null}
            <S.SelfMessageText>
              {isRepliedMessage ? splitRepliedMessage.pure : msg}
            </S.SelfMessageText>
          </S.SelfMessage>
          <S.SelfMessageInfo>
            <S.MessageTime>
              {item.editedAt ? 'edited ' : null}
              {new Date(time.$date).getHours() < 10
                ? `0${new Date(time.$date).getHours()}`
                : new Date(time.$date).getHours()}
              :
              {new Date(time.$date).getMinutes() < 10
                ? `0${new Date(time.$date).getMinutes()}`
                : new Date(time.$date).getMinutes()}
            </S.MessageTime>
            {/* <S.CheckIcon source={check} /> */}
          </S.SelfMessageInfo>
        </>
      </S.SelfMessageContainer>
      <ModalWindow
        onEdit={() => editMessage(item)}
        onCopy={() => copyMessage(item.msg)}
        onDelete={() => deleteMessage(item)}
        onClose={() => setModalOpen(false)}
        onReply={() => replyMessage(item)}
        isVisible={isModalOpen}
      />
    </>
  );
};

export default SelfMessage;
