import styled from 'styled-components';

export const SelfMessageContainer = styled.TouchableOpacity`
  max-width: 320px;
  justify-content: flex-end;
`;

export const SelfMessage = styled.View`
  color: white;
  font-size: 14px;
  font-weight: 400;
  padding: 10px 15px;
  border-radius: 15px;
  border-bottom-left-radius: 15px;
  border-bottom-right-radius: 0;
  background-color: #5954f9;
`;

export const RepliedSeparator = styled.View`
  width: 3px;
  height: 20px;
  background-color: white;
  margin-right: 5px;
`;

export const RepliedMessageContainer = styled.View`
  flex-direction: row;
  align-items: center;
`;

export const RepliedMessage = styled.Text`
  color: white;
  font-family: Roboto;
  font-size: 14px;
  font-weight: 700;
`;

export const SelfMessageText = styled.Text`
  color: white;
  font-size: 14px;
  font-weight: 400;
`;

export const SelfMessageInfo = styled.View`
  flex-direction: row;
  margin-right: 10px;
  align-items: center;
  justify-content: flex-end;
`;

export const MessageTime = styled.Text`
  color: #8b8d94;
  font-size: 12px;
  font-weight: 400;
  align-self: flex-end;
`;

export const CheckIcon = styled.Image`
  width: 16px;
  height: 9px;
  margin-left: 6px;
`;

export const OptionContainer = styled.View`
  width: 150px;
  background-color: red;
  position: absolute;
  right: 5px;
  top: 20px;
`;

export const Option = styled.TouchableOpacity`
  width: 100%;
  height: 17px;
  justify-content: center;
  align-items: center;
`;

export const OptionText = styled.Text``;
