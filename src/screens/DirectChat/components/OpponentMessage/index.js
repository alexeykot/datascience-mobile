import React from 'react';

import * as S from './styled';
import ModalWindow from '../EditingModal';

const OpponentMessage = ({
  msg,
  time,
  replyMessage,
  options,
  setOptions,
  item,
  splitRepliedMessage,
  isRepliedMessage,
}) => {
  const [isModalOpen, setModalOpen] = React.useState(false);
  const onMessagePress = ms => {
    if (options === ms._id) {
      setOptions('');
    } else {
      setOptions(ms._id);
    }
    setModalOpen(true);
  };

  const splitMessage = text => {
    const endIndex = text.indexOf(')');
    return text.substring(endIndex + 2);
  };
  return (
    <>
      <S.MessageContainer onPress={() => onMessagePress(item)}>
        {isRepliedMessage ? (
          <S.RepliedMessageContainer>
            <S.RepliedSeparator />
            <S.RepliedMessage>
              {splitMessage(splitRepliedMessage.replied)}
            </S.RepliedMessage>
          </S.RepliedMessageContainer>
        ) : null}
        <S.SelfMessageText>
          {isRepliedMessage ? splitRepliedMessage.pure : msg}
        </S.SelfMessageText>
        <S.MessageTime>
          {item.editedAt ? 'edited ' : null}
          {new Date(time.$date).getHours() < 10
            ? `0${new Date(time.$date).getHours()}`
            : new Date(time.$date).getHours()}
          :
          {new Date(time.$date).getMinutes() < 10
            ? `0${new Date(time.$date).getMinutes()}`
            : new Date(time.$date).getMinutes()}
        </S.MessageTime>
      </S.MessageContainer>
      <ModalWindow
        onReply={() => replyMessage(item)}
        onClose={() => setModalOpen(false)}
        isVisible={isModalOpen}
      />
    </>
  );
};

export default OpponentMessage;
