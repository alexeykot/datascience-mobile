import styled from 'styled-components';

export const MessageContainer = styled.TouchableOpacity`
  border-radius: 15px;
  border-bottom-left-radius: ${({self}) => (!self ? '0px' : '15px')};
  border-bottom-right-radius: ${({self}) => (self ? '0px' : '15px')};
  background-color: ${({self}) => (!self ? '#f7f5fa' : '#5954f9')};
  padding: 10px 15px;
  min-width: 100px;
  max-width: 320px;
`;

export const RepliedSeparator = styled.View`
  width: 3px;
  height: 20px;
  background-color: #5954f9;
  margin-right: 5px;
`;

export const RepliedMessageContainer = styled.View`
  flex-direction: row;
  align-items: center;
`;

export const SelfMessage = styled.View`
  font-size: 14px;
  font-weight: 400;
  padding: 10px 15px;
  border-radius: 15px;
  border-bottom-left-radius: 0;
  border-bottom-right-radius: 15px;
  background-color: #f7f5fa;
`;

export const SelfMessageText = styled.Text`
  color: #1b041e;
  font-family: Roboto;
  font-size: 14px;
  font-weight: 400;
`;

export const RepliedMessage = styled.Text`
  color: #1b041e;
  font-family: Roboto;
  font-size: 14px;
  font-weight: 700;
`;

export const MessageTime = styled.Text`
  color: #8b8d94;
  font-size: 12px;
  font-weight: 400;
  align-self: flex-end;
`;
