import styled from 'styled-components';

export const Container = styled.View`
  height: 60px;
  width: 100%;
  background-color: ${({color}) => (color ? color : 'transparent')};
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  padding: 0 15px;
`;

export const Title = styled.Text`
  color: #ffffff;
  font-family: Roboto;
  font-size: 16px;
  font-weight: 500;
  line-height: 16px;
`;

export const PresenceStatus = styled.Text`
  color: #ffffff;
  font-family: Roboto;
  font-size: 12px;
  font-weight: 400;
`;

export const PresenceContainer = styled.View`
  flex-direction: row;
  justify-content: center;
  align-items: center;
  margin-top: 7px;
`;

export const Presence = styled.View`
  width: 5px;
  height: 5px;
  border-radius: 5px;
  background-color: green;
  margin-left: 5px;
`;

export const TitleContainer = styled.View`
  align-items: center;
  justify-content: center;
`;

export const LeftButton = styled.TouchableOpacity`
  padding: 10px 10px 10px 0;
`;

export const RightButton = styled.TouchableOpacity`
  padding: 10px 0 10px 10px;
`;

export const FakeView = styled.View`
  padding: 10px 0 10px 10px;
`;

export const Icon = styled.Image``;
