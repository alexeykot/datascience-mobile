import React from 'react';
import arrow from '../../../../assets/images/arrow.png';
import moment from 'moment';

import * as S from './styled';
import { getLastSeen } from '../../helpers';

const Header = ({props, title, color, presence, lastSeen}) => {
  return (
    <S.Container color={color}>
      <S.LeftButton onPress={() => props.navigation.goBack()}>
        <S.Icon source={arrow} />
      </S.LeftButton>
      <S.TitleContainer>
        <S.Title>{title}</S.Title>
        {presence ? (
          <S.PresenceContainer>
            <S.PresenceStatus>
              {presence.presence === 'online' ? 'Онлайн' : getLastSeen(lastSeen)}
            </S.PresenceStatus>
            {presence.presence === 'online' ? <S.Presence /> : null}
          </S.PresenceContainer>
        ) : null}
      </S.TitleContainer>
      <S.FakeView />
    </S.Container>
  );
};

export default Header;
