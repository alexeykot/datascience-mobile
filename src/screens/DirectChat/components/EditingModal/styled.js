import styled from 'styled-components';

export const Container = styled.View`
  width: 100%;
  background-color: white;
  border-top-left-radius: 30px;
  border-top-right-radius: 30px;
  padding: 19px 30px 10px 30px;
  elevation: 20;
  align-items: center;
`;

export const Icon = styled.Image`
  margin-left: 15px;
`;

export const Row = styled.View`
  flex-direction: row;
  align-items: center;
  margin-bottom: 5px;
`;

export const Title = styled.Text`
  color: #8b8d94;
  font-family: Roboto;
  font-size: 12px;
  font-weight: 400;
  margin-bottom: 19px;
`;

export const Separator = styled.View`
  width: 100%;
  height: 1px;
  background-color: #c9ccd5;
  opacity: 0.5;
`;

export const Option = styled.TouchableOpacity`
  width: 100%;
  height: 36px;
  justify-content: center;
  margin-bottom: ${({isLast}) => (isLast ? 15 : 0)}px;
`;

export const OptionText = styled.Text`
  margin-left: 20px;
  color: #1b041e;
  font-family: Roboto;
  font-size: 14px;
  font-weight: 400;
`;
