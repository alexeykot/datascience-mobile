import React from 'react';
import {StyleSheet} from 'react-native';

import trash from '../../../../assets/images/trash-2.png';
import edit from '../../../../assets/images/edit-2.png';

import Modal from 'react-native-modal';

import * as S from './styled';

const ModalWindow = ({
  isVisible,
  onClose,
  title,
  options,
  onEdit,
  onDelete,
  onCopy,
  onReply,
}) => {
  const close = async () => {
    onClose();
  };
  const onCopyPress = () => {
    onCopy();
    onClose();
  };
  const onEditPress = () => {
    onEdit();
    onClose();
  };
  const onDeletePress = () => {
    onDelete();
    onClose();
  };
  const onReplyPress = () => {
    onReply();
    onClose();
  };
  return (
    <Modal
      onSwipeComplete={close}
      swipeDirection={['down']}
      onBackdropPress={close}
      style={styles.view}
      // isVisible={isVisible}>
      isVisible={false}>
      <S.Container isPadding={!!options}>
        {onCopy ? (
          <>
            <S.Row>
              <S.Icon source={edit} />
              <S.Option onPress={onCopyPress}>
                <S.OptionText>Копировать</S.OptionText>
              </S.Option>
            </S.Row>
            <S.Separator />
          </>
        ) : null}
        {false ? (
          <>
            <S.Row>
              <S.Icon source={edit} />
              <S.Option onPress={onEditPress}>
                <S.OptionText>Редактировать</S.OptionText>
              </S.Option>
            </S.Row>
            <S.Separator />
          </>
        ) : null}
        {false ? (
          <>
            <S.Row>
              <S.Icon source={edit} />
              <S.Option onPress={onReplyPress}>
                <S.OptionText>Ответить</S.OptionText>
              </S.Option>
            </S.Row>
          </>
        ) : null}
        {onDelete ? (
          <>
            <S.Row>
              <S.Icon source={trash} />
              <S.Option onPress={onDeletePress}>
                <S.OptionText>Удалить</S.OptionText>
              </S.Option>
            </S.Row>
          </>
        ) : null}
      </S.Container>
    </Modal>
  );
};

const styles = StyleSheet.create({
  view: {
    justifyContent: 'flex-end',
    margin: 0,
  },
  wrapper: {
    flex: 1,
  },
});

export default ModalWindow;
