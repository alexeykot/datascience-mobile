import moment from 'moment';
import _ from 'lodash';

export const addDateSeparators = messages => {
  // const messagesReverse = messages.reverse();
  const arrWithDate = messages.map(msg => {
    msg.date = moment(msg.ts.$date).format('D MMMM YYYY');
    if (
      msg.date ===
      moment()
        .subtract(1, 'days')
        .format('D MMMM YYYY')
    ) {
      msg.date = 'Вчера';
    }
    if (msg.date === moment().format('D MMMM YYYY')) {
      msg.date = 'Сегодня';
    }
    return msg;
  });
  const grouped = _.groupBy(arrWithDate, item => item.date);
  Object.keys(grouped).map(key => {
    grouped[key][grouped[key].length - 1].separator = true;
  });
  let arr = [];
  Object.keys(grouped).forEach(key => arr.push(...grouped[key]));
  return arr;
};

export const isRepliedMessage = msg => msg.startsWith('[ ]');

export const splitRepliedMessage = (msg, chat = []) => {
  if (!isRepliedMessage(msg)) {
    return msg;
  }

  const startIndex = msg.indexOf('=');
  const endIndex = msg.indexOf(')');
  const id = msg.substring(startIndex + 1, endIndex);
  // const {data: message} = await httpChat.get(
  //   `/api/v1/chat.getMessage?msgId=${id}`,
  // );
  let message = '';
  if (chat.length) {
    const findMessage = chat.find(m => m._id === id);
    message = findMessage.msg;
  }
  const endIndex1 = msg.indexOf(')');
  const pureMessage = msg.substring(endIndex1 + 2);
  return {
    replied: message,
    pure: pureMessage,
  };
};

export const getLastSeen = date => {
  const formatted = moment(date);
  const currentDate = moment(new Date());
  const isSameDate = moment(currentDate).isSame(formatted, 'date');
  if (isSameDate) {
    return `Был онлайн ${moment(date).format('DD-MM-YYYY HH:mm')}`;
    // return 'Был онлайн сегодня';
  }
  return `Был онлайн ${moment(date).format('DD-MM-YYYY HH:mm')}`;
};
