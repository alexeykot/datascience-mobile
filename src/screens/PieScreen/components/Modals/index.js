import PurchaseModal from './PurchaseModal';
import SuccessModal from './SuccessModal';
import HowModal from './HowModal';

export default {
  PurchaseModal,
  SuccessModal,
  HowModal,
};
