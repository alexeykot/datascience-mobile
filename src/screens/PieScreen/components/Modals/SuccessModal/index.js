import React from 'react';
import _ from 'lodash';
import {StyleSheet} from 'react-native';
import Modal from 'react-native-modal';

import markLogo from '../../../../../assets/images/markLogo.png';
import closeIcon from '../../../../../assets/images/close-blue.png';

import * as S from './styled';
import http from '../../../../../services/http';

const SuccessModal = ({isVisible, onClose, pie, navigation}) => {
  const close = () => {
    onClose();
  };
  const openPies = async () => {
    close();
    const {data} = await http.get('/pie/default/index');
    const categories = data.data.entities
      .map(p => {
        if (p.categories.length) {
          return p.categories;
        }
      })
      .filter(c => c !== undefined)
      .flat();
    const uniqCategories = _.uniqBy(categories, c => c.id);
    navigation.navigate('PiesScreen', {
      items: data.data.entities,
      categories: uniqCategories,
    });
  };
  return (
    <Modal
      onSwipeComplete={close}
      swipeDirection={['left', 'right']}
      onBackdropPress={close}
      style={styles.view}
      isVisible={isVisible}>
      <S.Container>
        <S.CloseIconButton onPress={close}>
          <S.Icon source={closeIcon} />
        </S.CloseIconButton>
        <S.ImageContainer>
          <S.PieImage source={{uri: pie.src.url}} />
          <S.MarkContainer>
            <S.Icon source={markLogo} />
          </S.MarkContainer>
        </S.ImageContainer>
        <S.Title>Ваш заказ успешно оформлен!</S.Title>
        <S.SubTitle>
          Поздравляем вас с покупкой, подробную информацию о том как вы можете
          получить свой заказ мы выслали вам на вашу электронную почту
        </S.SubTitle>
        <S.DefaultButton onPress={openPies}>
          <S.ButtonText>Хочу еще плюшки!</S.ButtonText>
        </S.DefaultButton>
        <S.CloseButton onPress={close}>
          <S.CloseButtonText>Закрыть</S.CloseButtonText>
        </S.CloseButton>
      </S.Container>
    </Modal>
  );
};

const styles = StyleSheet.create({
  view: {
    justifyContent: 'center',
    margin: 0,
  },
  wrapper: {
    flex: 1,
  },
});

export default SuccessModal;
