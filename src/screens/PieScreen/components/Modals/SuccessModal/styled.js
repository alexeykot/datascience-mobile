import styled from 'styled-components';

export const Container = styled.View`
  width: 90%;
  align-self: center;
  background-color: white;
  border-radius: 30px;
  padding: 66px 30px 29px 30px;
  /* padding-bottom: 30px; */
  align-items: center;
`;

export const MarkContainer = styled.View`
  width: 60px;
  height: 60px;
  border-radius: 30px;
  background-color: #5954f9;
  position: absolute;
  align-self: center;
  bottom: -30px;
  justify-content: center;
  align-items: center;
`;

export const FunnyIcon = styled.Image`
  position: absolute;
  align-self: center;
  top: -20px;
  height: 290px;
`;

export const Title = styled.Text`
  color: #140c09;
  font-family: Roboto;
  font-size: 24px;
  font-weight: 700;
  text-align: center;
`;

export const SubTitle = styled.Text`
  color: #140c09;
  font-family: Roboto;
  font-size: 14px;
  font-weight: 400;
  margin-top: 24px;
  text-align: center;
`;

export const ImageContainer = styled.View`
  width: 204px;
  height: 122px;
  elevation: 10;
  border-radius: 20px;
  background-color: #ffffff;
  margin-bottom: 46px;
`;

export const PieImage = styled.Image`
  flex: 1;
  border-radius: 20px;
`;

export const Icon = styled.Image``;

export const CloseIconButton = styled.TouchableOpacity`
  position: absolute;
  right: 20px;
  top: 20px;
  width: 30px;
  height: 30px;
  justify-content: flex-start;
  align-items: flex-end;
`;

export const CloseIcon = styled.Image``;

export const ButtonContainer = styled.View`
  padding: 0 23px;
  width: 100%;
`;

export const DefaultButton = styled.TouchableOpacity`
  height: 48px;
  border-radius: 24px;
  background-color: #5954f9;
  width: 100%;
  justify-content: center;
  align-items: center;
  margin-top: 64px;
`;

export const SecondaryButton = styled.TouchableOpacity`
  height: 48px;
  border-radius: 24px;
  background-color: white;
  border: 1px solid #5954f9;
  width: 100%;
  justify-content: center;
  align-items: center;
  margin-top: 10px;
`;

export const SecondaryButtonText = styled.Text`
  color: #5954f9;
  font-family: Roboto;
  font-size: 14px;
  font-weight: 700;
`;

export const ButtonText = styled.Text`
  color: #ffffff;
  font-family: Roboto;
  font-size: 14px;
  font-weight: 700;
`;

export const ConfirmText = styled.Text`
  color: #8b8d94;
  font-family: Roboto;
  font-size: 14px;
  font-weight: 400;
  margin-top: 32px;
`;

export const CloseButton = styled.TouchableOpacity``;

export const CloseButtonText = styled.Text`
  color: #5954f9;
  font-family: Roboto;
  font-size: 14px;
  font-weight: 700;
  margin-top: 30px;
`;
