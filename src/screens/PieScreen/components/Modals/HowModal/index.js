/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {StyleSheet} from 'react-native';
import Modal from 'react-native-modal';

import modalBg from '../../../../../assets/images/modalBg.png';
import funny from '../../../../../assets/images/funny.png';

import * as S from './styled';

const HowModal = ({isVisible, onClose, navigation}) => {
  const close = async () => {
    onClose();
  };
  return (
    <Modal
      onSwipeComplete={close}
      swipeDirection={['left', 'right']}
      onBackdropPress={close}
      style={styles.view}
      isVisible={isVisible}>
      <S.Container>
        <S.Icon style={{width: '100%', height: 250}} source={modalBg} />
        <S.Title style={{paddingHorizontal: 23}}>
          Как накопить на плюшки?
        </S.Title>
        <S.SubTitle style={{paddingHorizontal: 23}}>
          Копите бонусы DS ID быстрее, заполнив полную информацию в профиле,
          посещая наши мероприятия, а так же узнавайте больше как накопить из
          наших новостей
        </S.SubTitle>
        <S.ButtonContainer>
          <S.DefaultButton
            onPress={() => {
              onClose();
              navigation.navigate('AllEventsScreen');
            }}>
            <S.ButtonText>Выбрать мероприятие</S.ButtonText>
          </S.DefaultButton>
          {/* <S.SecondaryButton
            onPress={() => {
              navigation.navigate('UserInfoScreen');
              onClose();
            }}>
            <S.SecondaryButtonText>Заполнить профиль</S.SecondaryButtonText>
          </S.SecondaryButton> */}
        </S.ButtonContainer>
        <S.FunnyIcon source={funny} />
      </S.Container>
    </Modal>
  );
};

const styles = StyleSheet.create({
  view: {
    justifyContent: 'center',
    margin: 0,
  },
  wrapper: {
    flex: 1,
  },
});

export default HowModal;
