/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {StyleSheet, Image, TouchableOpacity, TextInput} from 'react-native';
import http from '../../../../../services/http';
import mark from '../../../../../assets/images/mark.png';

import Modal from 'react-native-modal';

import * as S from './styled';

const PurchaseModal = ({
  isVisible,
  onClose,
  pie,
  showSuccessModal,
  user,
  checkBonus,
  bonus,
}) => {
  const [count, setCount] = React.useState(1);
  const [checked, setChecked] = React.useState(false);
  const [step, setStep] = React.useState(0);
  const [email, setEmail] = React.useState(user.email);
  // const [editable, setEditable] = React.useState(false);
  let inputRef;
  const close = async () => {
    onClose();
  };
  const onButtonPress = async () => {
    if (step === 0) {
      setStep(step + 1);
    }
    if (step === 1) {
      const {data} = await http.post('/pie/order/create', {
        pieId: pie.id,
        quantity: count,
        email: email,
      });
      checkBonus();
      onClose();
      showSuccessModal();
    }
  };

  const onEditPress = () => {
    // setEditable(true);
    inputRef.focus();
    setEmail('');
  };

  return (
    <Modal
      onSwipeComplete={close}
      swipeDirection={['down']}
      onBackdropPress={close}
      style={styles.view}
      isVisible={isVisible}>
      <S.Container>
        <S.Title>Оформление заказа</S.Title>
        <S.Hint>
          Обменять {pie.rank * count} баллов на {pie.title}
        </S.Hint>
        {step === 0 ? (
          <>
            <S.SubTitle>Сколько нужно ?</S.SubTitle>
            <S.Row>
              <S.CountButton
                onPress={() => {
                  if (count > 1) {
                    setCount(count - 1);
                  }
                }}>
                <S.CountButtonText>-</S.CountButtonText>
              </S.CountButton>
              <S.CountText>{count}</S.CountText>
              <S.CountButton
                disabled={!(pie.rank * (count + 1) <= bonus)}
                onPress={() => {
                  if (pie.rank * (count + 1) <= bonus) {
                    setCount(count + 1);
                  }
                }}>
                <S.CountButtonText>+</S.CountButtonText>
              </S.CountButton>
            </S.Row>
          </>
        ) : null}
        {step === 1 ? (
          <>
            <S.ConfirmText>
              Пожалуйста, проверьте вашу электронную почту. После оформления
              заказа мы отправим на нее подробную информацию о том, как вы
              сможете получить свой заказ.
            </S.ConfirmText>
            <S.InputContainer>
              <S.InputHint>E-mail</S.InputHint>
              <S.InputTextContainer>
                <TextInput
                  ref={r => {
                    inputRef = r;
                  }}
                  onChangeText={t => setEmail(t)}
                  value={email}
                />
                <TouchableOpacity onPress={onEditPress}>
                  <S.EditText>Изменить</S.EditText>
                </TouchableOpacity>
              </S.InputTextContainer>
              <S.Separator />
            </S.InputContainer>
            <S.RuleContainer>
              <S.CheckBox
                onPress={() => setChecked(!checked)}
                checked={checked}>
                <Image source={mark} />
              </S.CheckBox>
              <S.Rules>
                Я выражаю согласие с Правилами Акции и ознакомлен с Условиями
                использования предоставленных мной сведений и выражаю свое
                согласие на ииспольхование
              </S.Rules>
            </S.RuleContainer>
          </>
        ) : null}
        <S.OptionsContainer />
        <S.DefaultButton
          disabled={step !== 0 && !checked}
          onPress={onButtonPress}>
          <S.ButtonText>
            {!step ? 'Продолжить' : `Оплатить ${pie.rank * count}`}
          </S.ButtonText>
        </S.DefaultButton>
      </S.Container>
    </Modal>
  );
};

const styles = StyleSheet.create({
  view: {
    justifyContent: 'flex-end',
    margin: 0,
  },
  wrapper: {
    flex: 1,
  },
});

export default PurchaseModal;
