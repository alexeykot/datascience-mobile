import styled from 'styled-components';

export const Container = styled.View`
  /* height: 400px; */
  width: 100%;
  background-color: white;
  border-top-left-radius: 30px;
  border-top-right-radius: 30px;
  padding: 42px 30px 10px 30px;
`;

export const Row = styled.View`
  flex-direction: row;
  align-self: center;
  align-items: center;
`;

export const CountButton = styled.TouchableOpacity`
  width: 48px;
  height: 48px;
  background-color: #f7f5fa;
  border-radius: 24px;
  justify-content: center;
  align-items: center;
  opacity: ${({disabled}) => (disabled ? 0.3 : 1)};
`;

export const CountButtonText = styled.Text`
  color: #5954f9;
  font-size: 36px;
`;

export const Title = styled.Text`
  color: #1b041e;
  font-family: Roboto;
  font-size: 18px;
  font-weight: 700;
`;

export const Hint = styled.Text`
  color: #1b041e;
  font-family: Roboto;
  font-size: 14px;
  font-weight: 400;
  margin-top: 13px;
`;

export const SubTitle = styled.Text`
  color: #1b041e;
  font-family: Roboto;
  font-size: 14px;
  font-weight: 700;
  align-self: center;
  margin-top: 24px;
`;
export const OptionsContainer = styled.View`
  margin-top: 32px;
`;

export const Option = styled.View`
  flex-direction: row;
  justify-content: space-between;
`;

export const OptionText = styled.Text`
  color: #1b041e;
  font-family: Roboto;
  font-size: 16px;
  font-weight: 700;
`;

export const CountText = styled.Text`
  color: #1b041e;
  font-family: Roboto;
  font-size: 24px;
  font-weight: 700;
  margin: 0 30px;
`;

export const DefaultButton = styled.TouchableOpacity`
  height: 48px;
  border-radius: 24px;
  background-color: #5954f9;
  width: 100%;
  opacity: ${({disabled}) => (disabled ? 0.5 : 1)};
  justify-content: center;
  align-items: center;
`;

export const ButtonText = styled.Text`
  color: #ffffff;
  font-family: Roboto;
  font-size: 14px;
  font-weight: 700;
`;

export const ConfirmText = styled.Text`
  color: #8b8d94;
  font-family: Roboto;
  font-size: 14px;
  font-weight: 400;
  margin-top: 32px;
`;

export const InputContainer = styled.View`
  margin-top: 30px;
  width: 100%;
`;

export const InputTextContainer = styled.View`
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  margin: 11px 0 0 0;
`;

export const Email = styled.Text`
  color: #130b04;
  font-family: Roboto;
  font-size: 16px;
  font-weight: 400;
  display: ${({editable}) => (editable ? 'none' : 'flex')};
`;

export const EditText = styled.Text`
  color: #5954f9;
  font-family: Roboto;
  font-size: 12px;
  font-weight: 400;
`;

export const InputHint = styled.Text`
  color: #8b8d94;
  font-family: Roboto;
  font-size: 12px;
  font-weight: 400;
`;

export const Separator = styled.View`
  width: 100%;
  height: 1px;
  background-color: #c9ccd5;
  opacity: 0.5;
`;

export const RuleContainer = styled.View`
  flex-direction: row;
  margin-top: 21px;
`;

export const Rules = styled.Text`
  color: #8b8d94;
  font-family: Roboto;
  font-size: 12px;
  font-weight: 400;
  margin-left: 12px;
`;

export const CheckBox = styled.TouchableOpacity`
  width: 28px;
  height: 28px;
  border-radius: 10px;
  border-width: 1px;
  border-color: ${({checked}) => (!checked ? '#5954f9' : 'white')};
  background-color: ${({checked}) => (checked ? '#5954f9' : 'white')};
  justify-content: center;
  align-items: center;
`;
