import styled from 'styled-components';
import {Dimensions} from 'react-native';

export const Container = styled.ScrollView`
  background-color: white;
`;

export const ImageBackground = styled.Image`
  width: 100%;
  height: ${Dimensions.get('window').height / 3}px;
  position: absolute;
  top: 0px;
  right: 0px;
  left: 0px;
`;

export const OptionsContainer = styled.View`
  width: 100%;
  border-top-left-radius: 30px;
  border-top-right-radius: 30px;
  background-color: white;
  margin-top: ${Dimensions.get('window').height / 3 - 85}px;
  /* padding: 0 15px; */
`;

export const EventTime = styled.Text`
  color: #5754f4;
  font-family: Roboto;
  font-size: 14px;
  font-weight: 700;
  line-height: 14px;
  text-transform: uppercase;
`;

export const InfoView = styled.View`
  margin-top: 100px;
  padding: 0 15px;
  margin-bottom: 10px;
`;

export const EventTitle = styled.Text`
  color: #140c09;
  font-family: Roboto;
  font-size: 24px;
  font-weight: 700;
  line-height: 30px;
  padding-right: 135px;
`;

export const EventDescription = styled.Text`
  color: #1b041e;
  font-family: Roboto;
  font-size: 14px;
  font-weight: 400;
  line-height: 18px;
  margin-top: 14px;
`;

export const PartnerContainer = styled.View`
  flex-direction: row;
  margin-top: 17px;
`;

export const PartnerText = styled.Text`
  color: #1b041e;
  font-family: Roboto;
  font-size: 14px;
  font-weight: 400;
  line-height: 18px;
`;

export const Partner = styled.Text`
  color: #5954f9;
  font-family: Roboto;
  font-size: 14px;
  font-weight: 400;
  line-height: 18px;
`;

export const Separator = styled.View`
  height: 1px;
  background-color: #c9ccd5;
  background-color: #f7f5fa;
  /* opacity: 0.5; */
  margin-top: ${({marginTop}) => (marginTop ? marginTop : 0)}px;
  width: 100%;
`;

// export const MoreButton = styled.TouchableOpacity`
//   width: 100%;
//   justify-content: space-between;
//   flex-direction: row;
//   margin-top: 22px;
//   align-items: center;
// `;

// export const MoreText = styled.Text`
//   color: #1b041e;
//   font-family: Roboto;
//   font-size: 14px;
// `;

export const MoreIcon = styled.Image``;

export const GetButton = styled.TouchableOpacity`
  width: 300px;
  height: 48px;
  border-radius: 24px;
  elevation: 10;
  background-color: ${({disabled}) => (disabled ? '#c9ccd5' : '#5754f4')};
  align-self: center;
  justify-content: center;
  align-items: center;
  flex-direction: row;
  margin-top: 24px;
`;

export const SubTitle = styled.Text`
  color: #1b041e;
  font-family: Roboto;
  font-size: 18px;
  font-weight: 700;
  margin-top: 32px;
`;

export const GetButtonText = styled.Text`
  color: #ffffff;
  font-family: Roboto;
  font-size: 14px;
  font-weight: 500;
  margin-right: 8px;
`;

export const ScoreText = styled.Text`
  margin-top: 13px;
  color: #8b8d94;
  font-family: Roboto;
  font-size: 14px;
  font-weight: 500;
  align-self: center;
`;
export const StatusContainer = styled.View`
  padding: 8px 9px;
  background-color: ${({color}) => (color ? color : 'red')};
  position: absolute;
  top: -20;
  left: 30;
  border-radius: 12px;
  width: 134px;
  height: 24px;
  justify-content: center;
  align-items: center;
`;

export const StatusText = styled.Text`
  color: #ffffff;
  font-family: Roboto;
  font-size: 10px;
  font-weight: 700;
  text-transform: uppercase;
`;

export const Icon = styled.Image`
  margin-right: 16px;
`;

export const MoreButton = styled.TouchableOpacity``;

export const MoreText = styled.Text`
  font-family: Roboto;
  font-size: 14px;
  font-weight: 400;
  color: #5954f9;
`;
