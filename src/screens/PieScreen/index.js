/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {Animated, View, Dimensions} from 'react-native';
import {connect} from 'react-redux';

// import Header from '../Event/components/Header';
import Header from '../../components/Header';

import Bonus from '../../assets/icons/bonus.svg';
import * as S from './styled';
import FavoritesStorage from '../../services/storage/favorites';
import http from '../../services/http';
import Modals from './components/Modals';
import {formatDescription} from '../Event/DescriptionScreen/helpers';
// import {formatEventTime, formatEvent} from './helpers';

class PieScreen extends React.Component {
  state = {
    event: null,
    enableScrollViewScroll: true,
    showFullText: false,
    isFavorite: false,
    bonus: 0,
    showMoreButton: true,
    showPurchaseModal: false,
    showSuccessModal: false,
    showHowModal: false,
    showHeaderColor: false,
    loading: false,
    pieInfo: {
      condition: '',
    },
  };

  async componentDidMount() {
    this.props.navigation.addListener('focus', async () => {
      await this.checkBonus();
      const isFavorite = await this.isAddedToFavorite();
      this.setState({isFavorite});
    });
  }

  checkBonus = async () => {
    const bonus = await http.get('/score/balance/index');
    this.setState({bonus: bonus.data.data.entity.value});
  };

  componentWillMount() {
    this._animatedValue = new Animated.Value(0);
    this.props.navigation.addListener('focus', async () => {
      await this.checkBonus();
      await this.getPieInfo();
    });
  }

  getPieInfo = async () => {
    this.setState({loading: true});
    const {event: pie} = this.props.route.params;
    const {data: pieInfo} = await http.get(`/pie/default/view?id=${pie.id}`);
    this.setState({loading: false, pieInfo: pieInfo.data.entity});
  };

  addToFavorite = async () => {
    const eventWithType = {
      ...this.props.route.params.event,
      type: 'Плюшки',
    };
    this.setState({isFavorite: true});
    const favorites = await FavoritesStorage.get();
    favorites.push(eventWithType);
    await FavoritesStorage.save([...favorites]);
  };

  deleteFromFavorite = async () => {
    const favorites = await FavoritesStorage.get();
    const filteredFavorites = favorites.filter(
      f => f.id !== this.props.route.params.event.id,
    );
    this.setState({isFavorite: false});
    await FavoritesStorage.save(filteredFavorites);
  };

  isAddedToFavorite = async () => {
    const favorites = await FavoritesStorage.get();
    if (!favorites) {
      return false;
    }
    const favoriteEvents = favorites.filter(f => f.type === 'Плюшки');
    if (!favoriteEvents.length) {
      return false;
    }
    const favEvent = favoriteEvents.find(
      f => f.id === this.props.route.params.event.id,
    );
    return !!favEvent;
  };

  onLikePress = async () => {
    const isFavorite = await this.isAddedToFavorite();
    if (isFavorite) {
      this.deleteFromFavorite();
    } else {
      this.addToFavorite();
    }
  };

  onTextLayout = e => {
    if (this.state.showMoreButton) {
      this.setState({
        showFullText: e.nativeEvent.lines.length < 4,
      });
    }
  };

  closePurchase = () => this.setState({showPurchaseModal: false});
  closeSuccess = () => this.setState({showSuccessModal: false});
  closeHow = () => this.setState({showHowModal: false});

  render() {
    const {event: eventInfo} = this.props.route.params;
    const {showFullText, showMoreButton, showHeaderColor, loading} = this.state;
    return (
      <>
        <S.Container
          // scrollEnabled={this.state.enableScrollViewScroll}
          stickyHeaderIndices={[0]}
          onScroll={e => {
            if (
              e.nativeEvent.contentOffset.y >
              Dimensions.get('window').height / 3 - 70
            ) {
              this.setState({showHeaderColor: true});
            } else {
              this.setState({showHeaderColor: false});
            }
          }}
          ref={myScroll => (this._myScroll = myScroll)}>
          <Header
            onLikePress={this.onLikePress}
            isFavorite={this.state.isFavorite}
            title={showHeaderColor ? eventInfo.title : ''}
            color={showHeaderColor ? '#5954f9' : 'transparent'}
            // color="#5954f9"
          />
          <S.ImageBackground source={{uri: eventInfo.src.url}} />
          <S.OptionsContainer
            onStartShouldSetResponderCapture={() => {
              this.setState({enableScrollViewScroll: false});
              if (
                this._myScroll.contentOffset === 0 &&
                this.state.enableScrollViewScroll === false
              ) {
                this.setState({enableScrollViewScroll: true});
              }
            }}>
            <Animated.View
              style={{
                marginTop: 30,
                paddingHorizontal: 15,
                marginBottom: 10,
              }}>
              <S.EventTitle>{eventInfo.title}</S.EventTitle>
              {/* <S.Separator marginTop={27} /> */}
              <S.GetButton
                disabled={this.state.bonus < eventInfo.rank}
                onPress={() => this.setState({showPurchaseModal: true})}>
                <S.GetButtonText>Получить за {eventInfo.rank}</S.GetButtonText>
                <Bonus fill="white" />
              </S.GetButton>
              <S.ScoreText>
                {this.state.bonus < eventInfo.rank
                  ? `Вам не хватает ${eventInfo.rank - this.state.bonus} баллов`
                  : `Ваш баланс: ${this.state.bonus} баллов`}
              </S.ScoreText>
              {this.state.bonus < eventInfo.rank ? (
                <S.MoreButton
                  onPress={() => this.setState({showHowModal: true})}
                  style={{alignSelf: 'center'}}>
                  <S.MoreText>Как накопить ?</S.MoreText>
                </S.MoreButton>
              ) : null}
              <S.SubTitle>Описание</S.SubTitle>
              <S.EventDescription
                onTextLayout={this.onTextLayout}
                numberOfLines={showFullText ? 5 : 1}>
                {eventInfo.announce}
              </S.EventDescription>
              {!showFullText && (
                <S.MoreButton
                  onPress={() =>
                    this.setState({showFullText: true, showMoreButton: false})
                  }>
                  <S.MoreText>Читать далее..</S.MoreText>
                </S.MoreButton>
              )}
              {this.state.pieInfo.condition ? (
                <>
                  <S.SubTitle>Условия покупки</S.SubTitle>
                  <S.EventDescription>
                    {formatDescription(this.state.pieInfo.condition)}
                  </S.EventDescription>
                </>
              ) : null}
            </Animated.View>
          </S.OptionsContainer>
        </S.Container>
        {this.state.showPurchaseModal ? (
          <Modals.PurchaseModal
            pie={eventInfo}
            isVisible={this.state.showPurchaseModal}
            onClose={this.closePurchase}
            showSuccessModal={() => this.setState({showSuccessModal: true})}
            checkBonus={this.checkBonus}
            user={this.props.user}
            bonus={this.state.bonus}
          />
        ) : null}
        <Modals.SuccessModal
          pie={eventInfo}
          onClose={this.closeSuccess}
          navigation={this.props.navigation}
          isVisible={this.state.showSuccessModal}
          // isVisible={true}
        />
        <Modals.HowModal
          onClose={this.closeHow}
          navigation={this.props.navigation}
          isVisible={this.state.showHowModal}
        />
      </>
    );
  }
}

const mapStateToProps = ({auth}) => ({
  token: auth.token,
  user: auth.userData,
});

export default connect(
  mapStateToProps,
  null,
)(PieScreen);
