import styled from 'styled-components';

export const ShopCard = styled.TouchableOpacity`
  width: 161px;
  border-radius: 20px;
  margin-bottom: ${({isVertical}) => (isVertical ? 20 : 20)}px;
  margin-horizontal: ${({isVertical}) => (isVertical ? 5 : 0)}px;
`;

export const RankContainer = styled.View`
  flex-direction: row;
`;

export const Rank = styled.Text`
  font-size: 16px;
  font-weight: 700;
  margin-right: 6px;
`;

export const ShopImageContainer = styled.View`
  margin-bottom: 8px;
  height: 96px;
  width: 161px;
  border-radius: 20px;
`;

export const ShopImage = styled.Image`
  /* background-color: #ffffff; */
  background-color: #ffffff;
  width: 100%;
  height: 100%;
  border-radius: 20px;
`;

export const ShopText = styled.Text`
  font-size: 14px;
  color: black;
  font-weight: 500;
`;
