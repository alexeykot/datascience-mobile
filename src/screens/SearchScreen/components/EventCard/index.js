import React from 'react';

import * as S from './styled';

const EventCard = ({event, isVertical, onPress}) => {
  return (
    <S.Container onPress={onPress} isVertical={isVertical}>
      <S.EventImage source={{uri: event.src.url}} />
      <S.Date>21 - 25 сентября {'   '} Москва</S.Date>
      <S.Title>{event.title}</S.Title>
    </S.Container>
  );
};

export default EventCard;
