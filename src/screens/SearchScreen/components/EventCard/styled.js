import styled from 'styled-components';

export const Container = styled.TouchableOpacity`
  margin-top: ${({isVertical}) => (isVertical ? 30 : 0)}px;
  width: 300px;
`;

export const EventImage = styled.Image`
  width: 300px;
  height: 160px;
  border-radius: 15px;
`;

export const Date = styled.Text`
  color: #8b8d94;
  font-size: 12px;
  font-weight: 400;
  margin-top: 10px;
`;

export const Title = styled.Text`
  color: #1b041e;
  font-family: Roboto;
  font-size: 16px;
  font-weight: 700;
  text-align: left;
`;
