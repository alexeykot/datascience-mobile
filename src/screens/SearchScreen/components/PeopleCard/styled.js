import styled from 'styled-components';

export const Container = styled.TouchableOpacity`
  align-items: center;
  max-width: ${({isVertical}) => (!isVertical ? 65 : 200)}px;
  height: 70px;
  padding-left: ${({isVertical}) => (!isVertical ? 0 : 30)}px;
  flex-direction: ${({isVertical}) => (!isVertical ? 'column' : 'row')};
`;

export const InfoContainer = styled.View`
  margin-left: 14px;
`;

export const Avatar = styled.Image``;

export const PeopleIconContainer = styled.View`
  width: 54px;
  justify-content: center;
  align-items: center;
  height: 54px;
  background-color: #f5f3fa;
  border-radius: 27px;
`;

export const PeopleAvatar = styled.Image`
  width: 54px;
  height: 54px;
  border-radius: 27px;
`;

export const Name = styled.Text`
  color: #1b041e;
  font-family: Roboto;
  font-size: ${({isVertical}) => (!isVertical ? 12 : 14)}px;
  font-weight: 700;
  text-align: ${({isVertical}) => (!isVertical ? 'center' : 'left')};
  margin-top: 7px;
`;

export const Job = styled.Text`
  color: #8b8d94;
  font-family: Roboto;
  font-size: 12px;
  text-align: ${({isVertical}) => (!isVertical ? 'center' : 'left')};
  margin-top: 5px;
`;
