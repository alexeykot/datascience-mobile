import React from 'react';

import fade from '../../../../assets/images/fade.png';

import * as S from './styled';

const NewsCard = ({news: item, onCardPress, isVertical}) => {
  return (
    <S.Wrapper isVertical={isVertical}>
      <S.EventCard
        isVertical={isVertical}
        onPress={onCardPress}
        style={{elevation: 4}}>
        <S.EventBg source={{uri: item.src.url}} />
        <S.EventInfoContainer>
          <S.EventTitle numberOfLines={6}>{item.title}</S.EventTitle>
          {/* <S.FadeImg source={fade} /> */}
        </S.EventInfoContainer>
      </S.EventCard>
    </S.Wrapper>
  );
};

export default NewsCard;
