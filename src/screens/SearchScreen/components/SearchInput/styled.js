import styled from 'styled-components';

export const SearchContainer = styled.View`
  width: 300px;
  height: 40px;
  elevation: 10;
  border-radius: 20px;
  background-color: #ffffff;
  position: absolute;
  top: -20px;
  align-self: center;
  flex-direction: row;
  align-items: center;
  padding-left: 5px;
`;

export const IconContainer = styled.View`
  width: 20px;
  height: 20px;
  opacity: 0.5;
  justify-content: center;
  align-items: center;
`;

export const ResetButton = styled.TouchableOpacity`
  width: 40px;
  height: 40px;
  justify-content: center;
  align-items: center;
  z-index: 1111;
`;

export const SearchInput = styled.TextInput`
  /* width: 100%; */
  width: 80%;
  height: 100%;
  border-bottom-right-radius: 30px;
  border-top-right-radius: 30px;
`;
