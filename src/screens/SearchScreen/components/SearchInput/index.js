import React from 'react';

import Search from '../../../../assets/icons/search.svg';
import Reset from '../../../../assets/icons/close-circle-fill.svg';

import * as S from './styled';

const SearchInput = ({
  setInputFocused,
  setInputBlured,
  onTextChange,
  onSubmit,
  resetQuery,
  value,
  setRef,
}) => {
  return (
    <S.SearchContainer>
      <S.IconContainer>
        <Search fill="#c9ccd5" />
      </S.IconContainer>
      <S.SearchInput
        onFocus={setInputFocused}
        onBlur={setInputBlured}
        onChangeText={onTextChange}
        onSubmitEditing={onSubmit}
        placeholder="Поиск"
        value={value}
        ref={r => setRef(r)}
      />
      {value ? (
        <S.ResetButton onPress={resetQuery}>
          <Reset />
        </S.ResetButton>
      ) : null}
    </S.SearchContainer>
  );
};

export default SearchInput;
