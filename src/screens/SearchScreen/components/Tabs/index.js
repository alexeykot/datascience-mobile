import React from 'react';

import * as S from './styled';

const tabs = ['Мероприятия', 'Люди', 'Новости', 'Плюшки'];
const Tabs = ({selectedCategory, setActiveCategory}) => {
  return (
    <>
      <S.Container>
        <S.TabsContainer showsHorizontalScrollIndicator={false} horizontal>
          <S.Tab
            onPress={() => setActiveCategory('')}
            isSelected={!selectedCategory}>
            <S.TabText isSelected={!selectedCategory}>Все</S.TabText>
          </S.Tab>
          {tabs.map(tab => (
            <S.Tab
              onPress={() => setActiveCategory(tab)}
              isSelected={selectedCategory === tab}>
              <S.TabText isSelected={selectedCategory === tab}>{tab}</S.TabText>
            </S.Tab>
          ))}
        </S.TabsContainer>
        <S.BorderContainer>
          <S.Border />
        </S.BorderContainer>
      </S.Container>
    </>
  );
};

export default Tabs;
