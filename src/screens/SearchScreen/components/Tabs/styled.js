import styled from 'styled-components';

export const Container = styled.View`
  /* height: 40px; */
  width: 100%;
  margin-top: 20px;
  /* border-bottom-width: 1px;
  border-color: rgba(201, 204, 213, 0.5); */
  padding-bottom: 8px;
`;

export const TabsContainer = styled.ScrollView`
  width: 100%;
`;

export const Tab = styled.TouchableOpacity`
  border-color: ${({isSelected}) => (isSelected ? '#5954f9' : 'transparent')};
  border-bottom-width: 3px;
  height: 50px;
  margin-horizontal: 10px;
  padding-horizontal: 5px;
  align-items: center;
  justify-content: center;
`;

export const TabText = styled.Text`
  color: ${({isSelected}) => (isSelected ? '#5954f9' : '#8b8d94')};
  font-family: Roboto;
  font-size: 14px;
  font-weight: 500;
`;

export const Border = styled.View`
  width: 100%;
  height: 1px;
  background-color: #c9ccd5;
  opacity: 0.5;
  z-index: 1;
`;

export const BorderContainer = styled.View`
  width: 100%;
`;
