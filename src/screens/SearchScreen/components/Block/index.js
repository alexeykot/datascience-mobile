/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {Dimensions} from 'react-native';
import Carousel from 'react-native-snap-carousel';

import Coffee from '../../../../assets/icons/coffe.svg';

import * as S from './styled';

const handleStyles = (align, type) =>
  type !== 'Плюшки'
    ? {
        alignItems: align,
      }
    : {
        ...pieStyles,
        alignItems: align,
      };

const pieStyles = {
  flexDirection: 'row',
  flexWrap: 'wrap',
  width: '100%',
  paddingTop: 20,
  zIndex: 1,
  justifyContent: 'center',
};

const Block = ({
  title,
  itemWidth,
  data,
  renderItem,
  isShowed,
  isVertical,
  renderVerticalItems,
  type = '',
  align = 'center',
}) => (
  <>
    {isShowed ? (
      <S.ScrollContainer>
        {data.length && !isVertical ? (
          <S.CategoryTitlte>{title}</S.CategoryTitlte>
        ) : null}
        {!isVertical ? (
          <Carousel
            layout={'default'}
            data={data}
            containerCustomStyle={{
              paddingLeft: 15,
            }}
            renderItem={renderItem}
            inactiveSlideScale={1}
            inactiveSlideOpacity={1}
            activeSlideAlignment="start"
            sliderWidth={Dimensions.get('window').width}
            itemWidth={itemWidth}
          />
        ) : (
          <>
            {data.length ? (
              <S.VerticalScroll
                contentContainerStyle={handleStyles(align, type)}>
                {data.map((item, index) => renderVerticalItems(item))}
                {title === 'Новости' || title === 'Мероприятия' ? (
                  <S.FakeBlock />
                ) : null}
              </S.VerticalScroll>
            ) : (
              <S.NoItemsFound>
                <Coffee />
                <S.NoItemsTitle>Ничего не найдено</S.NoItemsTitle>
                <S.NoItemsHint>Попробуйте сделать другой запрос</S.NoItemsHint>
              </S.NoItemsFound>
            )}
          </>
        )}
      </S.ScrollContainer>
    ) : null}
  </>
);

export default Block;
