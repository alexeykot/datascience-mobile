import styled from 'styled-components';

export const ScrollContainer = styled.View`
  width: 100%;
`;

export const CategoryTitlte = styled.Text`
  color: #1b041e;
  font-family: Roboto;
  font-size: 18px;
  font-weight: 700;
  margin-top: 30px;
  padding-left: 15px;
  margin-bottom: 20px;
`;

export const VerticalScroll = styled.ScrollView`
  width: 100%;
`;

export const FakeBlock = styled.View`
  width: 100%;
  height: 50px;
`;

export const NoItemsFound = styled.View`
  width: 100%;
  align-items: center;
  margin-top: 40px;
`;

export const NoItemsTitle = styled.Text`
  color: #140c09;
  font-family: Roboto;
  font-size: 16px;
  font-weight: 700;
`;

export const NoItemsHint = styled.Text`
  color: #140c09;
  font-family: Roboto;
  font-size: 14px;
  font-weight: 400;
`;
