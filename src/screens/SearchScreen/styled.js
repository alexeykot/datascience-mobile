import {Dimensions} from 'react-native';
import styled from 'styled-components';

export const Container = styled.View`
  background-color: #5954f9;
  flex: 1;
`;

export const ScrollContainer = styled.ScrollView`
  width: 100%;
`;

export const OptionsContainer = styled.View`
  width: 100%;
  /* height: ${Dimensions.get('screen').height * 0.83}; */
  height: 100%;
  border-top-left-radius: 30px;
  border-top-right-radius: 30px;
  background-color: white;
  margin-top: 30px;
  padding: ${({isFocused}) => (isFocused ? '0 30px' : '0')};
  /* padding: 0 30px; */
`;

export const Title = styled.Text`
  color: #1b041e;
  font-family: Roboto;
  font-size: 18px;
  font-weight: 700;
  margin-top: 50px;
`;

export const HistoryItem = styled.TouchableOpacity`
  width: 100%;
  margin-top: 18px;
`;

export const HistoryText = styled.Text`
  color: #1b041e;
  font-family: Roboto;
  font-size: 14px;
  font-weight: 400;
`;

export const Separator = styled.View`
  width: 100%;
  height: 1px;
  background-color: #c9ccd5;
  opacity: 0.5;
  margin-top: 16px;
`;

export const FakeView = styled.View`
  height: 150px;
  width: 100%;
`;
