/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {ActivityIndicator, Dimensions} from 'react-native';
import {connect} from 'react-redux';
import SearchHistoryStorage from '../../services/storage/searchHistory';

import Coffee from '../../assets/icons/coffe.svg';
import Header from '../../screens/Event/components/Header';
import http from '../../services/http';
import EventCard from './components/EventCard';
import EventVerticalCard from '../MainScreen/components/EventCard';
import SearchInput from './components/SearchInput';
import PeopleCard from './components/PeopleCard';
import PieCard from './components/PieCard';
import NewsCard from './components/NewsCard';
import Tabs from './components/Tabs';

import * as S from './styled';
import Block from './components/Block';
import {addCategory, findInCategories, formatPeopeName} from './helpers';
import {
  NoItemsFound,
  NoItemsHint,
  NoItemsTitle,
} from './components/Block/styled';
import BaseScreen from '../../components/BaseScreen';

// const tabs = ['Мероприятия', 'Люди', 'Новости', 'Плюшки'];
// const history = ['ASdasdadasd', 'Яндекс', 'Google'];

class SearchScreen extends React.Component {
  state = {
    isInputFocused: false,
    inputQuery: '',
    loading: false,
    events: [],
    pies: [],
    news: [],
    crossMembers: [],
    selectedCategory: '',
    noItems: false,
    history: [],
  };

  inputRef = null;
  componentWillMount() {
    this.props.navigation.addListener('focus', async () => {
      this.fetchInfo();
      if (this.props.route.params.searchString) {
        this.setState({inputQuery: this.props.route.params.searchString});
      }
    });
  }

  fetchInfo = async () => {
    this.setState({loading: true});
    try {
      const events = await http.get(
        `/event/event/index?access-token=${this.props.token}`,
      );
      this.setState({
        events: addCategory(events.data.data.entities, 'Мероприятия'),
      });
    } catch (err) {
      console.log('events-err', err);
    }
    try {
      const {data} = await http.get(
        `/pie/default/index?access-token=${this.props.token}`,
      );
      this.setState({pies: addCategory(data.data.entities, 'Плюшки')});
    } catch (err) {
      console.log('pie-err', err);
    }
    try {
      const news = await http.get(
        `/news/default/index?access-token=${this.props.token}`,
      );
      this.setState({news: addCategory(news.data.data.entities, 'Новости')});
    } catch (err) {
      console.log('news-err', err);
    }
    try {
      const crossMembers = await http.get(
        `/event/client/environment?access-token=${this.props.token}`,
      );
      this.setState({
        crossMembers: addCategory(
          formatPeopeName(crossMembers.data.data.entities),
          'Люди',
        ),
      });
    } catch (err) {
      console.log('crossMembers-err', err);
    }
    this.setState({loading: false});
  };

  filterData = data => {
    const {inputQuery} = this.state;
    if (!data.length) {
      return data;
    }
    const filtered = data.filter(item =>
      item.title.toLocaleLowerCase().includes(inputQuery.toLocaleLowerCase()),
    );
    return filtered;
  };

  setActiveCategory = category => this.setState({selectedCategory: category});

  setInputFocused = async () => {
    const searchHistory = await SearchHistoryStorage.get();
    this.setState({history: searchHistory, isInputFocused: true});
  };

  setInputBlured = () => this.setState({isInputFocused: false});
  setRef = ref => {
    this.inputRef = ref;
  };
  resetQuery = () => this.setState({inputQuery: '', noItems: false});
  onSearch = text => {
    this.setState({inputQuery: text});
  };

  onHistoryItemPress = query => {
    this.setState({inputQuery: query});
    this.onSubmit(query);
    this.inputRef.blur();
    this.setState({isInputFocused: false});
  };

  devidePiesToRow = pies => {
    let obj = {};
    for (let i = 0; i < pies.length; i += 2) {
      obj[i] = [pies[i], pies[i + 1]].filter(a => a !== undefined);
    }
    return obj;
  };

  onSubmit = async inputQuery => {
    const {events, crossMembers, news, pies} = this.state;
    if (!inputQuery) {
      return;
    }
    const searchHistory = await SearchHistoryStorage.get();
    await SearchHistoryStorage.save([inputQuery, ...searchHistory]);
    const allItems = [...events, ...crossMembers, ...news, ...pies];
    const filteredItems = allItems
      .filter(i => i.title)
      .filter(item =>
        item.title.toLocaleLowerCase().includes(inputQuery.toLocaleLowerCase()),
      );
    console.log('filteter', inputQuery, allItems, filteredItems);
    if (filteredItems.length) {
      this.setState({
        selectedCategory: filteredItems[0].category,
        noItems: false,
      });
    } else {
      this.setState({noItems: true});
    }
  };

  render() {
    const {
      isInputFocused,
      loading,
      events,
      crossMembers,
      news,
      pies,
      selectedCategory,
      inputQuery,
      noItems,
      history,
    } = this.state;
    return (
      <BaseScreen
        title="Поиск"
        withSearch
        scrollableOutsideContainer
        containerPadding={isInputFocused ? '0 30px' : '0'}
        searchProps={{
          filterText: inputQuery,
          inputPlaceholder: 'Поиск',
          onResetPress: this.resetQuery,
          onInputChange: this.onSearch,
          setInputFocused: this.setInputFocused,
          setInputBlured: this.setInputBlured,
          onSubmit: () => this.onSubmit(inputQuery),
          setRef: this.setRef,
        }}
        content={
          <>
            {isInputFocused ? (
              <>
                <S.Title>История поиска</S.Title>
                {history.splice(0, 3).map((i, index) => (
                  <S.HistoryItem onPress={() => this.onHistoryItemPress(i)}>
                    <S.HistoryText>{i}</S.HistoryText>
                    {index !== history.length - 1 && <S.Separator />}
                  </S.HistoryItem>
                ))}
              </>
            ) : (
              <>
                {!loading && events.length ? (
                  <>
                    <Tabs
                      setActiveCategory={this.setActiveCategory}
                      selectedCategory={selectedCategory}
                    />
                    <S.ScrollContainer>
                      {noItems && !selectedCategory ? (
                        <NoItemsFound>
                          <Coffee />
                          <NoItemsTitle>Ничего не найдено</NoItemsTitle>
                          <NoItemsHint>
                            Попробуйте сделать другой запрос
                          </NoItemsHint>
                        </NoItemsFound>
                      ) : null}
                      <Block
                        title="Мероприятия"
                        data={this.filterData(events)}
                        renderItem={({item, index}) => (
                          <EventCard
                            onPress={() =>
                              this.props.navigation.navigate(
                                'EventMainScreen',
                                {
                                  event: item,
                                },
                              )
                            }
                            event={item}
                          />
                        )}
                        renderVerticalItems={item => (
                          <EventVerticalCard
                            onCardPress={() =>
                              this.props.navigation.navigate(
                                'EventMainScreen',
                                {
                                  event: item,
                                },
                              )
                            }
                            styles={{
                              marginTop: 20,
                              elevation: 5,
                            }}
                            event={item}
                          />
                        )}
                        itemWidth={308}
                        isVertical={selectedCategory === 'Мероприятия'}
                        isShowed={
                          !selectedCategory ||
                          selectedCategory === 'Мероприятия'
                        }
                      />
                      <Block
                        title="Люди"
                        data={this.filterData(crossMembers)}
                        renderItem={({item, index}) => (
                          <PeopleCard
                            onPress={() =>
                              this.props.navigation.navigate('MemberScreen', {
                                member: item,
                              })
                            }
                            member={item}
                          />
                        )}
                        renderVerticalItems={item => (
                          <PeopleCard
                            onPress={() =>
                              this.props.navigation.navigate('MemberScreen', {
                                member: item,
                              })
                            }
                            isVertical
                            member={item}
                          />
                        )}
                        align="flex-start"
                        isVertical={selectedCategory === 'Люди'}
                        itemWidth={80}
                        isShowed={
                          !selectedCategory || selectedCategory === 'Люди'
                        }
                      />
                      <Block
                        title="Новости"
                        data={this.filterData(news)}
                        renderItem={({item, index}) => (
                          <NewsCard
                            onCardPress={() =>
                              this.props.navigation.navigate(
                                'NewsDetailsScreen',
                                {
                                  event: item,
                                },
                              )
                            }
                            news={item}
                          />
                        )}
                        renderVerticalItems={item => (
                          <NewsCard
                            onCardPress={() =>
                              this.props.navigation.navigate(
                                'NewsDetailsScreen',
                                {
                                  event: item,
                                },
                              )
                            }
                            isVertical
                            news={item}
                          />
                        )}
                        itemWidth={338}
                        isVertical={selectedCategory === 'Новости'}
                        isShowed={
                          !selectedCategory || selectedCategory === 'Новости'
                        }
                      />
                      <Block
                        title="Плюшки"
                        type="Плюшки"
                        align="flex-start"
                        data={this.filterData(pies)}
                        renderItem={({item, index}) => (
                          <PieCard
                            onPress={() =>
                              this.props.navigation.navigate('PieScreen', {
                                event: item,
                              })
                            }
                            pie={item}
                          />
                        )}
                        renderVerticalItems={item => (
                          <PieCard
                            onPress={() =>
                              this.props.navigation.navigate('PieScreen', {
                                event: item,
                              })
                            }
                            isVertical
                            pie={item}
                          />
                        )}
                        isVertical={selectedCategory === 'Плюшки'}
                        itemWidth={175}
                        isShowed={
                          !selectedCategory || selectedCategory === 'Плюшки'
                        }
                      />
                      <S.FakeView />
                    </S.ScrollContainer>
                  </>
                ) : (
                  <ActivityIndicator size="large" />
                )}
              </>
            )}
          </>
        }
      />
    );
  }
}

const mapStateToProps = ({auth}) => ({
  token: auth.token,
});

export default connect(
  mapStateToProps,
  null,
)(SearchScreen);
