export const findInCategories = (query, categories) => {
  const filteredCategories = categories.filter(category =>
    category.toLocaleLowerCase().includes(query.toLocaleLowerCase()),
  );
  return filteredCategories;
};

export const addCategory = (data, category) => {
  if (!data.length) {
    return data;
  }
  const formatted = data.map(item => ({
    ...item,
    category: category,
  }));
  return formatted;
};

export const formatPeopeName = data => {
  if (!data.length) {
    return data;
  }
  const formatted = data.map(item => ({
    ...item,
    title: item.fullname,
  }));
  return formatted;
};
