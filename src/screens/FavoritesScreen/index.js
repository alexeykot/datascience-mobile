import React from 'react';
import HeartFill from '../../assets/icons/heart-02-fill.svg';

import * as S from './styled';

import FavoritesStorage from '../../services/storage/favorites';
import PieCard from './components/PieCard';
import PeopleCard from './components/PeopleCard';
import {getTime} from '../../helpers/dateHelpers';
import {connect} from 'react-redux';
import BaseScreen from '../../components/BaseScreen';
import {getClientStatus} from '../MainScreen/helpers';

const tabs = ['События', 'Спикеры', 'Плюшки', 'Люди'];
const caseDictionary = {
  События: 'Добавьте событие в избранное и оно появится здесь',
  Спикеры: 'Добавьте спикера в избранное и он появится здесь ',
  Плюшки: 'Добавьте плюшку в избранное и она появится здесь',
  Люди: 'Добавьте человека в избранное и он появится здесь',
};

const FavoritesScreen = props => {
  const [selectedTab, setSelectedTab] = React.useState(tabs[0]);
  const [filterText, setFilterText] = React.useState('');
  const [favorites, setFavorites] = React.useState(null);

  React.useEffect(() => {
    props.navigation.addListener('focus', async () => {
      const favoritesFromStorage = await FavoritesStorage.get();
      setFavorites(favoritesFromStorage);
    });
  });

  const filterByText = (evs, text) => {
    if (!evs.length) {
      return [];
    }
    if (evs[0].type === 'Люди') {
      return evs.filter(e =>
        e.fullname.toLocaleLowerCase().includes(text.toLocaleLowerCase()),
      );
    }
    if (evs[0].type === 'Спикеры') {
      return evs.filter(e =>
        e.fullName.toLocaleLowerCase().includes(text.toLocaleLowerCase()),
      );
    } else {
      return evs.filter(e =>
        e.title.toLocaleLowerCase().includes(text.toLocaleLowerCase()),
      );
    }
  };

  const filterFavorites = () => {
    const filteredFavorites = favorites.filter(f => f.type === selectedTab);
    return filterByText(filteredFavorites, filterText);
  };

  const getCity = event => {
    if (!event.place) {
      return '';
    }
    const city = event.place.split(',')[1];
    return city;
  };

  const getEventRequests = (event, myEvents) => {
    const find = myEvents.find(e => e.id === event.id);
    if (find && find.requests) {
      return true;
    } else {
      return false;
    }
  };

  const renderBlock = favorite => {
    if (favorite.type === 'События') {
      return (
        <S.EventContainer
          onPress={() =>
            props.navigation.navigate('EventMainScreen', {
              event: favorite,
              status: getClientStatus(favorite, props.myEvents),
            })
          }>
          <S.EventCard source={{uri: favorite.src.url}} />
          <S.LikeContainer>
            <HeartFill />
          </S.LikeContainer>
          {getEventRequests(favorite, props.myEvents) ? (
            <S.StatusContainer color="#3dd576">
              <S.StatusText>Я зарегестрирован</S.StatusText>
            </S.StatusContainer>
          ) : null}
          <S.EventTitle>{favorite.title}</S.EventTitle>
          <S.EventDateContainer>
            <S.DateText>
              {getTime(favorite)}
              {favorite.place ? (
                <S.DateText>
                  {'  '}&#8226;{'  '}
                </S.DateText>
              ) : null}
            </S.DateText>
            <S.DateText>{getCity(favorite)}</S.DateText>
          </S.EventDateContainer>
        </S.EventContainer>
      );
    }
    if (favorite.type === 'Плюшки') {
      return (
        <PieCard
          onPress={() =>
            props.navigation.navigate('PieScreen', {
              event: favorite,
            })
          }
          pie={favorite}
        />
      );
    }
    if (favorite.type === 'Люди') {
      return (
        <PeopleCard
          onPress={() =>
            props.navigation.navigate('MemberScreen', {
              member: favorite,
            })
          }
          speaker={favorite}
        />
      );
    }
    if (favorite.type === 'Спикеры') {
      return (
        <PeopleCard
          onPress={() =>
            props.navigation.navigate('SpeakerScreen', {
              speaker: favorite,
            })
          }
          isSpeaker
          speaker={favorite}
        />
      );
    }
    return <S.FakeView />;
  };

  return (
    <BaseScreen
      title="Избранное"
      scrollableOutsideContainer
      withSearch
      withTabs
      tabsProps={{
        tabs: tabs,
        selectedTab: selectedTab,
        onTabPress: tab => setSelectedTab(tab),
      }}
      searchProps={{
        filterText: filterText,
        inputPlaceholder: 'Поиск',
        onInputChange: text => setFilterText(text),
        onResetPress: () => setFilterText(''),
      }}
      content={
        <S.VerticalScroll
          showsVerticalScrollIndicator={false}
          contentContainerStyle={{paddingBottom: 150}}>
          {favorites
            ? filterFavorites().map(favorite => renderBlock(favorite))
            : null}
          {favorites && !filterFavorites().length ? (
            <S.FakeBlock>
              <S.FakeText>
                {filterText
                  ? 'Ничего не найдено'
                  : `${caseDictionary[selectedTab]}`}
              </S.FakeText>
            </S.FakeBlock>
          ) : null}
          <S.PaddingView />
        </S.VerticalScroll>
      }
    />
  );
};

const mapStateToProps = ({events}) => ({
  myEvents: events.myEvents,
});

export default connect(
  mapStateToProps,
  null,
)(FavoritesScreen);
