import React from 'react';

import People from '../../../../assets/icons/people.svg';
import * as S from './styled';

const PeopleCard = props => {
  const {isSpeaker, speaker, onPress} = props;
  const getAvatar = people => {
    if (speaker.src && speaker.src.url) {
      return true;
    }
    if (speaker.photo && speaker.photo.url) {
      return true;
    }
    return false;
  };
  return (
    <S.Container
      onPress={onPress}
      isPaddingHorizontal={props.isPaddingHorizontal}>
      {getAvatar(speaker) ? (
        <S.Avatar
          source={{uri: isSpeaker ? speaker.src.url : speaker.photo.url}}
        />
      ) : (
        <S.PeopleIconContainer>
          <People />
        </S.PeopleIconContainer>
      )}
      <S.InfoContainer>
        <S.Name>{isSpeaker ? speaker.fullName : speaker.fullname}</S.Name>
        {/* <S.Job numberOfLines={1} ellipsizeMode="tail">
          {props.speaker.post}
        </S.Job> */}
      </S.InfoContainer>
    </S.Container>
  );
};

export default PeopleCard;
