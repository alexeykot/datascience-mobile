import styled from 'styled-components';

export const Container = styled.TouchableOpacity`
  width: 100%;
  /* padding: 6px 28px 6px 23px; */
  /* padding: 6px 0; */
  padding: ${({isPaddingHorizontal}) =>
    isPaddingHorizontal ? '6px 28px 6px 23px' : '6px 0'};
  flex-direction: row;
  align-items: center;
`;

export const InfoContainer = styled.View`
  max-width: 211px;
`;

export const Name = styled.Text`
  color: #1b041e;
  font-family: 'Roboto';
  font-size: 14px;
  font-weight: 700;
`;

export const Job = styled.Text`
  color: #8b8d94;
  font-family: 'Roboto';
  font-size: 12px;
  font-weight: 400;
`;

export const PeopleIconContainer = styled.View`
  width: 54px;
  justify-content: center;
  align-items: center;
  height: 54px;
  background-color: #f5f3fa;
  border-radius: 27px;
  margin-right: 15px;
`;

export const Avatar = styled.Image`
  width: 54px;
  height: 54px;
  border-radius: 27px;
  margin-right: 15px;
`;
