import React from 'react';

import Bonus from '../../../../assets/icons/bonus.svg';
import HeartFill from '../../../../assets/icons/heart-02-fill.svg';

import * as S from './styled';
const PieCard = ({index, pie, onPress, isFirst}) => {
  return (
    <S.ShopCard onPress={onPress} activeOpacity={0.7} isFirst={isFirst}>
      <S.ShopImageContainer style={{elevation: 2}}>
        <S.ShopImage source={{uri: pie.src.url}} />
        <S.LikeContainer>
          <HeartFill />
        </S.LikeContainer>
      </S.ShopImageContainer>
      <S.Row>
        <S.ShopText>{pie.title}</S.ShopText>
        <S.RankContainer>
          <S.Rank>{pie.rank}</S.Rank>
          <Bonus fill="#5954f9" />
        </S.RankContainer>
      </S.Row>
    </S.ShopCard>
  );
};

export default PieCard;
