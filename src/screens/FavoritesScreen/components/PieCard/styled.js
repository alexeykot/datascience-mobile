import styled from 'styled-components';

export const ShopCard = styled.TouchableOpacity`
  width: 100%;
  margin-top: 20px;
  border-radius: 20px;
`;

export const RankContainer = styled.View`
  flex-direction: row;
`;

export const Row = styled.View`
  flex-direction: row;
  width: 100%;
  justify-content: space-between;
`;

export const Rank = styled.Text`
  font-size: 16px;
  font-weight: 700;
  margin-right: 6px;
`;

export const ShopImageContainer = styled.View`
  margin-bottom: 8px;
  height: 160px;
  width: 100%;
  border-radius: 20px;
`;

export const ShopImage = styled.Image`
  /* background-color: #ffffff; */
  background-color: #ffffff;
  width: 100%;
  height: 100%;
  border-radius: 20px;
`;

export const ShopText = styled.Text`
  font-size: 14px;
  color: black;
  font-weight: 700;
`;

export const LikeContainer = styled.View`
  position: absolute;
  top: 15px;
  right: 15px;
`;
