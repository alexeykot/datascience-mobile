import styled from 'styled-components';

export const Container = styled.View`
  background-color: #5954f9;
`;

export const OptionsContainer = styled.View`
  width: 100%;
  height: 700px;
  border-top-left-radius: 30px;
  border-top-right-radius: 30px;
  background-color: white;
  margin-top: 40px;
  padding: 0 15px;
`;

export const TabsContainer = styled.View`
  width: 100%;
  height: 50px;
  margin-top: 40px;
  flex-direction: row;
`;

export const Border = styled.View`
  width: 100%;
  height: 1px;
  background-color: #c9ccd5;
  opacity: 0.5;
  z-index: 1;
`;

export const BorderContainer = styled.View`
  width: 100%;
`;

export const SelectedBorder = styled.View`
  height: 3px;
  border-radius: 2px;
  background-color: #5954f9;
  /* width: 58px; */
  width: ${({borderW}) => borderW}px;
  position: absolute;
  left: ${({borderX}) => borderX}px;
  top: -1px;
  z-index: 1111;
`;

export const Tab = styled.TouchableOpacity`
  width: 25%;
  height: 100%;
  justify-content: center;
  align-items: center;
  /* border-bottom-width: ${({selected}) => (selected ? 3 : 1)}px;
  border-color: ${({selected}) => (selected ? '#5954f9' : '#c9ccd5')}; */
`;

export const FakeView = styled.View`
  width: 100%;
  height: 70px;
  background-color: red;
  margin-top: 20px;
  border-radius: 10px;
`;

export const EventContainer = styled.TouchableOpacity`
  width: 100%;
`;

export const VerticalScroll = styled.ScrollView``;

export const ResetButton = styled.TouchableOpacity`
  width: 50px;
  height: 40px;
  justify-content: center;
  align-items: center;
  position: absolute;
  right: 0px;
  z-index: 1111;
`;

export const FakeBlock = styled.View`
  width: 100%;
  height: 200px;
  background-color: white;
  padding-top: 20px;
  justify-content: center;
  align-items: center;
`;

export const StatusContainer = styled.View`
  padding: 8px 9px;
  background-color: ${({color}) => color};
  position: absolute;
  bottom: 90;
  left: 20;
  border-radius: 12px;
  min-width: 140px;
  height: 24px;
  justify-content: center;
  align-items: center;
`;

export const StatusText = styled.Text`
  color: #ffffff;
  font-family: Roboto;
  font-size: 10px;
  font-weight: 700;
  text-transform: uppercase;
`;

export const FakeText = styled.Text`
  color: #8b8d94;
  font-size: 14px;
  font-weight: 500;
  text-align: center;
  align-self: center;
  padding: 0 20px;
`;

export const LikeIcon = styled.Image`
  position: absolute;
  top: 15px;
  right: 15px;
`;

export const LikeContainer = styled.View`
  position: absolute;
  top: 35px;
  right: 15px;
`;

export const EventTitle = styled.Text`
  color: #140c09;
  font-family: Roboto;
  font-size: 16px;
  font-weight: 700;
  margin-top: 15px;
`;

export const EventDateContainer = styled.View`
  flex-direction: row;
  width: 100%;
  margin-top: 9px;
`;

export const DateText = styled.Text`
  color: #8b8d94;
  font-family: Roboto;
  font-size: 12px;
  font-weight: 400;
`;

export const EventCard = styled.Image`
  width: 100%;
  height: 160px;
  margin-top: 20px;
  border-radius: 10px;
`;

export const TabTitle = styled.Text`
  color: ${({selected}) => (selected ? '#5954f9' : '#1b041e')};
  font-family: Roboto;
  font-size: 14px;
  font-weight: 700;
`;

export const SearchIcon = styled.View`
  position: absolute;
  right: 14px;
`;

export const SearchInput = styled.TextInput`
  width: 100%;
  height: 100%;
  padding-left: 15px;
  border-radius: 20px;
  opacity: 0.38;
`;

export const InputContainer = styled.View`
  position: absolute;
  align-self: center;
  width: 300;
  top: -25;
  height: 40px;
  elevation: 5;
  border-radius: 20px;
  background-color: white;
  justify-content: center;
  align-items: center;
`;

export const PlaceHolder = styled.Text`
  opacity: 0.38;
  color: #8b8d94;
  font-family: Roboto;
  font-size: 14px;
  font-weight: 500;
  position: absolute;
`;

export const PaddingView = styled.View`
  height: 200px;
  width: 100%;
`;
