/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {
  TextInput,
  Image,
  StyleSheet,
  Text,
  ActivityIndicator,
  Alert,
  Keyboard,
} from 'react-native';
import CountDown from 'react-native-countdown-component';
import {connect} from 'react-redux';
import {
  CodeField,
  Cursor,
  useBlurOnFulfill,
  useClearByFocusCell,
} from 'react-native-confirmation-code-field';

import Logo from '../../assets/icons/logo.svg';
import ellipse from '../../assets/images/ellips.png';
import * as S from './styled';
import {codeConfirmRequest, mobileConfirmRequest} from '../../redux/auth';
import http from '../../services/http';

const styles = StyleSheet.create({
  root: {flex: 1, padding: 20},
  title: {textAlign: 'center', fontSize: 30},
  codeFiledRoot: {marginTop: 20},
  cell: {
    width: 40,
    height: 40,
    lineHeight: 38,
    fontSize: 24,
    // borderWidth: 2,
    // borderColor: '#00000030',
    textAlign: 'center',
  },
  errorCell: {
    borderBottomWidth: 2,
    borderBottomColor: 'red',
    color: 'red',
  },
  focusCell: {
    borderColor: '#000',
  },
});

const CodeConfirmScreen = props => {
  const [value, setValue] = React.useState('');
  const [loading, setLoading] = React.useState(false);
  const [isError, setError] = React.useState(false);
  const [resendAvailable, setResendAvailable] = React.useState(false);
  const ref = useBlurOnFulfill({value, cellCount: 6});
  const onInputChange = async text => {
    setValue(text);
    setError(false);
    // console.log('123123', text, text.length);
    if (text.length === 6) {
      // props.codeConfirmRequest(text);
      setLoading(true);
      const data = await http
        .post('/verify/default/phone-verify', {
          entity: `7${props.route.params.extractedPhoneNumber}`,
          code: value,
        })
        .catch(err => {
          console.log(err);
          setError(true);
          props.navigation.navigate('Registration');
        });
      setLoading(false);
      if (data && data.data.code === 200) {
        props.navigation.navigate('Registration');
      }
      // console.log('datadatadata', data);
      // props.navigation.navigate('Registration');
    }
  };

  const onResendPress = () => {
    props.mobileConfirmRequest(props.route.params.extractedPhoneNumber);
    setResendAvailable(false);
    setError(false);
    setValue('');
  };
  return (
    <S.Container>
      <Logo width={117} height={32} />
      <S.Title>Теперь введите код</S.Title>
      <S.SubTitleContainer onPress={() => props.navigation.goBack()}>
        <S.SubTitle>
          {/* Мы отправили код на номер +7 {props.route.params.phoneNumber} */}
          Код отправили сообщением на +7{' '}
          {props.route.params.formattedPhoneNumber}
        </S.SubTitle>
        <S.Hint> Изменить</S.Hint>
      </S.SubTitleContainer>
      {!loading ? (
        <CodeField
          ref={ref}
          {...props}
          value={value}
          onChangeText={onInputChange}
          cellCount={6}
          // autoFocus={true}
          rootStyle={styles.codeFiledRoot}
          keyboardType="number-pad"
          renderCell={({index, symbol, isFocused}) => (
            <Text
              key={index}
              style={[
                styles.cell,
                isFocused && styles.focusCell,
                isError && styles.errorCell,
              ]}>
              {symbol ||
                (isFocused ? (
                  <Cursor />
                ) : (
                  <S.CodePlaceHolder source={ellipse} />
                ))}
            </Text>
          )}
        />
      ) : (
        <ActivityIndicator size="large" />
      )}
      <S.ResendButton
        disabled={!resendAvailable}
        onPress={onResendPress}
        resendAvailable={resendAvailable}>
        <S.ResendText resendAvailable={resendAvailable}>
          {resendAvailable ? 'Получить код' : 'Получить новый код через 00 :'}
        </S.ResendText>
        {!resendAvailable && (
          <CountDown
            until={19}
            onFinish={() => setResendAvailable(true)}
            size={10}
            digitTxtStyle={{color: 'black', fontSize: 14, fontWeight: '700'}}
            digitStyle={{
              backgroundColor: 'rgba(252, 252, 252, 0.6)',
              marginLeft: 0,
            }}
            timeToShow={['S']}
            timeLabels={{m: null, s: null}}
            // style={{
            //   position: 'absolute',
            //   bottom: -40,
            // }}
          />
        )}
      </S.ResendButton>
    </S.Container>
  );
};
// const mapStateToProps = ({ auth }) => ({
//   loading: auth.loading,
//   // isBonusOpen: bet.isBonusOpen,
//   // categories: bet.categories,
// });

const mapDispatchToProps = {
  codeConfirmRequest,
  mobileConfirmRequest,
};

export default connect(
  null,
  mapDispatchToProps,
)(CodeConfirmScreen);

// export default CodeConfirmScreen;
