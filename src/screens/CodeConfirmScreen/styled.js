import styled from 'styled-components';

export const Container = styled.View`
  flex: 1;
  background-color: white;
  padding: 50px 20px 20px 20px;
  align-items: center;
  flex-direction: column;
`;

export const Hint = styled.Text`
  font-size: 14px;
  color: #4eadfe;
  z-index: 123123;
`;

export const Title = styled.Text`
  color: #1b041e;
  font-size: 18px;
  margin-top: 37px;
  font-weight: 700;
  text-align: center;
`;

export const SubTitle = styled.Text`
  color: #1b041e;
  font-size: 14px;
  margin-top: 12px;
  font-weight: 400;
  text-align: center;
  /* padding: 0 50px; */
`;

export const SubTitleContainer = styled.Text`
  padding: 0 50px;
  text-align: center;
`;

export const CodeContainer = styled.View`
  flex-direction: row;
  margin-top: 30px;
`;

export const CodePlaceHolder = styled.Image`
  width: 10px;
  height: 10px;
`;

export const ResendButton = styled.TouchableOpacity`
  width: 100%;
  height: 40px;
  justify-content: center;
  align-items: center;
  flex-direction: row;
  border: 1px solid #4eadfe;
  background-color: ${({resendAvailable}) =>
    !resendAvailable ? 'white' : '#4eadfe'};
  border-radius: 10px;
  margin-top: 40px;
`;

export const ResendText = styled.Text`
  color: ${({resendAvailable}) => (resendAvailable ? 'white' : 'black')};
  font-size: 14px;
  font-weight: 700;
`;
