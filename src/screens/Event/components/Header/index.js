import React from 'react';

import Back from '../../../../assets/icons/back-01.svg';
import Heart from '../../../../assets/icons/heart-02.svg';
import HeartFill from '../../../../assets/icons/heart-02-fill.svg';

import * as S from './styled';

const titlesWithoutLikeButton = [
  'Спикеры',
  'Программа',
  'Уведомления',
  'Пресса',
  'Как добраться',
  'С кем пересекался',
  // 'Мои знакомые',
  'Приятные плюшки',
  'Мои данные',
  'Написать сообщение',
  'Мероприятия',
];

const Header = ({
  props,
  title,
  color,
  onLikePress,
  isFavorite,
  withoutRightButton = false,
}) => {
  const [isLiked, setLiked] = React.useState(false);
  // React.useEffect(() => {
  //   async function isAddedToFav() {
  //     if (isFavorite === undefined) {
  //       return;
  //     }
  //     const is = await isFavorite();
  //     setLiked(is);
  //   }
  //   isAddedToFav();
  // });
  return (
    <S.Container color={color}>
      <S.LeftButton onPress={() => props.navigation.goBack()}>
        <Back />
      </S.LeftButton>
      <S.Title>{title}</S.Title>
      {!titlesWithoutLikeButton.includes(title) && !withoutRightButton ? (
        <S.RightButton onPress={onLikePress}>
          {!isFavorite ? <Heart /> : <HeartFill />}
        </S.RightButton>
      ) : (
        <S.FakeView />
      )}
    </S.Container>
  );
};

export default Header;
