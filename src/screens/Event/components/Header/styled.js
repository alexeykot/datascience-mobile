import styled from 'styled-components';

export const Container = styled.View`
  height: 60px;
  width: 100%;
  background-color: ${({color}) => (color ? color : 'transparent')};
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  padding: 0 15px;
`;

export const Title = styled.Text`
  color: #ffffff;
  font-family: Roboto;
  font-size: 16px;
  font-weight: 500;
  line-height: 16px;
`;

export const LeftButton = styled.TouchableOpacity`
  padding: 10px 10px 10px 0;
`;

export const RightButton = styled.TouchableOpacity`
  padding: 10px 0 10px 10px;
`;

export const FakeView = styled.View`
  padding: 10px 0 10px 10px;
`;

export const Icon = styled.Image`
  background-color: ${({isFavorite}) => (isFavorite ? 'white' : 'transparent')};
`;
