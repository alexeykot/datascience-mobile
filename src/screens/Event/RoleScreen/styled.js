import styled from 'styled-components';

export const Container = styled.View`
  flex: 1;
  background-color: #5954f9;
`;

export const OptionsContainer = styled.ScrollView`
  width: 100%;
  height: 700px;
  border-top-left-radius: 30px;
  border-top-right-radius: 30px;
  background-color: white;
  margin-top: 40px;
  /* padding: 0 15px; */
`;

export const ButtonContainer = styled.View`
  justify-content: center;
  align-items: center;
  width: 100%;
  height: 58px;
  background-color: white;
`;

export const Button = styled.TouchableOpacity`
  width: 300px;
  height: 48px;
  elevation: 10;
  border-radius: 24px;
  background-color: ${({disabled}) => (disabled ? 'grey' : '#5954f9')};
  align-self: center;
  justify-content: center;
  align-items: center;
  margin-bottom: 10px;
`;

export const ButtonText = styled.Text`
  color: #ffffff;
  font-family: Roboto;
  font-size: 14px;
  font-weight: 500;
`;

export const Card = styled.TouchableOpacity`
  width: 300px;
  height: 148px;
  opacity: ${({disabled}) => (disabled ? 0.5 : 1)};
  border-radius: 10px;
  border: 1px solid #5954f9;
  elevation: ${({isChoosed}) => (isChoosed ? 0 : 10)};
  border-width: ${({isChoosed}) => (isChoosed ? 1 : 0)}px;
  background-color: #ffffff;
  align-self: center;
  margin-top: 10px;
  justify-content: center;
  align-items: center;
`;

export const CardText = styled.Text`
  color: #140c09;
  font-family: Roboto;
  font-size: 14px;
  font-weight: 400;
  margin-top: 9px;
`;

export const ReasonText = styled.Text`
  color: #140c09;
  font-family: Roboto;
  font-size: 14px;
  font-weight: 400;
  margin-top: 9px;
  text-align: center;
  opacity: 0.5;
`;
