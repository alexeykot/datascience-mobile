import React from 'react';
import {View, Image} from 'react-native';
import moment from 'moment';
import smi from '../../../assets/images/smi.png';

import * as S from './styled';
import Header from '../components/Header';

const RoleScreen = props => {
  const [role, setRole] = React.useState(0);
  // const [roles, setRoles] = React.useState([]);
  // React.useEffect(() => {
  //   async function fetchRoles() {
  //     const {data} = await http.get(
  //       `/registration/form/index?id=${props.route.params.id}`,
  //     );
  //     setRoles(data.data.entities);
  //   }
  //   fetchRoles();
  // }, [props.route.params.id]);
  const isEnabledRole = r =>
    moment(r.beginDate).isBefore(moment()) &&
    moment(r.finishDate).isAfter(moment());
  return (
    <>
      <S.Container>
        <Header title="Вы участвуете как" props={props} withoutRightButton />
        <S.OptionsContainer>
          <View
            style={{
              marginTop: 20,
              paddingHorizontal: 15,
              marginBottom: 10,
            }}>
            {props.route.params.roles.length
              ? props.route.params.roles.map(r => (
                  <S.Card
                    disabled={!isEnabledRole(r)}
                    onPress={() => setRole(r.role)}
                    key={r.role}
                    isChoosed={role === r.role}>
                    <Image source={smi} />
                    <S.CardText>{r.title}</S.CardText>
                    {r.reason ? <S.ReasonText>{r.reason}</S.ReasonText> : null}
                  </S.Card>
                ))
              : null}
          </View>
        </S.OptionsContainer>
      </S.Container>
      <S.ButtonContainer>
        <S.Button
          disabled={!role}
          onPress={() =>
            props.navigation.navigate('RegistrationScreen', {
              id: props.route.params.id,
              role: role,
              token: props.route.params.token,
            })
          }>
          <S.ButtonText>Продолжить</S.ButtonText>
        </S.Button>
      </S.ButtonContainer>
    </>
  );
};

export default RoleScreen;
