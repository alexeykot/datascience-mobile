import styled from 'styled-components';

export const Container = styled.ScrollView`
  flex: 1;
  background-color: #5954f9;
`;

export const TopContainer = styled.View`
  width: 100%;
  padding: 0 15px;
  margin-top: 80px;
`;

export const SpeakerInfoBlock = styled.View`
  width: 100%;
  height: 164px;
  border-radius: 30px;
  background-color: #ffffff;
  align-items: center;
`;

export const SpeakerImage = styled.Image`
  width: 120px;
  height: 120px;
  border-radius: 60px;
  background-color: red;
  position: absolute;
  top: -60px;
  align-self: center;
`;

export const Name = styled.Text`
  color: #1b041e;
  font-family: Roboto;
  font-size: 20px;
  font-weight: 700;
  margin-top: 76px;
  text-align: center;
`;

export const Post = styled.Text`
  color: #8b8d94;
  font-family: Roboto;
  font-size: 12px;
  font-weight: 400;
  text-align: center;
  margin-top: 13px;
`;

export const Title = styled.Text`
  color: #ffffff;
  font-family: Roboto;
  font-size: 18px;
  font-weight: 700;
  margin-top: 35px;
  margin-bottom: 10px;
  padding-left: 5px;
`;

export const BottomContainer = styled.View`
  margin-top: 40px;
  width: 100%;
  border-top-right-radius: 30px;
  border-top-left-radius: 30px;
  background-color: #ffffff;
  padding: 34px 41px 20px 38px;
`;

export const BottomTitle = styled.Text`
  color: #1b041e;
  font-family: Roboto;
  font-size: 18px;
  font-weight: 700;
`;

export const Bio = styled.Text`
  color: #1b041e;
  font-family: Roboto;
  font-size: 14px;
  font-weight: 400;
  margin-top: 14px;
`;

export const MoreButton = styled.TouchableOpacity``;

export const MoreText = styled.Text`
  font-family: Roboto;
  font-size: 14px;
  font-weight: 400;
  color: #5954f9;
`;

export const SocialContainer = styled.View`
  flex-direction: row;
  width: 106px;
  justify-content: space-between;
  align-items: center;
`;

export const SocialButton = styled.TouchableOpacity`
  width: 48px;
  height: 48px;
  background-color: #5954f9;
  border-radius: 24px;
  justify-content: center;
  align-items: center;
`;

export const Icon = styled.Image``;

export const LoadingBlock = styled.View`
  height: 124px;
  justify-content: center;
  align-items: center;
`;

export const FakeBloc = styled.View`
  width: 100%;
  height: 150px;
  background-color: white;
`;
