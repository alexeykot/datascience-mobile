/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {Dimensions, Linking} from 'react-native';
import Carousel, {Pagination} from 'react-native-snap-carousel';

import Header from '../../../components/Header';
import EventCard from '../../MainScreen/components/EventCard';

import facebook from '../../../assets/images/facebook.png';
import vk from '../../../assets/images/vk.png';

import * as S from './styled';
import {formatDescription} from '../DescriptionScreen/helpers';
import http from '../../../services/http';
import FavoritesStorage from '../../../services/storage/favorites';
import {ActivityIndicator} from 'react-native';

class SpeakerScreen extends React.Component {
  state = {
    activeSliderIndex: 0,
    viewAllText: false,
    events: [],
    isFavorite: false,
    speakerInfo: {
      connectFb: '',
      connectVk: '',
    },
    loading: true,
  };

  async componentDidMount() {
    this.props.navigation.addListener('focus', async () => {
      this.setState({loading: true});
      const {data} = await http.get(
        `/speaker/default/view?id=${this.props.route.params.speaker.id}`,
      );
      this.setState({
        events: data.data.entity.elements.filter(e => !!e.event.id),
        speakerInfo: data.data.entity,
      });
      const isFav = await this.isAddedToFavorite();
      this.setState({isFavorite: isFav, loading: false});
    });
  }

  eventPagination = () => {
    const {events, activeSliderIndex} = this.state;
    return (
      <>
        {events.length ? (
          <Pagination
            dotsLength={events.length}
            // containerStyle={{ height: 60 }}
            dotContainerStyle={{
              marginHorizontal: 4,
            }}
            activeDotIndex={activeSliderIndex}
            dotStyle={{
              width: 14,
              height: 4,
              borderRadius: 2,
              backgroundColor: 'white',
              // marginHorizontal: -20,
              // paddingHorizontal: 0,
            }}
            inactiveDotStyle={{
              width: 4,
              height: 4,
              borderRadius: 5,
              backgroundColor: 'white',
              // marginHorizontal: -5,
              // paddingHorizontal: -10,
            }}
            containerStyle={{
              marginTop: -20,
              marginBottom: -40,
            }}
            inactiveDotOpacity={1}
            inactiveDotScale={1}
          />
        ) : null}
      </>
    );
  };

  deleteFromFavorite = async () => {
    this.setState({isFavorite: false});
    const favoritesF = await FavoritesStorage.get();
    const filteredFavorites = favoritesF.filter(
      f => f.id !== this.props.route.params.speaker.id,
    );
    await FavoritesStorage.save(filteredFavorites);
  };

  isAddedToFavorite = async () => {
    const favorites = await FavoritesStorage.get();
    if (!favorites) {
      return false;
    }
    const favoriteEvents = favorites.filter(f => f.type === 'Спикеры');
    if (!favoriteEvents.length) {
      return false;
    }
    const favEvent = favoriteEvents.find(
      f => f.id === this.props.route.params.speaker.id,
    );
    console.log('favEvents', favEvent);
    return !!favEvent;
  };

  onLikePress = async () => {
    const isFav = await this.isAddedToFavorite();
    console.log('isFav', isFav);
    if (isFav) {
      this.deleteFromFavorite();
    } else {
      this.addToFavorite();
    }
  };

  addToFavorite = async () => {
    this.setState({isFavorite: true});
    const eventWithType = {
      ...this.props.route.params.speaker,
      type: 'Спикеры',
    };
    const favoritesFromStorage = await FavoritesStorage.get();
    favoritesFromStorage.push(eventWithType);
    await FavoritesStorage.save([...favoritesFromStorage]);
  };

  openLink = link => Linking.openURL(link);

  render() {
    const {speaker} = this.props.route.params;
    const {isFavorite, events, viewAllText, speakerInfo} = this.state;
    return (
      <S.Container>
        <Header
          isFavorite={isFavorite}
          onLikePress={this.onLikePress}
          props={this.props}
          title="Cпикеры"
          withoutRightButton={this.state.loading}
        />
        <S.TopContainer>
          <S.SpeakerInfoBlock>
            <S.SpeakerImage source={{uri: speaker.src.url}} />
            <S.Name>{speaker.fullName}</S.Name>
            <S.Post>{speaker.post}</S.Post>
          </S.SpeakerInfoBlock>
          <S.Title>Где и когда выступает</S.Title>
          {this.state.loading ? (
            <S.LoadingBlock>
              <ActivityIndicator size="large" color="white" />
            </S.LoadingBlock>
          ) : (
            <>
              <Carousel
                layout={'default'}
                data={events}
                renderItem={({item, index}) => (
                  <EventCard
                    isOne={events.length === 1}
                    key={item.id}
                    event={item.event}
                    onCardPress={() =>
                      this.props.navigation.navigate('EventMainScreen', {
                        event: item.event,
                      })
                    }
                  />
                )}
                inactiveSlideScale={1}
                inactiveSlideOpacity={1}
                activeSlideAlignment={events.length === 1 ? 'center' : 'start'}
                sliderWidth={Dimensions.get('window').width}
                itemWidth={events.length === 1 ? 353 : 345}
                // itemWidth={345}
                onSnapToItem={index =>
                  this.setState({activeSliderIndex: index})
                }
              />
              {this.eventPagination()}
            </>
          )}
        </S.TopContainer>
        <S.BottomContainer>
          <S.BottomTitle>Биография</S.BottomTitle>
          <S.Bio numberOfLines={viewAllText ? 100 : 7}>
            {formatDescription(speaker.info)}
          </S.Bio>
          <S.MoreButton
            onPress={() =>
              this.setState({viewAllText: !this.state.viewAllText})
            }>
            <S.MoreText>
              {!viewAllText ? 'Читать далее...' : 'Скрыть'}
            </S.MoreText>
          </S.MoreButton>
          {!speakerInfo.connectVk && !speakerInfo.connectFb ? (
            <S.FakeBloc />
          ) : (
            <>
              <S.BottomTitle style={{marginTop: 31, marginBottom: 20}}>
                Как связаться
              </S.BottomTitle>
              <S.SocialContainer>
                {speakerInfo.connectVk ? (
                  <S.SocialButton
                    onPress={() => this.openLink(speakerInfo.connectVk)}>
                    <S.Icon source={vk} />
                  </S.SocialButton>
                ) : null}
                {speakerInfo.connectFb ? (
                  <S.SocialButton
                    onPress={() => this.openLink(speakerInfo.connectFb)}>
                    <S.Icon source={facebook} />
                  </S.SocialButton>
                ) : null}
              </S.SocialContainer>
            </>
          )}
        </S.BottomContainer>
      </S.Container>
    );
  }
}
export default SpeakerScreen;
