import styled from 'styled-components';
import {Dimensions} from 'react-native';

export const Container = styled.ScrollView`
  background-color: #5954f9;
`;

export const OptionsContainer = styled.View`
  width: 100%;
  height: ${Dimensions.get('window').height - 160}px;
  border-top-left-radius: 30px;
  border-top-right-radius: 30px;
  background-color: white;
  margin-top: 140px;
  /* padding: 0 15px; */
`;

export const EventTime = styled.Text`
  color: #5754f4;
  font-family: Roboto;
  font-size: 14px;
  font-weight: 700;
  line-height: 14px;
  text-transform: uppercase;
`;

export const InfoView = styled.View`
  margin-top: 100px;
  padding: 0 15px;
  margin-bottom: 10px;
`;

export const EventTitle = styled.Text`
  color: #1b041e;
  font-family: Roboto;
  font-size: 18px;
  font-weight: 700;
  line-height: 20px;
  /* text-align: center; */
`;

export const EventDescription = styled.Text`
  color: #1b041e;
  font-family: Roboto;
  font-size: 14px;
  font-weight: 400;
  line-height: 22px;
`;
