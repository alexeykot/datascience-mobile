/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {Animated, StyleSheet} from 'react-native';
import WebView from 'react-native-webview';
import FavoritesStorage from '../../../services/storage/favorites';

import Header from '../components/Header';

import * as S from './styled';
import {formatEventTime, formatDescription} from './helpers';

class DescriptionScreen extends React.Component {
  componentWillMount() {
    this._animatedValue = new Animated.Value(0);
  }

  isAddedToFavorite = async () => {
    const favorites = await FavoritesStorage.get();
    if (!favorites) {
      return false;
    }
    const favoriteEvents = favorites.filter(f => f.type === 'События');
    if (!favoriteEvents.length) {
      return false;
    }
    const favEvent = favoriteEvents.find(
      f => f.id === this.props.route.params.event.id,
    );
    return !!favEvent;
  };

  onLikePress = async () => {
    const isFavorite = await this.isAddedToFavorite();
    if (isFavorite) {
      this.deleteFromFavorite();
    } else {
      this.addToFavorite();
    }
  };

  render() {
    const {event: eventInfo} = this.props.route.params;
    const interpolatedOpacity = this._animatedValue.interpolate({
      inputRange: [0, 150],
      outputRange: [1, 0.5],
      extrapolate: 'clamp',
    });

    const event = Animated.event([
      {
        nativeEvent: {
          contentOffset: {
            y: this._animatedValue,
          },
        },
      },
    ]);
    return (
      <>
        <S.Container stickyHeaderIndices={[0]} onScroll={event}>
          <Header
            onLikePress={this.onLikePress}
            props={this.props}
            title="Описание"
          />
          <S.OptionsContainer>
            <Animated.Image
              source={{uri: eventInfo.src.url}}
              style={[
                style.eventImage,
                {
                  width: interpolatedOpacity.interpolate({
                    inputRange: [0.5, 1],
                    outputRange: [250, 330],
                  }),
                  opacity: interpolatedOpacity.interpolate({
                    inputRange: [0.7, 1],
                    outputRange: [0, 1],
                  }),
                  transform: [
                    {
                      rotateX: interpolatedOpacity.interpolate({
                        inputRange: [0.7, 1],
                        outputRange: ['80deg', '0deg'],
                      }),
                    },
                  ],
                },
              ]}
            />
            <Animated.View
              style={[
                style.timeView,
                {
                  top: interpolatedOpacity.interpolate({
                    inputRange: [0.5, 1],
                    outputRange: [-25, 25],
                    extrapolate: 'clamp',
                  }),
                },
              ]}>
              <S.EventTime>
                {formatEventTime(eventInfo.beginDate, eventInfo.finishDate)}
              </S.EventTime>
            </Animated.View>
            <Animated.View
              style={{
                flex: 1,
                marginTop: interpolatedOpacity.interpolate({
                  inputRange: [0.5, 1],
                  outputRange: [50, 100],
                  extrapolate: 'clamp',
                }),
                paddingHorizontal: 15,
                marginBottom: 10,
              }}>
              <S.EventTitle>{eventInfo.title}</S.EventTitle>
              <WebView
                automaticallyAdjustContentInsets
                textZoom={300}
                style={{
                  flex: 1,
                  backgroundColor: 'transparent',
                }}
                source={{html: eventInfo.description}}
              />
            </Animated.View>
          </S.OptionsContainer>
        </S.Container>
      </>
    );
  }
}

const style = StyleSheet.create({
  eventImage: {
    height: 180,
    position: 'absolute',
    borderRadius: 15,
    alignSelf: 'center',
    top: -130,
    backgroundColor: 'white',
  },
  timeView: {
    position: 'absolute',
    alignSelf: 'center',
    width: 300,
    height: 40,
    elevation: 5,
    borderRadius: 20,
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default DescriptionScreen;
