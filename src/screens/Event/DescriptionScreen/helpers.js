import moment from 'moment';
import 'moment/locale/ru';

export const formatEventTime = (beginDate, finishDate) =>
  `${moment
    .utc(beginDate)
    .local()
    .locale('ru')
    .format('DD MMMM')} - ${moment
    .utc(finishDate)
    .local()
    .locale('ru')
    .format('DD MMMM')}, ${moment
    .utc(finishDate)
    .local()
    .locale('ru')
    .format('YYYY')}`;

// const literals = {
//   '&nbsp;': ' ',
//   '&laquo;': '«',
//   '&raquo;': '»',
//   '/<p>': '\n',
//   '</p>': '',
// };

export const formatDescription = string => {
  const newStr = string
    .replace(/&nbsp;/g, ' ')
    .replace(/&laquo;/g, '«')
    .replace(/&raquo;/g, '»')
    .replace(/<p>/g, '\n')
    .replace(/<pre>/g, '')
    .replace(new RegExp('</pre>', 'g'), '')
    .replace(/<strong>/g, '')
    .replace(new RegExp('</strong>', 'g'), '')
    .replace(new RegExp('</p>', 'g'), '')
    .replace(new RegExp('<em>', 'g'), '')
    .replace(new RegExp('</em>', 'g'), '');
  return newStr;
};

// export const formatDescription1 = string => {
//   let newStr;
//   Object.keys(literals).forEach(
//     key => (newStr = string.replace(new RegExp(key, 'g'), literals[key])),
//   );
//   return newStr;
// };
