/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {StyleSheet, Dimensions, Animated} from 'react-native';
import {Text, View} from 'react-native';

import SlidingUpPanel from 'rn-sliding-up-panel';

const {height} = Dimensions.get('window');
import MapView, {ProviderPropType, Marker} from 'react-native-maps';
import HTML from 'react-native-render-html';

import CustomMarker from './components/Marker';
import Header from '../../../components/Header';

// import fadeMap from '../../../assets/images/fadeMap.png';
// import fadeMap2 from '../../../assets/images/fade2.png';
// import fadeMap3 from '../../../assets/images/fade3.png';
import LinearGradient from 'react-native-linear-gradient';

import {silverTheme} from './themes';
import * as S from './styled';

const TypeEnum = {
  map: 0,
  scheme: 1,
};

class MapScreen extends React.Component {
  state = {
    type: TypeEnum.map,
    enable: false,
    showTopFade: false,
    enableScroll: false,
    allowDragging: true,
  };

  componentWillMount() {
    this._animatedValue = new Animated.Value(0);
  }
  async setCameraZoom() {
    const camera = await this.map.getCamera();
    this.map.animateCamera({
      zoom: camera.zoom + 1,
    });
  }

  async setCameraPitch() {
    const camera = await this.map.getCamera();
    this.map.animateCamera({
      zoom: camera.zoom - 1,
    });
  }

  googleMapOpenUrl = ({latitude, longitude}) => {
    const latLng = `${latitude},${longitude}`;
    return `google.navigation:q=${latLng}`;
  };

  handleScroll = e => {
    this.setState({enableScroll: e === height - 150});
  };

  handleInnerScroll = e => {
    console.log('e.nativeEvent.contentOffset.y', e.nativeEvent.contentOffset.y);
    this.setState({allowDragging: e.nativeEvent.contentOffset.y === 0});
  };

  render() {
    const {event: eventInfo} = this.props.route.params;
    const {type} = this.state;
    const interpolatedOpacity = this._animatedValue.interpolate({
      inputRange: [300, 400],
      outputRange: [0, 1],
      extrapolate: 'clamp',
    });

    const event = Animated.event([
      {
        nativeEvent: {
          contentOffset: {
            y: this._animatedValue,
          },
        },
      },
    ]);
    return (
      <>
        <S.Container>
          <Header title="Как добраться" withoutRightButton />
          {eventInfo.halls
            .filter(hall => hall.id !== 'all')
            .some(h => h.src && h.src.url) ? (
            <S.SliderContainer>
              <S.SliderButton
                chosed={type === TypeEnum.map}
                onPress={() => this.setState({type: TypeEnum.map})}>
                <S.SliderButtonText chosed={type === TypeEnum.map}>
                  Локация
                </S.SliderButtonText>
              </S.SliderButton>
              <S.SliderButton
                chosed={type === TypeEnum.scheme}
                onPress={() => this.setState({type: TypeEnum.scheme})}>
                <S.SliderButtonText chosed={type === TypeEnum.scheme}>
                  Схема залов
                </S.SliderButtonText>
              </S.SliderButton>
            </S.SliderContainer>
          ) : null}
          {this.state.type === TypeEnum.map ? (
            <S.OptionsContainer isScheme={type === TypeEnum.scheme}>
              <MapView
                ref={ref => {
                  this.map = ref;
                }}
                provider={this.props.provider}
                style={styles.map}
                initialRegion={{
                  latitude: parseFloat(
                    eventInfo.coordinates.latitude
                      ? eventInfo.coordinates.latitude
                      : 46,
                  ),
                  longitude: parseFloat(
                    eventInfo.coordinates.longitude
                      ? eventInfo.coordinates.longitude
                      : 24,
                  ),
                  latitudeDelta: 0.0043,
                  longitudeDelta: 0.0034,
                }}
                customMapStyle={silverTheme}>
                {eventInfo.coordinates.latitude &&
                eventInfo.coordinates.longitude ? (
                  <CustomMarker coordinates={eventInfo.coordinates} />
                ) : null}
              </MapView>
              <S.MapButtonContainer>
                <S.MapButton onPress={() => this.setCameraZoom()}>
                  <S.ZoomIcon>+</S.ZoomIcon>
                </S.MapButton>
                <S.MapButton isMargined onPress={() => this.setCameraPitch()}>
                  <S.ZoomIcon>-</S.ZoomIcon>
                </S.MapButton>
              </S.MapButtonContainer>
              {eventInfo.route ? (
                <SlidingUpPanel
                  allowDragging={this.state.allowDragging}
                  onDragEnd={this.handleScroll}
                  draggableRange={{top: height - 150, bottom: height * 0.4}}
                  showBackdrop={false}>
                  <LinearGradient
                    style={{
                      position: 'absolute',
                      bottom: 0,
                      width: Dimensions.get('screen').width,
                      height: '100%',
                    }}
                    start={{
                      x: 0,
                      y: 0,
                    }}
                    end={{
                      x: 0,
                      y: 0.1,
                    }}
                    colors={[
                      'rgba(255, 255, 255, 0)',
                      'rgba(255, 255, 255, 1)',
                    ]}
                    pointerEvents={'none'}
                  />
                  <View style={styles.panel}>
                    <S.DescriptionTitle>Описание</S.DescriptionTitle>
                    <S.Scrollable
                      scrollEnabled={this.state.enableScroll}
                      onScroll={this.handleInnerScroll}>
                      <HTML html={eventInfo.route} />
                    </S.Scrollable>
                  </View>
                </SlidingUpPanel>
              ) : null}
            </S.OptionsContainer>
          ) : (
            <S.OptionsContainerScroll isScheme={type === TypeEnum.scheme}>
              {eventInfo.halls
                .filter(hall => hall.id !== 'all')
                .map((hall, index) => (
                  <>
                    {hall.src && hall.src.url ? (
                      <>
                        <S.EventTitle>{hall.title}</S.EventTitle>
                        <S.SchemeContainer
                          isMargined={
                            eventInfo.halls.length > 1 &&
                            index + 2 === eventInfo.halls.length
                          }
                          source={{uri: hall.src.url}}
                        />
                      </>
                    ) : null}
                  </>
                ))}
            </S.OptionsContainerScroll>
          )}
        </S.Container>
      </>
    );
  }
}

MapScreen.propTypes = {
  provider: ProviderPropType,
};

const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  panel: {
    // flex: 1,
    backgroundColor: 'transparent',
    position: 'relative',
    paddingLeft: 37,
    // height: 700,
  },
  panelHeader: {
    height: 120,
    backgroundColor: '#b197fc',
    alignItems: 'center',
    justifyContent: 'center',
  },
  favoriteIcon: {
    position: 'absolute',
    top: -24,
    right: 24,
    backgroundColor: '#2b8a3e',
    width: 48,
    height: 48,
    padding: 8,
    borderRadius: 24,
    zIndex: 1,
  },
  map: {
    ...StyleSheet.absoluteFillObject,
    // width: 150,
    // height: 100,
    // borderRadius: 13,
    // margin: 3,
  },
});

export default MapScreen;
