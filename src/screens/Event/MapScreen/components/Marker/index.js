import React from 'react';
import {Marker} from 'react-native-maps';

import * as S from './styled';

const CustomMarker = ({coordinates}) => {
  return (
    <Marker
      coordinate={{
        latitude: parseFloat(coordinates.latitude),
        longitude: parseFloat(coordinates.longitude),
      }}
      title={'title'}
      description={'description'}>
      <S.Round />
      <S.Container />
    </Marker>
  );
};

export default CustomMarker;
