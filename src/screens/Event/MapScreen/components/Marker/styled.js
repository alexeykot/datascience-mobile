import styled from 'styled-components';

export const Container = styled.View`
  width: 48px;
  height: 48px;
  border-radius: 24px;
  background-color: #5954f9;
  opacity: 0.18;
  /* justify-content: center;
  align-items: center; */
`;

export const Round = styled.View`
  width: 14px;
  height: 14px;
  border-radius: 7px;
  background-color: #5954f9;
  position: absolute;
  left: 35%;
  top: 35%;
`;
