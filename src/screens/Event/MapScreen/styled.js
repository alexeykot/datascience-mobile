import styled from 'styled-components';
import {Dimensions} from 'react-native';

export const Container = styled.View`
  background-color: #5954f9;
  flex: 1;
`;

export const Marker = styled.View`
  width: 50px;
  height: 50px;
  border-radius: 25px;
  background-color: red;
`;

export const Scrollable = styled.ScrollView`
  width: 100%;
`;

export const OptionsContainer = styled.View`
  width: 100%;
  height: ${Dimensions.get('screen').height * 0.83}px;
  border-top-left-radius: 30px;
  border-top-right-radius: 30px;
  background-color: white;
  margin-top: 20px;
  padding: ${({isScheme}) => (isScheme ? '0 30px' : '0')};
  overflow: hidden;
  position: relative;
  /* padding: 0 15px; */
`;

export const ScrollDescription = styled.ScrollView``;

export const OptionsContainerScroll = styled.ScrollView`
  width: 100%;
  height: 700px;
  border-top-left-radius: 30px;
  border-top-right-radius: 30px;
  background-color: white;
  margin-top: 20px;
  padding: ${({isScheme}) => (isScheme ? '0 30px' : '0')};
  overflow: hidden;
  position: relative;
`;

export const EventTime = styled.Text`
  color: #5754f4;
  font-family: Roboto;
  font-size: 14px;
  font-weight: 700;
  line-height: 14px;
  text-transform: uppercase;
`;

export const InfoView = styled.View`
  margin-top: 100px;
  padding: 0 15px;
  margin-bottom: 10px;
`;

export const EventTitle = styled.Text`
  color: #1b041e;
  font-family: Roboto;
  font-size: 18px;
  font-weight: 700;
  line-height: 20px;
  text-align: center;
  margin-top: 20;
`;

export const EventDescription = styled.Text`
  color: #1b041e;
  font-family: Roboto;
  font-size: 14px;
  font-weight: 400;
  line-height: 22px;
`;

export const FadeMap = styled.Image`
  /* position: absolute;
  bottom: 0px;
  right: 0;
  left: 0; */
  width: 100%;
  /* height: 180px; */
  /* z-index: 123; */
`;

export const FadeContainer = styled.View`
  position: absolute;
  bottom: 0px;
  right: 0;
  left: 0;
  width: 100%;
  z-index: 1;
`;

export const FadeMapBg = styled.ImageBackground`
  width: 100%;
  /* height: 100%; */
  /* background-color: red; */
  z-index: 123;
`;

export const MapButtonContainer = styled.View`
  position: absolute;
  top: 50px;
  right: 10px;
`;

export const MapButton = styled.TouchableOpacity`
  width: 40px;
  height: 40px;
  border-radius: 20px;
  elevation: 5;
  background-color: #ffffff;
  justify-content: center;
  align-items: center;
  text-align: center;
  margin-top: ${({isMargined}) => (isMargined ? 5 : 0)}px;
`;

export const NavigateButton = styled.TouchableOpacity`
  width: 40px;
  height: 40px;
  border-radius: 20px;
  elevation: 5;
  background-color: #5954f9;
  justify-content: center;
  align-items: center;
  text-align: center;
  margin-top: 35px;
`;

export const DistanceText = styled.Text`
  color: #5954f9;
  font-family: Roboto;
  font-size: 10px;
  font-weight: 700;
  text-transform: uppercase;
  margin-top: 7px;
  text-align: center;
`;

export const NavigateImage = styled.Image``;

export const ZoomIcon = styled.Text`
  font-size: 30px;
  color: #c9ccd5;
`;

export const DescriptionContainer = styled.View`
  /* position: absolute;
  bottom: ${({hasButtons}) =>
    !hasButtons ? 0 : Dimensions.get('window').height * 0.212}px;
  bottom: ${Dimensions.get('window').height * 0.212}px; */
  align-self: center;
  width: 100%;
  padding-top: 200px;
  padding-left: 37px;
  /* height: 200px; */
  /* height: ${({hasButtons}) => (!hasButtons ? 400 : 200)}px; */
  /* height: 900px; */
  z-index: 2;
`;

export const DesriptionScroll = styled.ScrollView``;

export const DescriptionTitle = styled.Text`
  color: #1b041e;
  font-family: Roboto;
  font-size: 16px;
  font-weight: 700;
  margin-bottom: 16px;
`;

export const Ellipse = styled.View`
  width: 10px;
  height: 10px;
  background-color: #ffa8af;
  border-radius: 5px;
  margin-right: 15px;
`;

export const DescriptionField = styled.View`
  flex-direction: row;
  align-items: center;
  margin-bottom: 15px;
  width: 100%;
  height: 200px;
`;

export const DescriptionFieldText = styled.Text`
  color: #1b041e;
  font-family: Roboto;
  font-size: 14px;
  font-weight: 400;
`;

export const SliderContainer = styled.View`
  width: 209px;
  height: 48px;
  elevation: 5;
  border-radius: 24px;
  background-color: #ffffff;
  position: absolute;
  bottom: 10px;
  align-self: center;
  z-index: 222;
  flex-direction: row;
  padding: 9px 20px 9px 10px;
  justify-content: space-between;
  align-items: center;
`;

export const SliderButton = styled.TouchableOpacity`
  /* width: 78px;
  height: 30px; */
  padding: 5px 10px;
  border-radius: 15px;
  background-color: ${({chosed}) => (chosed ? '#5954f9' : 'white')};
  justify-content: center;
  align-items: center;
`;

export const SliderButtonText = styled.Text`
  color: ${({chosed}) => (chosed ? 'white' : 'black')};
  font-family: Roboto;
  font-size: 14px;
  font-weight: 700;
`;

export const SchemeContainer = styled.Image`
  width: 100%;
  height: 286px;
  border-radius: 30px;
  background-color: white;
  margin-top: 17px;
  margin-bottom: ${({isMargined}) => (isMargined ? 120 : 20)}px;
`;
