import moment from 'moment';
import _ from 'lodash';

export const formSpeakersString = event => {
  let string = '';
  for (let i = 0; i < event.speakers.length; i++) {
    string += `${event.speakers[i].fullName} (${event.speakers[i].post})${
      i === event.speakers.length - 1 ? '' : ', '
    }`;
  }
  return string;
};

export const formEventDates = event => {
  const dates = event.elements.map(element => element.date);
  const sortedArray = _.orderBy(
    dates,
    o => {
      return moment(o).format('YYYYMMDD');
    },
    ['asc'],
  );
  return sortedArray;
};

export const detectIndicatorPercents = event => {
  // moment(event.timeEnd, 'HH:mm:ss').diff(moment(), 'hours');
  const period = moment(event.timeEnd, 'HH:mm:ss').diff(
    moment(event.time, 'HH:mm:ss'),
    'minutes',
  );
  const timeFromNow = moment(event.timeEnd, 'HH:mm:ss').diff(
    moment(),
    'minutes',
  );
  const percents = ((period - timeFromNow) / period) * 100;
  if (percents > 100) {
    return 0;
  }
  return percents;
};

export const isGoingNow = (timeStart, timeEnd, date) => {
  const fullDateStart = date + ' ' + timeStart;
  const fullDateEnd = date + ' ' + timeEnd;
  console.log('fullDateStart', fullDateStart);
  return moment().isBetween(
    moment(fullDateStart, 'YYYY-MM-DD HH:mm:ss'),
    moment(fullDateEnd, 'YYYY-MM-DD HH:mm:ss'),
  );
};

export const isAfter = (timeStart, timeEnd, date) => {
  const fullDateStart = date + ' ' + timeStart;
  const fullDateEnd = date + ' ' + timeEnd;
  const isLate = moment().isAfter(moment(fullDateEnd, 'YYYY-MM-DD HH:mm:ss'));
  return isLate;
};

export const isBefore = (timeStart, timeEnd, date) => {
  const fullDateStart = date + ' ' + timeStart;
  const notStartedYet = moment().isBefore(
    moment(fullDateStart, 'YYYY-MM-DD HH:mm:ss'),
  );
  return notStartedYet;
};
