import styled from 'styled-components';

export const Container = styled.TouchableOpacity`
  width: 330px;
  min-height: 180px;
  border-radius: 15px;
  /* elevation: ${({isEnded}) => (isEnded ? 0 : 5)}; */
  background-color: #ffffff;
  padding: 23px 23px 19px 23px;
  border: ${({isEnded}) =>
    isEnded ? '2px solid #f7f5fa' : '0px solid #f7f5fa'};
  margin-bottom: ${({isLast}) => (isLast ? 200 : 10)}px;
  align-self: center;
  /* opacity: ${({isEnded}) => (isEnded ? 0.5 : 1)}; */
`;

export const InfoContainer = styled.View`
  flex-direction: row;
  align-items: center;
`;

export const Time = styled.Text`
  color: #5954f9;
  font-family: 'Roboto';
  font-size: 14px;
  font-weight: 400;
  margin-right: 8px;
`;

export const PlaceContainer = styled.View`
  padding: 7px 10px 8px 10px;
  border-radius: 12px;
  background-color: rgba(228, 229, 234, 0.38);
  /* opacity: 0.38; */
`;

export const Place = styled.Text`
  color: #8b8d94;
  font-family: 'Roboto';
  font-size: 12px;
  font-weight: 400;
`;

export const Title = styled.Text`
  color: #1b041e;
  font-family: 'Roboto';
  font-size: 16px;
  font-weight: 700;
  margin-top: 12px;
`;

export const Speakers = styled.Text`
  color: #8b8d94;
  font-family: 'Roboto';
  font-size: 12px;
  font-weight: 400;
  margin-top: 9px;
`;

export const CurrentContainer = styled.View`
  flex-direction: row;
  margin-top: 8px;
  align-items: center;
`;

export const Current = styled.Text`
  color: #1b041e;
  font-family: 'Roboto';
  font-size: 10px;
  font-weight: 700;
  line-height: 20px;
  text-transform: uppercase;
`;

export const CurrentBar = styled.View`
  width: 192px;
  height: 7px;
  border-radius: 4px;
  background-color: #f5f5f7;
  margin-left: 13px;
`;

export const Fill = styled.View`
  /* width: 42px; */
  position: absolute;
  left: 0;
  width: ${({percents}) => percents}%;
  height: 7px;
  border-radius: 4px;
  background-color: #f99635;
`;
