import React from 'react';
import moment from 'moment';

import {
  formSpeakersString,
  detectIndicatorPercents,
  isGoingNow,
  isAfter,
  isBefore,
} from '../../helpers';

import * as S from './styled';

const PerfomanceCard = ({
  event,
  isLast,
  onPress,
  isGoing,
  isLate,
  notStartedYet,
}) => {
  return (
    <S.Container
      style={{
        opacity: isLate ? 0.5 : 1,
      }}
      activeOpacity={1}
      isEnded={isLate}
      onPress={onPress}
      isLast={isLast}>
      <S.InfoContainer>
        <S.Time>
          {moment(event.time, 'HH:mm:ss').format('HH:mm')}-
          {moment(event.timeEnd, 'HH:mm:ss').format('HH:mm')}
        </S.Time>
        <S.PlaceContainer>
          <S.Place>{event.hall.title}</S.Place>
        </S.PlaceContainer>
      </S.InfoContainer>
      <S.Title>{event.title}</S.Title>
      {formSpeakersString(event) ? (
        <S.Speakers>Спикеры: {formSpeakersString(event)}</S.Speakers>
      ) : null}
      <S.CurrentContainer>
        {isGoing ? (
          <>
            <S.Current>прямо сейчас</S.Current>
            <S.CurrentBar>
              <S.Fill percents={detectIndicatorPercents(event)} />
            </S.CurrentBar>
          </>
        ) : null}
        {isLate ? (
          <>
            <S.Current>закончено</S.Current>
            <S.CurrentBar />
          </>
        ) : null}
        {notStartedYet ? (
          <>
            <S.Current>не началось</S.Current>
            <S.CurrentBar />
          </>
        ) : null}
      </S.CurrentContainer>
    </S.Container>
  );
};

export default PerfomanceCard;
