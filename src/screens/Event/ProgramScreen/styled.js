import styled from 'styled-components';

export const Container = styled.View`
  background-color: #5954f9;
`;

export const OptionsContainer = styled.View`
  width: 100%;
  /* height: 700px; */
  border-top-left-radius: 30px;
  border-top-right-radius: 30px;
  background-color: white;
  margin-top: 20px;
  padding-top: 10px;
`;

export const DateContainer = styled.View`
  position: absolute;
  align-self: center;
  width: 300px;
  top: 55;
  height: 40;
  elevation: 5;
  border-radius: 20;
  background-color: white;
  margin: 0 20px;
`;

export const OptionsScroll = styled.ScrollView`
  width: 100%;
  height: 100%;
`;

export const StyledView = styled.View`
  position: absolute;
  align-self: center;
  width: 300px;
  top: -25px;
  height: 40px;
  elevation: 5;
  border-radius: 20;
  background-color: white;
  /* justify-content: center;
  align-items: center; */
`;

export const SlideLeftButton = styled.TouchableOpacity`
  position: absolute;
  left: 0;
  top: 10;
  align-self: center;
  width: 15px;
  height: 15px;
  background-color: red;
`;

export const EventTime = styled.Text`
  color: #5754f4;
  font-family: Roboto;
  font-size: 14px;
  font-weight: 700;
  line-height: 14px;
  text-transform: uppercase;
  align-self: center;
  margin: auto;
`;

export const InfoView = styled.View`
  margin-top: 100px;
  padding: 0 15px;
  margin-bottom: 10px;
`;

export const EventTitle = styled.Text`
  color: #1b041e;
  font-family: Roboto;
  font-size: 18px;
  font-weight: 700;
  line-height: 20px;
  text-align: center;
`;

export const EventDescription = styled.Text`
  color: #1b041e;
  font-family: Roboto;
  font-size: 14px;
  font-weight: 400;
  line-height: 22px;
`;

export const HallSliderContainer = styled.View`
  width: 340px;
  padding-left: 5px;
  padding-right: 5px;
  height: 48px;
  elevation: 5;
  border-radius: 24px;
  background-color: #ffffff;
  position: absolute;
  bottom: 10px;
  align-self: center;
`;
export const HallSlider = styled.ScrollView`
  /* width: 340px;
  height: 48px;
  elevation: 5;
  border-radius: 24px;
  background-color: #ffffff;
  position: absolute;
  bottom: 10px;
  align-self: center; */
  width: 100%;
`;

export const HallButton = styled.TouchableOpacity`
  height: 26px;
  padding: 0px 10px;
  justify-content: center;
  align-items: center;
  border-radius: 13px;
  background-color: ${({isActive}) => (isActive ? '#5954f9' : '#ffffff')};
`;

export const HallButtonText = styled.Text`
  color: ${({isActive}) => (isActive ? '#ffffff' : '#1b041e')};
  font-family: 'Roboto';
  font-size: 14px;
  font-weight: ${({isActive}) => (isActive ? '700' : '400')};
`;

export const ArrowLeft = styled.TouchableOpacity`
  width: 20px;
  height: 20px;
  background-color: transparent;
  position: absolute;
  left: 5px;
  top: 10px;
  justify-content: center;
  align-items: center;
`;

export const ArrowRight = styled.TouchableOpacity`
  width: 20px;
  height: 20px;
  background-color: transparent;
  justify-content: center;
  align-items: center;
  position: absolute;
  right: 5px;
  top: 10px;
`;

export const Fade = styled.Image`
  width: 335px;
  height: 128px;
  position: absolute;
  top: 5;
  align-self: center;
  z-index: 123;
`;

export const FadeHallRight = styled.Image`
  width: 93px;
  height: 48px;
  position: absolute;
  right: 0;
  top: 0;
  bottom: 0;
  z-index: 123;
`;

export const FadeHallLeft = styled.Image`
  width: 98px;
  height: 48px;
  position: absolute;
  left: 0;
  top: 0;
  bottom: 0;
  z-index: 123;
`;
