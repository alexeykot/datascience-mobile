/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import Carousel from 'react-native-snap-carousel';
import {View} from 'react-native';
import _ from 'lodash';

import fade from '../../../assets/images/fadeProgram.png';
import fadeHallLeft from '../../../assets/images/fadeHallLeft.png';
import fadeHallRight from '../../../assets/images/fadeHallRight.png';

import Back from '../../../assets/icons/back.svg';
import Next from '../../../assets/icons/next.svg';
import Header from '../../../components/Header';

import * as S from './styled';
import PerfomanceCard from './components/PerfomanceCard';
import {formEventDates, isAfter, isBefore, isGoingNow} from './helpers';
import moment from 'moment';

const mock = {
  createdAt: '2020-04-29 18:13:27',
  date: '2020-06-26',
  description:
    '<p>На конференции выступят руководители Банка России, федеральных министерств и ведомств,&nbsp;члены Совета Федерации и депутаты Государственной Думы РФ.</p>↵<p>&nbsp;</p>',
  hall: {
    createdAt: '2020-06-05 12:17:58',
    id: 44,
    src: {
      url:
        'https://ai-id.dev-vps.ru/render/storage/29/26/a92f0f2a2697a8008885a4dae6f851510fc25f88.png',
    },
    title: 'Зал 1',
    updatedAt: '2020-06-18 08:55:09',
  },
  id: 7,
  speakers: [],
  time: '11:00:00',
  timeEnd: '16:00:00',
  title: 'Tetekj kjete',
  updatedAt: '2020-06-25 08:23:53',
};
const styles = {
  alignSelf: 'center',
  width: 250,
  height: 40,
  position: 'relative',
  backgroundColor: 'white',
};

class ProgramScreen extends React.Component {
  state = {
    activeDateIndex: 0,
    activeHallIndex: 0,
    activeHallId: 'all',
    eventInfo: null,
    dates: [],
  };

  componentWillMount() {
    const unsubscribe = this.props.navigation.addListener('focus', () => {
      const {event: eventInfo} = this.props.route.params;
      const date = _.uniq(formEventDates(eventInfo));
      // const newDate = '2020-06-26';
      // const newEvent = {
      //   ...eventInfo,
      //   elements: [...eventInfo.elements, mock],
      // };
      this.setState({eventInfo: eventInfo, dates: date});
    });
  }

  onHallPress = (hall, index) => {
    // const filteredHalls = this.state.eventInfo.elements.filter(
    //   element => element.hall.id === hall.id,
    // );
    // const newEventInfo = {
    //   ...this.state.eventInfo,
    //   elements: filteredHalls,
    // };
    // this.setState({eventInfo: newEventInfo});
    this.setState({activeHallIndex: index, activeHallId: hall.id});
  };

  filterHallsByDate = hallId => {
    let arr = [];
    if (!this.state.eventInfo) {
      return arr.length;
    }
    arr = this.state.eventInfo.elements
      .filter(element => element.hall.id === hallId)
      .filter(
        filteredByDate =>
          filteredByDate.date === this.state.dates[this.state.activeDateIndex],
      );
    return arr.length;
  };

  onCardPress = event => {
    const newEvent = {
      ...this.state.eventInfo,
      event,
    };
    this.props.navigation.navigate('EventPerfomanceScreen', {
      event: newEvent,
    });
  };

  render() {
    const {event: eventInfo} = this.props.route.params;
    const {activeHallIndex, activeDateIndex} = this.state;
    const hallsArr = _.uniqBy(eventInfo.halls, 'id');
    console.log('hallsArr', eventInfo);
    return (
      <>
        <S.Container>
          <Header title="Программа" withoutRightButton />
          <S.DateContainer>
            {this.state.dates.length > 1 ? (
              <>
                <S.ArrowLeft onPress={() => this._carousel.snapToPrev()}>
                  <Back
                    width={14}
                    height={14}
                    opacity={activeDateIndex ? 1 : 0.5}
                    fill={activeDateIndex ? '#5954f9' : '#c9ccd5'}
                  />
                </S.ArrowLeft>
                <Carousel
                  ref={c => {
                    this._carousel = c;
                  }}
                  layout={'default'}
                  data={this.state.dates}
                  containerCustomStyle={styles}
                  renderItem={({item, index}) => (
                    <S.EventTime>{moment(item).format('DD MMMM')}</S.EventTime>
                  )}
                  inactiveSlideScale={1}
                  inactiveSlideOpacity={1}
                  activeSlideAlignment="center"
                  sliderWidth={250}
                  itemWidth={250}
                  onSnapToItem={index =>
                    this.setState({activeDateIndex: index})
                  }
                  // loop
                />
                <S.ArrowRight onPress={() => this._carousel.snapToNext()}>
                  <Next
                    width={14}
                    height={14}
                    opacity={
                      activeDateIndex + 1 !== this.state.dates.length ? 1 : 0.5
                    }
                    fill={
                      activeDateIndex + 1 !== this.state.dates.length
                        ? '#5954f9'
                        : '#c9ccd5'
                    }
                  />
                </S.ArrowRight>
              </>
            ) : (
              <S.EventTime>
                {moment(this.state.dates[0]).format('DD MMMM')}
              </S.EventTime>
            )}
          </S.DateContainer>
          <S.OptionsContainer>
            <S.Fade source={fade} />
            <S.OptionsScroll>
              <View
                style={{
                  marginTop: 50,
                  // paddingHorizontal: 15,
                  paddingBottom: 200,
                  marginBottom: 10,
                }}>
                {this.state.eventInfo
                  ? this.state.eventInfo.elements
                      .filter(element =>
                        this.state.activeHallId !== 'all'
                          ? element.hall.id === this.state.activeHallId
                          : element.hall.title,
                      )
                      .filter(
                        filteredByDate =>
                          filteredByDate.date ===
                          this.state.dates[this.state.activeDateIndex],
                      )
                      .map((event, index) => (
                        <PerfomanceCard
                          key={`${event.time} + ${event.title} + ${
                            event.timeEnd
                          }`}
                          isGoing={isGoingNow(
                            event.time,
                            event.timeEnd,
                            event.date,
                          )}
                          isLate={isAfter(
                            event.time,
                            event.timeEnd,
                            event.date,
                          )}
                          notStartedYet={isBefore(
                            event.time,
                            event.timeEnd,
                            event.date,
                          )}
                          isLast={
                            index === this.state.eventInfo.elements.length - 1
                          }
                          event={event}
                          onPress={() => this.onCardPress(event)}
                        />
                      ))
                  : null}
              </View>
            </S.OptionsScroll>
          </S.OptionsContainer>
        </S.Container>
        <S.HallSliderContainer>
          {/* <S.FadeHallLeft source={fadeHallLeft} />
          <S.FadeHallRight source={fadeHallRight} /> */}
          <S.HallSlider
            horizontal
            showsHorizontalScrollIndicator={false}
            contentContainerStyle={{
              alignItems: 'center',
              paddingHorizontal: 25,
            }}>
            {hallsArr.map((hall, index) => (
              <>
                {hall.id === 'all' || this.filterHallsByDate(hall.id) ? (
                  <S.HallButton
                    isHide={this.filterHallsByDate(hall.id)}
                    onPress={() => this.onHallPress(hall, index)}
                    isActive={index === activeHallIndex}>
                    <S.HallButtonText isActive={index === activeHallIndex}>
                      {hall.title}
                    </S.HallButtonText>
                  </S.HallButton>
                ) : null}
              </>
            ))}
          </S.HallSlider>
        </S.HallSliderContainer>
      </>
    );
  }
}

export default ProgramScreen;
