import styled from 'styled-components';

export const Container = styled.View`
  background-color: #5954f9;
`;

export const OptionsContainer = styled.View`
  width: 100%;
  /* height: 20px; */
  border-top-left-radius: 30px;
  border-top-right-radius: 30px;
  background-color: white;
  margin-top: 20px;
  /* padding: 0 37px; */
  /* align-items: center; */
`;

export const OptionsScroll = styled.ScrollView`
  width: 100%;
  /* height: 100%; */
`;

export const EventTime = styled.Text`
  color: #5754f4;
  font-family: Roboto;
  font-size: 14px;
  font-weight: 700;
  line-height: 14px;
  text-transform: uppercase;
`;

export const InfoView = styled.View`
  margin-top: 100px;
  padding: 0 15px;
  margin-bottom: 10px;
`;

export const EventTitle = styled.Text`
  color: #1b041e;
  font-family: Roboto;
  font-size: 18px;
  font-weight: 700;
  line-height: 20px;
  margin-top: ${({marginTop}) => marginTop}px;
  margin-bottom: ${({marginBottom}) => (marginBottom ? marginBottom : 0)}px;
`;

export const EventDescription = styled.Text`
  color: #1b041e;
  font-family: Roboto;
  font-size: 14px;
  font-weight: 400;
  line-height: 22px;
  margin-top: 14px;
`;

export const EventAddress = styled.Text`
  color: #8b8d94;
  font-family: Roboto;
  font-size: 14px;
  font-weight: 400;
  line-height: 18px;
  text-align: center;
`;

export const CurrentContainer = styled.View`
  flex-direction: row;
  width: 100%;
  justify-content: space-between;
`;

export const CurrentTime = styled.Text`
  color: #1b041e;
  font-family: Roboto;
  font-size: 10px;
  font-weight: 700;
  line-height: 20px;
  text-transform: uppercase;
`;

export const RemainingTime = styled.Text`
  color: #8b8d94;
  font-family: Roboto;
  font-size: 10px;
  font-weight: 400;
  line-height: 20px;
  text-transform: uppercase;
`;
export const CurrentBar = styled.View`
  width: 100%;
  height: 7px;
  border-radius: 4px;
  background-color: #f5f5f7;
  /* margin-left: 13px; */
`;

export const Fill = styled.View`
  /* width: 42px; */
  position: absolute;
  left: 0;
  width: ${({percents}) => percents}%;
  height: 7px;
  border-radius: 4px;
  background-color: #f99635;
`;

export const HallButton = styled.View`
  padding: 0 20px;
  height: 24px;
  border-radius: 12px;
  background-color: #5954f9;
  margin-top: 19px;
  justify-content: center;
  align-items: center;
`;

export const HallButtonText = styled.Text`
  color: #ffffff;
  font-family: Roboto;
  font-size: 12px;
  font-weight: 400;
  line-height: 20px;
`;

export const MapContainer = styled.View`
  width: 100%;
  height: 286px;
  border-radius: 30px;
  background-color: white;
  margin-top: 17px;
  overflow: hidden;
  position: relative;
  margin-bottom: ${({isLastBlock}) => (isLastBlock ? 200 : 0)}px;
`;

export const SchemeContainer = styled.Image`
  width: 100%;
  height: 286px;
  border-radius: 30px;
  background-color: white;
  margin-top: 17px;
  margin-bottom: 200px;
`;

export const MapButtonContainer = styled.View`
  position: absolute;
  top: 100px;
  right: 10px;
`;

export const MapButton = styled.TouchableOpacity`
  width: 40px;
  height: 40px;
  border-radius: 20px;
  elevation: 5;
  background-color: #ffffff;
  justify-content: center;
  align-items: center;
  text-align: center;
  margin-top: ${({isMargined}) => (isMargined ? 5 : 0)}px;
`;

export const NavigateButton = styled.TouchableOpacity`
  width: 40px;
  height: 40px;
  border-radius: 20px;
  elevation: 5;
  background-color: #5954f9;
  justify-content: center;
  align-items: center;
  text-align: center;
  margin-top: 35px;
`;

export const DistanceText = styled.Text`
  color: #5954f9;
  font-family: Roboto;
  font-size: 10px;
  font-weight: 700;
  text-transform: uppercase;
  margin-top: 7px;
  text-align: center;
`;

export const NavigateImage = styled.Image``;

export const ZoomIcon = styled.Text`
  font-size: 30px;
  color: #c9ccd5;
`;
