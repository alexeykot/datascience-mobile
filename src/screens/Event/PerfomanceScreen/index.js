/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {Animated, StyleSheet, Linking} from 'react-native';
import MapView from 'react-native-maps';

// import Header from '../components/Header';
import Header from '../../../components/Header';
import Speaker from '../SpeakersScreen/components/Speaker';

import corner from '../../../assets/images/corner.png';
import * as S from './styled';
import {silverTheme} from '../MapScreen/themes';
import CustomMarker from '../MapScreen/components/Marker';
import moment from 'moment';
import {
  detectIndicatorPercents,
  isGoingNow,
  isAfter,
  isBefore,
} from '../ProgramScreen/helpers';
import {formatDescription} from '../DescriptionScreen/helpers';

class PerfomanceScreen extends React.Component {
  async setCameraZoom() {
    const camera = await this.map.getCamera();
    this.map.animateCamera({
      zoom: camera.zoom + 1,
    });
  }

  async setCameraPitch() {
    const camera = await this.map.getCamera();
    this.map.animateCamera({
      zoom: camera.zoom - 1,
    });
  }

  googleMapOpenUrl = ({latitude, longitude}) => {
    const latLng = `${latitude},${longitude}`;
    return `google.navigation:q=${latLng}`;
  };

  navigate = () => {
    const {event: eventInfo} = this.props.route.params;
    Linking.openURL(
      this.googleMapOpenUrl({
        latitude: parseFloat(eventInfo.coordinates.latitude),
        longitude: parseFloat(eventInfo.coordinates.longitude),
      }),
    );
  };

  getRemainingTime = () => {
    const {event: eventInfo} = this.props.route.params;
    const period = moment(eventInfo.event.timeEnd, 'HH:mm:ss').diff(
      moment(eventInfo.event.time, 'HH:mm:ss'),
      'minutes',
    );
    const timeFromNow = moment(eventInfo.event.time, 'HH:mm:ss').diff(
      moment(),
      'minutes',
    );
    const diff = period + timeFromNow;
    const remainingHours = parseInt(diff / 60);
    const remainingMinutes = diff - remainingHours * 60;
    return `${remainingHours}:${
      remainingMinutes < 10 ? `0${remainingMinutes}` : remainingMinutes
    }`;
  };
  render() {
    const {event: eventInfo} = this.props.route.params;
    const isNow = isGoingNow(
      eventInfo.event.time,
      eventInfo.event.timeEnd,
      eventInfo.event.date,
    );
    const isEnded = isAfter(
      eventInfo.event.time,
      eventInfo.event.timeEnd,
      eventInfo.event.date,
    );
    const notStarted = isBefore(
      eventInfo.event.time,
      eventInfo.event.timeEnd,
      eventInfo.event.date,
    );
    return (
      <S.Container
        stickyHeaderIndices={[0]}
        ref={myScroll => (this._myScroll = myScroll)}>
        <Header title="Программа" withoutRightButton />
        <Animated.View
          style={[
            style.timeView,
            {
              top: 55,
            },
          ]}>
          <S.EventTime>
            {moment(eventInfo.event.date).format('DD MMMM')},{' '}
            {moment(eventInfo.event.time, 'HH:mm:ss').format('HH:mm')}-
            {moment(eventInfo.event.timeEnd, 'HH:mm:ss').format('HH:mm')}
          </S.EventTime>
        </Animated.View>
        <S.OptionsContainer>
          <S.OptionsScroll>
            <Animated.View
              style={{
                marginTop: 50,
                paddingHorizontal: 37,
                marginBottom: 10,
              }}>
              <S.CurrentBar>
                {isNow ? (
                  <S.Fill percents={detectIndicatorPercents(eventInfo.event)} />
                ) : null}
              </S.CurrentBar>
              <S.CurrentContainer>
                {isNow ? <S.CurrentTime>Прямо сейчас</S.CurrentTime> : null}
                {isEnded ? <S.CurrentTime>Закончилось</S.CurrentTime> : null}
                {notStarted ? <S.CurrentTime>Не началось</S.CurrentTime> : null}
                {isNow ? (
                  <S.RemainingTime>{this.getRemainingTime()}</S.RemainingTime>
                ) : null}
              </S.CurrentContainer>
              <S.HallButton>
                <S.HallButtonText>
                  {eventInfo.event.hall.title}
                </S.HallButtonText>
              </S.HallButton>
              <S.EventTitle marginTop={16}>
                {eventInfo.event.title}
              </S.EventTitle>
              <S.EventDescription>
                {formatDescription(eventInfo.event.description)}
              </S.EventDescription>
              {eventInfo.event.speakers.length ? (
                <S.EventTitle marginTop={30}>Спикеры</S.EventTitle>
              ) : null}
              {eventInfo.event.speakers.map(speaker => (
                <Speaker navigation={this.props.navigation} speaker={speaker} />
              ))}
              {eventInfo.coordinates.latitude &&
              eventInfo.coordinates.longitude ? (
                <>
                  <S.EventTitle marginTop={25}>Локация</S.EventTitle>
                  <S.MapContainer isLastBlock={!eventInfo.event.hall.src.url}>
                    <MapView
                      ref={ref => {
                        this.map = ref;
                      }}
                      provider={this.props.provider}
                      style={styles.map}
                      initialRegion={{
                        // latitude: LATITUDE,
                        // longitude: LONGITUDE,
                        latitude: parseFloat(eventInfo.coordinates.latitude),
                        longitude: parseFloat(eventInfo.coordinates.longitude),
                        latitudeDelta: 0.00043,
                        longitudeDelta: 0.00034,
                      }}
                      customMapStyle={silverTheme}>
                      <CustomMarker coordinates={eventInfo.coordinates} />
                    </MapView>
                    <S.MapButtonContainer>
                      <S.MapButton onPress={() => this.setCameraZoom()}>
                        <S.ZoomIcon>+</S.ZoomIcon>
                      </S.MapButton>
                      <S.MapButton
                        isMargined
                        onPress={() => this.setCameraPitch()}>
                        <S.ZoomIcon>-</S.ZoomIcon>
                      </S.MapButton>
                      <S.NavigateButton onPress={() => this.navigate()}>
                        <S.NavigateImage source={corner} />
                      </S.NavigateButton>
                      <S.DistanceText>985 м</S.DistanceText>
                    </S.MapButtonContainer>
                  </S.MapContainer>
                </>
              ) : null}
              {eventInfo.event.hall.src.url ? (
                <>
                  <S.EventTitle marginTop={30}>Схема</S.EventTitle>
                  <S.SchemeContainer
                    source={{uri: eventInfo.event.hall.src.url}}
                  />
                </>
              ) : null}
            </Animated.View>
          </S.OptionsScroll>
        </S.OptionsContainer>
      </S.Container>
    );
  }
}

const style = StyleSheet.create({
  eventImage: {
    height: 180,
    position: 'absolute',
    borderRadius: 15,
    alignSelf: 'center',
    top: -130,
    backgroundColor: 'red',
  },
  timeView: {
    position: 'absolute',
    alignSelf: 'center',
    width: 300,
    height: 40,
    elevation: 5,
    borderRadius: 20,
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
  },
});

const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  map: {
    ...StyleSheet.absoluteFillObject,
    // width: 150,
    // height: 100,
    // borderRadius: 13,
    // margin: 3,
  },
});

export default PerfomanceScreen;
