import styled from 'styled-components';

export const Container = styled.TouchableOpacity`
  width: 160px;
  height: 98px;
  elevation: 5;
  border-radius: 10px;
  background-color: #ffffff;
  margin: 10px 10px 0 0;
  justify-content: center;
  align-items: center;
`;

export const Name = styled.Text`
  color: #140c09;
  font-family: Roboto;
  font-size: 14px;
  font-weight: 400;
  margin-top: 10px;
`;

export const Icon = styled.Image``;
