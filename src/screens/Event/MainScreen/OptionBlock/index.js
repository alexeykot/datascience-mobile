import React from 'react';

import * as S from './styled';

const OptionBlock = ({option, onOptionPress}) => {
  return (
    <S.Container onPress={onOptionPress}>
      {/* <S.Icon source={option.source} /> */}
      {option.source}
      <S.Name>{option.name}</S.Name>
    </S.Container>
  );
};

export default OptionBlock;
