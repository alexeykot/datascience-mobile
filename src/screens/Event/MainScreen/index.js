/* eslint-disable react/no-did-mount-set-state */
/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {Animated, FlatList, StyleSheet, View, Image} from 'react-native';
import {connect} from 'react-redux';

import Header from '../components/Header';
import OptionBlock from './OptionBlock';

import * as S from './styled';
import {
  formatEventTime,
  formatEvent,
  eventWithNews,
  isActiveRegistration,
  getRegistrationText,
} from './helpers';

import ticket from '../../../assets/images/ticket-line.png';

import SpeakerIcon from '../../../assets/icons/speak-01.svg';
import DescriptionIcon from '../../../assets/icons/description-01.svg';
import PointIcon from '../../../assets/icons/point_01.svg';
import NewsIcon from '../../../assets/icons/news_01.svg';

import http from '../../../services/http';
import FavoritesStorage from '../../../services/storage/favorites';
import {ActivityIndicator} from 'react-native';

const statusColors = {
  0: '#ffa500',
  1: '#3dd576',
  2: '#ffa500',
};

const buttonStrings = {
  0: 'Заявка отправлена',
  1: 'Ваш билет',
};

const statusString = {
  0: 'жду подтверждения',
  1: 'я зарегистрирован',
};

class MainScreen extends React.Component {
  state = {
    event: null,
    loading: true,
    options: [
      {
        id: 1,
        name: 'Описание',
        source: <DescriptionIcon fill="#5954f9" />,
        screen: 'EventDescriptionScreen',
        field: 'description',
      },
      {
        id: 2,
        name: 'Спикеры',
        source: <SpeakerIcon fill="#5954f9" />,
        screen: 'EventSpeakersScreen',
        field: 'speakers',
      },
      {
        id: 3,
        name: 'Как добраться',
        source: <PointIcon fill="#5954f9" />,
        screen: 'EventMapScreen',
        field: 'speakers',
      },
      {
        id: 5,
        name: 'Программа',
        source: <NewsIcon fill="#5954f9" />,
        screen: 'EventProgramScreen',
        field: 'halls',
      },
      {
        id: 6,
        name: 'Пресса',
        source: <NewsIcon fill="#5954f9" />,
        screen: 'NewsScreen',
        field: 'news',
      },
    ],
    enableScrollViewScroll: true,
    isEnableRegistration: false,
    isFavorite: false,
    errorMessage: '',
  };

  // async componentDidMount() {
  //   await this.getInfo();
  // }

  // componentDidUpdate(prevProps) {
  //   if (this.props.eventInfo.id !== prevProps.eventInfo.id) {
  //     this.getInfo();
  //   }
  // }
  handleErrorStatus = errorMessage => {
    const {event, status} = this.props.route.params;
    if (status !== undefined) {
      if (this.isButtonDisabled()) {
        return 'Заявка отправлена';
      }
      return 'Ваш билет';
    }
    switch (errorMessage) {
      case 'User is already registered':
        return 'Пользователь уже зарегистрирован';
      case 'Registration for event is finished':
        return 'Регистрация завершена';
      default:
        return 'Регистрация еще не началась';
    }
  };

  getInfo = async () => {
    this.setState({loading: true});
    const {event} = this.props.route.params;
    const isFavorite = await this.isAddedToFavorite();
    this.setState({isFavorite});
    const {data} = await http.get(`/event/event/view?id=${event.id}`);
    const newsData = await http.get('/news/default/index');
    this.setState({
      event: eventWithNews(
        formatEvent(data.data.entity),
        newsData.data.data.entities,
      ),
    });
    try {
      const {data: rolesData} = await http.get(
        `/registration/form/index?id=${event.id}`,
      );
      console.log('rolesData', rolesData);
    } catch (error) {
      console.log('rolesFestchError: ', error.response);
    }
    try {
      await http.get(`/registration/form/view?id=${event.id}`);
      this.setState({isEnableRegistration: true, errorMessage: ''});
    } catch (err) {
      console.log('errerer', err.response);
      this.setState({isEnableRegistration: false, errorMessage: ''});
      if (err.response.data.code === 422) {
        this.setState({errorMessage: err.response.data.data.message});
      }
    }
    this.setState({loading: false});
  };

  componentWillMount() {
    this._animatedValue = new Animated.Value(0);
    this.props.navigation.addListener('focus', async () => {
      await this.getInfo();
    });
    this.props.navigation.addListener('blur', () => {
      this.setState({loading: true});
      if (this._myScroll) {
        this._myScroll.scrollTo({
          y: 0,
          animated: true,
        });
      }
    });
  }

  onOptionBlockPress = item => {
    if (item.name === 'Пресса') {
      this.props.navigation.navigate(item.screen, {
        items: this.state.event.news,
        onCardPress: newsItem =>
          this.props.navigation.navigate('NewsDetailsScreen', {
            event: newsItem,
          }),
      });
      return;
    }
    this.props.navigation.navigate(item.screen, {
      event: this.state.event,
    });
  };

  renderOptionBlock = item => {
    if (
      !this.state.event ||
      !this.state.event[item.field] ||
      !this.state.event[item.field].length
    ) {
      return null;
    }
    return (
      <OptionBlock
        onOptionPress={() => this.onOptionBlockPress(item)}
        option={item}
      />
    );
  };
  get statusItemId() {
    return this.props.route.params.status;
  }

  // isButtonDisabled = () => {
  //   if (
  //     this.props.route.params.event.requests &&
  //     this.props.route.params.event.requests.length
  //   ) {
  //     const statusId = this.props.route.params.event.requests[0].status.id;
  //     return statusId !== 1;
  //   }
  //   return !this.state.isEnableRegistration;
  // };

  isButtonDisabled = () => {
    if (this.props.route.params.status !== undefined) {
      const statusId = this.props.route.params.status;
      return statusId !== 1;
    }
    return !this.state.isEnableRegistration;
  };

  registerOnEvent = async () => {
    const {event: eventInfo} = this.props.route.params;
    if (this.state.event.hasTicket) {
      this.props.navigation.navigate('TicketScreen', {
        ticket: eventInfo,
      });
      return;
    }
    if (this.isButtonDisabled()) {
      alert(this.state.errorMessage);
      return;
    }
    if (this.props.route.params.status === 1) {
      this.props.navigation.navigate('TicketScreen', {
        ticket: eventInfo,
      });
      return;
    }

    const {data} = await http.get(
      `/registration/form/index?id=${eventInfo.id}`,
    );

    if (data.data.entities.length === 1) {
      this.props.navigation.navigate('RegistrationScreen', {
        id: eventInfo.id,
        role: data.data.entities[0].role,
        token: this.props.token,
      });
      return;
    }
    this.props.navigation.navigate('RoleScreen', {
      id: eventInfo.id,
      token: this.props.token,
      roles: data.data.entities,
    });
  };

  addToFavorite = async () => {
    const eventWithType = {
      ...this.props.route.params.event,
      type: 'События',
    };
    this.setState({isFavorite: true});
    const favorites = await FavoritesStorage.get();
    favorites.push(eventWithType);
    await FavoritesStorage.save([...favorites]);
  };

  deleteFromFavorite = async () => {
    const favorites = await FavoritesStorage.get();
    const filteredFavorites = favorites.filter(
      f => f.id !== this.props.route.params.event.id,
    );
    this.setState({isFavorite: false});
    await FavoritesStorage.save(filteredFavorites);
  };

  isAddedToFavorite = async () => {
    const favorites = await FavoritesStorage.get();
    if (!favorites) {
      return false;
    }
    const favoriteEvents = favorites.filter(f => f.type === 'События');
    if (!favoriteEvents.length) {
      return false;
    }
    const favEvent = favoriteEvents.find(
      f => f.id === this.props.route.params.event.id,
    );
    return !!favEvent;
  };

  onLikePress = async () => {
    const isFavorite = await this.isAddedToFavorite();
    console.log('onLikePress', isFavorite);
    if (isFavorite) {
      this.deleteFromFavorite();
    } else {
      this.addToFavorite();
    }
  };

  render() {
    const {event: eventInfo} = this.props.route.params;
    const interpolatedOpacity = this._animatedValue.interpolate({
      inputRange: [0, 150],
      outputRange: [1, 0.5],
      extrapolate: 'clamp',
    });

    const event = Animated.event([
      {
        nativeEvent: {
          contentOffset: {
            y: this._animatedValue,
          },
        },
      },
    ]);
    return (
      <View
        onStartShouldSetResponderCapture={() => {
          this.setState({enableScrollViewScroll: true});
        }}>
        <S.Container
          // scrollEnabled={this.state.enableScrollViewScroll}
          stickyHeaderIndices={[0]}
          ref={myScroll => (this._myScroll = myScroll)}
          onScroll={event}>
          <Header
            props={this.props}
            title="Мероприятие"
            onLikePress={this.onLikePress}
            isFavorite={this.state.isFavorite}
          />
          <S.OptionsContainer
            onStartShouldSetResponderCapture={() => {
              this.setState({enableScrollViewScroll: false});
              if (
                this._myScroll.contentOffset === 0 &&
                this.state.enableScrollViewScroll === false
              ) {
                this.setState({enableScrollViewScroll: true});
              }
            }}>
            <Animated.View
              // source={{uri: eventInfo.src.url}}
              style={[
                style.eventImage,
                {
                  width: interpolatedOpacity.interpolate({
                    inputRange: [0.5, 1],
                    outputRange: [250, 330],
                  }),
                  opacity: interpolatedOpacity.interpolate({
                    inputRange: [0.7, 1],
                    outputRange: [0, 1],
                  }),
                  transform: [
                    {
                      rotateX: interpolatedOpacity.interpolate({
                        inputRange: [0.7, 1],
                        outputRange: ['80deg', '0deg'],
                      }),
                    },
                  ],
                },
              ]}>
              <Image
                source={{uri: eventInfo.src.url}}
                style={{borderRadius: 15, ...StyleSheet.absoluteFill}}
              />
              {this.props.route.params.status !== undefined ? (
                <S.StatusContainer color={statusColors[this.statusItemId]}>
                  <S.StatusText>{statusString[this.statusItemId]}</S.StatusText>
                </S.StatusContainer>
              ) : null}
            </Animated.View>
            <Animated.View
              style={[
                style.timeView,
                {
                  top: interpolatedOpacity.interpolate({
                    inputRange: [0.5, 1],
                    outputRange: [-25, 25],
                    extrapolate: 'clamp',
                  }),
                },
              ]}>
              <S.EventTime>
                {formatEventTime(eventInfo.beginDate, eventInfo.finishDate)}
              </S.EventTime>
            </Animated.View>
            <Animated.View
              style={{
                marginTop: interpolatedOpacity.interpolate({
                  inputRange: [0.5, 1],
                  outputRange: [50, 100],
                  extrapolate: 'clamp',
                }),
                paddingHorizontal: 15,
                marginBottom: 10,
              }}>
              <S.EventTitle>{eventInfo.title}</S.EventTitle>
              <S.EventAddress>{eventInfo.place}</S.EventAddress>
            </Animated.View>
            {!this.state.loading ? (
              <FlatList
                contentContainerStyle={{
                  paddingLeft: 15,
                  paddingBottom: 70,
                  alignItems: 'center',
                }}
                nestedScrollEnabled
                data={this.state.options}
                renderItem={({item}) => this.renderOptionBlock(item)}
                numColumns={2}
                keyExtractor={(item, index) => index.toString()}
              />
            ) : (
              <ActivityIndicator color="#5954f9" size="large" />
            )}
          </S.OptionsContainer>
        </S.Container>
        {/* {!this.state.loading ? (
          <S.RegisterButton
            disabled={this.isButtonDisabled()}
            isDisabled={this.isButtonDisabled()}
            onPress={this.registerOnEvent}>
            {this.props.route.params.status === 1 ? (
              <S.Icon source={ticket} />
            ) : null}
            <S.RegisterButtonText>
              {this.state.errorMessage
                ? this.handleErrorStatus(this.state.errorMessage)
                : this.props.route.params.status === 0
                ? 'Заявка отправлена'
                : 'Зарегистрироваться'}
            </S.RegisterButtonText>
          </S.RegisterButton>
        ) : null} */}
        {!this.state.loading ? (
          <S.RegisterButton
            disabled={
              !isActiveRegistration(
                this.state.event.registrationBeginDate,
                this.state.event.registrationEndDate,
                this.state.event.hasTicket,
              )
            }
            isDisabled={
              !isActiveRegistration(
                this.state.event.registrationBeginDate,
                this.state.event.registrationEndDate,
                this.state.event.hasTicket,
              )
            }
            onPress={this.registerOnEvent}>
            {/* {this.props.route.params.status === 1 ? (
              <S.Icon source={ticket} />
            ) : null} */}
            {this.state.event.hasTicket ? <S.Icon source={ticket} /> : null}
            <S.RegisterButtonText>
              {getRegistrationText(
                this.state.event.registrationBeginDate,
                this.state.event.registrationEndDate,
                this.state.event.hasTicket,
              )}
            </S.RegisterButtonText>
          </S.RegisterButton>
        ) : null}
      </View>
    );
  }
}

const style = StyleSheet.create({
  eventImage: {
    height: 180,
    position: 'absolute',
    borderRadius: 15,
    alignSelf: 'center',
    top: -130,
    backgroundColor: 'white',
  },
  timeView: {
    position: 'absolute',
    alignSelf: 'center',
    width: 300,
    height: 40,
    elevation: 5,
    borderRadius: 20,
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
  },
});

const mapStateToProps = ({auth}) => ({
  token: auth.token,
});

export default connect(
  mapStateToProps,
  null,
)(MainScreen);
