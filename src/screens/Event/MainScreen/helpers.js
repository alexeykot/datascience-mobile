import moment from 'moment';
import _ from 'lodash';
import 'moment/locale/ru';

export const formatEventTime = (beginDate, finishDate) =>
  `${moment
    .utc(beginDate)
    .local()
    .locale('ru')
    .format('DD MMMM')} - ${moment
    .utc(finishDate)
    .local()
    .locale('ru')
    .format('DD MMMM')}, ${moment
    .utc(finishDate)
    .local()
    .locale('ru')
    .format('YYYY')}`;

export const formatEvent = event => {
  return {
    ...event,
    speakers: selectSpeakers(event),
    halls: selectHalls(event),
  };
};

export const selectSpeakers = event => {
  const speakers = event.elements.map(element => element.speakers);
  if (!speakers.length) {
    return [];
  }
  const arr = [];
  for (let i = 0; i < speakers.length; i++) {
    for (let y = 0; y < speakers[i].length; y++) {
      arr.push(speakers[i][y]);
    }
  }
  const uniqArr = _.uniqBy(arr, 'id');
  return uniqArr;
};

export const selectHalls = event => {
  const halls = event.elements.map(element => element.hall);
  const allHallObject = {
    id: 'all',
    title: 'Все',
  };
  return [allHallObject, ...halls];
};

export const eventWithNews = (event, news) => {
  const eventNews = news.filter(newNews => newNews.event.id === event.id);
  return {
    ...event,
    news: eventNews,
  };
};

export const isActiveRegistration = (beginDate, finishDate, hasTicket) => {
  if (hasTicket) {
    return true;
  }
  return (
    moment().isSameOrBefore(moment(finishDate)) &&
    moment().isSameOrAfter(moment(beginDate))
  );
};

export const getRegistrationText = (beginDate, finishDate, hasTicket) => {
  if (isActiveRegistration(beginDate, finishDate)) {
    return hasTicket ? 'Ваш билет' : 'Зарегистрироваться';
  } else {
    if (moment().isBefore(moment(beginDate))) {
      return `Регистрация начнется: \n ${moment(beginDate).format(
        'DD.MM.YYYY в HH:mm',
      )}`;
    }
    if (moment().isAfter(moment(finishDate))) {
      return 'Регистрация закончена';
    }
  }
};
