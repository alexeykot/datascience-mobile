import {Dimensions} from 'react-native';
import styled from 'styled-components';

export const Container = styled.ScrollView`
  background-color: #5954f9;
`;

export const OptionsContainer = styled.View`
  width: 100%;
  height: ${Dimensions.get('window').height - 160}px;
  border-top-left-radius: 30px;
  border-top-right-radius: 30px;
  background-color: white;
  margin-top: 140px;
  /* padding: 0 15px; */
`;

export const EventTime = styled.Text`
  color: #5754f4;
  font-family: Roboto;
  font-size: 14px;
  font-weight: 700;
  line-height: 14px;
  text-transform: uppercase;
`;

export const InfoView = styled.View`
  margin-top: 100px;
  padding: 0 15px;
  margin-bottom: 10px;
`;

export const EventTitle = styled.Text`
  color: #140c09;
  font-family: Roboto;
  font-size: 24px;
  font-weight: 700;
  line-height: 30px;
  text-align: center;
`;

export const EventAddress = styled.Text`
  color: #8b8d94;
  font-family: Roboto;
  font-size: 14px;
  font-weight: 400;
  line-height: 18px;
  text-align: center;
`;

export const RegisterButton = styled.TouchableOpacity`
  width: 300px;
  height: 48px;
  elevation: 10;
  border-radius: 24px;
  background-color: ${({isDisabled}) => (isDisabled ? 'grey' : ' #5754f4')};
  position: absolute;
  bottom: 10;
  align-self: center;
  justify-content: center;
  align-items: center;
  flex-direction: row;
`;

export const RegisterButtonText = styled.Text`
  color: #ffffff;
  font-family: Roboto;
  font-size: 14px;
  font-weight: 500;
  text-align: center;
`;

export const StatusContainer = styled.View`
  padding: 8px 9px;
  background-color: ${({color}) => (color ? color : 'red')};
  position: absolute;
  bottom: 40;
  /* left: 20; */
  margin-left: 5%;
  border-radius: 12px;
  min-width: 134px;
  height: 24px;
  justify-content: center;
  align-items: center;
`;

export const StatusText = styled.Text`
  color: #ffffff;
  font-family: Roboto;
  font-size: 10px;
  font-weight: 700;
  text-transform: uppercase;
`;

export const Icon = styled.Image`
  margin-right: 16px;
`;
