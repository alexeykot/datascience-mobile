import React from 'react';
import People from '../../../../../assets/icons/people.svg';

import * as S from './styled';

const Speaker = props => {
  const {speaker} = props;
  return (
    <S.Container
      onPress={() =>
        props.navigation.navigate('SpeakerScreen', {
          speaker: props.speaker,
        })
      }
      isPaddingHorizontal={props.isPaddingHorizontal}>
      {speaker.src && speaker.src.url ? (
        <S.Avatar source={{uri: props.speaker.src.url}} />
      ) : (
        <S.MockAvatar>
          <People />
        </S.MockAvatar>
      )}
      <S.InfoContainer>
        <S.Name>{props.speaker.fullName}</S.Name>
        <S.Job numberOfLines={1} ellipsizeMode="tail">
          {props.speaker.post}
        </S.Job>
      </S.InfoContainer>
    </S.Container>
  );
};

export default Speaker;
