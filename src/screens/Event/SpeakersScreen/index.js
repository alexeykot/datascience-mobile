import React from 'react';

import Speaker from './components/Speaker';

import * as S from './styled';
import BaseScreen from '../../../components/BaseScreen';

class SpeakersScreen extends React.Component {
  state = {
    filterText: '',
  };

  render() {
    const {event: eventInfo} = this.props.route.params;
    const filteredItems = eventInfo.speakers.filter(speaker =>
      speaker.fullName.toLocaleLowerCase().includes(this.state.filterText),
    );

    const itemsToDisplay = this.state.filterText
      ? filteredItems
      : eventInfo.speakers;
    return (
      <BaseScreen
        title="Спикеры"
        scrollableOutsideContainer
        withSearch
        searchProps={{
          inputPlaceholder: 'Поиск спикера',
          onInputChange: text =>
            this.setState({filterText: text.toLocaleLowerCase()}),
          onResetPress: () => this.setState({filterText: ''}),
          filterText: this.state.filterText,
        }}
        containerPadding={0}
        content={
          <S.Container>
            {itemsToDisplay.map(speaker => (
              <Speaker
                navigation={this.props.navigation}
                isPaddingHorizontal
                speaker={speaker}
              />
            ))}
          </S.Container>
        }
      />
    );
  }
}

export default SpeakersScreen;
