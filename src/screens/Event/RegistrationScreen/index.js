import React from 'react';
import qs from 'qs';
import {Alert} from 'react-native';

import Header from '../components/Header';
import DateModal from './components/DateModal';
import * as S from './styled';
import http from '../../../services/http';
import RadioButton from './components/RadioButton';
import CheckBox from './components/СheckBox';
import Picker from './components/Picker';
import FilePicker from './components/FilePicker';

import {formatInputsFromKey} from './helpers';
import Axios from 'axios';
import {ActivityIndicator} from 'react-native';

const RegistrationScreen = props => {
  const [inputs, setInputs] = React.useState([]);
  const [isDateVisible, setDateVisible] = React.useState(false);
  const [loading, setLoading] = React.useState(false);
  React.useEffect(() => {
    console.log('sesesese', props.route.params.id);
    async function fetchInputs() {
      const {data} = await http.get(
        `/registration/form/view?id=${props.route.params.id}`,
      );
      console.log('data-inputs', data);
      setInputs(data.data.entities);
    }
    fetchInputs();
  }, [inputs.length, props.route.params.id]);

  const onInputChange = (name, text) => {
    const newInputs = inputs.map(inp => {
      if (inp.name === name) {
        inp.value = text;
        return inp;
      }
      return inp;
    });
    setInputs(newInputs);
  };

  const register = async () => {
    try {
      const {token, role, id} = props.route.params;
      setLoading(true);
      const {data} = await Axios({
        method: 'post',
        url: `https://ai-id.dev-vps.ru/api/v1/registration/form/register?id=${id}&access-token=${token}&role=${role}`,
        data: qs.stringify(formatInputsFromKey(inputs)),
        headers: {
          'content-type': 'application/x-www-form-urlencoded;charset=utf-8',
        },
      });
      console.log('data', data);
      if (data.code === 203 && data.data.errors.length) {
        Alert.alert('Некорректное значение', data.data.errors[0]);
      }
      if (data.code === 200) {
        props.navigation.navigate('MainScreen');
      }
      setLoading(false);
    } catch (err) {
      console.log('err', err.response);
      setLoading(false);
      if (err.response.status === 500) {
        props.navigation.navigate('MainScreen');
      }
    }
  };

  const renderInput = input => {
    console.log('input', input);
    if (input.type === 'header') {
      return <S.Title>{input.label}</S.Title>;
    }
    if (input.type === 'textarea') {
      return (
        <>
          <S.Input
            onChangeText={text => onInputChange(input.name, text)}
            multiline={true}
            value={input.value}
          />
        </>
      );
    }
    if (input.type === 'date') {
      return (
        <>
          <S.TouchedInput onPress={() => setDateVisible(true)}>
            <S.Value>{input.value}</S.Value>
          </S.TouchedInput>
          <DateModal
            input={input}
            title={input.label}
            isVisible={isDateVisible}
            onInputChange={date => onInputChange(input.name, date)}
            onClose={() => setDateVisible(false)}
          />
        </>
      );
    }
    if (input.type === 'text' && input.subtype === 'tel') {
      return (
        <>
          <S.Input
            editable={false}
            onChangeText={text => onInputChange(input.name, text)}
            value={input.value}
          />
        </>
      );
    }
    if (input.type === 'text') {
      return (
        <>
          <S.Input
            editable={input.type !== 'date'}
            onChangeText={text => onInputChange(input.name, text)}
            value={input.value}
          />
        </>
      );
    }
    if (input.type === 'radio-group') {
      return (
        <>
          <RadioButton options={input.values} />
        </>
      );
    }
    if (input.type === 'file') {
      return (
        <>
          <FilePicker />
        </>
      );
    }
    if (input.type === 'select' || input.type === 'radio-group') {
      return (
        <>
          {/* <Picker
            selectedValue={input.value}
            onValueChange={item => onInputChange(input.name, item)}
            style={{height: 50, width: 150}}>
            {input.values.map(value => (
              <Picker.Item label={value.label} value={value.value} />
            ))}
          </Picker> */}
          <Picker
            selectedValue={input.value ? input.value : input.values[0]}
            name={input.name}
            options={input.values}
            onOptionPress={item => onInputChange(input.name, item)}
          />
        </>
      );
    }
    return null;
  };
  return (
    <S.Background>
      <S.Container>
        <Header title="Регистрация" props={props} withoutRightButton={true} />
        <S.OptionsContainer style={{paddingHorizontal: 30}}>
          {inputs.map(input => (
            <>
              {!['header', 'paragraph'].includes(input.type) ? (
                <S.Label>{input.label}</S.Label>
              ) : null}
              {renderInput(input)}
            </>
          ))}
          <S.FakeView />
        </S.OptionsContainer>
      </S.Container>
      <S.Button disabled={loading} onPress={register}>
        {loading ? (
          <ActivityIndicator size="large" color="white" />
        ) : (
          <S.ButtonText>Зарегистрироваться</S.ButtonText>
        )}
      </S.Button>
    </S.Background>
  );
};

export default RegistrationScreen;
