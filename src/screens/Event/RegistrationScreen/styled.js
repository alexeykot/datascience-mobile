import styled from 'styled-components';

export const Background = styled.View`
  flex: 1;
  background-color: white;
`;

export const Container = styled.View`
  flex: 1;
  background-color: #5954f9;
`;

export const OptionsContainer = styled.ScrollView`
  width: 100%;
  border-top-left-radius: 30px;
  border-top-right-radius: 30px;
  background-color: white;
  margin-top: 40px;
  /* padding: 0 15px; */
`;

export const Title = styled.Text`
  color: #1b041e;
  font-family: Roboto;
  font-size: 18px;
  font-weight: 700;
  margin-top: 30px;
`;

export const Input = styled.TextInput`
  width: 100%;
  height: 40px;
  border-color: #c9ccd5;
  border-bottom-width: 1px;
`;

export const TouchedInput = styled.TouchableOpacity`
  width: 100%;
  height: 40px;
  padding-left: 5px;
  border-color: #c9ccd5;
  justify-content: center;
  border-bottom-width: 1px;
`;

export const Value = styled.Text``;

export const Label = styled.Text`
  color: #8b8d94;
  font-family: Roboto;
  font-size: 12px;
  font-weight: 400;
  margin-top: 27px;
`;

export const Select = styled.View`
  width: 50px;
  height: 50px;
  background-color: ${({isSelected}) => (isSelected ? 'red' : 'yellow')};
  margin-bottom: 10px;
`;

export const FakeView = styled.View`
  width: 100%;
  height: 70px;
  background-color: transparent;
`;

export const Button = styled.TouchableOpacity`
  width: 300px;
  height: 48px;
  elevation: 10;
  border-radius: 24px;
  background-color: #5954f9;
  opacity: ${({disabled}) => (disabled ? 0.5 : 1)};
  /* margin-bottom: 10px; */
  align-self: center;
  justify-content: center;
  align-items: center;
  position: absolute;
  bottom: 10px;
`;

export const ButtonText = styled.Text`
  color: #ffffff;
  font-family: Roboto;
  font-size: 14px;
  font-weight: 500;
`;
