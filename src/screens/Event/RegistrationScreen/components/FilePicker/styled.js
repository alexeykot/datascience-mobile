import styled from 'styled-components';

export const Container = styled.View`
  flex-direction: row;
  width: 100%;
  margin-top: 10px;
  flex-wrap: wrap;
`;

export const FileScroll = styled.ScrollView``;

export const FileButton = styled.TouchableOpacity`
  width: 68px;
  height: 68px;
  border-radius: 20px;
  background-color: #f7f5fa;
  justify-content: center;
  align-items: center;
  margin-top: 10px;
`;

export const FileButtonText = styled.Text`
  font-size: 32px;
  line-height: 32px;
  color: #5954f9;
`;

export const FileContainer = styled.View`
  width: 68px;
  height: 68px;
  border-radius: 20px;
  margin-left: 10px;
  margin-top: 10px;
`;

export const Img = styled.Image`
  flex: 1;
  border-radius: 20px;
`;

export const FadeMask = styled.View`
  flex: 1;
  border-radius: 20px;
  background-color: black;
  opacity: 0.5;
`;

export const File = styled.View`
  flex: 1;
  background-color: #f7f5fa;
  border-radius: 20px;
  justify-content: center;
  align-items: center;
`;

export const DeleteButton = styled.TouchableOpacity`
  width: 24px;
  height: 24px;
  background-color: #5954f9;
  position: absolute;
  top: -7px;
  right: -7px;
  z-index: 3333;
  border-radius: 24px;
  justify-content: center;
  align-items: center;
  transform: rotate(45deg);
`;

export const DeleteButtonText = styled.Text`
  font-size: 12px;
  line-height: 14px;
  color: white;
`;

export const FileType = styled.Text`
  color: red;
`;
