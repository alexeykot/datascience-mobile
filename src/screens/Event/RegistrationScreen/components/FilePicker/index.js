import React from 'react';
import DocumentPicker from 'react-native-document-picker';

import * as S from './styled';

const FilePicker = () => {
  const [files, setFiles] = React.useState([]);
  const onPress = async () => {
    const res = await DocumentPicker.pick({
      type: [DocumentPicker.types.allFiles],
    });
    setFiles([...files, res]);
    console.log('res', res);
  };
  const onDelete = file => {
    const newFiles = files.filter(f => f.uri !== file.uri);
    setFiles(newFiles);
  };
  return (
    <S.Container>
      <S.FileButton onPress={onPress}>
        <S.FileButtonText>+</S.FileButtonText>
      </S.FileButton>
      {files.map(file => (
        <S.FileContainer>
          <S.DeleteButton onPress={() => onDelete(file)}>
            <S.DeleteButtonText>+</S.DeleteButtonText>
          </S.DeleteButton>
          {file.type.split('/')[0] === 'image' ? (
            <S.FadeMask>
              <S.Img source={{uri: file.uri}} />
            </S.FadeMask>
          ) : (
            <S.File>
              <S.FileType>.{file.type.split('/')[1]}</S.FileType>
            </S.File>
          )}
        </S.FileContainer>
      ))}
    </S.Container>
  );
};

export default FilePicker;
