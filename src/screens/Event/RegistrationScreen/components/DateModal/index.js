/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {StyleSheet, Dimensions} from 'react-native';
import moment from 'moment';
import DatePicker from 'react-native-date-picker';

import Modal from 'react-native-modal';

import * as S from './styled';

const DateModal = ({
  isVisible,
  onClose,
  title,
  options,
  onInputChange,
  onOptionPress,
  input,
}) => {
  // const [birthday, setBirthday] = React.useState(null);
  const close = async () => {
    onClose();
  };
  return (
    <Modal
      onSwipeComplete={close}
      swipeDirection={['down']}
      onBackdropPress={close}
      style={styles.view}
      backdropOpacity={0}
      isVisible={isVisible}>
      <S.Container isPadding={!!options}>
        <S.Title>{title}</S.Title>
        <S.Separator />
        <S.OptionsContainer>
          <DatePicker
            style={{
              backgroundColor: 'white',
              width: Dimensions.get('screen').width,
              borderTopWidth: 1,
              borderColor: '#f7f5fa',
            }}
            date={input.value}
            mode="date"
            onDateChange={date =>
              onInputChange(moment(date).format('DD.MM.YYYY'))
            }
          />
        </S.OptionsContainer>
        {/* <S.DateButton>
          <S.ButtonText>Сохранить</S.ButtonText>
        </S.DateButton> */}
      </S.Container>
    </Modal>
  );
};

const styles = StyleSheet.create({
  view: {
    justifyContent: 'flex-end',
    margin: 0,
  },
  wrapper: {
    flex: 1,
  },
});

export default DateModal;
