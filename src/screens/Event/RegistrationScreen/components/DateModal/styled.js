import styled from 'styled-components';

export const Container = styled.View`
  /* height: 400px; */
  width: 100%;
  background-color: white;
  border-top-left-radius: 30px;
  border-top-right-radius: 30px;
  padding: 19px 30px 10px 30px;
  padding: ${({isPadding}) =>
    isPadding ? ' 19px 30px 10px 30px' : '19px 0 10px 0'};
  elevation: 20;
  align-items: center;
`;

export const Row = styled.View`
  flex-direction: row;
  justify-content: space-between;
`;

export const Title = styled.Text`
  color: #8b8d94;
  font-family: Roboto;
  font-size: 12px;
  font-weight: 400;
  margin-bottom: 19px;
`;

export const Hint = styled.Text`
  color: #8b8d94;
  font-family: Roboto;
  font-size: 14px;
  font-weight: 400;
  margin-top: 16px;
`;

export const OptionsContainer = styled.ScrollView`
  margin-top: 10px;
  width: 100%;
  max-height: 300px;
`;

export const Separator = styled.View`
  width: 100%;
  height: 1px;
  background-color: #c9ccd5;
  opacity: 0.5;
`;

export const DefaultButton = styled.TouchableOpacity`
  height: 48px;
  border-radius: 24px;
  border: 1px solid #c9ccd5;
  background-color: transparent;
  opacity: 0.5;
  width: 100%;
  justify-content: center;
  align-items: center;
`;

export const DateButton = styled.TouchableOpacity`
  height: 48px;
  border-radius: 24px;
  /* border: 1px solid #c9ccd5; */
  background-color: red;
  opacity: 0.5;
  align-self: center;
  width: 70%;
  justify-content: center;
  align-items: center;
`;

export const ButtonText = styled.Text`
  color: #1b041e;
  font-family: Roboto;
  font-size: 14px;
  font-weight: 700;
`;

export const Option = styled.TouchableOpacity`
  width: 100%;
  height: 36px;
  justify-content: center;
  align-items: center;
  margin-bottom: ${({isLast}) => (isLast ? 15 : 0)}px;
`;

export const OptionText = styled.Text`
  color: #1b041e;
  font-family: Roboto;
  font-size: 14px;
  font-weight: 400;
`;
