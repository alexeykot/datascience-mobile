import React from 'react';
import {StyleSheet} from 'react-native';
import Modal from 'react-native-modal';

import * as S from './styled';

const sexEnum = {
  1: 'Мужской',
  2: 'Женский',
};

// const getValue = (value, name) => {
//   if (name === 'Client[sex]') {
//     return sexEnum[value];
//   }
//   return value.label;
// };

const getValue = (value, values, name) => {
  if (name === 'Client[sex]') {
    return sexEnum[value];
  }
  // const search = values.find(
  //   v => v.value === (value.value ? value.value : value),
  // );
  return values[0].label;
};

const Picker = ({selectedValue, options, name, onOptionPress}) => {
  const [isModalVisible, setModalVisible] = React.useState(false);
  const onPress = value => {
    setModalVisible(false);
    onOptionPress(value, name);
  };
  return (
    <>
      <S.Container onPress={() => setModalVisible(true)}>
        <S.Value>{getValue(selectedValue, options, name)}</S.Value>
      </S.Container>
      <Modal
        style={styles.view}
        isVisible={isModalVisible}
        onBackdropPress={() => setModalVisible(false)}
        backdropOpacity={0.3}>
        <S.ModalContainer>
          {options.map(o => (
            <>
              <S.ModalOption onPress={() => onPress(o.value)}>
                <S.Value>{o.label}</S.Value>
              </S.ModalOption>
            </>
          ))}
        </S.ModalContainer>
      </Modal>
    </>
  );
};

const styles = StyleSheet.create({
  view: {
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default Picker;
