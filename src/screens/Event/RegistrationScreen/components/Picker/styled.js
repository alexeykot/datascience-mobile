import styled from 'styled-components';

export const Container = styled.TouchableOpacity`
  width: 100%;
  height: 40px;
  border-color: #c9ccd5;
  border-bottom-width: 1px;
  justify-content: center;
`;

export const Value = styled.Text``;

export const ModalContainer = styled.View`
  padding-top: 10px;
  background-color: white;
  width: 200px;
`;

export const ModalOption = styled.TouchableOpacity`
  width: 100%;
  height: 30px;
  margin-bottom: 10px;
  justify-content: center;
  align-items: center;
`;
