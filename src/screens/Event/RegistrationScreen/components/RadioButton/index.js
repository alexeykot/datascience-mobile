import React from 'react';

import * as S from './styled';

const RadioButton = ({options}) => {
  const [opts, setOpts] = React.useState(options);
  const onPress = value => {
    const newOptions = opts.map(o => ({
      ...o,
      selected: o.value === value,
    }));
    setOpts(newOptions);
  };
  return (
    <>
      {opts.map(option => (
        <S.Container onPress={() => onPress(option.value)}>
          <S.Box filled={option.selected}>
            {option.selected ? <S.Inner /> : null}
          </S.Box>
          <S.Label>{option.label}</S.Label>
        </S.Container>
      ))}
    </>
  );
};

export default RadioButton;
