import styled from 'styled-components';

export const Container = styled.TouchableOpacity`
  width: 100%;
  flex-direction: row;
  margin-top: 20px;
`;

export const Box = styled.View`
  width: 20px;
  height: 20px;
  border-radius: 20px;
  border: 1px solid #c9ccd5;
  background-color: white;
  justify-content: center;
  align-items: center;
`;

export const Inner = styled.View`
  width: 10px;
  height: 10px;
  border-radius: 10px;
  background-color: #5954f9;
`;

export const Label = styled.Text`
  margin-left: 10px;
`;
