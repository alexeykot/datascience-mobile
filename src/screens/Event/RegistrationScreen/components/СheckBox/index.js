import React from 'react';

import * as S from './styled';

const CheckBox = ({options}) => {
  const [opts, setOpts] = React.useState(options);
  const onPress = value => {
    const newOptions = opts.map(o => {
      if (o.value === value) {
        return {
          ...o,
          selected: !o.selected,
        };
      } else {
        return {
          ...o,
        };
      }
    });
    setOpts(newOptions);
  };
  return (
    <>
      {opts.map(option => (
        <S.Container onPress={() => onPress(option.value)}>
          <S.Box filled={option.selected} />
          <S.Label>{option.label}</S.Label>
        </S.Container>
      ))}
    </>
  );
};

export default CheckBox;
