import styled from 'styled-components';

export const Container = styled.TouchableOpacity`
  width: 100%;
  flex-direction: row;
`;

export const Box = styled.View`
  width: 20px;
  height: 20px;
  background-color: ${({filled}) => (filled ? 'red' : 'black')};
`;

export const Label = styled.Text`
  margin-left: 10px;
`;
