export const formatInputsFromKey = inputs => {
  const initialValue = {};
  return inputs.reduce((obj, item) => {
    return {
      ...obj,
      [item.name]: item.value,
    };
  }, initialValue);
};
