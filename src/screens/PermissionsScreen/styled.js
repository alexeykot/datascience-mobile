import styled from 'styled-components';

export const Container = styled.ScrollView`
  flex: 1;
  background-color: white;
  padding: 0 14px;
`;

export const Header = styled.View`
  width: 100%;
  height: 130px;
  border-radius: 10px;
  background-color: #5754f4;
  margin-top: 30px;
  padding: 22px 12px 18px 12px;
  align-items: center;
`;

export const HeaderTitle = styled.Text`
  font-size: 18px;
  font-weight: 700;
  line-height: 28px;
  color: #ffffff;
  margin-top: 13px;
`;

export const HeaderDescription = styled.Text`
  font-size: 14px;
  color: #ffffff;
  text-align: center;
`;

export const Toggle = styled.Image`
  width: 24px;
  height: 16px;
`;

export const Button = styled.TouchableOpacity`
  width: 160px;
  height: 48px;
  border-radius: 24px;
  background-color: #5754f4;
  position: absolute;
  align-self: center;
  bottom: 26px;
  justify-content: center;
  align-items: center;
`;

export const ButtonText = styled.Text`
  color: #ffffff;
  font-size: 14px;
`;

export const HintText = styled.Text`
  margin-top: 30px;
  color: #8b8d94;
  font-size: 12px;
  text-align: center;
`;

export const BlankBlock = styled.View`
  height: 100px;
`;
