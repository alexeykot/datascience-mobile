import React from 'react';

import {PermissionsAndroid} from 'react-native';
import toggle from '../../assets/images/toggle-right.png';

import mail from '../../assets/images/mail.png';
import folder from '../../assets/images/folder.png';
import navigation from '../../assets/images/navigation.png';

import PermissionBlock from './сomponents/PermissionBlock';
import * as S from './styled';

const permissions = [
  {
    title: 'смс',
    description:
      'Во время регистрации вам не придется вводить код подтверждения номера телефона',
    icon: mail,
  },
  {
    title: 'ГЕОЛОКАЦИЯ',
    description:
      'Вам не придется указывать город при регистрации и мы будем показывать ближайшие к вам мероприятия',
    icon: navigation,
  },
  {
    title: 'ФАЙЛЫ',
    description:
      'Будет здорово, если вы загрузите свою аватарку и сможете сохранить приглашение на мероприятия',
    icon: folder,
  },
];
const PermissionsScreen = props => {
  const onPermissionRequest = async () => {
    await PermissionsAndroid.requestMultiple([
      PermissionsAndroid.PERMISSIONS.SEND_SMS,
      PermissionsAndroid.PERMISSIONS.RECEIVE_SMS,
      PermissionsAndroid.PERMISSIONS.READ_SMS,
      PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
      PermissionsAndroid.PERMISSIONS.CAMERA,
      PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
    ]);
    props.navigation.navigate('ChatScreen');
  };
  return (
    <>
      <S.Container>
        <S.Header>
          <S.Toggle source={toggle} />
          <S.HeaderTitle>ДЛЯ ВАШЕГО КОМФОРТА</S.HeaderTitle>
          <S.HeaderDescription>
            приложению потребуется доступ к некоторым функциям вашего телефона
          </S.HeaderDescription>
        </S.Header>
        {permissions.map(permission => (
          <PermissionBlock permission={permission} />
        ))}
        <S.HintText>
          Ваш отказ от предоставления доступа к данным функциям вашего телефона
          не повлияет на работу приложения
        </S.HintText>
        <S.BlankBlock />
      </S.Container>
      <S.Button onPress={onPermissionRequest}>
        <S.ButtonText>Понятно</S.ButtonText>
      </S.Button>
    </>
  );
};

export default PermissionsScreen;
