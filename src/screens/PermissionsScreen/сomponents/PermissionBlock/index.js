import React from 'react';

import SendIcon from '../../../../assets/icons/sendIcon.svg';

import * as S from './styled';

const PermissionBlock = ({permission}) => (
  <S.Container>
    <S.ImageContainer>
      <S.Icon source={permission.icon} />
    </S.ImageContainer>
    <S.InfoBlock>
      <S.Title>{permission.title}</S.Title>
      <S.Description>{permission.description}</S.Description>
    </S.InfoBlock>
  </S.Container>
);

export default PermissionBlock;
