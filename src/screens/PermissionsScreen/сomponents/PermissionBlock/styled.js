import styled from 'styled-components';

export const Container = styled.View`
  width: 100%;
  background-color: white;
  flex-direction: row;
  margin-top: 25px;
`;

export const ImageContainer = styled.View`
  flex: 1;
  align-items: center;
`;

export const Icon = styled.Image``;

export const InfoBlock = styled.View`
  flex: 5;
`;

export const Title = styled.Text`
  font-size: 18px;
  text-transform: uppercase;
  font-weight: 700;
  color: #443fd9;
`;

export const Description = styled.Text`
  color: #130b04;
  font-size: 14px;
  margin-top: 5px;
`;
