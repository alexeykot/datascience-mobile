export const formatMembers = membersArr => {
  let arr = [];
  for (let i = 0; i < membersArr.length; i++) {
    arr.push(...membersArr[i]);
  }
  return arr;
};

export const displayBlock = (statuses, blockId) => {
  const findBlock = statuses.find(block => block.id === blockId);
  return findBlock.enabled;
};

export const isClientEvent = (event, clientEvents) => {
  if (!clientEvents.length) {
    return false;
  }
  const is = clientEvents.find(e => e.id === event.id);
  return Boolean(is);
};

export const getClientStatus = (event, clientEvents) => {
  if (!isClientEvent(event, clientEvents)) {
    return undefined;
  }
  const ev = clientEvents.find(e => e.id === event.id);
  if (ev.requests.length && ev.requests[0].status) {
    return ev.requests[0].status.id;
  }
  return '1';
};
