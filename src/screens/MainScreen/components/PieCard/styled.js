import styled from 'styled-components';

export const ShopCard = styled.TouchableOpacity`
  margin-left: ${({isFirst}) => (isFirst ? '15px' : 0)};
  margin-right: 8px;
  width: 161px;
  margin-top: 12px;
  border-radius: 20px;
`;

export const RankContainer = styled.View`
  flex-direction: row;
`;

export const Rank = styled.Text`
  font-size: 16px;
  font-weight: 700;
  margin-right: 6px;
`;

export const ShopImageContainer = styled.View`
  margin-bottom: 8px;
  height: 96px;
  width: 161px;
  border-radius: 20px;
`;

export const ShopImage = styled.Image`
  /* background-color: #ffffff; */
  background-color: #ffffff;
  width: 100%;
  height: 100%;
  border-radius: 20px;
`;

export const ShopText = styled.Text`
  font-size: 14px;
  color: black;
  font-weight: 500;
`;
