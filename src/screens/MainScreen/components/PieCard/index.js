import React from 'react';

import Bonus from '../../../../assets/icons/bonus.svg';

import * as S from './styled';
const PieCard = ({index, pie, onPress}) => {
  return (
    <S.ShopCard onPress={onPress} activeOpacity={1} isFirst={!index}>
      <S.ShopImageContainer style={{elevation: 2}}>
        <S.ShopImage source={{uri: pie.src.url}} />
      </S.ShopImageContainer>
      <S.ShopText>{pie.title}</S.ShopText>
      <S.RankContainer>
        <S.Rank>{pie.rank}</S.Rank>
        <Bonus fill="#5954f9" />
      </S.RankContainer>
    </S.ShopCard>
  );
};

export default PieCard;
