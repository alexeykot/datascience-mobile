import React from 'react';

import People from '../../../../assets/icons/people.svg';

import * as S from './styled';

const PeopleCard = props => {
  const {member, onPress} = props;
  return (
    <S.Container onPress={onPress} isFirst={props.isFirst}>
      {!member.photo.url ? (
        <S.PeopleIconContainer>
          <People />
        </S.PeopleIconContainer>
      ) : (
        <S.PeopleAvatar source={{uri: member.photo.url}} />
      )}
      <S.Name numberOfLines={2}>{member.fullname}</S.Name>
      <S.Job>{member.fields.workPlace}</S.Job>
    </S.Container>
  );
};

export default PeopleCard;
