import styled from 'styled-components';

export const Container = styled.TouchableOpacity`
  margin-left: ${({isFirst}) => (isFirst ? 15 : 28)}px;
  align-items: center;
  max-width: 65px;
`;

export const Avatar = styled.Image``;

export const Name = styled.Text`
  color: #1b041e;
  font-family: Roboto;
  font-size: 12px;
  font-weight: 700;
  text-align: center;
  margin-top: 7px;
`;

export const Job = styled.Text`
  color: #8b8d94;
  font-family: Roboto;
  font-size: 12px;
  text-align: center;
  margin-top: 5px;
`;

export const PeopleIconContainer = styled.View`
  width: 54px;
  justify-content: center;
  align-items: center;
  height: 54px;
  background-color: #f5f3fa;
  border-radius: 27px;
`;

export const PeopleAvatar = styled.Image`
  width: 54px;
  height: 54px;
  border-radius: 27px;
`;
