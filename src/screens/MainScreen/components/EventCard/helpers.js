import moment from 'moment';
import 'moment/locale/ru';

export const formatEventTime = time =>
  `${moment
    .utc(time)
    .local()
    .locale('ru')
    .format('DD MMMM')}, ${moment(time)
    .utc(time)
    .local()
    .format('LT')}`;
