import React from 'react';
import LinearGradient from 'react-native-linear-gradient';
import {getTime} from '../../../../helpers/dateHelpers';
// import {formatEventTime} from './helpers';

import * as S from './styled';

const EventCard = ({onCardPress, event, isOne, styles = {}}) => {
  const getCity = () => {
    if (!event.place) {
      return '';
    }
    const city = event.place.split(',')[1];
    return city;
  };
  return (
    <S.EventCard style={styles} isOne={isOne} onPress={onCardPress}>
      {event.src ? <S.EventBg source={{uri: event.src.url}} /> : null}
      <S.EventInfoContainer>
        <S.EventTitle numberOfLines={4}>{event.title}</S.EventTitle>
        <S.Time>
          {getTime(event)} {event.place ? <S.Time>&#8226;</S.Time> : null}
          {getCity()}
        </S.Time>
        <LinearGradient
          colors={['transparent', 'white']}
          start={{x: 0, y: 0.2}}
          end={{x: 0, y: 0.83}}
          style={{
            position: 'absolute',
            bottom: 25,
            left: 0,
            right: 0,
            top: 0,
            zIndex: 1111,
            borderBottomRightRadius: 15,
          }}
        />
      </S.EventInfoContainer>
    </S.EventCard>
  );
};

export default EventCard;
