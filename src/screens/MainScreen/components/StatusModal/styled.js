import styled from 'styled-components';

export const LargeContainer = styled.View`
  width: 330px;
  height: 427px;
  padding: 20px;
  background-color: white;
  align-self: center;
  justify-content: center;
  align-items: center;
  border-radius: 30px;
  justify-content: center;
  align-items: center;
`;

export const Title = styled.Text`
  color: #1b041e;
  font-family: Roboto;
  font-size: 24px;
  font-weight: 700;
  text-align: center;
`;

export const StatusContainer = styled.View`
  height: 24px;
  border-radius: 12px;
  background-color: #5954f9;
  padding: 0 14px;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  margin-top: 20px;
`;

export const StatusText = styled.Text`
  color: #ffffff;
  font-family: Roboto;
  font-size: 14px;
  font-weight: 700;
  margin-left: 10px;
`;

export const BlackText = styled.Text`
  color: #1b041e;
  font-family: Roboto;
  font-size: 16px;
  font-weight: 700;
  text-align: center;
  margin-top: 20px;
`;

export const BlueText = styled.Text`
  color: #5954f9;
  font-family: Roboto;
  font-size: 16px;
  font-weight: 700;
  text-align: center;
  margin-top: 20px;
`;

export const HintContainer = styled.View`
  flex-direction: row;
  align-self: center;
  align-items: center;
  margin-top: 20px;
`;

export const HintText = styled.Text`
  color: #8b8d94;
  font-family: Roboto;
  font-size: 12px;
  font-weight: 400;
  margin-left: 10px;
`;

export const Button = styled.TouchableOpacity`
  width: 100%;
  height: 48px;
  elevation: 10;
  border-radius: 24px;
  background-color: #5954f9;
  justify-content: center;
  align-items: center;
  margin-top: 40px;
`;

export const ButtonText = styled.Text`
  color: #ffffff;
  font-family: Roboto;
  font-size: 14px;
  font-weight: 500;
`;

export const CloseButton = styled.TouchableOpacity`
  width: 50px;
  height: 50px;
  justify-content: center;
  align-items: center;
  position: absolute;
  top: 0;
  right: 0;
`;
