import React from 'react';
import Modal from 'react-native-modal';
import Close from '../../../../assets/icons/close-01.svg';
import Status from '../../../../assets/icons/status.svg';
import Guru from '../../../../assets/icons/guru-white.svg';
import ErrorWarning from '../../../../assets/icons/error-warning-line.svg';

import * as S from './styled';

const StatusModal = ({isVisible, onClose, onPress}) => {
  const close = () => {
    onClose();
  };
  const onButtonPress = () => {
    onClose();
    onPress();
  };
  return (
    <Modal onBackdropPress={close} isVisible={isVisible}>
      <S.LargeContainer>
        <S.CloseButton onPress={close}>
          <Close />
        </S.CloseButton>
        <Status height={70} width={70} />
        <S.Title>Ваш статус</S.Title>
        <S.StatusContainer>
          <Guru />
          <S.StatusText>Соучастник</S.StatusText>
        </S.StatusContainer>
        <S.BlackText>
          Участвуйте в мероприятиях, проявляйте активность и получайте новые
          статусы.
        </S.BlackText>
        <S.HintContainer>
          <ErrorWarning fill="#8b8d94" />
          <S.HintText>Механизм находится в разработке</S.HintText>
        </S.HintContainer>
        <S.Button onPress={onButtonPress}>
          <S.ButtonText>Выбрать мероприятие</S.ButtonText>
        </S.Button>
      </S.LargeContainer>
    </Modal>
  );
};

export default StatusModal;
