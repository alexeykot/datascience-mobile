import React from 'react';
import Modal from 'react-native-modal';
import Close from '../../../../assets/icons/close-01.svg';
import Speaker from '../../../../assets/icons/speaker.svg';

import * as S from './styled';

const SpeakerModal = ({isVisible, onClose}) => {
  const close = () => {
    onClose();
  };
  return (
    <Modal onBackdropPress={close} isVisible={isVisible}>
      <S.LargeContainer>
        <S.CloseButton onPress={close}>
          <Close />
        </S.CloseButton>
        <Speaker height={70} width={70} />
        <S.BlackText>
          Хотите стать спикером на мероприятии? Напишите нам на электронную
          почту
        </S.BlackText>
        <S.BlueText>speaker@ds-id.ru</S.BlueText>
      </S.LargeContainer>
    </Modal>
  );
};

export default SpeakerModal;
