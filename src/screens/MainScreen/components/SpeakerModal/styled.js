import styled from 'styled-components';

export const LargeContainer = styled.View`
  width: 320px;
  height: 270px;
  padding: 20px;
  background-color: white;
  align-self: center;
  justify-content: center;
  align-items: center;
  border-radius: 30px;
  justify-content: center;
  align-items: center;
`;

export const BlackText = styled.Text`
  color: #1b041e;
  font-family: Roboto;
  font-size: 16px;
  font-weight: 700;
  text-align: center;
  margin-top: 20px;
`;

export const BlueText = styled.Text`
  color: #5954f9;
  font-family: Roboto;
  font-size: 16px;
  font-weight: 700;
  text-align: center;
  margin-top: 20px;
`;

export const CloseButton = styled.TouchableOpacity`
  width: 50px;
  height: 50px;
  justify-content: center;
  align-items: center;
  position: absolute;
  top: 0;
  right: 0;
`;
