import styled from 'styled-components';

export const EventCard = styled.TouchableOpacity`
  width: 330px;
  height: 130px;
  background-color: white;
  margin-right: 8px;
  border-radius: 15px;
  flex-direction: row;
`;

export const EventBg = styled.Image`
  width: 125px;
  height: 100%;
  border-top-left-radius: 15px;
  border-bottom-left-radius: 15px;
`;

export const EventInfoContainer = styled.View`
  width: 205px;
  height: 100%;
  flex-direction: column;
  border-top-right-radius: 15px;
  border-bottom-right-radius: 15px;
  padding: 15px 15px 11px 13px;
  justify-content: space-between;
  z-index: 1;
`;

export const FadeImg = styled.Image`
  flex: 1;
  z-index: 123;
  position: absolute;
  right: 0;
  bottom: 0;
`;

export const EventTitle = styled.Text`
  color: black;
  font-weight: 700;
  font-size: 14px;
  z-index: 1;
`;
