import styled from 'styled-components';

export const Container = styled.View`
  /* height: 400px; */
  width: 100%;
  background-color: white;
  border-top-left-radius: 30px;
  border-top-right-radius: 30px;
  padding: 28px 30px 10px 30px;
`;

export const Row = styled.View`
  flex-direction: row;
  justify-content: space-between;
`;

export const Title = styled.Text`
  color: #1b041e;
  font-family: Roboto;
  font-size: 18px;
  font-weight: 700;
`;

export const Hint = styled.Text`
  color: #8b8d94;
  font-family: Roboto;
  font-size: 14px;
  font-weight: 400;
  margin-top: 16px;
`;

export const OptionsContainer = styled.View`
  margin-top: 32px;
`;

export const Option = styled.View`
  flex-direction: row;
  justify-content: space-between;
`;

export const Separator = styled.View`
  height: 1px;
  background-color: #c9ccd5;
  opacity: 0.5;
  width: 100%;
  margin-top: 15px;
  margin-bottom: 15px;
`;

export const OptionText = styled.Text`
  color: #1b041e;
  font-family: Roboto;
  font-size: 16px;
  font-weight: 700;
`;

export const DefaultButton = styled.TouchableOpacity`
  height: 48px;
  border-radius: 24px;
  border: 1px solid #c9ccd5;
  background-color: transparent;
  opacity: 0.5;
  width: 100%;
  justify-content: center;
  align-items: center;
`;

export const ButtonText = styled.Text`
  color: #1b041e;
  font-family: Roboto;
  font-size: 14px;
  font-weight: 700;
`;
