import React from 'react';
import {StyleSheet, Switch} from 'react-native';

import {
  setStatusForBlockRequest,
  setDefaultStatuses,
} from '../../../../redux/auth';
import DisplayBlockStorage from '../../../../services/storage/displayBlocks';
import Modal from 'react-native-modal';

import * as S from './styled';
import {connect} from 'react-redux';

const ModalWindow = ({
  isVisible,
  onClose,
  displayBlocks,
  setStatusForBlockRequest: setStatusForBlock,
  setDefaultStatuses: setDefaultStatus,
}) => {
  const close = async () => {
    await DisplayBlockStorage.save(displayBlocks);
    onClose();
  };
  return (
    <Modal
      onSwipeComplete={close}
      swipeDirection={['down']}
      onBackdropPress={close}
      style={styles.view}
      isVisible={isVisible}>
      <S.Container>
        <S.Title>Разделы на главном</S.Title>
        <S.Hint>
          Выберите, что вы хотели бы видеть на главном экране приложения
        </S.Hint>
        <S.OptionsContainer>
          {displayBlocks.map(option => (
            <>
              <S.Option>
                <S.OptionText>{option.name}</S.OptionText>
                <Switch
                  trackColor={{false: 'grey', true: '#5954f9'}}
                  thumbColor="#ffffff"
                  ios_backgroundColor="#3e3e3e"
                  onValueChange={() =>
                    setStatusForBlock(option.id, !option.enabled)
                  }
                  value={option.enabled}
                />
              </S.Option>
              <S.Separator />
            </>
          ))}
        </S.OptionsContainer>
        <S.DefaultButton onPress={() => setDefaultStatus()}>
          <S.ButtonText>Вернуть по умолчанию</S.ButtonText>
        </S.DefaultButton>
      </S.Container>
    </Modal>
  );
};

const styles = StyleSheet.create({
  view: {
    justifyContent: 'flex-end',
    margin: 0,
  },
  wrapper: {
    flex: 1,
  },
});

const mapStateToProps = ({auth}) => ({
  displayBlocks: auth.displayBlocks,
});

const mapDispatchToProps = {
  setStatusForBlockRequest,
  setDefaultStatuses,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ModalWindow);
