import styled from 'styled-components';

export const Container = styled.ScrollView`
  flex: 1;
  background-color: white;
`;

export const ImageBg = styled.ImageBackground`
  flex: 1;
`;
export const Header = styled.View`
  width: 100%;
  height: 42px;
  padding: 17px 15px 0 15px;
  flex-direction: row;
  justify-content: space-between;
`;

export const RoundButtonLeft = styled.TouchableOpacity`
  width: 40px;
  height: 40px;
  border-radius: 20px;
  background-color: #6a33e6;
  justify-content: center;
  align-items: center;
`;

export const RoundButtonRight = styled.TouchableOpacity`
  width: 40px;
  height: 40px;
  border-radius: 20px;
  background-color: #6a33e6;
  justify-content: center;
  align-items: center;
`;

export const NotificationsHint = styled.View`
  width: 10px;
  height: 10px;
  border-radius: 10px;
  position: absolute;
  top: 6px;
  right: 6px;
  background-color: red;
`;

export const FindInputContainer = styled.View``;

export const FindInput = styled.TextInput`
  width: 228px;
  height: 40px;
  border-radius: 20px;
  background-color: #6a33e6;
  justify-content: space-between;
  align-items: center;
  flex-direction: row;
  padding: 0 25px 0 12px;
  color: white;
  opacity: 0.4;
`;

export const SearchIconContainer = styled.View`
  position: absolute;
  right: 20px;
  z-index: 1111;
  top: 14px;
`;

export const ResetButton = styled.TouchableOpacity`
  width: 50px;
  height: 40px;
  justify-content: center;
  align-items: center;
  position: absolute;
  right: 0px;
  z-index: 1111;
  /* background-color: red; */
`;

export const FindText = styled.Text`
  font-size: 14px;
  font-weight: 500;
  color: white;
`;

export const CardContainer = styled.View`
  width: 100%;
  padding: 0 15px;
`;

export const MoreButton = styled.TouchableOpacity`
  position: absolute;
  right: 17px;
  top: 19px;
  width: 25px;
  height: 25px;
  justify-content: center;
  align-items: center;
`;

export const UserCard = styled.View`
  width: 100%;
  height: 216px;
  background-color: rgba(98, 94, 249, 0.4);
  /* opacity: 0.38; */
  border-radius: 20px;
  /* box-shadow: 0 5px 48px 3px rgba(19, 57, 110, 0.6); */
  margin-top: 98px;
  flex-direction: column;
  align-items: center;
`;

// export const UserCardBg = styled.ImageBackground`
//   width: 100%;
//   height: 216px;
//   background-color: transparent;
//   margin-top: 82px;
//   flex-direction: column;
//   align-items: center;
// `;

export const UserAvatar = styled.Image`
  width: 120px;
  height: 120px;
  border-radius: 120px;
  position: absolute;
  /* left: 30%; */
  align-self: center;
  top: -50px;
  background-color: #d7f5ff;
`;

export const PeopleIconContainer = styled.View`
  width: 54px;
  justify-content: center;
  align-items: center;
  height: 54px;
  background-color: #f5f3fa;
  border-radius: 27px;
`;

export const UserName = styled.Text`
  font-size: 18px;
  font-weight: 700;
  color: white;
  margin-top: 78px;
  margin-bottom: 8px;
`;

// export const ParticipantImage = styled.ImageBackground`
//   width: 117px;
//   height: 24px;
//   justify-content: center;
//   align-items: center;
// `;

export const ParticipantImage = styled.TouchableOpacity`
  /* width: 80px; */
  padding: 0 14px;
  height: 24px;
  border-radius: 12px;
  background-color: #ffffff;
  /* justify-content: center; */
  align-items: center;
  flex-direction: row;
`;

export const ParticipantText = styled.Text`
  font-size: 14px;
  color: #5954f9;
  font-weight: 700;
  margin-left: 10px;
`;

export const UserInfo = styled.View`
  height: 50px;
  width: 100%;
  margin-top: 15px;
  padding: 0 30px;
  flex-direction: row;
  justify-content: space-between;
`;

export const InfoBlock = styled.View`
  height: 50px;
  flex-direction: column;
  align-items: center;
`;

export const InfoBlockTouch = styled.TouchableOpacity`
  height: 50px;
  flex-direction: column;
  align-items: center;
`;

export const BonusRow = styled.View`
  flex-direction: row;
  align-items: center;
`;

export const BlockTitle = styled.Text`
  font-size: 14px;
  color: white;
  font-weight: 400;
`;

export const BlockInfo = styled.Text`
  font-size: 24px;
  color: white;
  font-weight: 700;
`;

export const Title = styled.Text`
  font-size: 16px;
  font-weight: 700;
  color: white;
  margin-top: 24px;
  margin-bottom: 20px;
`;

export const EventsScroll = styled.ScrollView`
  width: 100%;
`;

export const ScrollContainer = styled.View`
  width: 100%;
  /* padding-left: 15px; */
`;

export const Time = styled.Text`
  color: #8b8d94;
  font-size: 12px;
  font-weight: 400;
  position: absolute;
  bottom: 10px;
  left: 20px;
`;

export const WhereContainer = styled.View`
  width: 100%;
  background-color: white;
  /* border-top-left-radius: 30px;
  border-top-right-radius: 30px;
  margin-top: 30px; */
  padding-bottom: 24px;
`;

export const WhereActions = styled.View`
  width: 100%;
  padding: ${({isPaddingBottom}) =>
    isPaddingBottom ? '0px 15px 19px 15px' : '0px 15px 0 15px'};
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`;

export const CrossActions = styled.View`
  width: 100%;
  flex-direction: row;
  justify-content: space-between;
  margin-bottom: 12px;
  padding: 0 15px;
`;

export const WhereTitle = styled.Text`
  color: #1b041e;
  font-family: Roboto;
  font-size: 18px;
  font-weight: 700;
`;

export const TitleRow = styled.View`
  flex-direction: row;
  align-items: center;
`;

export const ViewAllButton = styled.TouchableOpacity``;

export const ViewAllText = styled.Text`
  color: #5954f9;
  font-size: 14px;
  font-weight: 500;
`;

export const WhereScroll = styled.ScrollView`
  width: 100%;
  height: 314px;
  padding-left: 15px;
  padding-right: 15px;
`;

export const WhereEventCard = styled.TouchableOpacity`
  width: 254px;
  height: 280px;
  background-color: black;
  margin-right: 8px;
  border-radius: 20px;
`;

export const WhereEventBg = styled.Image`
  width: 254px;
  height: 135px;
  border-top-left-radius: 20px;
  border-top-right-radius: 20px;
  justify-content: flex-end;
`;

export const FadeBg = styled.Image`
  width: 254px;
  height: 103px;
  position: absolute;
  top: 80px;
`;

export const WhereInfoContainer = styled.View`
  /* margin-top: 82px; */
  margin-left: 20px;
  padding-right: 20px;
  position: absolute;
  bottom: 27px;
`;

export const WhereEventTitle = styled.Text`
  font-size: 14px;
  color: white;
  font-weight: 700;
  margin-top: 5px;
`;

export const NewEventContainer = styled.View`
  width: 100%;
  height: 213px;
  background-color: white;
  padding: 19px 0;
`;

export const NewEventActions = styled.View`
  width: 100%;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  padding: 0 15px;
  margin-bottom: 12px;
`;

export const Icon = styled.Image``;

export const CrossContainer = styled.View`
  width: 100%;
  /* height: 190px; */
  background-color: white;
  padding: 24px 0;
`;

export const PeopleContainer = styled.View`
  width: 100%;
  justify-content: center;
  align-items: center;
  margin-top: 21px;
  padding-horizontal: 23px;
`;

export const NoEventsContainer = styled.View`
  width: 100%;
  height: 120px;
  justify-content: center;
  align-items: center;
  background-color: transparent;
  padding-horizontal: 35px;
`;

export const NoEventsText = styled.Text`
  color: #a8a3f7;
  font-family: Roboto;
  font-size: 14px;
  font-weight: 400;
  line-height: 16px;
  text-align: center;
  margin-top: 15px;
`;

export const NoEventsTextWhite = styled.Text`
  color: white;
  font-family: Roboto;
  font-size: 14px;
  font-weight: 400;
  line-height: 16px;
`;

export const BorderContainer = styled.View`
  width: 100%;
  border-top-left-radius: 30px;
  border-top-right-radius: 30px;
  background-color: white;
  height: 24px;
  margin-top: 30px;
`;

export const NoEventsLogo = styled.View`
  width: 54px;
  height: 54px;
  justify-content: center;
  align-items: center;
  background-color: #f5f3fa;
  opacity: 0.1;
  border-radius: 27px;
`;
export const PeopleScroll = styled.ScrollView`
  width: 100%;
`;

export const HintText = styled.Text`
  margin-top: 11px;
  color: #cacbce;
  text-align: center;
  font-size: 14px;
  font-weight: 500;
`;

export const Touchable = styled.TouchableOpacity``;

export const HintColorText = styled.Text`
  color: #5954f9;
  /* text-align: center; */
  font-size: 14px;
  font-weight: 500;
`;
export const ShopContainer = styled.View`
  background-color: white;
  width: 100%;
  height: 236px;
`;

export const FakeView = styled.View`
  width: 100%;
  height: ${({height}) => height}px;
  background-color: transparent;
  justify-content: center;
  align-items: center;
`;

export const StatusContainer = styled.View`
  padding: 8px 9px;
  background-color: ${({color}) => color};
  /* position: absolute;
  bottom: 90;
  left: 20; */
  border-radius: 12px;
  width: 140px;
  height: 24px;
  justify-content: center;
  align-items: center;
`;

export const StatusText = styled.Text`
  color: #ffffff;
  font-family: Roboto;
  font-size: 10px;
  font-weight: 700;
  text-transform: uppercase;
`;
