/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {Dimensions, ActivityIndicator, StatusBar} from 'react-native';
import {connect} from 'react-redux';
import _ from 'lodash';
import LinearGradient from 'react-native-linear-gradient';
import AnimatedLinearGradient from 'react-native-animated-linear-gradient';
import Carousel, {Pagination} from 'react-native-snap-carousel';
import CrossModal from '../../components/Modals/CrossModal';

import PieCard from './components/PieCard';
import NewsCard from './components/NewsCard';
import EventCard from './components/EventCard';
import PeopleCard from './components/PeopleCard';

import {saveNewNotificaion, makeNotificationsUnseen} from '../../redux/auth';
import {setSocket} from '../../redux/chat';
import {setMyEvents} from '../../redux/events';

//IMAGES PNG
import more from '../../assets/images/more.png';
import userBg from '../../assets/images/userBg.png';
import fadeBg from '../../assets/images/fadeBg.png';
import mainBg from '../../assets/images/mainBg.png';
import testCardImage from '../../assets/images/testCardImage.png';
import setka from '../../assets/images/setkaVert.png';

//ICONS SVG
import AvatarIcon from '../../assets/icons/avatarIcon.svg';
import Search from '../../assets/icons/search.svg';
import User from '../../assets/icons/user.svg';
import Id from '../../assets/icons/id.svg';
import Guru from '../../assets/icons/guru.svg';
import Bonus from '../../assets/icons/bonus.svg';
import Question from '../../assets/icons/question.svg';
import More from '../../assets/icons/more-2-fill.svg';
import People from '../../assets/icons/people.svg';
import EventsIcon from '../../assets/icons/event.svg';
import Reset from '../../assets/icons/close-circle-fill.svg';

import * as S from './styled';
import http from '../../services/http';
import * as sockets from '../../../startSocket';
import ChatStorage from '../../services/storage/chat';
import moment from 'moment';
import {
  formatMembers,
  displayBlock,
  isClientEvent,
  getClientStatus,
} from './helpers';
import ModalWindow from './components/Modal';
import {handleAndroidBackButton, exitAlert} from '../../helpers/backHandler';
import SpeakerModal from './components/SpeakerModal';
import StatusModal from './components/StatusModal';
import {getTime} from '../../helpers/dateHelpers';

const colors = [
  'rgb(106, 57, 171)',
  'rgb(151, 52, 160)',
  'rgb(57, 104, 171)',
  'rgb(54, 52, 160)',
];

const statusColors = {
  0: '#ffa500',
  1: '#3dd576',
  2: '#ffa500',
};

const statuses = {
  0: 'Жду подтверждения',
  1: 'Я зарегистрирован',
};

class MainScreen extends React.Component {
  state = {
    pies: [],
    news: [],
    events: [],
    clientEvents: [],
    activeNews: 0,
    activeEvent: 0,
    crossMembers: [],
    isUnseen: false,
    isModalVisible: false,
    isCrossModalVisible: false,
    bonus: 0,
    speakerModalVisible: false,
    statusModalVisible: false,
    searchString: '',
    // LOADERS
    loading: false,
    piesLoading: false,
    newsLoading: false,
    eventsLoading: false,
    clientEventsLoading: false,
    bonusLoading: false,
    isComponentMounted: false,
  };

  componentWillReceiveProps(nextProps) {
    if (!_.isEqual(this.props.notifications, nextProps.notifications)) {
      const isUnseen = nextProps.notifications.some(notif => !notif.seen);
      if (isUnseen) {
        this.props.makeNotificationsUnseen();
      }
      // this.setState({isUnseen: isUnseen});
    }
  }

  componentWillMount() {
    this.props.navigation.addListener('focus', async () => {
      handleAndroidBackButton(exitAlert);
      // this.fetchInfo();
      if (this.state.isComponentMounted) {
        this.checkInfo();
      }
    });
    this.props.navigation.addListener('blur', async () => {
      handleAndroidBackButton(() => this.props.navigation.goBack());
    });
  }

  componentWillUnmount() {
    this.setState({isComponentMounted: false});
  }

  async componentDidMount() {
    this.setState({isComponentMounted: true});
    const isUnseen = this.props.notifications.some(notif => !notif.seen);
    if (isUnseen) {
      this.props.makeNotificationsUnseen();
    }
    //SOCKET CONNECTION
    try {
      const {data: chatData} = await http.get(
        `/cabinet/profile/token?access-token=${this.props.token}`,
      );
      console.log('Socket Conenction Token', chatData);
      await ChatStorage.save(chatData.data.entity);
      const socket = new WebSocket('wss://chat-ai-id.dev-vps.ru/websocket');
      this.props.setSocket(socket);
      socket.onopen = () => {
        console.log('Socket Opened');
        sockets.connect(socket);
        sockets.login(socket, chatData.data.entity);
        sockets.subscribe(socket);
      };
    } catch (err) {
      console.log('Chat Error');
    }

    this.fetchInfo();
  }

  checkInfo = async () => {
    try {
      const bonus = await http.get(
        `/score/balance/index?access-token=${this.props.token}`,
      );
      this.setState({bonus: bonus.data.data.entity.value});
    } catch (err) {
      console.log('bonus-err', err);
    }
    try {
      const clientEvents = await http.get(
        `/event/client/index?access-token=${this.props.token}`,
      );
      this.props.setMyEvents(clientEvents.data.data.entities);
      this.setState({
        clientEvents: clientEvents.data.data.entities,
      });
    } catch (err) {
      console.log('clientEvents-err', err);
    }
    try {
      const events = await http.get(
        `/event/event/index?access-token=${this.props.token}`,
      );
      this.setState({events: events.data.data.entities});
    } catch (err) {
      console.log('events-err', err);
    }
    try {
      const {data} = await http.get(
        `/pie/default/index?access-token=${this.props.token}`,
      );
      this.setState({pies: data.data.entities});
    } catch (err) {
      console.log('pie-err', err);
    }
    try {
      const news = await http.get(
        `/news/default/index?access-token=${this.props.token}`,
      );
      this.setState({news: news.data.data.entities});
    } catch (err) {
      console.log('news-err', err);
    }
    try {
      const crossMembers = await http.get(
        `/event/client/environment?access-token=${this.props.token}`,
      );
      this.setState({crossMembers: crossMembers.data.data.entities});
    } catch (err) {
      console.log('crossMembers-err', err);
    }
    // this.setState({loading: false});
  };

  fetchInfo = async () => {
    // this.setState({loading: true});
    try {
      this.setState({bonusLoading: true});
      const bonus = await http.get('/score/balance/index');
      this.setState({bonus: bonus.data.data.entity.value, bonusLoading: false});
    } catch (err) {
      this.setState({bonusLoading: false});
      console.log('bonus-err', err);
    }
    try {
      this.setState({clientEventsLoading: true});
      const clientEvents = await http.get('/event/client/index');
      this.props.setMyEvents(clientEvents.data.data.entities);
      this.setState({
        clientEvents: clientEvents.data.data.entities,
        clientEventsLoading: false,
      });
      console.log('clientEvents', clientEvents);
    } catch (err) {
      this.setState({clientEventsLoading: false});
      console.log('clientEvents-err', err);
    }
    try {
      this.setState({eventsLoading: true});
      const events = await http.get('/event/event/index');
      console.log('allEvents', events);
      this.setState({events: events.data.data.entities, eventsLoading: false});
    } catch (err) {
      this.setState({eventsLoading: false});
      console.log('events-err', err);
    }
    try {
      this.setState({piesLoading: true});
      const {data} = await http.get('/pie/default/index');
      this.setState({pies: data.data.entities, piesLoading: false});
    } catch (err) {
      this.setState({piesLoading: false});
      console.log('pie-err', err);
    }
    try {
      this.setState({newsLoading: true});
      const news = await http.get('/news/default/index');
      this.setState({news: news.data.data.entities, newsLoading: false});
    } catch (err) {
      this.setState({newsLoading: false});
      console.log('news-err', err);
    }
    try {
      const crossMembers = await http.get('/event/client/environment');
      this.setState({crossMembers: crossMembers.data.data.entities});
    } catch (err) {
      console.log('crossMembers-err', err);
    }
    // this.setState({loading: false});
  };

  get pagination() {
    return (
      <>
        {this.props.user ? (
          <Pagination
            dotsLength={this.state.news.slice(0, 5).length}
            // containerStyle={{ height: 60 }}
            activeDotIndex={this.state.activeNews}
            dotStyle={{
              width: 14,
              height: 4,
              borderRadius: 2,
              backgroundColor: '#5954f9',
            }}
            inactiveDotStyle={{
              width: 4,
              height: 4,
              borderRadius: 5,
              backgroundColor: '#5954f9',
            }}
            containerStyle={{
              marginTop: -47,
              marginBottom: -35,
            }}
            inactiveDotOpacity={1}
            inactiveDotScale={1}
          />
        ) : null}
      </>
    );
  }

  get eventPagination() {
    return (
      <>
        {this.props.user ? (
          <Pagination
            dotsLength={this.withoutLastEvents(this.state.clientEvents).length}
            dotContainerStyle={{
              marginHorizontal: 4,
            }}
            activeDotIndex={this.state.activeEvent}
            dotStyle={{
              width: 14,
              height: 4,
              borderRadius: 2,
              backgroundColor: 'white',
            }}
            inactiveDotStyle={{
              width: 4,
              height: 4,
              borderRadius: 5,
              backgroundColor: 'white',
            }}
            containerStyle={{
              marginTop: -20,
              marginBottom: -40,
            }}
            inactiveDotOpacity={1}
            inactiveDotScale={1}
          />
        ) : null}
      </>
    );
  }

  navigateToPies = () => {
    const {pies} = this.state;
    const categories = pies
      .map(pie => {
        if (pie.categories.length) {
          return pie.categories;
        }
      })
      .filter(c => c !== undefined)
      .flat();
    const uniqCategories = _.uniqBy(categories, c => c.id);
    this.props.navigation.navigate('PiesScreen', {
      items: pies,
      categories: uniqCategories,
    });
  };

  getCity = event => {
    if (!event.place) {
      return '';
    }
    const city = event.place.split(',')[1];
    return city;
  };

  withoutLastEvents = events => {
    if (!events.length) {
      return [];
    }
    return events.filter(ev => !moment().isAfter(ev.finishDate));
  };

  render() {
    const {
      pies,
      news,
      events,
      clientEvents,
      crossMembers,
      bonus,
      loading,
      bonusLoading,
      clientEventsLoading,
      eventsLoading,
      newsLoading,
      piesLoading,
      searchString,
    } = this.state;
    console.log('events', events);
    const {isUnseen, displayBlocks} = this.props;
    return (
      <S.Container>
        <StatusBar barStyle="light-content" backgroundColor="#5954f9" />
        <ModalWindow
          onClose={() => this.setState({isModalVisible: false})}
          isVisible={this.state.isModalVisible}
        />
        <CrossModal
          onClose={() => this.setState({isCrossModalVisible: false})}
          isVisible={this.state.isCrossModalVisible}
        />
        <AnimatedLinearGradient
          points={{start: {x: 0, y: 0}, end: {x: 1, y: 0}}}
          customColors={colors}
          speed={2000}
        />
        <S.Header>
          <S.RoundButtonLeft
            onPress={() =>
              this.props.navigation.navigate('ProfileScreen', {
                bonus: this.state.bonus,
              })
            }>
            <User fill="white" />
          </S.RoundButtonLeft>
          {/* <S.FindInput
            onPress={() =>
              this.props.navigation.navigate('SearchScreen', {
                searchString: 'Все',
              })
            }> */}
          <S.FindInputContainer>
            <S.FindInput
              onChangeText={text => this.setState({searchString: text})}
              value={this.state.searchString}
              onSubmitEditing={() => {
                if (searchString) {
                  this.props.navigation.navigate('SearchScreen', {
                    searchString: this.state.searchString,
                  });
                  this.setState({searchString: ''});
                }
              }}
              placeholder="Поиск"
              placeholderTextColor="white"
            />
            {!searchString ? (
              <S.SearchIconContainer>
                <Search fill="white" />
              </S.SearchIconContainer>
            ) : (
              <S.ResetButton onPress={() => this.setState({searchString: ''})}>
                <Reset />
              </S.ResetButton>
            )}
          </S.FindInputContainer>
          {/* <S.FindText>Поиск</S.FindText> */}
          {/* <Search fill="white" /> */}
          <S.RoundButtonRight
            onPress={() =>
              this.props.navigation.navigate('NotificationsScreen')
            }>
            {isUnseen ? <S.NotificationsHint /> : null}
            <Id fill="white" />
          </S.RoundButtonRight>
        </S.Header>
        <S.CardContainer>
          <S.UserCard>
            <S.UserAvatar
              source={{
                uri: this.props.user.photo.url
                  ? this.props.user.photo.url
                  : 'data:image/jpeg;base64,' + this.props.avatar,
              }}
            />
            <S.MoreButton onPress={() => this.setState({isModalVisible: true})}>
              {/* <S.Icon source={more} /> */}
              <More />
            </S.MoreButton>
            {this.props.user ? (
              <S.UserName>
                {this.props.user.name} {this.props.user.surname}
              </S.UserName>
            ) : null}
            <S.ParticipantImage
              onPress={() => this.setState({statusModalVisible: true})}>
              <Guru />
              <StatusModal
                onPress={() =>
                  this.props.navigation.navigate('AllEventsScreen')
                }
                isVisible={this.state.statusModalVisible}
                onClose={() => this.setState({statusModalVisible: false})}
              />
              <S.ParticipantText>Соучастник</S.ParticipantText>
            </S.ParticipantImage>
            <S.UserInfo>
              <S.InfoBlock>
                <S.BlockTitle>Куда иду</S.BlockTitle>
                {!clientEventsLoading ? (
                  <S.BlockInfo>
                    {this.withoutLastEvents(clientEvents).length}
                  </S.BlockInfo>
                ) : (
                  <ActivityIndicator size="small" color="white" />
                )}
              </S.InfoBlock>
              <S.InfoBlock>
                <S.BlockTitle>Мой счет</S.BlockTitle>
                <S.BonusRow>
                  {!bonusLoading ? (
                    <S.BlockInfo style={{marginRight: 9}}>{bonus}</S.BlockInfo>
                  ) : (
                    <ActivityIndicator size="small" color="white" />
                  )}
                  <Bonus fill="white" />
                </S.BonusRow>
              </S.InfoBlock>
              <S.InfoBlockTouch
                onPress={() => this.setState({speakerModalVisible: true})}>
                <S.BlockTitle>Я - спикер</S.BlockTitle>
                <S.BlockInfo>?</S.BlockInfo>
              </S.InfoBlockTouch>
              <SpeakerModal
                isVisible={this.state.speakerModalVisible}
                onClose={() => this.setState({speakerModalVisible: false})}
              />
            </S.UserInfo>
          </S.UserCard>
        </S.CardContainer>
        <S.ScrollContainer>
          <S.Title style={{paddingLeft: 15}}>Мои ближайшие мероприятия</S.Title>
          {!clientEventsLoading ? (
            <>
              {this.withoutLastEvents(clientEvents).length ? (
                <Carousel
                  layout={'default'}
                  data={this.withoutLastEvents(clientEvents)}
                  containerCustomStyle={{
                    paddingLeft:
                      this.withoutLastEvents(clientEvents).length === 1
                        ? 0
                        : 15,
                  }}
                  renderItem={({item, index}) => (
                    <EventCard
                      isOne={this.withoutLastEvents(clientEvents).length === 1}
                      key={item.id}
                      event={item}
                      onCardPress={() =>
                        this.props.navigation.navigate('EventMainScreen', {
                          event: item,
                          status: item.requests[0].status.id,
                        })
                      }
                    />
                  )}
                  inactiveSlideScale={1}
                  inactiveSlideOpacity={1}
                  activeSlideAlignment={
                    this.withoutLastEvents(clientEvents).length === 1
                      ? 'center'
                      : 'start'
                  }
                  sliderWidth={Dimensions.get('window').width}
                  itemWidth={
                    this.withoutLastEvents(clientEvents).length === 1
                      ? 330
                      : 338
                  }
                  onSnapToItem={index => this.setState({activeEvent: index})}
                />
              ) : (
                <S.NoEventsContainer>
                  <S.NoEventsLogo>
                    <EventsIcon />
                  </S.NoEventsLogo>
                  <S.NoEventsText>
                    <S.NoEventsTextWhite
                      onPress={() =>
                        this.props.navigation.navigate('AllEventsScreen')
                      }>
                      Зарегистрируйтесь{' '}
                    </S.NoEventsTextWhite>
                    на одно из мероприятий и оно появится здесь!
                  </S.NoEventsText>
                </S.NoEventsContainer>
              )}
            </>
          ) : (
            <S.FakeView height={120}>
              <ActivityIndicator size="large" color="white" />
            </S.FakeView>
          )}
          {this.eventPagination}
        </S.ScrollContainer>
        <S.BorderContainer />
        {displayBlock(displayBlocks, 'where') &&
        this.withoutLastEvents(events).length ? (
          <S.WhereContainer>
            <S.WhereActions isPaddingBottom>
              <S.WhereTitle style={{fontFamily: 'Roboto'}}>
                Куда пойти
              </S.WhereTitle>
              <S.ViewAllButton
                onPress={() =>
                  this.props.navigation.navigate('AllEventsScreen')
                }>
                <S.ViewAllText style={{fontFamily: 'Roboto'}}>
                  Все
                </S.ViewAllText>
              </S.ViewAllButton>
            </S.WhereActions>
            {!eventsLoading ? (
              <Carousel
                layout={'default'}
                data={this.withoutLastEvents(events)
                  .sort((a, b) => moment(a.beginDate).diff(b.beginDate))
                  .slice(0, 10)}
                containerCustomStyle={{
                  paddingLeft:
                    this.withoutLastEvents(events).length > 1 ? 15 : 0,
                }}
                renderItem={({item, index}) => {
                  return (
                    <S.WhereEventCard
                      activeOpacity={0.8}
                      onPress={() =>
                        this.props.navigation.navigate('EventMainScreen', {
                          event: item,
                          status: getClientStatus(
                            item,
                            this.state.clientEvents,
                          ),
                        })
                      }>
                      <S.WhereEventBg source={{uri: item.src.url}} />
                      <S.FadeBg source={fadeBg} />
                      <S.WhereInfoContainer>
                        {isClientEvent(item, this.state.clientEvents) ? (
                          <S.StatusContainer
                            color={
                              statusColors[
                                getClientStatus(item, this.state.clientEvents)
                              ]
                            }>
                            <S.StatusText>
                              {
                                statuses[
                                  getClientStatus(item, this.state.clientEvents)
                                ]
                              }
                            </S.StatusText>
                          </S.StatusContainer>
                        ) : null}
                        <S.WhereEventTitle numberOfLines={3}>
                          {item.title}
                        </S.WhereEventTitle>
                        {/* <LinearGradient
                          colors={['transparent', 'black']}
                          start={{x: 0, y: 0}}
                          end={{x: 0, y: 1}}
                          style={{
                            position: 'absolute',
                            bottom: 0,
                            left: 0,
                            right: 0,
                            top: 0,
                            zIndex: 1111,
                          }}
                        /> */}
                      </S.WhereInfoContainer>
                      <S.Time numberOfLines={1}>
                        {getTime(item)}
                        {item.place ? (
                          <S.Time>
                            {'    '}
                            &#8226;
                            {'    '}
                          </S.Time>
                        ) : null}
                        {this.getCity(item)}
                      </S.Time>
                    </S.WhereEventCard>
                  );
                }}
                inactiveSlideScale={1}
                inactiveSlideOpacity={1}
                activeSlideAlignment={
                  this.withoutLastEvents(events).length > 1 ? 'start' : 'center'
                }
                sliderWidth={Dimensions.get('window').width}
                itemWidth={
                  this.withoutLastEvents(events).length > 1 ? 262 : 254
                }
              />
            ) : (
              <S.FakeView height={280}>
                <ActivityIndicator size="large" color="black" />
              </S.FakeView>
            )}
          </S.WhereContainer>
        ) : null}
        {displayBlock(displayBlocks, 'new') ? (
          <S.NewEventContainer>
            <S.NewEventActions>
              <S.WhereTitle>Что нового?</S.WhereTitle>
              <S.ViewAllButton
                onPress={() =>
                  this.props.navigation.navigate('NewsScreen', {
                    items: this.state.news,
                    onCardPress: item =>
                      this.props.navigation.navigate('NewsDetailsScreen', {
                        event: item,
                      }),
                  })
                }>
                <S.ViewAllText>Все</S.ViewAllText>
              </S.ViewAllButton>
            </S.NewEventActions>
            {!newsLoading ? (
              <>
                <Carousel
                  layout={'default'}
                  data={news.slice(0, 5)}
                  containerCustomStyle={{paddingLeft: 15}}
                  renderItem={({item, index}) => (
                    <NewsCard
                      onCardPress={() =>
                        this.props.navigation.navigate('NewsDetailsScreen', {
                          event: item,
                        })
                      }
                      key={item.id}
                      news={item}
                    />
                  )}
                  inactiveSlideScale={1}
                  inactiveSlideOpacity={1}
                  activeSlideAlignment="start"
                  sliderWidth={Dimensions.get('window').width}
                  itemWidth={338}
                  loop
                  onSnapToItem={index => this.setState({activeNews: index})}
                />
                {this.pagination}
              </>
            ) : (
              <S.FakeView height={120}>
                <ActivityIndicator size="large" color="black" />
              </S.FakeView>
            )}
          </S.NewEventContainer>
        ) : null}
        {displayBlock(displayBlocks, 'cross') ? (
          <S.CrossContainer>
            <S.CrossActions>
              <S.TitleRow>
                <S.WhereTitle style={{marginRight: 10}}>
                  С кем пересекался
                </S.WhereTitle>
                <S.Touchable
                  onPress={() => this.setState({isCrossModalVisible: true})}>
                  <Question />
                </S.Touchable>
              </S.TitleRow>
              <S.ViewAllButton
                onPress={() =>
                  this.props.navigation.navigate('CrossMembersScreen', {
                    crossMembers: crossMembers,
                  })
                }>
                <S.ViewAllText>Все</S.ViewAllText>
              </S.ViewAllButton>
            </S.CrossActions>
            {!crossMembers.length ? (
              <S.PeopleContainer>
                <S.PeopleIconContainer>
                  <People />
                </S.PeopleIconContainer>
                <S.HintText>
                  Посетите{' '}
                  <S.HintColorText
                    onPress={() =>
                      this.props.navigation.navigate('AllEventsScreen')
                    }>
                    любое мероприятие
                  </S.HintColorText>{' '}
                  и здесь появится люди, с которыми вы пересекались
                </S.HintText>
              </S.PeopleContainer>
            ) : (
              <S.PeopleScroll horizontal showsHorizontalScrollIndicator={false}>
                {crossMembers.map((member, index) => (
                  <PeopleCard
                    onPress={() =>
                      this.props.navigation.navigate('MemberScreen', {
                        member: member,
                      })
                    }
                    member={member}
                    isFirst={!index}
                  />
                ))}
              </S.PeopleScroll>
            )}
          </S.CrossContainer>
        ) : null}
        {displayBlock(displayBlocks, 'pies') ? (
          <S.ShopContainer>
            <S.WhereActions>
              <S.WhereTitle>Приятные плюшки</S.WhereTitle>
              <S.ViewAllButton onPress={this.navigateToPies}>
                <S.ViewAllText>Все</S.ViewAllText>
              </S.ViewAllButton>
            </S.WhereActions>
            <S.EventsScroll horizontal showsHorizontalScrollIndicator={false}>
              {pies.length
                ? pies.map((pie, index) => (
                    <PieCard
                      onPress={() =>
                        this.props.navigation.navigate('PieScreen', {
                          event: pie,
                        })
                      }
                      key={pie.id}
                      pie={pie}
                      index={index}
                    />
                  ))
                : null}
            </S.EventsScroll>
          </S.ShopContainer>
        ) : null}
        {/* <Image
          source={setka}
          style={{
            position: 'absolute',
            top: -18,
            left: 8,
            right: 0,
            bottom: 0,
          }}
        /> */}
        {/* <Image
          source={setka}
          style={{
            position: 'absolute',
            top: -18,
            left: 0,
            right: 0,
            bottom: 0,
          }}
        /> */}
      </S.Container>
    );
  }
}
const mapStateToProps = ({auth}) => ({
  user: auth.userData,
  token: auth.token,
  avatar: auth.avatar,
  notifications: auth.notifications,
  isUnseen: auth.isUnseen,
  displayBlocks: auth.displayBlocks,
});

const mapDispatchToProps = {
  saveNewNotificaion,
  makeNotificationsUnseen,
  setSocket,
  setMyEvents,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(MainScreen);
