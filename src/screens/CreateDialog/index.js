/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import Header from '../Event/components/Header';
import {StyleSheet, View} from 'react-native';

import Search from '../../assets/icons/search.svg';
import * as S from './styled';
import {setSocket} from '../../redux/chat';
import * as sockets from '../../../startSocket';
import ChatStorage from '../../services/storage/chat';
import http from '../../services/http';
import Contact from './components/Contact';
import {connect} from 'react-redux';

class CreateDialog extends React.Component {
  state = {
    contacts: [],
    filterText: '',
  };
  componentWillMount() {
    this.props.navigation.addListener('focus', async () => {
      const {data} = await http.get('/event/client/environment?chatUserOnly=1');
      this.setState({contacts: data.data.entities});
    });
  }
  createDirectMessage = async (userName, fullName) => {
    let {socket} = this.props.route.params;
    console.log('userName', userName, this.state.contacts, socket);
    if (socket.readyState === 3) {
      console.log('readyState - Closed');
      const {data: chatData} = await http.get(
        `/cabinet/profile/token?access-token=${this.props.token}`,
      );
      console.log('Socket Conenction Token', chatData);
      await ChatStorage.save(chatData.data.entity);
      socket = new WebSocket('wss://chat-ai-id.dev-vps.ru/websocket');
      this.props.setSocket(socket);
      socket.onopen = () => {
        console.log('Socket Opened');
        sockets.connect(socket);
        sockets.login(socket, chatData.data.entity);
        sockets.subscribe(socket);
      };
    }
    sockets.connect(socket);
    socket.onmessage = async ({data}) => {
      const formatted = JSON.parse(data);
      console.log('CreateDialog - data', formatted);
      if (formatted.msg === 'ping') {
        socket.send(
          JSON.stringify({
            msg: 'pong',
          }),
        );
      }
      if (formatted.msg === 'result' && formatted.id === 'createDir') {
        this.props.navigation.navigate('DirectChat', {
          roomId: formatted.result.rid,
          socket: socket,
          opponentName: userName,
          fullName: fullName,
        });
      }
    };
    socket.send(
      JSON.stringify({
        msg: 'method',
        method: 'createDirectMessage',
        id: 'createDir',
        params: [userName],
      }),
    );
  };
  render() {
    const filteredItems = this.state.contacts.filter(contact =>
      contact.fullname.toLocaleLowerCase().includes(this.state.filterText),
    );
    return (
      <>
        <S.Container>
          <Header title="Написать сообщение" props={this.props} />
          <S.OptionsContainer>
            <View
              style={[
                style.timeView,
                {
                  top: -25,
                },
              ]}>
              <S.SearchIcon>
                <Search fill="#5954f9" />
              </S.SearchIcon>
              <S.EventTime
                onChangeText={text =>
                  this.setState({filterText: text.toLocaleLowerCase()})
                }
                placeholder="Поиск"
              />
            </View>
            <View
              style={{
                marginTop: 50,
                paddingHorizontal: 15,
                marginBottom: 10,
              }}>
              {filteredItems.map(contact => (
                <Contact
                  onContactPress={this.createDirectMessage}
                  contact={contact}
                />
              ))}
            </View>
          </S.OptionsContainer>
        </S.Container>
      </>
    );
  }
}

const style = StyleSheet.create({
  eventImage: {
    height: 180,
    position: 'absolute',
    borderRadius: 15,
    alignSelf: 'center',
    top: -130,
    backgroundColor: 'red',
  },
  timeView: {
    position: 'absolute',
    alignSelf: 'center',
    width: 300,
    height: 40,
    elevation: 5,
    borderRadius: 20,
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
  },
});

const mapDispatchToProps = {
  setSocket,
};

export default connect(
  null,
  mapDispatchToProps,
)(CreateDialog);
