import styled from 'styled-components';

export const Container = styled.View`
  background-color: #5954f9;
`;

export const OptionsContainer = styled.View`
  width: 100%;
  height: 700px;
  border-top-left-radius: 30px;
  border-top-right-radius: 30px;
  background-color: white;
  margin-top: 40px;
  /* padding: 0 15px; */
`;

export const SearchIcon = styled.View`
  position: absolute;
  right: 14px;
`;

export const EventTime = styled.TextInput`
  /* color: #5754f4;
  font-family: Roboto;
  font-size: 14px;
  font-weight: 700;
  line-height: 14px; */
  /* text-transform: uppercase; */
  /* background-color: red; */
  width: 100%;
  height: 100%;
  padding-left: 15px;
  border-radius: 20px;
  opacity: 0.38;
`;

export const PlaceHolder = styled.Text`
  opacity: 0.38;
  color: #8b8d94;
  font-family: Roboto;
  font-size: 14px;
  font-weight: 500;
  position: absolute;
`;

export const InfoView = styled.View`
  margin-top: 100px;
  padding: 0 15px;
  margin-bottom: 10px;
`;

export const EventTitle = styled.Text`
  color: #1b041e;
  font-family: Roboto;
  font-size: 18px;
  font-weight: 700;
  line-height: 20px;
  text-align: center;
`;

export const EventDescription = styled.Text`
  color: #1b041e;
  font-family: Roboto;
  font-size: 14px;
  font-weight: 400;
  line-height: 22px;
`;
