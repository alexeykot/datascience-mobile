import React from 'react';

import fakeAvatar from '../../../../assets/images/fakeAvatar.png';
import * as S from './styled';

const Contact = props => {
  return (
    <S.Container
      onPress={() =>
        props.onContactPress(
          props.contact.chat.username,
          props.contact.fullname,
        )
      }
      isPaddingHorizontal={props.isPaddingHorizontal}>
      {props.contact.photo.url ? (
        <S.Avatar source={{uri: props.contact.photo.url}} />
      ) : (
        <S.FakeAvatar source={fakeAvatar} />
      )}
      <S.InfoContainer>
        <S.Name>{props.contact.fullname}</S.Name>
        {/* <S.Job numberOfLines={1} ellipsizeMode="tail">
          {props.speaker.post}
        </S.Job> */}
      </S.InfoContainer>
    </S.Container>
  );
};

export default Contact;
