import styled from 'styled-components';
import {Dimensions} from 'react-native';

export const ScrollContainer = styled.ScrollView`
  background-color: white;
`;

export const PaddingBlock = styled.View`
  width: 100%;
  height: ${Dimensions.get('screen').height * 0.5};
`;

export const ButtonContainer = styled.View`
  width: 100%;
  padding: 0 30px;
  position: absolute;
  bottom: 10px;
  z-index: 1231;
`;
export const SaveButton = styled.TouchableOpacity`
  width: 100%;
  align-self: center;
  height: 48px;
  elevation: 20;
  border-radius: 24px;
  background-color: #5954f9;
  opacity: ${({disabled}) => (disabled ? 0.5 : 1)};
  justify-content: center;
  align-items: center;
`;

export const ButtonText = styled.Text`
  color: #ffffff;
  font-family: Roboto;
  font-size: 14px;
`;
