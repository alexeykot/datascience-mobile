import React from 'react';

import * as S from './styled';
import {isEqual} from 'lodash';
import Input from './components/Input';
import BaseScreen from '../../components/BaseScreen';
import {workInputs, infoInputs} from './config';
import {Keyboard} from 'react-native';
import {connect} from 'react-redux';
import {modifyInputsArr, formUserData} from './helpers';
import http from '../../services/http';
import {saveUser} from '../../redux/auth';
import {ActivityIndicator} from 'react-native';

const UserInfoScreen = props => {
  const [filled, setFilled] = React.useState('Личные данные');
  const [loading, setLoading] = React.useState(false);
  const [dictionary, setDictionary] = React.useState(null);
  const [infoInputsValue, setInfoInputsValue] = React.useState(infoInputs);
  const [workInputsValue, setWorkInputsValue] = React.useState(workInputs);
  const [isKeyboardShow, setKeyboardShow] = React.useState(false);
  const setBlock = type => {
    setFilled(type);
  };
  React.useEffect(() => {
    setInfoInputsValue(i => modifyInputsArr(i, props.user));
    setWorkInputsValue(i => modifyInputsArr(i, props.user));
    Keyboard.addListener('keyboardDidShow', () => {
      setKeyboardShow(true);
    });
    Keyboard.addListener('keyboardDidHide', () => {
      setKeyboardShow(false);
    });
    async function fetchOptions() {
      const {data: institution} = await http.get(
        `/cabinet/institution/index?access-token=${props.token}`,
      );
      const {data: experience} = await http.get(
        `/cabinet/experience/index?access-token=${props.token}`,
      );
      const {data: industry} = await http.get(
        `/cabinet/industry/index?access-token=${props.token}`,
      );

      const dictionaryObj = {
        institution: institution.data,
        experience: experience.data,
        industry: industry.data,
      };
      setDictionary(dictionaryObj);
    }
    fetchOptions();
    return () => {
      Keyboard.removeAllListeners('keyboardDidShow');
      Keyboard.removeAllListeners('keyboardDidHide');
    };
  }, [props.user, props.token]);
  // const inputs = filled === 1 ? workInputs : infoInputsValue;
  const onInfoInputChange = (type, text) => {
    const newInputs = infoInputsValue.map(inp => {
      if (inp.type === type) {
        inp.value = text;
        return inp;
      }
      return inp;
    });
    setInfoInputsValue(newInputs);
  };
  const onWorkInputChange = (type, text) => {
    const newInputs = workInputsValue.map(inp => {
      if (inp.type === type) {
        inp.value = text;
        return inp;
      }
      return inp;
    });
    setWorkInputsValue(newInputs);
  };
  const saveData = async () => {
    setLoading(true);
    const data = formUserData(infoInputsValue, workInputsValue, props.user);
    try {
      const resp = await http.post(
        `/cabinet/profile/update?access-token=${props.token}`,
        data,
      );
      props.saveUser(data);
      console.log('resp', resp);
    } catch (err) {
      console.log('errSaveData', err.response);
    }
    setLoading(false);
  };

  return (
    <>
      <BaseScreen
        title="Мои данные"
        scrollableOutsideContainer
        withTabs
        tabsProps={{
          tabs: ['Личные данные', 'Учеба и работа'],
          selectedTab: filled,
          onTabPress: tab => setBlock(tab),
        }}
        containerPadding="0 30px"
        content={
          <>
            {filled === 'Личные данные' ? (
              <S.ScrollContainer showsVerticalScrollIndicator={false}>
                {infoInputsValue.map(input => (
                  <Input
                    onInputChage={onInfoInputChange}
                    value={input.value}
                    input={input}
                  />
                ))}
                <Input
                  input={{
                    type: 'phone',
                    title: 'Телефон',
                    value: `+${props.user.phone}`,
                  }}
                  isDisabled={true}
                />
                <S.PaddingBlock />
              </S.ScrollContainer>
            ) : (
              <S.ScrollContainer showsVerticalScrollIndicator={false}>
                {workInputsValue.map(input => (
                  <Input
                    onInputChage={onWorkInputChange}
                    value={input.value}
                    input={input}
                    dictionary={dictionary}
                  />
                ))}
                <S.PaddingBlock />
              </S.ScrollContainer>
            )}
          </>
        }
      />
      {!isKeyboardShow ? (
        <S.ButtonContainer>
          <S.SaveButton
            disabled={
              isEqual(
                formUserData(infoInputsValue, workInputsValue, props.user),
                props.user,
              ) || loading
            }
            onPress={saveData}>
            {!loading ? (
              <S.ButtonText>Сохранить изменения</S.ButtonText>
            ) : (
              <ActivityIndicator color="white" />
            )}
          </S.SaveButton>
        </S.ButtonContainer>
      ) : null}
    </>
  );
};

const mapStateToProps = ({auth}) => ({
  user: auth.userData,
  token: auth.token,
});

const mapDispatchToProps = {
  saveUser,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(UserInfoScreen);
