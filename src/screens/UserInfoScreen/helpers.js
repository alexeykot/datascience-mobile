import _ from 'lodash';
import moment from 'moment';

export const translateValueFromDictionary = (type, value, dictionary) => {
  const valueFromDictionary = [
    'sex',
    'experienceInDs',
    'industry',
    'institution',
  ];
  if (type === 'birthday') {
    return moment(value, 'YYYY-MM-DD').format('DD.MM.YYYY');
  }
  if (valueFromDictionary.includes(type)) {
    if (type === 'sex') {
      return value === 2 ? 'женский' : 'мужской';
    } else {
      if (dictionary === null) {
        return '';
      }
      const findInDictionary = dictionary.find(
        option => option.value === value,
      );
      if (findInDictionary) {
        return findInDictionary.label;
      }
      return value;
    }
  }
  return value;
};

export const modifyInputValue = (input, userData) => {
  const userField = {
    ...userData,
    ...userData.fields,
  };
  console.log('userField', userField);
  const hasThisProperty = _.has(userField, input.type);
  if (hasThisProperty) {
    return {
      ...input,
      // value: translateValueFromDictionary(input.type, userField[input.type]),
      value: userField[input.type],
    };
  }
  return {
    ...input,
    value: '',
  };
};

export const modifyInputsArr = (inputs, userData) => {
  if (userData === undefined) {
    return inputs;
  }
  const formatted = inputs.map(input => modifyInputValue(input, userData));
  return formatted;
};

export const titleFromValue = (dictionary, value) => {
  const findRow = dictionary.find(row => row.value === value);
  if (findRow !== undefined) {
    return findRow.label;
  }
  return value;
};

export const formUserData = (infoInputs, workInputs, userData) => {
  let formattedUserData = {
    ...userData,
  };
  const inputsWithoutPhone = infoInputs.filter(input => input.type !== 'phone');
  for (let i = 0; i < inputsWithoutPhone.length; i++) {
    formattedUserData[inputsWithoutPhone[i].type] = inputsWithoutPhone[i].value;
  }
  const formattedFields = {
    ...userData.fields,
  };
  for (let i = 0; i < workInputs.length; i++) {
    formattedFields[workInputs[i].type] = workInputs[i].value;
  }
  const wholeObject = {
    ...formattedUserData,
    fields: Object.values(formattedFields).every(field => !field)
      ? []
      : formattedFields,
    // fields: formattedFields,
  };
  return wholeObject;
};
