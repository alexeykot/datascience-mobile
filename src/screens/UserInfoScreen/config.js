export const infoInputs = [
  {
    title: 'Фамилия',
    type: 'surname',
  },
  {
    title: 'Имя',
    type: 'name',
  },
  {
    title: 'Отчество',
    type: 'patronymic',
  },
  {
    title: 'Пол',
    type: 'sex',
  },
  {
    title: 'Дата рождения',
    type: 'birthday',
  },
  {
    title: 'Город',
    type: 'address',
  },
  {
    title: 'E-Mail',
    type: 'email',
  },
  // {
  //   title: 'Телефон',
  //   type: 'phone',
  // },
];

export const workInputs = [
  {
    title: 'Об учебе',
    type: 'institution',
  },
  {
    title: 'О работе',
    type: 'industry',
  },
  {
    title: 'Компания',
    type: 'organization',
  },
  {
    title: 'Должность',
    type: 'workPlace',
  },
  {
    title: 'Мой опыт в DS',
    type: 'experienceInDs',
  },
];
