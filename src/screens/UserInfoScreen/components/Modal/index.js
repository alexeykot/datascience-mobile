import React from 'react';
import {StyleSheet, Dimensions} from 'react-native';
import moment from 'moment';
import DatePicker from 'react-native-date-picker';

import Modal from 'react-native-modal';

import * as S from './styled';

const ModalWindow = ({
  isVisible,
  onClose,
  title,
  options,
  onOptionPress,
  input,
}) => {
  const [birthday, setBirthday] = React.useState(null);
  const close = async () => {
    onClose();
  };
  const isDate = input.type === 'birthday';
  return (
    <Modal
      onSwipeComplete={close}
      swipeDirection={['down']}
      onBackdropPress={close}
      style={styles.view}
      backdropOpacity={0}
      isVisible={isVisible}>
      <S.Container isPadding={!!options}>
        <S.Title>{title}</S.Title>
        <S.Separator />
        <S.OptionsContainer>
          {options ? (
            options.map((option, index) => {
              const isLast = index === options.length - 1;
              return (
                <>
                  <S.Option
                    onPress={() => onOptionPress(option.value)}
                    isLast={isLast}>
                    <S.OptionText>{option.label}</S.OptionText>
                  </S.Option>
                  {!isLast ? <S.Separator /> : null}
                </>
              );
            })
          ) : (
            <DatePicker
              style={{
                backgroundColor: 'white',
                width: Dimensions.get('screen').width,
                borderTopWidth: 1,
                borderColor: '#f7f5fa',
              }}
              mode="date"
              // date={moment(input.value, 'YYYY-MM-DD').toDate()}
              onDateChange={
                date => setBirthday(moment(date).format('YYYY-MM-DD'))
                // date => console.log('date', date)
                // onOptionPress(moment(date).format('YYYY-MM-DD'))
              }
            />
          )}
        </S.OptionsContainer>
        {!isDate ? (
          <S.DefaultButton onPress={close}>
            <S.ButtonText>Отмена</S.ButtonText>
          </S.DefaultButton>
        ) : (
          <S.DateButton onPress={() => onOptionPress(birthday)}>
            <S.ButtonText>Сохранить</S.ButtonText>
          </S.DateButton>
        )}
      </S.Container>
    </Modal>
  );
};

const styles = StyleSheet.create({
  view: {
    justifyContent: 'flex-end',
    margin: 0,
  },
  wrapper: {
    flex: 1,
  },
});

export default ModalWindow;
