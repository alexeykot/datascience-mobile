import React from 'react';

import * as S from './styled';
import Modal from '../Modal';
import http from '../../../../services/http';
import {connect} from 'react-redux';
import {titleFromValue, translateValueFromDictionary} from '../../helpers';

const inputsWithTouch = [
  'industry',
  'institution',
  'experienceInDs',
  'sex',
  'birthday',
];
const dictionary = {
  experienceInDs: 'experience',
  industry: 'industry',
  institution: 'institution',
};
const sexOptions = [
  {
    label: 'женский',
    value: 2,
  },
  {
    label: 'мужской',
    value: 1,
  },
];
const Input = ({
  input,
  value,
  token,
  onInputChage,
  isDisabled = false,
  dictionary: optionsDictionary,
}) => {
  const [options, setOptions] = React.useState(null);
  const [isModalShow, setModalShow] = React.useState(false);
  const onInputPress = async () => {
    if (input.type === 'sex') {
      setOptions(sexOptions);
      setModalShow(true);
      return;
    }
    if (input.type === 'birthday') {
      setOptions(null);
      setModalShow(true);
      return;
    }
    const {data} = await http.get(
      `/cabinet/${dictionary[input.type]}/index?access-token=${token}`,
    );
    console.log('data', data);
    setOptions(data.data);
    setModalShow(true);
  };
  const optionPick = text => {
    onInputChage(input.type, text);
    setModalShow(false);
  };
  const renderInput = () => {
    if (inputsWithTouch.includes(input.type)) {
      return (
        <>
          <Modal
            onOptionPress={optionPick}
            options={options}
            title={input.title}
            input={input}
            onClose={() => setModalShow(false)}
            isVisible={isModalShow}
          />
          <S.TouchedInput onPress={onInputPress}>
            <S.Hint>{input.title}</S.Hint>
            <S.Value>
              {translateValueFromDictionary(
                input.type,
                input.value,
                optionsDictionary
                  ? optionsDictionary[dictionary[input.type]]
                  : null,
              )}
            </S.Value>
          </S.TouchedInput>
        </>
      );
    }
    return (
      <S.Container>
        <S.Hint>{input.title}</S.Hint>
        <S.StyledInput
          onChangeText={text => onInputChage(input.type, text)}
          value={input.value}
          editable={!isDisabled}
        />
      </S.Container>
    );
  };
  return <>{renderInput(input)}</>;
};

const mapStateToProps = ({auth}) => ({
  token: auth.token,
});

export default connect(
  mapStateToProps,
  null,
)(Input);
