import styled from 'styled-components';

export const Container = styled.View`
  width: 100%;
  height: 50px;
  margin-top: 12px;
  border-color: #c9ccd5;
  border-bottom-width: 1px;
  padding-bottom: 10px;
  opacity: ${({isDisabled}) => (isDisabled ? 0.5 : 1)};
`;

export const Hint = styled.Text`
  color: #8b8d94;
  font-family: Roboto;
  font-size: 12px;
  font-weight: 400;
`;

export const Value = styled.Text`
  margin-top: 4px;
`;

export const StyledInput = styled.TextInput`
  width: 100%;
  padding-top: 2px;
  padding-left: 0;
`;

export const TouchedInput = styled.TouchableOpacity`
  width: 100%;
  height: 50px;
  margin-top: 12px;
  border-color: #c9ccd5;
  border-bottom-width: 1px;
`;

export const Separator = styled.View`
  width: 100%;
  height: 1px;
  background-color: #c9ccd5;
`;
