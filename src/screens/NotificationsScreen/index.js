import React from 'react';

import * as S from './styled';

import DateBlock from './components/DateBlock';
import NotificationsStorage from '../../services/storage/notifications';
import Header from '../Event/components/Header';
import {connect} from 'react-redux';
import {formatNotifications, sortByDate} from './helpers';
import {TouchableOpacity, Text, Clipboard} from 'react-native';
import { makeNotificationsSeen } from '../../redux/auth';

const mockData = [
  {
    collapseKey: 'com.datascience',
    data: {},
    from: '164714382968',
    messageId: '0:1593888706005898%558b968d558b968d',
    notification: {android: {}, body: '&laquo;a&raquo;', title: 'a'},
    sentTime: 1593888706003,
    ttl: 2419200,
  },
];

// const mockData1 = [
//   {
//     text: 'Вы получили 500 баллов за выступление в качестве спикера',
//     seen: false,
//     date: '2020-06-22',
//     time: '15:10',
//   },
//   {
//     text: 'Вы добавили мероприятие AI Journey 2020 в избранное ',
//     seen: false,
//     date: '2020-06-22',
//     time: '17:00',
//   },
// ];

const NotificationsScreen = props => {
  console.log('NotificationsScreen', props.notifications);
  React.useEffect(() => {
    const unsubscribe = props.navigation.addListener('blur', async () => {
      const notifications = await NotificationsStorage.get();
      console.log('NotificaStorage', notifications);
      if (notifications !== null) {
        notifications.forEach(notif => (notif.seen = true));
        props.makeNotificationsSeen();
        await NotificationsStorage.save(notifications);
      }
    });
    return unsubscribe;
  });
  const formatted = formatNotifications(props.notifications);
  const sortedByDate = sortByDate(Object.keys(formatted));
  return (
    <>
      <Header color="#5954f9" title="Уведомления" props={props} />
      <S.Container>
        {sortedByDate.map(date => (
          <DateBlock notifications={formatted[date]} date={date} />
        ))}
      </S.Container>
    </>
  );
};

const mapStateToProps = ({auth}) => ({
  notifications: auth.notifications,
  deviceToken: auth.deviceToken,
});

const mapDispatchToProps = {
  makeNotificationsSeen,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(NotificationsScreen);
