import _ from 'lodash';
import moment from 'moment';

const DATE_MASK = 'D MMMM';

export const formatNotifications = notifications => {
  const notificationsWithDate = notifications.map(notification => {
    const newNotif = {
      ...notification,
      date: moment(notification.sentTime).format(DATE_MASK),
      time: moment(notification.sentTime).format('HH:MM'),
    };
    return newNotif;
  });
  const groupedByDate = _.groupBy(notificationsWithDate, 'date');
  return groupedByDate;
};

export const sortByTime = notifications => {
  const sortedByTime = notifications.sort(
    (d1, d2) =>
      new Date(d2.sentTime).getTime() - new Date(d1.sentTime).getTime(),
  );
  return sortedByTime;
};

export const sortByDate = notifications => {
  const sortedArray = notifications.sort((a, b) =>
    moment(b, DATE_MASK).diff(moment(a, DATE_MASK)),
  );
  return sortedArray;
};

export const isToday = date => {
  return moment().format(DATE_MASK) === date;
};

export const isYesterday = date => {
  return (
    moment()
      .subtract(1, 'days')
      .format(DATE_MASK) === date
  );
};

export const formatDay = date => {
  if (isToday(date)) {
    return `Сегодня, ${date}`;
  }
  if (isYesterday(date)) {
    return `Вчера, ${date}`;
  }
  return date;
};

export const formatDescription = string => {
  const newStr = string
    .replace(/&nbsp;/g, ' ')
    .replace(/&laquo;/g, '«')
    .replace(/&raquo;/g, '»')
    .replace(/<p>/g, '\n')
    .replace(/<pre>/g, '')
    .replace(new RegExp('</pre>', 'g'), '')
    .replace(/<strong>/g, '')
    .replace(new RegExp('</strong>', 'g'), '')
    .replace(new RegExp('</p>', 'g'), '')
    .replace(new RegExp('<em>', 'g'), '')
    .replace(new RegExp('</em>', 'g'), '');
  return newStr;
};
