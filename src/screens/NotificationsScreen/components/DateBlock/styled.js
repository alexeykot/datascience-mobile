import styled from 'styled-components';

export const Container = styled.View`
  width: 100%;
  margin-top: 20px;
  padding-horizontal: 15px;
`;

export const Date = styled.Text`
  color: #8b8d94;
  font-family: Roboto;
  font-size: 14px;
  font-weight: 400;
  align-self: center;
  margin-bottom: 9px;
`;
