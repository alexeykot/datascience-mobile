import React from 'react';

import Notification from '../Notification';

import * as S from './styled';
import {sortByTime, formatDay, isToday} from '../../helpers';

const DateBlock = ({date, notifications}) => {
  const sortedByTime = sortByTime(notifications);
  return (
    <S.Container>
      <S.Date>{formatDay(date)}</S.Date>
      {sortedByTime.map(notification => (
        <Notification notification={notification} seen={false} />
      ))}
    </S.Container>
  );
};

export default DateBlock;
