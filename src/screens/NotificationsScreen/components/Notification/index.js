import React from 'react';

import seenIcon from '../../../../assets/images/seen.png';
import unseenIcon from '../../../../assets/images/unseen.png';
import * as S from './styled';
import moment from 'moment';
import {formatDescription} from '../../helpers';

const Notification = ({isLast, notification}) => {
  const {seen} = notification;
  return (
    <S.Container isLast={isLast}>
      <S.RoundIcon>
        <S.Icon source={seen ? seenIcon : unseenIcon} />
      </S.RoundIcon>
      <S.TextContainer>
        <S.Text seen={seen}>
          {formatDescription(notification.notification.body)}
        </S.Text>
        <S.Time>{moment(notification.sentTime).format('HH:mm')}</S.Time>
      </S.TextContainer>
    </S.Container>
  );
};

export default Notification;
