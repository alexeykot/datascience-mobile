import styled from 'styled-components';

export const Container = styled.TouchableOpacity`
  flex-direction: row;
  width: 100%;
  justify-content: space-between;
  margin-bottom: ${({isLast}) => (isLast ? 0 : 15)}px;
`;

export const RoundIcon = styled.View`
  width: 40px;
  height: 40px;
  background-color: #f7f5fa;
  border-radius: 20px;
  justify-content: center;
  align-items: center;
`;

export const TextContainer = styled.View`
  width: 283px;
  padding: 13px 24px 25px 10px;
  background-color: #f7f5fa;
  border-radius: 15px;
  border-bottom-left-radius: 0px;
`;

export const Text = styled.Text`
  color: #1b041e;
  font-family: Roboto;
  font-size: 14px;
  font-weight: ${({seen}) => (seen ? '400' : '700')};
`;

export const Time = styled.Text`
  color: #8b8d94;
  font-family: Roboto;
  font-size: 12px;
  font-weight: 400;
  position: absolute;
  right: 13px;
  bottom: 10px;
`;

export const Icon = styled.Image``;
