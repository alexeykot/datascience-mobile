import {Dimensions} from 'react-native';
import styled from 'styled-components';

export const PaddingContainer = styled.View`
  height: ${Dimensions.get('screen').height * 0.5}px;
  width: 100%;
`;
