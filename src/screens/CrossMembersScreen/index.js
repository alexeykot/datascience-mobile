/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {Animated, ScrollView} from 'react-native';

import * as S from './styled';
import Member from './components/Member';
import http from '../../services/http';
import BaseScreen from '../../components/BaseScreen';

class CrossMembersScreen extends React.Component {
  state = {
    filterText: '',
    crossMembers: [],
  };

  async componentDidMount() {
    const crossMembers = await http.get('/event/client/environment');
    this.setState({crossMembers: crossMembers.data.data.entities});
  }
  componentWillMount() {
    this._animatedValue = new Animated.Value(0);
  }
  render() {
    const {crossMembers} = this.state;
    const filteredItems = crossMembers
      .filter(speaker => speaker.fullname)
      .filter(speaker =>
        speaker.fullname.toLocaleLowerCase().includes(this.state.filterText),
      );

    const itemsToDisplay = this.state.filterText ? filteredItems : crossMembers;
    return (
      <BaseScreen
        title="С кем пересекался"
        containerPadding="15px 0"
        withSearch
        searchProps={{
          filterText: this.state.filterText,
          onResetPress: () => this.setState({filterText: ''}),
          inputPlaceholder: 'Поиск участника',
          onInputChange: text =>
            this.setState({filterText: text.toLocaleLowerCase()}),
        }}
        scrollableOutsideContainer
        content={
          <ScrollView
            style={{
              marginTop: 50,
              paddingHorizontal: 15,
              marginBottom: 10,
            }}>
            {itemsToDisplay.map(speaker => (
              <Member
                onPress={() =>
                  this.props.navigation.navigate('MemberScreen', {
                    member: speaker,
                  })
                }
                navigation={this.props.navigation}
                isPaddingHorizontal
                speaker={speaker}
              />
            ))}
            <S.PaddingContainer />
          </ScrollView>
        }
      />
    );
  }
}

export default CrossMembersScreen;
