import React from 'react';

import FakeAvatar from '../../../../assets/icons/people.svg';
import * as S from './styled';

const Member = props => {
  return (
    <S.Container
      onPress={props.onPress}
      isPaddingHorizontal={props.isPaddingHorizontal}>
      {!props.speaker.photo.url ? (
        <S.PeopleIconContainer>
          <FakeAvatar />
        </S.PeopleIconContainer>
      ) : (
        <S.Avatar source={{uri: props.speaker.photo.url}} />
      )}
      <S.InfoContainer>
        <S.Name>{props.speaker.fullname}</S.Name>
        {/* <S.Job numberOfLines={1} ellipsizeMode="tail">
          {props.speaker.post}
        </S.Job> */}
      </S.InfoContainer>
    </S.Container>
  );
};

export default Member;
