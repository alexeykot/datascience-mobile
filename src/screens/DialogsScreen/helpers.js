export const getOpponentInfo = (room, me) => {
  console.log('getOpponentInfo', room, me);
  const usernames = room.usernames;
  const opponentName = usernames.filter(name => name !== me.username);
  return {
    ...room,
    opponentName,
  };
};
