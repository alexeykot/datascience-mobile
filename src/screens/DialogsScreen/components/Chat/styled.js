import styled from 'styled-components';
import {Dimensions} from 'react-native';

export const Wrapper = styled.ScrollView``;

export const Block = styled.TouchableOpacity`
  width: 70px;
  height: 100%;
  background-color: ${({isFirst}) => (isFirst ? '#5954f9' : '#f11e3f')};
  padding-top: 16px;
  border-bottom-left-radius: ${({isFirst}) => (isFirst ? 15 : 0)}px;
  border-top-left-radius: ${({isFirst}) => (isFirst ? 15 : 0)}px;
  margin-left: ${({isFirst}) => (isFirst ? 17 : 0)}px;
  align-items: center;
`;

export const DeleteText = styled.Text`
  color: #ffffff;
  font-family: Roboto;
  font-size: 10px;
  font-weight: 400;
  line-height: 16px;
`;

export const DeleteIcon = styled.Image``;

export const Container = styled.TouchableOpacity`
  /* width: 100%; */
  width: ${Dimensions.get('window').width - 30}px;
  flex-direction: row;
  margin-bottom: 12px;
`;

export const UserName = styled.Text`
  color: #1b041e;
  font-family: Roboto;
  font-size: 14px;
  font-weight: 700;
`;

export const Avatar = styled.View`
  width: 54px;
  height: 54px;
  border-radius: 54px;
  background-color: #f2f2ff;
  justify-content: center;
  align-items: center;
`;

export const Icon = styled.Image``;

export const MessageInfoContainer = styled.View`
  flex-direction: row;
  justify-content: space-between;
  flex: 1;
  margin-bottom: 7px;
  padding-top: 3px;
`;

export const MessageContainer = styled.View`
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  flex: 1;
`;

export const UnreadView = styled.View`
  width: 20px;
  height: 20px;
  border-radius: 20px;
  background-color: #5954f9;
  justify-content: center;
  align-items: center;
`;

export const UnreadText = styled.Text`
  font-size: 10px;
  color: white;
`;

export const MessageTime = styled.Text`
  color: #8b8d94;
  font-family: Roboto;
  font-size: 12px;
  font-weight: 400;
`;

export const Column = styled.View`
  margin-left: 15px;
  flex: 1;
`;

export const Message = styled.Text`
  color: #8b8d94;
  font-family: Roboto;
  font-size: 12px;
  font-weight: 400;
  line-height: 16px;
`;

export const BlueText = styled.Text`
  color: #5954f9;
  font-family: Roboto;
  font-size: 12px;
  font-weight: 400;
  line-height: 16px;
`;
