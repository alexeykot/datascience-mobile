import React from 'react';

import moment from 'moment';
import chatAvatar from '../../../../assets/images/chatAvatar.png';
import trash from '../../../../assets/images/trash.png';
import Hide from '../../../../assets/icons/trash-2-01.svg';
import * as S from './styled';
import {
  isRepliedMessage,
  splitRepliedMessage,
} from '../../../DirectChat/helpers';

const Chat = ({
  room,
  me,
  onPress,
  unreadInfo,
  onDeleteRoom,
  fullRoomInfo,
  onHideRoom,
}) => {
  const date = new Date(room.lastMessage.ts);
  const findUnread = unreadInfo
    ? unreadInfo.find(r => r.rid === room.lastMessage.rid)
    : null;
  let scrollRef;
  return (
    <S.Wrapper
      snapToAlignment="center"
      horizontal
      ref={r => {
        scrollRef = r;
      }}
      snapToInterval={200}
      showsHorizontalScrollIndicator={false}>
      <S.Container
        onPress={() =>
          onPress(room.lastMessage.rid, fullRoomInfo.name, fullRoomInfo.fname)
        }>
        <S.Avatar>
          <S.Icon source={chatAvatar} />
        </S.Avatar>
        <S.Column>
          <S.MessageInfoContainer>
            <S.UserName>{fullRoomInfo.fname}</S.UserName>
            <S.MessageTime>{`${
              date.getHours() < 10 ? `0${date.getHours()}` : date.getHours()
            }:${
              date.getMinutes() < 10
                ? `0${date.getMinutes()}`
                : date.getMinutes()
            }`}</S.MessageTime>
          </S.MessageInfoContainer>
          <S.MessageContainer>
            {room.lastMessage.u.username !== me.username ? (
              <S.Message numberOfLines={1}>
                {isRepliedMessage(room.lastMessage.msg)
                  ? splitRepliedMessage(room.lastMessage.msg).pure
                  : room.lastMessage.msg}
              </S.Message>
            ) : (
              <S.Message numberOfLines={1}>
                <S.BlueText>Вы: </S.BlueText>
                {isRepliedMessage(room.lastMessage.msg)
                  ? splitRepliedMessage(room.lastMessage.msg).pure
                  : room.lastMessage.msg}
              </S.Message>
            )}
            {findUnread.unread ? (
              <S.UnreadView>
                <S.UnreadText>{findUnread.unread}</S.UnreadText>
              </S.UnreadView>
            ) : null}
          </S.MessageContainer>
        </S.Column>
      </S.Container>
      <S.Block
        isFirst
        onPress={() => {
          onHideRoom(room.lastMessage.rid);
          scrollRef.scrollTo({x: 0, y: 0});
        }}>
        {/* <S.DeleteIcon source={trash} /> */}
        <Hide />
        <S.DeleteText>Скрыть</S.DeleteText>
      </S.Block>
      {/* <S.Block onPress={() => onDeleteRoom(room.lastMessage.rid)}> */}
      <S.Block onPress={() => scrollRef.scrollTo({x: 0, y: 0})}>
        <S.DeleteIcon source={trash} />
        <S.DeleteText>Удалить</S.DeleteText>
      </S.Block>
    </S.Wrapper>
  );
};

export default Chat;
