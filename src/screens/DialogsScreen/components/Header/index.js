import React from 'react';

import * as S from './styled';
import Edit from '../../../../assets/icons/edit-01.svg';

const Header = ({props, title, color, socket}) => {
  return (
    <S.Container color={color}>
      <S.FakeView />
      <S.Title>{title}</S.Title>
      <S.RightButton
        onPress={() =>
          props.navigation.navigate('CreateDialog', {
            socket: socket,
          })
        }>
        <Edit />
      </S.RightButton>
    </S.Container>
  );
};

export default Header;
