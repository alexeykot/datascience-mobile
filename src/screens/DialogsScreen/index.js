/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {
  StyleSheet,
  View,
  Alert,
  ActivityIndicator,
  AppState,
} from 'react-native';

import Search from '../../assets/icons/search.svg';
import _ from 'lodash';
import moment from 'moment';
import * as S from './styled';
import Header from './components/Header';
import httpChat from '../../services/http/chat';
import {connect} from 'react-redux';
import {requestGetMyself, setSocket} from '../../redux/chat';
import Chat from './components/Chat';
import HidedChatStorage from '../../services/storage/hidedChats';
import ChatStorage from '../../services/storage/chat';
import * as sockets from '../../../startSocket';
import http from '../../services/http';
import Reset from '../../assets/icons/close-circle-fill.svg';
import Edit from '../../assets/icons/edit-color.svg';

// import DismissKeyboardHOC from '../../components/HOC/DismissKeyboard';
// const Background = DismissKeyboardHOC(S.Container);

class DialogsScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      filterText: '',
      rooms: [],
      unreadInfo: [],
      loading: true,
      contacts: [],
      isSwiped: false,
    };
  }

  async componentWillMount() {
    AppState.addEventListener('focus', async () => {
      this.props.navigation.navigate('MainScreen');
    });

    this.props.navigation.addListener('focus', async () => {
      const chatToken = await ChatStorage.get();
      const {socket} = this.props;
      sockets.connect(socket);
      sockets.login(socket, chatToken);
      sockets.subscribe(socket);

      const {data: contacts} = await http.get(
        '/event/client/environment?chatUserOnly=1',
      );
      this.setState({contacts: contacts.data.entities});
      try {
        if (this.props.me === null) {
          this.props.requestGetMyself();
        }

        this.props.socket.onmessage = async ({data}) => {
          const formatted = JSON.parse(data);
          console.log('onmessage', formatted);
          if (
            formatted.msg === 'result' &&
            formatted.id === 'status' &&
            formatted.result
          ) {
            if (formatted.result.update && formatted.result.update.length) {
              const hidedChats = await HidedChatStorage.get();
              const rooms = formatted.result.update
                // .filter(room => room.lastMessage !== undefined)
                .filter(room => room.name !== 'notification.bot')
                .filter(room => room.t === 'd')
                .filter(room => room.open);
              let roomsWithoutHide = [...rooms];
              // if (hidedChats.length) {
              //   roomsWithoutHide = rooms.filter(r => r._id !== hidedChats[0]);
              // }
              const roomsInfo = await Promise.all(
                roomsWithoutHide.map(async room => ({
                  ...room,
                  data: await httpChat.get(
                    `/api/v1/rooms.info?roomId=${room.rid}`,
                  ),
                })),
              );

              const withLastMessage = roomsInfo.filter(
                r => r.data.data.room.lastMessage,
              );
              console.log('roomsInfo', roomsInfo);
              this.setState({rooms: withLastMessage, loading: false});
              const unreadInfo = await httpChat.get(
                'api/v1/subscriptions.get?updatedSince=2017-11-25T15:08:17.248Z',
              );
              this.setState({unreadInfo: unreadInfo.data.update});
            }
          }
          if (formatted.msg === 'ping') {
            sockets.pong(this.props.socket);
          }
          if (formatted.msg === 'changed') {
            const roomsInfo = await Promise.all(
              this.state.rooms.map(room =>
                httpChat.get(
                  `/api/v1/rooms.info?roomId=${
                    room.data.data.room.lastMessage.rid
                  }`,
                ),
              ),
            );
            const unreadInfo = await httpChat.get(
              'api/v1/subscriptions.get?updatedSince=2017-11-25T15:08:17.248Z',
            );
            this.setState({unreadInfo: unreadInfo.data.update});
            this.setState({rooms: roomsInfo});
          }
          if (formatted.id === 'delete-msg') {
            const roomsInfo = await Promise.all(
              this.state.rooms.map(room =>
                httpChat.get(
                  `/api/v1/rooms.info?roomId=${room.data.room.lastMessage.rid}`,
                ),
              ),
            );
            const unreadInfo = await httpChat.get(
              'api/v1/subscriptions.get?updatedSince=2017-11-25T15:08:17.248Z',
            );
            this.setState({unreadInfo: unreadInfo.data.update});
            this.setState({rooms: roomsInfo});
          }
        };
        this.send();
      } catch (err) {
        console.log('Error-DialogsScreen', err.response);
      }
    });
  }

  onHideRoom = roomId => {
    sockets.sendJsonParams(this.props.socket, {
      msg: 'method',
      method: 'hideRoom',
      id: 'hide-room',
      params: [roomId],
    });
    sockets.test(this.props.socket);
  };

  onDeleteRoom = async rid => {
    Alert.alert('Подтверждение', 'Вы уверены, что хотите скрыть диалог?', [
      {
        text: 'Отменить',
        style: 'cancel',
      },
      {
        text: 'Да, уверен',
        onPress: async () => {
          // const hidedChats = await HidedChatStorage.get();
          // hidedChats.push(rid);
          // await HidedChatStorage.save(hidedChats);
          // sockets.getRooms(this.props.socket);
          try {
            await httpChat.post('/api/v1/rooms.leave', {
              roomId: rid,
              // oldest: '2016-09-30T13:42:25.304Z',
              // latest: '2020-10-08T13:42:25.304Z',
            });
          } catch (err) {
            console.log('errrClean', err.response);
          }
          sockets.test(this.props.socket);
        },
      },
    ]);
  };

  setRead = async rid => {
    await httpChat.post('/api/v1/subscriptions.read', {
      rid: rid,
    });
  };

  getRooms = message => {
    const formatted = JSON.parse(message);
    if (formatted.msg === 'result' && formatted.id === 'getRooms') {
      return formatted.result.update;
    }
  };

  send = async () => {
    this.setState({loading: true});
    // sockets.getRooms(this.props.socket);
    sockets.test(this.props.socket);
  };

  sortChatsByDate = chats => {
    const sorted = _.orderBy(chats, r => moment(r.lm).format('YYYYDDMM'), [
      'asc',
    ]);
    return sorted;
  };
  chatRefs = [];
  render() {
    const filteredItems = this.state.rooms.filter(room =>
      room.fname
        .toLocaleLowerCase()
        .includes(this.state.filterText.toLocaleLowerCase()),
    );
    return (
      <>
        <S.Container>
          <Header props={this.props} socket={this.props.socket} title="Чат" />
          <S.OptionsContainer>
            <View style={style.timeView}>
              {this.state.filterText ? (
                <S.ResetButton onPress={() => this.setState({filterText: ''})}>
                  <Reset />
                </S.ResetButton>
              ) : (
                <S.SearchIcon>
                  <Search fill="#5954f9" />
                </S.SearchIcon>
              )}
              {/* <S.SearchIcon>
                <Search fill="#5954f9" />
              </S.SearchIcon> */}

              <S.EventTime
                onChangeText={text => this.setState({filterText: text})}
                value={this.state.filterText}
                placeholder="Поиск"
              />
            </View>
            {this.state.loading && this.state.contacts.length ? (
              <ActivityIndicator
                style={{position: 'absolute', top: 30, alignSelf: 'center'}}
                size="small"
              />
            ) : null}
            {this.state.rooms.length && this.state.unreadInfo.length ? (
              <View
                style={{
                  marginTop: 50,
                  paddingHorizontal: 15,
                  marginBottom: 10,
                }}>
                {this.sortChatsByDate(filteredItems).map((room, index) => (
                  <Chat
                    onPress={(rid, opponentName, fullName) => {
                      this.setRead(rid);
                      this.props.navigation.navigate('DirectChat', {
                        roomId: rid,
                        socket: this.props.socket,
                        opponentName: opponentName,
                        fullName: fullName,
                      });
                    }}
                    setRef={r => {
                      this.chatRefs[index] = r;
                    }}
                    onDeleteRoom={this.onDeleteRoom}
                    onHideRoom={this.onHideRoom}
                    me={this.props.me}
                    ref={this.chatRefs[index]}
                    unreadInfo={this.state.unreadInfo}
                    room={room.data.data.room}
                    fullRoomInfo={room}
                  />
                ))}
              </View>
            ) : (
              <S.FakeBlock>
                <S.FakeText> Начните диалог, нажав на иконку</S.FakeText>
                <S.IconButton
                  onPress={() =>
                    this.props.navigation.navigate('CreateDialog', {
                      socket: this.props.socket,
                    })
                  }>
                  <Edit />
                </S.IconButton>
              </S.FakeBlock>
            )}
          </S.OptionsContainer>
        </S.Container>
      </>
    );
  }
}

const style = StyleSheet.create({
  eventImage: {
    height: 180,
    position: 'absolute',
    borderRadius: 15,
    alignSelf: 'center',
    top: -130,
    backgroundColor: 'red',
  },
  timeView: {
    position: 'absolute',
    alignSelf: 'center',
    width: 300,
    top: -25,
    height: 40,
    elevation: 5,
    borderRadius: 20,
    paddingRight: 50,
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
  },
});

const mapDispatchToProps = {
  requestGetMyself,
  setSocket,
};

const mapStateToProps = ({chat}) => ({
  me: chat.me,
  socket: chat.socket,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(DialogsScreen);
