import styled from 'styled-components';

export const Container = styled.View`
  background-color: #5954f9;
`;

export const OptionsContainer = styled.View`
  width: 100%;
  height: 700px;
  border-top-left-radius: 30px;
  border-top-right-radius: 30px;
  background-color: white;
  margin-top: 20px;
  /* padding: 0 15px; */
`;

export const SearchIcon = styled.View`
  position: absolute;
  right: 14px;
`;

export const ResetButton = styled.TouchableOpacity`
  width: 50px;
  height: 40px;
  justify-content: center;
  align-items: center;
  position: absolute;
  right: 0px;
  z-index: 1111;
  /* background-color: red; */
`;

export const FakeBlock = styled.View`
  width: 100%;
  height: 200px;
  background-color: white;
  margin-top: 25px;
  justify-content: center;
  flex-direction: row;
  align-items: center;
`;

export const IconButton = styled.TouchableOpacity``;

export const FakeText = styled.Text`
  color: #8b8d94;
  font-size: 14px;
  font-weight: 500;
  text-align: center;
  align-self: center;
  padding: 0 20px;
`;

export const MockChat = styled.View`
  width: 100%;
  height: 50px;
  background-color: red;
`;

export const EventTime = styled.TextInput`
  /* color: #5754f4;
  font-family: Roboto;
  font-size: 14px;
  font-weight: 700;
  line-height: 14px; */
  /* text-transform: uppercase; */
  /* background-color: red; */
  width: 100%;
  height: 100%;
  padding-left: 15px;
  border-radius: 20px;
  opacity: 0.38;
`;

export const PlaceHolder = styled.Text`
  opacity: 0.38;
  color: #8b8d94;
  font-family: Roboto;
  font-size: 14px;
  font-weight: 500;
  position: absolute;
`;

export const InfoView = styled.View`
  margin-top: 100px;
  padding: 0 15px;
  margin-bottom: 10px;
`;

export const EventTitle = styled.Text`
  color: #1b041e;
  font-family: Roboto;
  font-size: 18px;
  font-weight: 700;
  line-height: 20px;
  text-align: center;
`;

export const EventDescription = styled.Text`
  color: #1b041e;
  font-family: Roboto;
  font-size: 14px;
  font-weight: 400;
  line-height: 22px;
`;
