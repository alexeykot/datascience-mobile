import React from 'react';
import {Dimensions} from 'react-native';
import moment from 'moment';

import * as S from './styled';
import Card from './components/Card';

import http from '../../services/http';
import BaseScreen from '../../components/BaseScreen';
import {getClientStatus} from '../MainScreen/helpers';
import {connect} from 'react-redux';

const tabs = ['Ближайшие', 'Прошедшие'];

const MyEventsScreen = props => {
  const [selectedTab, setSelectedTab] = React.useState(tabs[0]);
  const [borderLeft, setBorderLeft] = React.useState(0);
  const [events, setEvents] = React.useState([]);
  React.useEffect(() => {
    async function fetchEvents() {
      const clientEvents = await http.get('/event/client/index');
      console.log(clientEvents);
      setEvents(clientEvents.data.data.entities);
    }
    fetchEvents();
  }, [events.length]);

  const filterByDate = evs => {
    const formatted = evs.filter(e =>
      selectedTab === 'Прошедшие'
        ? moment(e.finishDate).isBefore(moment())
        : !moment(e.finishDate).isBefore(moment()),
    );
    return formatted;
  };
  return (
    <BaseScreen
      title="Мои ближайшие мероприятия"
      scrollableOutsideContainer
      withTabs
      tabsProps={{
        tabs: tabs,
        onTabPress: tab => setSelectedTab(tab),
        selectedTab: selectedTab,
      }}
      content={
        <>
          <S.VerticalScroll
            showsVerticalScrollIndicator={false}
            contentContainerStyle={{
              alignItems: 'center',
              paddingHorizontal: 20,
            }}>
            {filterByDate(events).map(e => (
              <Card
                onCardPress={() =>
                  props.navigation.navigate('EventMainScreen', {
                    event: e,
                    status: getClientStatus(e, props.myEvents),
                  })
                }
                item={e}
              />
            ))}
            <S.FakeBlock />
          </S.VerticalScroll>
        </>
      }
    />
  );
};

const mapStateToProps = ({events}) => ({
  myEvents: events.myEvents,
});

export default connect(
  mapStateToProps,
  null,
)(MyEventsScreen);
