import React from 'react';
import moment from 'moment';

import * as S from './styled';

import Card from './components/Card';
import http from '../../services/http';
import {getClientStatus, isClientEvent} from '../MainScreen/helpers';
import BaseScreen from '../../components/BaseScreen';
import {connect} from 'react-redux';

const tabs = ['Ближайшие', 'Прошедшие'];

const AllEventsScreen = props => {
  const [items, setItems] = React.useState([]);
  const [clientEvents, setClientEvents] = React.useState([]);
  const [selectedTab, setSelectedTab] = React.useState(tabs[0]);
  React.useEffect(() => {
    props.navigation.addListener('focus', async () => {
      const events = await http.get('/event/event/index');
      const client = await http.get('/event/client/index');
      setItems(events.data.data.entities);
      setClientEvents(client.data.data.entities);
    });
  });

  const filterByDate = evs => {
    const formatted = evs.filter(e =>
      selectedTab === 'Прошедшие'
        ? moment(e.finishDate).isBefore(moment())
        : !moment(e.finishDate).isBefore(moment()),
    );
    return formatted;
  };
  return (
    <BaseScreen
      title="Мероприятия"
      withTabs
      scrollableOutsideContainer
      tabsProps={{
        tabs: tabs,
        onTabPress: tab => setSelectedTab(tab),
        selectedTab: selectedTab,
      }}
      content={
        <S.VerticalScroll
          showsVerticalScrollIndicator={false}
          contentContainerStyle={{
            paddingHorizontal: 20,
            paddingBottom: 350,
            alignItems: 'center',
          }}>
          {filterByDate(items).map(item => (
            <Card
              onCardPress={() =>
                props.navigation.navigate('EventMainScreen', {
                  event: item,
                  status: getClientStatus(item, props.myEvents),
                })
              }
              isClient={isClientEvent(item, clientEvents)}
              key={item.id}
              item={item}
            />
          ))}
          {!filterByDate(items).length && selectedTab === 'Ближайшие' ? (
            <S.FakeBlock>
              <S.FakeText>Ближайших событий нет</S.FakeText>
            </S.FakeBlock>
          ) : null}
        </S.VerticalScroll>
      }
    />
  );
};

const mapStateToProps = ({events}) => ({
  myEvents: events.myEvents,
});

export default connect(
  mapStateToProps,
  null,
)(AllEventsScreen);
