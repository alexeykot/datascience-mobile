import React from 'react';
import {getTime} from '../../../../helpers/dateHelpers';

import * as S from './styled';

const Card = ({item, onCardPress, isClient}) => {
  const getCity = event => {
    if (!event.place) {
      return '';
    }
    const city = event.place.split(',')[1];
    return city;
  };
  return (
    <S.Container onPress={onCardPress}>
      <S.Image source={{uri: item.src.url}} />
      {isClient ? (
        <S.StatusContainer>
          <S.StatusText>я зарегестрирован</S.StatusText>
        </S.StatusContainer>
      ) : null}
      <S.Time>
        {getTime(item)}
        {item.place ? (
          <S.Time>
            {'    '}&#8226;{'    '}
          </S.Time>
        ) : null}
        {getCity(item)}
      </S.Time>
      <S.Title>{item.title}</S.Title>
    </S.Container>
  );
};

export default Card;
