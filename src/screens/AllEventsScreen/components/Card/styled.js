import styled from 'styled-components';

export const Container = styled.TouchableOpacity`
  width: 100%;
  margin-top: 26px;
`;

export const Image = styled.Image`
  width: 100%;
  height: 160px;
  border-radius: 15px;
  background-color: #f7f5fa;
  align-self: center;
`;

export const Time = styled.Text`
  color: #8b8d94;
  font-size: 12px;
  font-weight: 400;
  margin-top: 10px;
  /* padding-left: 10px; */
`;

export const Title = styled.Text`
  /* padding-left: 10px; */
  color: #1b041e;
  font-family: Roboto;
  font-size: 14px;
  font-weight: 700;
  /* margin-top: 8px; */
  text-align: left;
`;

export const StatusContainer = styled.View`
  padding: 8px 9px;
  /* background-color: ${({color}) => (color ? color : 'red')}; */
  background-color: #3dd576;
  position: absolute;
  top: 120;
  left: 10%;
  /* margin-left: 5%; */
  border-radius: 12px;
  width: 134px;
  height: 24px;
  justify-content: center;
  align-items: center;
`;

export const StatusText = styled.Text`
  color: #ffffff;
  font-family: Roboto;
  font-size: 10px;
  font-weight: 700;
  text-transform: uppercase;
`;
