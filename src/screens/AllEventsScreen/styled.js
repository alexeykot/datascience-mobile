import styled from 'styled-components';

export const VerticalScroll = styled.ScrollView`
  align-self: center;
  width: 100%;
`;

export const FakeBlock = styled.View`
  width: 100%;
  height: 400px;
  background-color: white;
  padding-top: 20px;
  justify-content: center;
  align-items: center;
`;

export const FakeText = styled.Text`
  color: #8b8d94;
  font-size: 14px;
  font-weight: 500;
  text-align: center;
  align-self: center;
  padding: 0 20px;
`;
