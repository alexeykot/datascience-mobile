import styled from 'styled-components';
import {Dimensions} from 'react-native';

export const Container = styled.ScrollView`
  flex: 1;
  background-color: white;
`;

export const InitialContainer = styled.View`
  height: ${Dimensions.get('window').height}px;
  width: 100%;
  background-color: white;
  justify-content: center;
  align-items: center;
`;

export const InitialButton = styled.TouchableOpacity`
  width: 160px;
  height: 40px;
  border-radius: 10px;
  background-color: #5954f9;
  justify-content: center;
  align-items: center;
  margin-top: 100px;
`;

export const InitialText = styled.Text`
  color: #130b04;
  font-weight: 700;
  font-size: 18px;
  text-align: center;
  padding: 0 40px;
`;

export const InitialButtonText = styled.Text`
  color: #ffffff;
  font-size: 14px;
`;

export const InitialLogo = styled.Image`
  width: 180px;
  height: 180px;
  margin-bottom: 53px;
`;

export const NumberPlaceholder = styled.Text`
  color: black;
  position: absolute;
  left: 13px;
  top: 20px;
`;

export const Input = styled.TextInput`
  height: 50px;
  flex: 6;
  padding-left: 25px;
`;

export const SendButton = styled.TouchableOpacity`
  width: 48px;
  height: 48px;
  border-radius: 48px;
  background-color: #5954f9;
  justify-content: center;
  align-items: center;
  /* opacity: ${({disabled}) => (disabled ? 0.3 : 1)}; */
`;

export const InputContainer = styled.View`
  flex-direction: row;
  height: 60px;
  width: 100%;
  position: relative;
  background-color: white;
  padding-right: 15px;
  align-items: center;
  border-color: #f7f5fa;
  border-top-width: 1px;
  margin-bottom: ${({keyboardShow}) => (keyboardShow ? keyboardShow : 0)};
`;

export const ChatMessage = styled.View`
  width: 100%;
  flex-direction: row;
  margin-bottom: ${({isInitialShow}) =>
    isInitialShow ? Dimensions.get('window').height - 65 : 20}px;
  /* margin-bottom: 20px; */
  margin-top: ${({isFirst}) => (isFirst ? '20px' : 0)};
  justify-content: ${({self}) => (!self ? 'flex-start' : 'flex-end')};
  padding: 0 8px;
`;

export const MessageContainer = styled.View`
  border-radius: 15px;
  border-bottom-left-radius: ${({self}) => (!self ? '0px' : '15px')};
  border-bottom-right-radius: ${({self}) => (self ? '0px' : '15px')};
  background-color: ${({self}) => (!self ? '#f7f5fa' : '#5954f9')};
  padding: 10px 15px;
  max-width: 320px;
`;

export const SelfMessageContainer = styled.View`
  max-width: 320px;
  justify-content: flex-end;
`;

export const SelfMessage = styled.View`
  color: white;
  font-size: 14px;
  font-weight: 400;
  padding: 10px 15px;
  border-radius: 15px;
  border-bottom-left-radius: ${({self}) => (!self ? '0px' : '15px')};
  border-bottom-right-radius: ${({self}) => (self ? '0px' : '15px')};
  background-color: #5954f9;
`;

export const SelfMessageText = styled.Text`
  color: white;
  font-size: 14px;
  font-weight: 400;
`;

export const SelfMessageInfo = styled.View`
  flex-direction: row;
  margin-right: 10px;
  align-items: center;
  justify-content: flex-end;
`;

export const CheckIcon = styled.Image`
  width: 16px;
  height: 9px;
  margin-left: 6px;
`;

export const Message = styled.Text`
  color: ${({self}) => (!self ? 'black' : 'white')};
  font-size: 14px;
  font-weight: 400;
`;

export const MessageTime = styled.Text`
  color: #8b8d94;
  font-size: 12px;
  font-weight: 400;
  align-self: flex-end;
`;

export const ChooseSexContainer = styled.View`
  flex-direction: row;
  /* position: absolute;
  bottom: 50px;
  left: 35%; */
  align-self: center;
`;

export const SexChooseButton = styled.TouchableOpacity`
  width: 50px;
  height: 50px;
  border-radius: 50px;
  background-color: green;
  justify-content: center;
  align-items: center;
`;

export const SexChooseText = styled.Text`
  color: white;
`;

export const DateChooseButton = styled.TouchableOpacity`
  width: 200px;
  height: 50px;
  border-radius: 20px;
  background-color: #5954f9;
  align-self: center;
  justify-content: center;
  align-items: center;
`;

export const CityContainer = styled.View`
  /* position: absolute;
  bottom: 50px;
  left: 33%; */
  background-color: white;
`;

export const CityButton = styled.TouchableOpacity`
  height: 50px;
  align-items: flex-start;
  justify-content: center;
  /* justify-content: flex-start; */
  border-color: #f7f5fa;
  border-top-width: 1px;
  padding-left: 25px;
`;

export const City = styled.Text`
  color: black;
`;

export const PhotoContainer = styled.View``;

export const Photo = styled.Image`
  height: 180px;
  width: 180px;
  border-radius: 6px;
`;
