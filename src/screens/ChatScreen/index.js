/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import TextInputMask from 'react-native-text-input-mask';
import DatePicker from 'react-native-date-picker';
import Emoji from 'react-native-emoji';
import moment from 'moment';
import {
  DatePickerAndroid,
  Alert,
  View,
  Keyboard,
  Dimensions,
  Image,
  PermissionsAndroid,
  TouchableOpacity,
  // TextInput as TextInputMask,
} from 'react-native';
import {connect} from 'react-redux';
import SmsListener from 'react-native-android-sms-listener';
import CountDown from 'react-native-countdown-component';

import {
  mobileConfirmRequest,
  requestFormData,
  requestSignUp,
  requestSignIn,
  requestSaveAvatar,
} from '../../redux/auth';
// import SexChooseMessage from './components/SexMessage';
// import AvatarPicker from '../AvatarPickScreen';
import {handleAndroidBackButton, exitAlert} from '../../helpers/backHandler';
import check from '../../assets/images/check.png';
import logo from '../../assets/images/logoMain.png';
// import SendIcon from '../../assets/icons/sendIcon2.svg';
import closeIcon from '../../assets/images/x-mark.png';
import sendIcon from '../../assets/images/sendIcon.png';
import Geolocation from '@react-native-community/geolocation';
import http from '../../services/http';

import * as S from './styled';
// import CityChooseMessage from './components/CityMessage';
import SexInput from './components/SexInput';
import CityInput from './components/CityInput';
import CodeInput from './components/CodeInput';
import PhotoInput from './components/PhotoInput';

const initialState = [
  {
    message: 'Для начала введите ваш номер телефона',
    self: false,
    type: 'number',
  },
];
const codeAnswers = [
  {
    message: 'С кодом все отлично, как вас зовут?',
    self: false,
    type: 'code-success-up',
  },
];
const wrongEmail = {
  message: 'Ой, это не e-mail ',
  self: false,
  type: 'wrong-email',
};

const botMessages = [
  {
    message: '',
    self: false,
    type: 'send-code',
  },
  {
    message: '',
    self: false,
    type: 'surname',
  },
  {
    message: 'И отчество.',
    self: false,
    type: 'thirdname',
  },
  {
    message: 'Выберите ваш пол',
    self: false,
    type: 'sex',
  },
  {
    message: 'Еще понадобится дата рождения',
    self: false,
    type: 'birthday',
  },
  {
    message: 'Ваш город?',
    self: false,
    type: 'city',
    city: '',
  },
  // {
  //   message: 'Еще понадобится дата рождения',
  //   self: false,
  //   type: 'birthday',
  // },
  // {
  //   message: 'А какой?',
  //   self: false,
  //   type: 'additional-city',
  // },
  {
    message: 'Осталось совсем немного. Введите ваш e-mail.',
    self: false,
    type: 'email',
  },
  {
    message: 'И, наконец, загрузите аватарку для вашего профиля',
    self: false,
    type: 'avatar',
  },
  {
    message: 'Отлично! Загружаю ваш личный кабинет...',
    self: false,
    type: 'finall',
  },
];

class ChatScreen extends React.Component {
  constructor(props) {
    super(props);
    this.scrollRef = React.createRef();
    this.inputRef = React.createRef();
  }
  state = {
    input: '',
    index: 0,
    cities: [],
    messages: initialState,
    isInitialShow: true,
    code: '',
    extractedNumber: '',
    smsPermissionGranted: true,
    isNewMember: true,
    keyboardShowHeight: 0,
    birthday: new Date('January 1, 2000 03:24:00'),
    isChangeNumberShow: false,
    inputRef: undefined,
    isCityInputVisible: false,
    cityChoosed: false,
  };
  async requestReadSmsPermission() {
    try {
      var granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.READ_SMS,
        {
          title: 'Auto Verification OTP',
          message: 'need access to read sms, to verify OTP',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        // alert('READ_SMS permissions granted', granted);
        this.setState({smsPermissionGranted: true});
        console.log('READ_SMS permissions granted', granted);
        granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.RECEIVE_SMS,
          {
            title: 'Receive SMS',
            message: 'Need access to receive sms, to verify OTP',
          },
        );
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          this.setState({smsPermissionGranted: true});
          // alert('RECEIVE_SMS permissions granted', granted);
          console.log('RECEIVE_SMS permissions granted', granted);
          SmsListener.addListener(async message => {
            // alert(message.originatingAddress);
            // console.log(message);
            try {
              await http.post('/cabinet/validation/phone', {
                phone: `7${this.state.extractedNumber}`,
              });
            } catch (error) {
              this.setState({isNewMember: false});
              const strArr = message.body.split(': ');
              const formatted = strArr[1].replace(/\s/g, '');
              const formattedNew = formatted.split('-');
              const code = formattedNew[0];
              this.props.requestSignIn({
                extractedNumber: this.state.extractedNumber,
                code: code,
              });
            }
            // this.setState({code: message.body});
            if (this.state.isNewMember) {
              setTimeout(
                () =>
                  this.setState(prevState => ({
                    messages: [...prevState.messages, codeAnswers[0]],
                  })),
                3000,
              );
            }
            //message.body will have the message.
            //message.originatingAddress will be the address.
          });
        } else {
          // alert('RECEIVE_SMS permissions denied');
          this.setState({smsPermissionGranted: false});
          console.log('RECEIVE_SMS permissions denied');
        }
      } else {
        this.setState({smsPermissionGranted: false});
        // alert('READ_SMS permissions denied');
        console.log('READ_SMS permissions denied');
      }
    } catch (err) {
      alert(err);
    }
  }
  componentWillReceiveProps(nextProps) {
    if (this.props.authenticated !== nextProps.authenticated) {
      if (!this.state.isNewMember) {
        const answer = {
          message: `Привет, ${
            nextProps.user.name
          }! Загружаю ваш личный кабинет`,
          self: false,
          type: 'initial-answer',
        };
        this.setState(prevState => ({
          messages: [...prevState.messages, answer],
        }));
      }
      setTimeout(() => this.props.navigation.navigate('TabsApp'), 4000);
    }
  }
  componentDidMount() {
    handleAndroidBackButton(exitAlert);
    this.requestReadSmsPermission();
    SmsListener.addListener(message => {
      console.log('message', message);
    });
  }
  componentWillMount() {
    this.keyboardDidShowSub = Keyboard.addListener(
      'keyboardDidShow',
      this.scrollByKeyboardAppear,
    );

    //IOS Check keyboard
    this.keyboardWillShowSub = Keyboard.addListener(
      'keyboardWillShow',
      this.handleKeyboardAppear,
    );
    this.keyboardWillHideSub = Keyboard.addListener(
      'keyboardWillHide',
      this.handleKeyboardDissapear,
    );
  }
  componentWillUnmount() {
    this.keyboardDidShowSub.remove();
    this.keyboardWillHideSub.remove();
    this.keyboardWillShowSub.remove();
  }

  handleKeyboardAppear = e => {
    // console.log('erere', e.endCoordinates.height);
    this.setState({keyboardShowHeight: e.endCoordinates.height});
  };

  handleKeyboardDissapear = () => this.setState({keyboardShowHeight: 0});

  scrollByKeyboardAppear = () => {
    if (
      this.state.messages[this.state.messages.length - 1].type !==
      'additional-city'
    ) {
      this.scrollRef.scrollToEnd({animated: true});
    }
  };
  findCities = async text => {
    // const {data} = await http.get(
    //   `https://search-maps.yandex.ru/v1/?format=json&apikey=f7e84c0d-9efa-42be-8dbb-1836c8a241dc&text=${text}&lang=ru_RU&type=geo`,
    // );
    const {data} = await http.get(`/yandex/suggest/index?term=${text}`);
    if (data.data) {
      this.setState({cities: data.data});
    } else {
      this.setState({cities: []});
    }
  };
  handleValidation = pattern => {
    const condition = new RegExp(pattern, 'g');
    const isValid = condition.test(this.state.input);
    return isValid;
  };

  fetchCity = async () => {
    Geolocation.getCurrentPosition(
      position => console.log(position),
      error => console.log(error.message),
    );

    const {data} = await http.get(
      'https://geocode-maps.yandex.ru/1.x/?format=json&apikey=742d386e-2303-4933-8495-63c7f79cbdd6&geocode=37.611347,55.760241',
    );
    return data.response.GeoObjectCollection.featureMember[0].GeoObject
      .description;
  };
  chooseDate = async () => {
    const {day, month, year} = await DatePickerAndroid.open();
    // console.log(date);
    this.sexChoose(`${day}-${month}-${year}`);
  };
  onCodeConfirm = async () => {
    try {
      const isCodeValid = await this.codeVerification(this.state.input);
      // if (!isCodeValid) {
      //   alert('Код неверный');
      //   return;
      // }
      await http.post('/cabinet/validation/phone', {
        phone: `7${this.state.extractedNumber}`,
      });
      this.setState(prevState => ({
        messages: [...prevState.messages, codeAnswers[0]],
      }));
      this.setState({input: ''});
    } catch (error) {
      this.setState({isNewMember: false});
      console.log('sing ined');
      this.props.requestSignIn({
        extractedNumber: this.state.extractedNumber,
        code: this.state.input,
      });
    }
    // ADD handle code
  };

  codeVerification = async code => {
    try {
      const data = await http.post('/verify/default/phone-verify', {
        entity: `7${this.state.extractedNumber}`,
        code: code,
      });
      if (data.data.code === 200) {
        return true;
      }
    } catch (error) {
      // alert(error.response.data.data.message);
      return false;
    }
  };
  changeNumber = () => {
    console.log('changeNumber');
    botMessages[0].message = `Я отправил код на номер ${
      this.state.input
    } Буквально минутку, я его проверю...`;
    this.props.mobileConfirmRequest(this.state.extractedNumber);
    this.props.requestFormData('phoneNumber', `7${this.state.extractedNumber}`);
    const messages = this.state.messages;
    messages[1].message = this.state.input;
    this.setState({input: '', isChangeNumberShow: false, messages: messages});
    console.log('this.state', this.state);
  };
  addEntryClick = async pattern => {
    const {messages, index, input, birthday} = this.state;
    if (
      messages[messages.length - 1].type !== 'birthday' &&
      messages[messages.length - 1].type !== 'city'
    ) {
      const isValid = this.handleValidation(pattern);
      if (!isValid) {
        if (
          messages[messages.length - 1].type === 'email' ||
          messages[messages.length - 1].type === 'wrong-email'
        ) {
          this.setState(prevState => ({
            messages: [...prevState.messages, wrongEmail],
          }));
          return;
        }
        return Alert.alert(
          'Некорректное значение',
          'Может содержать только буквы',
        );
      }
    }
    // PHONE NUMBER
    if (index === 0) {
      console.log('index === 0');
      // botMessages[0].message = `Я отправил код на номер ${input} Буквально минутку, я его проверю...`;
      botMessages[0].message = `${input}`;
      this.props.mobileConfirmRequest(this.state.extractedNumber);
      this.props.requestFormData(
        'phoneNumber',
        `7${this.state.extractedNumber}`,
      );
    }
    // NAME
    if (index === 1) {
      botMessages[1].message = `${input}, рад знакомству! Еще понадобится фаша фамилия.`;
      this.props.requestFormData('name', input);
    }
    if (index === 2) {
      this.props.requestFormData('surname', input);
    }
    if (index === 3) {
      this.props.requestFormData('patronymic', input);
    }
    if (
      messages[messages.length - 1].type === 'email' ||
      messages[messages.length - 1].type === 'wrong-email'
    ) {
      this.props.requestFormData('email', input);
    }
    if (messages[messages.length - 1].type === 'birthday') {
      this.props.requestFormData(
        'birthday',
        moment(birthday).format('YYYY-DD-MM'),
      );
    }
    if (messages[messages.length - 1].type === 'city') {
      this.props.requestFormData('address', input);
    }
    this.setState(prevState => ({
      messages: [
        ...prevState.messages,
        {
          message: input,
          self: true,
          type: 'self',
        },
        botMessages[index],
      ],
    }));
    // if (index === 0) {
    //   setTimeout(
    //     () =>
    //       this.setState(prevState => ({
    //         messages: [...prevState.messages, codeAnswers[0]],
    //       })),
    //     2000,
    //   );
    // }
    this.setState({index: this.state.index + 1});

    this.setState({input: ''});
    this.scrollRef.scrollToEnd({animated: true});
  };
  dateChoose = date => {
    this.props.requestFormData('birthday', moment(date).format('YYYY-MM-DD'));
    this.setState(prevState => ({
      messages: [
        ...prevState.messages,
        {
          message: moment(date).format('DD.MM.YYYY'),
          self: true,
          type: 'self',
        },
        botMessages[this.state.index],
      ],
    }));
    this.setState({index: this.state.index + 1});

    this.setState({input: ''});
    this.scrollRef.scrollToEnd({animated: true});
  };
  sexChoose = async sex => {
    const {index} = this.state;
    if (index === 4) {
      try {
        const data = await this.fetchCity();
        botMessages[4].city = data;
      } catch (err) {
        console.log('err', err);
        botMessages[4].city = 'data';
      }
    }
    this.props.requestFormData('sex', sex === 'мужской' ? 1 : 2);
    this.setState(prevState => ({
      messages: [
        ...prevState.messages,
        {
          message: sex,
          self: true,
          type: 'self',
        },
        botMessages[index],
      ],
    }));
    this.setState({index: this.state.index + 1});
    this.setState({input: ''});
  };
  avatarChoose = source => {
    console.log('source', source);
    const {index} = this.state;
    this.setState(prevState => ({
      messages: [
        ...prevState.messages,
        {
          message: source,
          self: true,
          type: 'my-avatar',
        },
        botMessages[index],
      ],
    }));
    // const avatar = {uri: 'data:image/jpeg;base64,' + source.data};
    this.props.requestSaveAvatar(source.data);
    this.setState({index: this.state.index + 1});
    this.setState({input: ''});
  };
  skipAvatarChoose = () => {
    const {index} = this.state;
    this.setState(prevState => ({
      messages: [...prevState.messages, botMessages[index]],
    }));
    // const avatar = {uri: 'data:image/jpeg;base64,' + source.data};
    this.setState({index: this.state.index + 1});
    this.setState({input: ''});
  };
  cityChoose = value => {
    const {index} = this.state;
    this.props.requestFormData('address', value);
    this.setState(prevState => ({
      messages: [
        ...prevState.messages,
        {
          message: value,
          self: true,
          type: 'self',
        },
        botMessages[index],
      ],
    }));
    this.setState({index: this.state.index + 1});

    this.setState({input: ''});
    this.scrollRef.scrollToEnd({animated: true});
  };

  setCityInputVisible = visible => {
    if (!visible) {
      this.setState({inputRef: undefined});
    }
    this.setState({isCityInputVisible: visible});
  };
  resendCode = () => {
    this.props.mobileConfirmRequest(this.state.extractedNumber);
    this.props.requestFormData('phoneNumber', `7${this.state.extractedNumber}`);
  };
  renderInput = () => {
    const {messages, input} = this.state;
    switch (messages[messages.length - 1].type) {
      // case 'number1':
      //   return <CodeInput />;
      case 'number':
        return (
          <>
            {!this.state.isInitialShow && (
              <S.InputContainer keyboardShow={this.state.keyboardShowHeight}>
                <TextInputMask
                  // refInput={ref => {
                  //   this.input = ref;
                  // }}
                  onChangeText={(formatted, extracted) => {
                    // onChangeText(extracted);
                    // onChangeFormatted(formatted);
                    this.setState({extractedNumber: extracted});
                    this.setState({input: `+7 ${formatted}`});
                  }}
                  style={{
                    flex: 6,
                    height: 50,
                    paddingLeft: 33,
                  }}
                  placeholder="Номер телефона"
                  autoFocus={true}
                  keyboardType="number-pad"
                  mask={'([000]) [000]-[00]-[00]'}
                />
                <S.NumberPlaceholder>+7</S.NumberPlaceholder>
                {input.length === 18 && (
                  <S.SendButton onPress={() => this.addEntryClick()}>
                    {/* <SendIcon height={22} width={20} /> */}
                    <Image source={sendIcon} style={{width: 20, height: 22}} />
                  </S.SendButton>
                )}
              </S.InputContainer>
            )}
          </>
        );
      case 'email':
        return (
          <S.InputContainer keyboardShow={this.state.keyboardShowHeight}>
            <S.Input
              placeholder="Напишите сообщение..."
              value={input}
              onChangeText={async text => {
                this.setState({input: text});
              }}
            />
            {input !== '' ? (
              <S.SendButton
                disabled={input === ''}
                onPress={() =>
                  this.addEntryClick(
                    /(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/,
                  )
                }>
                {/* <SendIcon height={22} width={20} /> */}
                <Image source={sendIcon} style={{width: 20, height: 22}} />
              </S.SendButton>
            ) : null}
          </S.InputContainer>
        );
      case 'wrong-email':
        return (
          <S.InputContainer keyboardShow={this.state.keyboardShowHeight}>
            <S.Input
              placeholder="Напишите сообщение..."
              value={input}
              onChangeText={async text => {
                this.setState({input: text});
              }}
            />
            {input !== '' ? (
              <S.SendButton
                disabled={input === ''}
                onPress={() =>
                  this.addEntryClick(
                    /(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/,
                  )
                }>
                {/* <SendIcon height={22} width={20} /> */}
                <Image source={sendIcon} style={{width: 20, height: 22}} />
              </S.SendButton>
            ) : null}
          </S.InputContainer>
        );
      case 'birthday':
        const {birthday} = this.state;
        return (
          <>
            <S.InputContainer>
              <S.Input
                value={moment(birthday).format('DD.MM.YYYY')}
                editable={false}
              />
              <S.SendButton
                onPress={() => this.dateChoose(this.state.birthday)}>
                {/* <SendIcon height={22} width={20} /> */}
                <Image source={sendIcon} style={{width: 20, height: 22}} />
              </S.SendButton>
            </S.InputContainer>
            <DatePicker
              style={{
                backgroundColor: 'white',
                width: Dimensions.get('screen').width,
                borderTopWidth: 1,
                borderColor: '#f7f5fa',
              }}
              mode="date"
              date={this.state.birthday}
              onDateChange={date => {
                this.setState({
                  birthday: date,
                  input: moment(date).format('DD.MM.YYYY'),
                });
              }}
            />
          </>
        );
      case 'send-code':
        if (this.state.smsPermissionGranted) {
          return (
            <S.InputContainer>
              <CountDown
                until={9}
                onFinish={() => this.setState({smsPermissionGranted: false})}
                size={10}
                digitTxtStyle={{
                  color: 'transparent',
                }}
                digitStyle={{
                  display: 'none',
                }}
                timeToShow={['S']}
                timeLabels={{m: null, s: null}}
                // style={{
                //   position: 'absolute',
                //   bottom: -40,
                // }}
              />
              <S.Input
                value={this.state.code}
                placeholder="Ваш код..."
                editable={false}
              />
            </S.InputContainer>
          );
        } else {
          return (
            <>
              {!this.state.isChangeNumberShow ? (
                <CodeInput
                  resendCode={this.resendCode}
                  onChangeNumberPress={() =>
                    this.setState({isChangeNumberShow: true})
                  }
                  input={() => (
                    <S.InputContainer
                      keyboardShow={this.state.keyboardShowHeight}>
                      <S.Input
                        value={input}
                        keyboardType="numeric"
                        placeholder="Напишите код"
                        onChangeText={text => {
                          this.setState({input: text});
                        }}
                      />
                      {input !== '' ? (
                        <S.SendButton onPress={this.onCodeConfirm}>
                          {/* <SendIcon height={22} width={20} /> */}
                          <Image
                            source={sendIcon}
                            style={{width: 20, height: 22}}
                          />
                        </S.SendButton>
                      ) : null}
                    </S.InputContainer>
                  )}
                />
              ) : (
                <S.InputContainer keyboardShow={this.state.keyboardShowHeight}>
                  <TextInputMask
                    // refInput={ref => {
                    //   this.input = ref;
                    // }}
                    onChangeText={(formatted, extracted) => {
                      // onChangeText(extracted);
                      // onChangeFormatted(formatted);
                      this.setState({extractedNumber: extracted});
                      this.setState({input: `+7 ${formatted}`});
                    }}
                    style={{
                      flex: 6,
                      height: 50,
                      paddingLeft: 33,
                    }}
                    placeholder="Номер телефона"
                    autoFocus={true}
                    keyboardType="number-pad"
                    mask={'([000]) [000]-[00]-[00]'}
                  />
                  <S.NumberPlaceholder>+7</S.NumberPlaceholder>
                  {input.length === 18 && (
                    <S.SendButton onPress={() => this.changeNumber()}>
                      {/* <SendIcon height={22} width={20} /> */}
                      <Image
                        source={sendIcon}
                        style={{width: 20, height: 22}}
                      />
                    </S.SendButton>
                  )}
                </S.InputContainer>
              )}
            </>
          );
        }
      case 'finall':
        this.props.requestSignUp();
        return null;
      case 'code-success-up':
        return (
          <S.InputContainer keyboardShow={this.state.keyboardShowHeight}>
            <S.Input
              value={input}
              placeholder="Напишите сообщение..."
              onChangeText={text => {
                this.setState({input: text});
              }}
            />
            {input !== '' ? (
              <S.SendButton onPress={() => this.addEntryClick()}>
                {/* <SendIcon height={22} width={20} /> */}
                <Image source={sendIcon} style={{width: 20, height: 22}} />
              </S.SendButton>
            ) : null}
          </S.InputContainer>
        );
      case 'sex':
        return <SexInput onButtonPress={this.sexChoose} />;
      case 'birthday':
        return null;
      case 'city':
        return (
          <CityInput
            onButtonPress={this.cityChoose}
            city={botMessages[4].city}
            setCityInputVisible={this.setCityInputVisible}
            inputRef={this.inputRef}
            inputCreated={this.state.inputRef}
            isCityInputVisible={this.state.isCityInputVisible}
            input={
              <>
                <S.InputContainer>
                  <S.Input
                    value={input}
                    ref={ref => {
                      if (!this.state.inputRef) {
                        this.setState({inputRef: true});
                      }
                      this.inputRef = ref;
                    }}
                    // onFocus={this.onFocusInput}
                    onChangeText={async text => {
                      this.setState({input: text});
                      await this.findCities(text);
                    }}
                  />
                  {this.state.input && !this.state.cityChoosed ? (
                    <TouchableOpacity
                      onPress={() => {
                        this.setCityInputVisible(false);
                        this.setState({input: ''});
                      }}>
                      <Image
                        source={closeIcon}
                        style={{width: 15, height: 15}}
                      />
                    </TouchableOpacity>
                  ) : null}
                  {this.state.cityChoosed ? (
                    <S.SendButton
                      disabled={input === ''}
                      onPress={() => this.cityChoose(this.state.input)}>
                      {/* <SendIcon height={22} width={20} /> */}
                      <Image
                        source={sendIcon}
                        style={{width: 20, height: 22}}
                      />
                    </S.SendButton>
                  ) : null}
                </S.InputContainer>
                {this.state.cities.length && input ? (
                  <S.CityContainer>
                    {this.state.cities.map(city => (
                      <S.CityButton
                        onPress={() => {
                          this.setState({input: city});
                          this.setState({cityChoosed: true});
                        }}>
                        <S.City>{city}</S.City>
                      </S.CityButton>
                    ))}
                  </S.CityContainer>
                ) : null}
              </>
            }
          />
        );
      case 'additional-city':
        return (
          <>
            <S.InputContainer>
              <S.Input
                value={input}
                // onFocus={this.onFocusInput}
                onChangeText={async text => {
                  this.setState({input: text});
                  await this.findCities(text);
                }}
              />
              <S.SendButton
                disabled={input === ''}
                onPress={() => this.addEntryClick()}
              />
            </S.InputContainer>
            {this.state.cities.length && input ? (
              <S.CityContainer>
                {this.state.cities.map(city => (
                  <S.CityButton
                    onPress={() =>
                      this.setState({input: city.properties.name})
                    }>
                    <S.City>{city.properties.name}</S.City>
                  </S.CityButton>
                ))}
              </S.CityContainer>
            ) : null}
          </>
        );
      case 'avatar':
        return (
          <PhotoInput
            onButtonPress={this.avatarChoose}
            onSkipPress={this.skipAvatarChoose}
          />
        );
      default:
        return (
          <>
            {!this.state.isInitialShow && (
              <S.InputContainer keyboardShow={this.state.keyboardShowHeight}>
                <S.Input
                  placeholder="Напишите сообщение..."
                  value={input}
                  autoFocus={true}
                  onChangeText={async text => {
                    this.setState({input: text});
                    // await findCities(text);
                  }}
                />
                {input !== '' ? (
                  <S.SendButton
                    disabled={input === ''}
                    onPress={() => this.addEntryClick(/^[A-zА-яЁё]+$/)}>
                    {/* <SendIcon height={22} width={20} /> */}
                    <Image source={sendIcon} style={{width: 20, height: 22}} />
                  </S.SendButton>
                ) : null}
              </S.InputContainer>
            )}
          </>
        );
    }
  };
  render() {
    return (
      <>
        {this.state.messages[this.state.messages.length - 1].type !==
          'additional-city' && (
          <S.Container
            scrollEnabled={!this.state.isInitialShow}
            ref={ref => {
              this.scrollRef = ref;
            }}
            onContentSizeChange={(e, h) => {
              if (!this.state.isInitialShow) {
                this.scrollRef.scrollTo({x: 0, y: h, animated: true});
              }
            }}>
            {this.state.isInitialShow && (
              <S.InitialContainer>
                <S.InitialLogo source={logo} />
                <S.InitialText>
                  Привет! Я робот DS:ID. Помогаю в работе с приложением.
                </S.InitialText>
                <S.InitialButton
                  onPress={() => {
                    // this.setState({isInitialShow: false});
                    this.scrollRef.scrollTo({
                      x: 0,
                      y: Dimensions.get('window').height,
                      animated: true,
                    });
                    setTimeout(
                      () => this.setState({isInitialShow: false}),
                      600,
                    );
                  }}>
                  <S.InitialButtonText>Привет!</S.InitialButtonText>
                </S.InitialButton>
              </S.InitialContainer>
            )}
            {this.state.messages.map((message, index) => {
              if (message.type === 'send-code') {
                return (
                  <S.ChatMessage
                    isInitialShow={this.state.isInitialShow}
                    isFirst={index === 0}
                    self={message.self}>
                    <S.MessageContainer self={message.self}>
                      <S.Message self={message.self}>
                        Я отправил код на номер
                      </S.Message>
                      <S.Message self={message.self}>
                        {message.message}
                      </S.Message>
                      <S.Message self={message.self}>
                        Буквально минутку, я его проверю...
                      </S.Message>
                      <S.MessageTime>{`${new Date().getHours()}:${new Date().getMinutes()}`}</S.MessageTime>
                    </S.MessageContainer>
                  </S.ChatMessage>
                );
              }
              if (message.type === 'my-avatar') {
                return (
                  <S.ChatMessage self={message.self}>
                    <S.PhotoContainer>
                      <S.Photo source={{uri: message.message.uri}} />
                      <S.SelfMessageInfo>
                        <S.MessageTime>{`${new Date().getHours()}:${new Date().getMinutes()}`}</S.MessageTime>
                        <S.CheckIcon source={check} />
                      </S.SelfMessageInfo>
                    </S.PhotoContainer>
                  </S.ChatMessage>
                );
              }
              if (message.self) {
                return (
                  <S.ChatMessage
                    isInitialShow={this.state.isInitialShow}
                    self={message.self}>
                    <S.SelfMessageContainer>
                      <S.SelfMessage self={message.self}>
                        <S.SelfMessageText>{message.message}</S.SelfMessageText>
                      </S.SelfMessage>
                      <S.SelfMessageInfo>
                        <S.MessageTime>{`${new Date().getHours()}:${new Date().getMinutes()}`}</S.MessageTime>
                        <S.CheckIcon source={check} />
                      </S.SelfMessageInfo>
                    </S.SelfMessageContainer>
                  </S.ChatMessage>
                );
              }
              if (message.type === 'wrong-email') {
                return (
                  <S.ChatMessage
                    isInitialShow={this.state.isInitialShow}
                    isFirst={index === 0}
                    self={message.self}>
                    <S.MessageContainer self={message.self}>
                      <S.Message self={message.self}>
                        {message.message}
                        <Emoji name="disappointed" style={{fontSize: 14}} />
                        {'Пожалуйста, введите корректный адрес.'}
                      </S.Message>
                      <S.MessageTime>{`${new Date().getHours()}:${new Date().getMinutes()}`}</S.MessageTime>
                    </S.MessageContainer>
                  </S.ChatMessage>
                );
              }
              return (
                <S.ChatMessage
                  isInitialShow={this.state.isInitialShow}
                  isFirst={index === 0}
                  self={message.self}>
                  <S.MessageContainer self={message.self}>
                    <S.Message self={message.self}>{message.message}</S.Message>
                    <S.MessageTime>{`${new Date().getHours()}:${new Date().getMinutes()}`}</S.MessageTime>
                  </S.MessageContainer>
                </S.ChatMessage>
              );
              // }
            })}
            {/* {this.state.messages[this.state.messages.length - 1].type ===
            'avatar' ? (
              <AvatarPicker onPick={this.avatarChoose} />
            ) : null} */}
            {/* <View style={{backgroundColor: 'red', height: 1000, width: 100}}></View> */}
          </S.Container>
        )}
        {this.renderInput()}
      </>
    );
  }
}

const mapDispatchToProps = {
  mobileConfirmRequest,
  requestFormData,
  requestSignUp,
  requestSignIn,
  requestSaveAvatar,
};

const mapStateToProps = ({auth}) => ({
  user: auth.userData,
  authenticated: auth.authenticated,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ChatScreen);

// export default ChatScreen;
