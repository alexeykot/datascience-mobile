import styled from 'styled-components';

export const ButtonsContainer = styled.View`
  width: 100%;
  height: 60px;
  flex-direction: row;
  justify-content: space-evenly;
  background-color: white;
  align-items: center;
`;

export const Button = styled.TouchableOpacity`
  width: 150px;
  height: 40px;
  border-radius: 10px;
  background-color: ${({isOther}) => (isOther ? 'white' : '#5954f9')};
  justify-content: center;
  align-items: center;
  border: 2px solid #5954f9;
`;

export const ButtonText = styled.Text`
  color: ${({isOther}) => (isOther ? '#5954f9' : 'white')};
  font-weight: 500;
`;
