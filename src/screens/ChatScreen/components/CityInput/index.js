import React from 'react';

import * as S from './styled';

const CityInput = ({
  onButtonPress,
  city,
  input,
  inputRef,
  inputCreated,
  setCityInputVisible,
  isCityInputVisible,
}) => {
  React.useEffect(() => {
    if (inputCreated && inputRef) {
      inputRef.focus();
    }
  });
  const formattedCity = city.split(',');
  // const formattedCity = 'Vjcrcr';
  const [isOtherCity, setIsOtherCity] = React.useState(false);
  const onOtherCityClick = () => {
    setCityInputVisible(true);
  };
  return (
    <>
      {!isCityInputVisible ? (
        <S.ButtonsContainer>
          <S.Button onPress={() => onButtonPress(formattedCity[0])}>
            <S.ButtonText>{formattedCity[0]}</S.ButtonText>
          </S.Button>
          <S.Button isOther onPress={onOtherCityClick}>
            <S.ButtonText isOther>Другой</S.ButtonText>
          </S.Button>
        </S.ButtonsContainer>
      ) : (
        input
      )}
    </>
  );
};

export default CityInput;
