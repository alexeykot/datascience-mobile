import styled from 'styled-components';

export const ButtonsContainer = styled.View`
  width: 100%;
  height: 60px;
  flex-direction: row;
  justify-content: space-evenly;
  background-color: white;
  align-items: center;
`;

export const Button = styled.TouchableOpacity`
  width: 150px;
  height: 40px;
  border-radius: 10px;
  background-color: #5954f9;
  justify-content: center;
  align-items: center;
`;

export const ButtonText = styled.Text`
  color: white;
`;
