import React from 'react';

import * as S from './styled';

const SexInput = ({onButtonPress}) => {
  return (
    <S.ButtonsContainer>
      <S.Button onPress={() => onButtonPress('мужской')}>
        <S.ButtonText>Мужской</S.ButtonText>
      </S.Button>
      <S.Button onPress={() => onButtonPress('женский')}>
        <S.ButtonText>Женский</S.ButtonText>
      </S.Button>
    </S.ButtonsContainer>
  );
};

export default SexInput;
