import styled from 'styled-components';

export const Container = styled.View`
  border-radius: 15px;
  border-bottom-left-radius: 0;
  border-bottom-right-radius: 15px;
  background-color: white;
  padding: 10px 15px;
  max-width: 320px;
`;

export const ButtonsContainer = styled.View`
  width: 100%;
  flex-direction: row;
  justify-content: space-between;
`;

export const Button = styled.TouchableOpacity`
  width: 100px;
  height: 60px;
  border-radius: 10px;
  background-color: #5954f9;
`;

export const ButtonText = styled.Text`
  color: white;
`;

export const Message = styled.Text`
  color: ${({self}) => (!self ? 'black' : 'white')};
  font-size: 14px;
  font-weight: 400;
`;

export const MessageTime = styled.Text`
  color: #8b8d94;
  font-size: 12px;
  font-weight: 400;
  align-self: flex-end;
`;
