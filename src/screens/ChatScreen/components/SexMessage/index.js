import React from 'react';

import * as S from './styled';

const SexChooseMessage = ({onButtonPress, index, title, city}) => {
  return (
    <S.Container>
      <S.Message>{title}</S.Message>
      {index < 5 ? (
        <S.ButtonsContainer>
          <S.Button onPress={() => onButtonPress('мужской')}>
            <S.ButtonText>Мужской</S.ButtonText>
          </S.Button>
          <S.Button onPress={() => onButtonPress('женский')}>
            <S.ButtonText>Женский</S.ButtonText>
          </S.Button>
        </S.ButtonsContainer>
      ) : null}
      <S.MessageTime>12:32</S.MessageTime>
    </S.Container>
  );
};

export default SexChooseMessage;
