import React from 'react';

import * as S from './styled';
import CountDown from 'react-native-countdown-component';

const CodeInput = ({onButtonPress, input, resendCode, onChangeNumberPress}) => {
  const [resendAvailable, setResendAvailable] = React.useState(false);
  const [showInput, setShowInput] = React.useState(false);
  const onResendPress = () => {
    resendCode();
    setResendAvailable(false);
  };
  return (
    <>
      {!showInput ? (
        <S.ButtonsContainer>
          <S.TopButtonsContainer>
            <S.Button>
              <S.ButtonText onPress={() => setShowInput(true)}>
                Ввести код вручную
              </S.ButtonText>
            </S.Button>
            <S.Button onPress={onChangeNumberPress}>
              <S.ButtonText>Изменить номер</S.ButtonText>
            </S.Button>
          </S.TopButtonsContainer>
          {!resendAvailable ? (
            <S.ResendButton>
              <S.ResendText>Отправить СМС повторно через 00:</S.ResendText>
              <CountDown
                until={9}
                onFinish={() => setResendAvailable(true)}
                size={10}
                digitTxtStyle={{
                  color: 'black',
                  fontSize: 14,
                  fontWeight: '300',
                }}
                digitStyle={{
                  backgroundColor: 'rgba(252, 252, 252, 0.6)',
                  marginLeft: -3,
                }}
                timeToShow={['S']}
                timeLabels={{m: null, s: null}}
                // style={{
                //   position: 'absolute',
                //   bottom: -40,
                // }}
              />
            </S.ResendButton>
          ) : (
            <S.Button isSend={true} onPress={onResendPress}>
              <S.ButtonText>Отправить СМС</S.ButtonText>
            </S.Button>
          )}
        </S.ButtonsContainer>
      ) : (
        input()
      )}
    </>
  );
};

export default CodeInput;
