import styled from 'styled-components';

export const ButtonsContainer = styled.View`
  width: 100%;
  /* height: 60px; */
  background-color: white;
  border-color: #f7f5fa;
  border-top-width: 1px;
`;

export const TopButtonsContainer = styled.View`
  width: 100%;
  padding: 7px 15px 0 14px;
  flex-direction: row;
  justify-content: space-between;
`;

export const ResendButton = styled.TouchableOpacity`
  padding: 14px 20px;
  background-color: white;
  border: 1px solid #5954f9;
  border-radius: 10px;
  margin: 10px 50px 10px 14px;
  flex-direction: row;
  align-items: center;
`;

export const ResendText = styled.Text`
  color: #1b041e;
  font-size: 14px;
`;

export const Button = styled.TouchableOpacity`
  /* width: 90px;
  height: 40px; */
  margin: ${({isSend}) => (isSend ? '10px 50px 10px 14px' : 0)};
  padding: 14px 20px;
  border-radius: 10px;
  background-color: #5954f9;
  justify-content: center;
  align-items: center;
`;

export const ButtonText = styled.Text`
  color: white;
  font-weight: 500;
`;
