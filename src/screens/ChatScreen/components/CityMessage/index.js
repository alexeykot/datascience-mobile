import React from 'react';

import * as S from './styled';

const CityChooseMessage = ({onButtonPress, index, title, city}) => {
  return (
    <S.Container>
      <S.Message>{title}</S.Message>
      {index === 5 ? (
        <S.ButtonsContainer>
          <S.Button onPress={() => onButtonPress(city)}>
            <S.ButtonText>{city}</S.ButtonText>
          </S.Button>
          <S.Button onPress={() => onButtonPress('Другой')}>
            <S.ButtonText>Другой</S.ButtonText>
          </S.Button>
        </S.ButtonsContainer>
      ) : null}
      <S.MessageTime>12:32</S.MessageTime>
    </S.Container>
  );
};

export default CityChooseMessage;
