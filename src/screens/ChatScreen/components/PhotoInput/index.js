import React from 'react';
import ImagePicker from 'react-native-image-picker';

import * as S from './styled';

const PhotoInput = ({onButtonPress, onSkipPress}) => {
  const chooseFile = () => {
    var options = {
      title: 'Выбрать фотографию',
      // customButtons: [{name: 'customOptionKey', title: 'Удалить фото'}],
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };
    ImagePicker.showImagePicker(options, response => {
      console.log('Response = ', response);
      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
        // } else if (response.customButton) {
        //   console.log('User tapped custom button: ', response.customButton);
        //   // alert(response.customButton);
        //   // this.setState({filePath: {}});
      } else {
        let source = response;
        // You can also display the image using data:
        // let source = { uri: 'data:image/jpeg;base64,' + response.data };
        // this.setState({
        //   filePath: source,
        // });
        onButtonPress(source);
      }
    });
  };
  return (
    <S.ButtonsContainer>
      <S.Button onPress={() => chooseFile()}>
        <S.ButtonText>Выбрать фото</S.ButtonText>
      </S.Button>
      <S.Button isOther onPress={() => onSkipPress()}>
        <S.ButtonText isOther>Пропустить</S.ButtonText>
      </S.Button>
    </S.ButtonsContainer>
  );
};

export default PhotoInput;
