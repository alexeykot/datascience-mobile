import {combineReducers} from 'redux';

import auth from './auth';
import chat from './chat';
import events from './events';

const appReducer = combineReducers({
  auth,
  chat,
  events,
});

const rootReducer = (state, action) => {
  if (action.type === 'USER_LOGOUT') {
    state = undefined;
  }

  return appReducer(state, action);
};

export default rootReducer;
