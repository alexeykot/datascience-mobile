import {call, put, takeEvery} from 'redux-saga/effects';

const SET_MY_EVENTS = 'events/SET_MY_EVENTS';

export const initialState = {
  myEvents: [],
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case SET_MY_EVENTS:
      return {...state, myEvents: action.payload};
    default:
      return state;
  }
}

// <<<ACTIONS>>>
export const setMyEvents = events => ({
  type: SET_MY_EVENTS,
  payload: events,
});

// <<<WORKERS>>>

// <<<WATCHERS>>>
