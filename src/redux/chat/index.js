import {
  call,
  put,
  takeEvery,
  select,
  takeLatest,
  cancel,
} from 'redux-saga/effects';

import httpChat from '../../services/http/chat';
import http from '../../services/http';
import ChatStorage from '../../services/storage/chat';

const GET_MYSELF_REQUEST = 'chat/GET_MYSELF';
const GET_MYSELF_SUCCESS = 'chat/GET_MYSELF_SUCCESS';

const SET_SOCKET = 'chat/SET_SOCKET';

const GET_ROOMINFO_REQUEST = 'chat/GET_ROOMINFO_REQUEST';
const GET_ROOMINFO_SUCCESS = 'chat/GET_ROOMINFO_SUCCESS';

export const initialState = {
  me: null,
  unread: null,
  socket: null,
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case GET_MYSELF_REQUEST:
      return {...state, loading: true};
    case GET_MYSELF_SUCCESS:
      return {
        ...state,
        me: action.me,
      };
    case SET_SOCKET:
      return {...state, socket: action.payload};
    default:
      return state;
  }
}

// <<<ACTIONS>>>
export const requestGetMyself = () => ({
  type: GET_MYSELF_REQUEST,
});

export const setSocket = socket => ({
  type: SET_SOCKET,
  payload: socket,
});

// <<<WORKERS>>>
function* getMyself() {
  try {
    const {data} = yield call(httpChat.get, '/api/v1/me');
    yield put({type: GET_MYSELF_SUCCESS, me: data});
  } catch (err) {
    console.log(err.response);
    if (err.reponse.status === 401) {
      const {data} = yield call(http.get, '/cabinet/profile/token');
      yield call(ChatStorage.save, data.data.entity);
      const {data: meData} = yield call(httpChat.get, '/api/v1/me');
      yield put({type: GET_MYSELF_SUCCESS, me: meData});
    }
  }
}

// <<<WATCHERS>>>
export function* watchGetMyself() {
  yield takeEvery(GET_MYSELF_REQUEST, getMyself);
}
