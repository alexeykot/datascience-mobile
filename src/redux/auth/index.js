import {
  call,
  put,
  takeEvery,
  select,
  takeLatest,
  cancel,
} from 'redux-saga/effects';
import messaging from '@react-native-firebase/messaging';
import {
  formCredentials,
  updateNotifications,
  updateOptionStatus,
} from './helpers';
import http from '../../services/http';
import TokenStorage from '../../services/storage/token';
import DeviceTokenStorage from '../../services/storage/deviceToken';
import PhoneNumberStorage from '../../services/storage/phoneNumber';
import {Platform} from 'react-native';

const SIGNUP_REQUEST = 'auth/SIGNUP_REQUEST';
const SIGNUP_SUCCESS = 'auth/SIGNUP_SUCCESS';
const SIGNUP_FAILURE = 'auth/SIGNUP_FAILURE';

const LOGOUT_REQUEST = 'auth/LOGOUT_REQUEST';
const LOGOUT_SUCCESS = 'auth/LOGOUT_SUCCESS';
const LOGOUT_FAILURE = 'auth/LOGOUT_FAILURE';

const SIGNIN_REQUEST = 'auth/SIGNIN_REQUEST';
const SIGNIN_SUCCESS = 'auth/SIGNIN_SUCCESS';
const SIGNIN_FAILURE = 'auth/SIGNIN_FAILURE';

const SAVE_USER = 'auth/SAVE_USER';

const MOBILE_CONFIRM_REQUEST = 'auth/MOBILE_CONFIRM_REQUEST';
const MOBILE_CONFIRM_SUCCESS = 'auth/MOBILE_CONFIRM_SUCCESS';
const MOBILE_CONFIRM_FAILURE = 'auth/MOBILE_CONFIRM_FAIULRE';

const CODE_CONFIRM_REQUEST = 'auth/CODE_CONFIRM_REQUEST';
const CODE_CONFIRM_SUCCESS = 'auth/CODE_CONFIRM_SUCCESS';
const CODE_CONFIRM_FAILURE = 'auth/CODE_CONFIRM_FAIULRE';

const SAVE_DATA = 'auth/SAVE_DATA';

const SAVE_AVATAR = 'auth/SAVE_AVATAR';

const UPDATE_AVATAR_REQUEST = 'auth/UPDATE_AVATAR_REQUEST';
const UPDATE_AVATAR_SUCCESS = 'auth/UPDATE_AVATAR_SUCCESS';

const NEW_NOTIFICATION = 'auth/NEW_NOTIFICATION';
const MAKE_NOTIFICATIONS_SEEN = 'auth/MAKE_NOTIFICATIONS_SEEN';
const MAKE_NOTIFICATIONS_UNSEEN = 'auth/MAKE_NOTIFICATIONS_UNSEEN';

const SET_BLOCK_STATUS_REQUEST = 'auth/SET_BLOCK_STATUS_REQUEST';
const SET_BLOCK_STATUS_SUCCESS = 'auth/SET_BLOCK_STATUS_SUCCESS';
const SET_BLOCK_STATUS_FAILURE = 'auth/SET_BLOCK_STATUS_FAILURE';

const SET_DEFAULT_STATUS = 'auth/SET_DEFAULT_STATUS';

export const optionsMock = [
  {
    name: 'Куда пойти',
    enabled: true,
    id: 'where',
  },
  {
    name: 'Что нового?',
    enabled: true,
    id: 'new',
  },
  {
    name: 'С кем пересекался',
    enabled: true,
    id: 'cross',
  },
  {
    name: 'Приятные плюшки',
    enabled: true,
    id: 'pies',
  },
];

export const initialState = {
  loading: false,
  authenticated: false,
  user: {},
  mobilePhone: null,
  userData: {
    name: '',
    surname: '',
    patronymic: '',
    sex: '',
    birthday: '2020-02-20',
    address: 'Россия, Санкт-Петербург, улица Бабушкина, 52',
    email: '',
    phoneNumber: '',
    photo: '',
  },
  // token:
  //   'uTFONI24zQmuU7B9d5LiNdRB6rlebp9H3OyRYVEes6Kwvfn_wANoVZZsd5M_8mjbdM1iE3STyAr9feeiEMNS2h2RH3-6HyV_StmPlcwJHGvDrqslzc9Ke4OdoIhrCJYT',
  token: '',
  avatar: '',
  notifications: [],
  deviceToken: null,
  isUnseen: false,
  displayBlocks: optionsMock,
  searchHistory: [],
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case SIGNUP_REQUEST:
      return {...state, loading: true};
    case SIGNUP_SUCCESS:
      return {
        ...state,
        authenticated: true,
        loading: false,
        // user: action.payload,
        token: action.token,
      };
    case SIGNUP_FAILURE:
      return {...state, loading: false};

    case SIGNIN_REQUEST:
      return {...state, loading: true};
    case SIGNIN_SUCCESS:
      return {
        ...state,
        loading: false,
        authenticated: true,
        userData: action.userData,
        token: action.token,
      };
    case MOBILE_CONFIRM_REQUEST:
      return {...state, loading: true};
    case MOBILE_CONFIRM_SUCCESS:
      return {...state, loading: false, mobilePhone: action.mobilePhone};
    case MOBILE_CONFIRM_FAILURE:
      return {...state, loading: false};

    case CODE_CONFIRM_REQUEST:
      return {...state, loading: true};
    case CODE_CONFIRM_SUCCESS:
      return {...state, loading: false};
    case CODE_CONFIRM_FAILURE:
      return {...state, loading: false};

    case SAVE_DATA:
      return formCredentials(state, action.field, action.payload);
    case SAVE_AVATAR:
      return {...state, avatar: action.payload};
    case UPDATE_AVATAR_SUCCESS:
      return {...state, userData: action.userData};

    case NEW_NOTIFICATION:
      return {
        ...state,
        notifications: [...state.notifications, action.notification],
      };

    case MAKE_NOTIFICATIONS_SEEN:
      return {
        ...state,
        notifications: updateNotifications(state.notifications),
        isUnseen: false,
      };
    case MAKE_NOTIFICATIONS_UNSEEN:
      return {
        ...state,
        isUnseen: true,
      };
    case SET_BLOCK_STATUS_SUCCESS:
      return {
        ...state,
        displayBlocks: action.payload,
      };

    case SET_DEFAULT_STATUS:
      return {
        ...state,
        displayBlocks: optionsMock,
      };
    case SAVE_USER:
      return {
        ...state,
        userData: action.payload,
      };
    case LOGOUT_SUCCESS:
      return {
        ...initialState,
      };
    default:
      return state;
  }
}

// <<<ACTIONS>>>
export const requestLogOut = () => ({
  type: 'USER_LOGOUT',
});

export const requestSignUp = credentials => ({
  type: SIGNUP_REQUEST,
  payload: credentials,
});

export const requestSignIn = payload => ({
  type: SIGNIN_REQUEST,
  payload,
});

export const saveUser = payload => ({
  type: SAVE_USER,
  payload,
});

export const mobileConfirmRequest = mobileNumber => {
  console.log('requestmobileConfirmRequest');
  return {
    type: MOBILE_CONFIRM_REQUEST,
    payload: mobileNumber,
  };
};

export const codeConfirmRequest = code => ({
  type: CODE_CONFIRM_REQUEST,
  payload: code,
});

export const requestFormData = (field, value) => {
  console.log(field, value);
  return {
    type: SAVE_DATA,
    payload: value,
    field,
  };
};
export const requestSaveAvatar = avatar => ({
  type: SAVE_AVATAR,
  payload: avatar,
});

export const requestUpdateAvatar = avatar => ({
  type: UPDATE_AVATAR_REQUEST,
  payload: avatar,
});

export const saveNewNotificaion = notification => {
  console.log('newAction', notification);
  return {
    type: NEW_NOTIFICATION,
    notification,
  };
};

export const makeNotificationsSeen = () => ({
  type: MAKE_NOTIFICATIONS_SEEN,
});

export const makeNotificationsUnseen = () => ({
  type: MAKE_NOTIFICATIONS_UNSEEN,
});

export const setStatusForBlockRequest = (option, status) => ({
  type: SET_BLOCK_STATUS_REQUEST,
  option: option,
  status: status,
});

export const setDefaultStatuses = () => ({
  type: SET_DEFAULT_STATUS,
});

// <<<WORKERS>>>
function* logOut() {
  try {
    yield call(TokenStorage.clearAll);
    yield put({
      type: LOGOUT_SUCCESS,
    });
  } catch (err) {
    console.log('err', err);
  }
}

function* signIn({payload}) {
  try {
    // const deviceToken = yield call(messaging().getToken);
    let deviceToken = yield select(state => state.auth.deviceToken);
    if (deviceToken === null) {
      deviceToken = yield call(messaging().getToken);
      yield call(DeviceTokenStorage.save, deviceToken);
    }
    console.log('devieTokenFromSignin', deviceToken);
    const params = {
      phone: `7${payload.extractedNumber}`,
      os: Platform.OS,
      token: deviceToken,
      code: payload.code,
    };
    console.log('params', params);
    const {data} = yield call(
      http.post,
      '/cabinet/authorization/index',
      params,
    );
    console.log('data-authorization', data);
    yield call(TokenStorage.save, data.data.entity['access-token']);
    // const token = 'uTFONI24zQmuU7B9d5LiNdRB6rlebp9H3OyRYVEes6Kwvfn_wANoVZZsd5M_8mjbdM1iE3STyAr9feeiEMNS2h2RH3-6HyV_StmPlcwJHGvDrqslzc9Ke4OdoIhrCJYT';
    const user = yield call(
      http.get,
      `/cabinet/profile/view?access-token=${data.data.entity['access-token']}`,
    );
    console.log('data-user', user);
    yield put({
      type: SIGNIN_SUCCESS,
      userData: user.data.data.entity,
      token: data.data.entity['access-token'],
    });
  } catch (error) {
    console.log('signIn', error.response);
    if (error.response.status === 422) {
      alert(error.response.data.data.message);
    }
  }
}
function* signUp({payload}) {
  try {
    let deviceToken = yield select(state => state.auth.deviceToken);
    if (deviceToken === null) {
      deviceToken = yield call(messaging().getToken);
      yield call(DeviceTokenStorage.save, deviceToken);
    }
    const phoneNumber = yield select(state => state.auth.userData.phoneNumber);
    const userData = yield select(state => state.auth.userData);
    const phoneInfo = {
      phone: phoneNumber,
      os: Platform.OS,
      token: deviceToken,
      email: userData.email,
    };
    const {data} = yield call(
      http.post,
      '/cabinet/registration/index',
      phoneInfo,
    );
    console.log('token', data.data.entity['access-token']);
    yield call(PhoneNumberStorage.save, phoneNumber);
    yield call(TokenStorage.save, data.data.entity['access-token']);

    const avatarUriBase64 = yield select(state => state.auth.avatar);
    console.log('userData', userData);
    yield call(
      http.post,
      `/cabinet/profile/update?access-token=${
        data.data.entity['access-token']
      }`,
      userData,
    );
    if (avatarUriBase64 !== '') {
      yield call(
        http.post,
        `/cabinet/profile/photo?access-token=${
          data.data.entity['access-token']
        }`,
        {
          name: 'avatar.jpeg',
          content: avatarUriBase64,
        },
      );
    }
    const user = yield call(
      http.get,
      `/cabinet/profile/view?access-token=${data.data.entity['access-token']}`,
    );
    yield put({
      type: SIGNUP_SUCCESS,
      token: data.data.entity['access-token'],
      userData: user.data.data.entity,
    });
  } catch (err) {
    console.log(err.response.data.data.message);
    yield put({type: SIGNUP_FAILURE, payload: 'Введите код снова'});
  }
}

function* mobileConfirm({payload}) {
  try {
    console.log('mobileConfirm', `7${payload}`);
    const data = yield call(http.post, '/verify/default/phone-code', {
      phone: `7${payload}`,
    });
    console.log('datdatdatdat', data);
    yield put({
      type: MOBILE_CONFIRM_SUCCESS,
      payload: payload,
      mobilePhone: payload,
    });
  } catch (err) {
    alert(err);
    console.log(err);
    yield put({type: MOBILE_CONFIRM_FAILURE});
  }
}

function* codeConfirm({payload}) {
  try {
    console.log('enteredCode', payload);
    const mobilePhone = yield select(state => state.auth.mobilePhone);
    console.log('sendedCodeNumber', mobilePhone);
    const data = yield call(http.post, '/verify/default/phone-verify', {
      phone: 79853069774,
      code: payload,
    });
    console.log('response', data);
    yield put({type: CODE_CONFIRM_SUCCESS, payload: payload});
  } catch (err) {
    console.log(err);
    yield put({type: CODE_CONFIRM_FAILURE});
  }
}

function* setOptionStatus({option, status}) {
  try {
    console.log('setOptionStatusSaga', option, status);
    const displayBlocks = yield select(state => state.auth.displayBlocks);
    const formatted = displayBlocks.map(block => {
      if (block.id === option) {
        return {
          ...block,
          enabled: status,
        };
      }
      return block;
    });
    // yield call(DisplayBlocksStorage.save, formatted);
    yield put({type: SET_BLOCK_STATUS_SUCCESS, payload: formatted});
  } catch (err) {
    console.log('err');
  }
}

function* updateAvatar({payload}) {
  try {
    const token = yield select(state => state.auth.token);
    console.log('avatar', payload);
    yield call(http.post, `/cabinet/profile/photo?access-token=${token}`, {
      name: 'avatar.jpeg',
      content: payload,
    });
    const user = yield call(
      http.get,
      `/cabinet/profile/view?access-token=${token}`,
    );
    console.log('data-user', user);
    yield put({
      type: UPDATE_AVATAR_SUCCESS,
      userData: user.data.data.entity,
    });
  } catch (err) {
    console.log('err', err.response);
  }
}
// <<<WATCHERS>>>
export function* watchSignUp() {
  yield takeEvery(SIGNUP_REQUEST, signUp);
}

export function* watchMobileConfirm() {
  yield takeEvery(MOBILE_CONFIRM_REQUEST, mobileConfirm);
}

export function* watchCodeConfirm() {
  yield takeEvery(CODE_CONFIRM_REQUEST, codeConfirm);
}

export function* watchSignIn() {
  yield takeEvery(SIGNIN_REQUEST, signIn);
}

export function* watchSetStatus() {
  yield takeEvery(SET_BLOCK_STATUS_REQUEST, setOptionStatus);
}

export function* watchLogOut() {
  yield takeEvery(LOGOUT_REQUEST, logOut);
}

export function* watchUpdateAvatar() {
  yield takeEvery(UPDATE_AVATAR_REQUEST, updateAvatar);
}
