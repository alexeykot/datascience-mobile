export const formCredentials = (state, field, value) => {
  console.log(field, value);
  return {
    ...state,
    userData: {
      ...state.userData,
      [field]: value,
    },
  };
};

export const updateNotifications = notifications => {
  console.log('notificationsnotificationsnotifications', notifications);
  if (!notifications.length) {
    return [];
  }
  notifications.forEach(notif => (notif.seen = true));
  console.log('updated', notifications);
  return notifications;
};

export const updateOptionStatus = (options, option, status) => {
  console.log('update', option, status);
  return options;
};
