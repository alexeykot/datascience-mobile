import {all, fork} from 'redux-saga/effects';

import * as authWatchers from './auth';
import * as chatWatchers from './chat';

export default function* root() {
  yield all([
    fork(authWatchers.watchSignUp),
    fork(authWatchers.watchMobileConfirm),
    fork(authWatchers.watchCodeConfirm),
    fork(authWatchers.watchSignIn),
    fork(authWatchers.watchSetStatus),
    fork(authWatchers.watchLogOut),
    fork(authWatchers.watchUpdateAvatar),
    fork(chatWatchers.watchGetMyself),
  ]);
}
