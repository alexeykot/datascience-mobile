import axios from 'axios';
import ChatStorage from '../storage/chat';
import baseChatUrl from '../../configs/chat';

const httpChat = axios.create({
  withCredentials: false,
  baseURL: baseChatUrl,
  headers: {'Content-Type': 'application/json'},
});

// const data = {
//   authToken: '5CW9WcXE0uYLB6B_YeV0Lia-U_qTAVl9ydtt5sI1v_J123',
//   userId: 'CMCJtiJuXMPzrrPCR',
// };

httpChat.interceptors.request.use(
  async request => {
    const data = await ChatStorage.get();
    const newRequest = {...request};
    newRequest.headers = {
      'X-Auth-Token': data.authToken,
      'X-User-Id': data.userId,
    };
    return newRequest;
  },
  error => {
    console.log('erererer', error);
  },
);

export default httpChat;
