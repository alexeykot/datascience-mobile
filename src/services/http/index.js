import axios from 'axios';
import TokenStorage from '../storage/token';
import baseURL from '../../configs/api';

const http = axios.create({
  withCredentials: false,
  baseURL,
  headers: {'Content-Type': 'application/json'},
});

http.interceptors.request.use(
  async request => {
    const token = await TokenStorage.get();
    // const token =
    //   'XYrMoqxD8_s5l8haWAzXikUPTMTLXlB8yw3PGx-0oaZbmCtjT4XmTj6YzbqAG2CiWxul03D-6cVD_daCHVdUd3RcZ6KQRYw8TRr5Vpy0z6ouk6EqLMhu6bHzdzSJ7pKQ';
    const newRequest = {...request};
    if (token) {
      newRequest.params = {'access-token': token};
    }
    return newRequest;
  },
  error => Promise.reject(error),
);
// http.interceptors.request.use(
//   async request => {
//     // const token = await TokenStorage.get();
//     const token = 'yMAuIUyZbjXuSW8lgpR53Gyn-c0dITGde8bgi_YA3N60v0DF-WsZMAe5AwPOX6P6HmwT6iyzTNPHZAm9jwXi4xbcPI50yT0d7QJEEhDwChB9UPRCoyZ-UHf3rkpJuMKT';
//     const newRequest = {...request};
//     console.log('requestToken', token);
//     if (token) {
//       newRequest.headers.Authorization = `Bearer ${token}`;
//     }
//     return newRequest;
//   },
//   error => Promise.reject(error),
// );
export default http;
