import Storage from './index';

class ChatStorage extends Storage {
  constructor() {
    super('Chat');
  }
}

export default new ChatStorage();
