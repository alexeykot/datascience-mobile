import Storage from './index';

class SearchHistoryStorage extends Storage {
  constructor() {
    super('SearchHistory');
  }
}

export default new SearchHistoryStorage();
