import Storage from './index';

class DeviceTokenStorage extends Storage {
  constructor() {
    super('Device-Token');
  }
}

export default new DeviceTokenStorage();
