import Storage from './index';

class NotificationsStorage extends Storage {
  constructor() {
    super('Notifications');
  }
}

export default new NotificationsStorage();
