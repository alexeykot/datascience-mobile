import Storage from './index';

class HidedChatsStorage extends Storage {
  constructor() {
    super('HidedChats');
  }
}

export default new HidedChatsStorage();
