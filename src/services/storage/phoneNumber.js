import Storage from './index';

class PhoneNumberStorage extends Storage {
  constructor() {
    super('PhoneNumber');
  }
}

export default new PhoneNumberStorage();
