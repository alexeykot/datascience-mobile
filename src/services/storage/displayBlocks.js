import Storage from './index';

class DisplayBlocksStorage extends Storage {
  constructor() {
    super('DisplayBlocksStorage');
  }
}

export default new DisplayBlocksStorage();
