import {AsyncStorage} from 'react-native';

export default class Storage {
  constructor(type) {
    this.type = type;
  }

  delete = async () => AsyncStorage.removeItem(`DS-${this.type}`);

  get = async () => {
    const item = await AsyncStorage.getItem(`DS-${this.type}`);

    return JSON.parse(item);
  };

  save = async item =>
    AsyncStorage.setItem(`DS-${this.type}`, JSON.stringify(item));

  clearAll = async () =>
    await AsyncStorage.multiRemove([
      'DS-Device-Token',
      'DS-DisplayBlocksStorage',
      'DS-Notifications',
      'DS-Token',
      'DS-Chat',
    ]);
}
