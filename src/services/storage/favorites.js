import Storage from './index';

class FavoritesStorage extends Storage {
  constructor() {
    super('Favorites');
  }
}

export default new FavoritesStorage();
