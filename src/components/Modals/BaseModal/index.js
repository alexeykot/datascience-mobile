import React from 'react';
import Modal from 'react-native-modal';
import Close from '../../../assets/icons/close-01.svg';

import * as S from './styled';

const BaseModal = ({isVisible, onClose, content, height = 240}) => {
  const close = () => {
    onClose();
  };
  return (
    <Modal onBackdropPress={close} isVisible={isVisible}>
      <S.LargeContainer height={height}>
        <S.CloseButton onPress={close}>
          <Close />
        </S.CloseButton>
        {content}
      </S.LargeContainer>
    </Modal>
  );
};

export default BaseModal;
