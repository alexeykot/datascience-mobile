import React from 'react';
import BaseModal from '../BaseModal';
import Status from '../../../assets/icons/Cross.svg';

import * as S from './styled';

const CrossModal = ({isVisible, onClose}) => {
  return (
    <BaseModal
      isVisible={isVisible}
      onClose={onClose}
      content={
        <>
          <Status height={70} width={70} />
          <S.BlackText>
            Здесь отображаются пользователи, которые были с вами на одном
            мероприятии
          </S.BlackText>
        </>
      }
    />
  );
};

export default CrossModal;
