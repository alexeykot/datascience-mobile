import styled from 'styled-components';

export const BlackText = styled.Text`
  color: #1b041e;
  font-family: Roboto;
  font-size: 16px;
  font-weight: 700;
  text-align: center;
  margin-top: 20px;
`;
