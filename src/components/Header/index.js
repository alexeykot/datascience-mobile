import React from 'react';
import {useNavigation} from '@react-navigation/native';

import Back from '../../assets/icons/back-01.svg';
import Heart from '../../assets/icons/heart-02.svg';
import HeartFill from '../../assets/icons/heart-02-fill.svg';

import * as S from './styled';

const Header = ({
  title,
  color,
  onRightPress,
  rightIcon,
  isFavorite = false,
  withoutRightButton = false,
}) => {
  const navigation = useNavigation();
  const renderIcon = () => {
    if (rightIcon) {
      return rightIcon;
    }
    return !isFavorite ? <Heart /> : <HeartFill />;
  };
  return (
    <S.Container color={color}>
      <S.LeftButton onPress={() => navigation.goBack()}>
        <Back />
      </S.LeftButton>
      <S.Title>{title}</S.Title>
      {!withoutRightButton ? (
        <S.RightButton onPress={onRightPress}>{renderIcon()}</S.RightButton>
      ) : null}
    </S.Container>
  );
};

export default Header;
