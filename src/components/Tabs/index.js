import React from 'react';
import {Dimensions} from 'react-native';

import * as S from './styled';

const Tabs = ({tabsProps: {tabs, selectedTab, onTabPress}}) => {
  return (
    <>
      <S.TabsContainer>
        {tabs.map((tab, index) => (
          <S.Tab
            count={tabs.length}
            selected={selectedTab === tab}
            onPress={() => {
              onTabPress(tab);
            }}>
            <S.TabTitle selected={selectedTab === tab}>{tab}</S.TabTitle>
            {selectedTab === tab ? <S.SelectedBorder /> : null}
          </S.Tab>
        ))}
      </S.TabsContainer>
      <S.BorderContainer>
        <S.Border />
      </S.BorderContainer>
    </>
  );
};

export default Tabs;
