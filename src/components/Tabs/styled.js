import styled from 'styled-components';

export const TabsContainer = styled.View`
  width: 100%;
  height: 50px;
  margin-top: 20px;
  flex-direction: row;
`;

export const Border = styled.View`
  width: 100%;
  height: 1px;
  background-color: #c9ccd5;
  opacity: 0.5;
  z-index: 1;
`;

export const BorderContainer = styled.View`
  width: 100%;
`;

export const SelectedBorder = styled.View`
  height: 3px;
  border-radius: 2px;
  background-color: #5954f9;
  width: 100%;
  position: absolute;
  bottom: 0px;
  z-index: 1111;
`;

export const Tab = styled.TouchableOpacity`
  width: ${({count}) => 100 / count}%;
  height: 100%;
  justify-content: center;
  align-items: center;
  padding-bottom: 10px;
`;

export const TabTitle = styled.Text`
  color: ${({selected}) => (selected ? '#5954f9' : '#1b041e')};
  font-family: Roboto;
  font-size: 14px;
  font-weight: 700;
`;
