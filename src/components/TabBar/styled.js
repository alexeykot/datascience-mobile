import styled from 'styled-components';

export const Wrapper = styled.View`
  width: 100%;
  height: 60px;
  background-color: white;
`;

export const Container = styled.View`
  height: 60px;
  width: 100%;
  border-radius: 15px;
  elevation: 10;
  border-bottom-left-radius: 0;
  border-bottom-right-radius: 0;
  background-color: white;
  flex-direction: row;
  padding: 0 15px;
  justify-content: space-between;
`;

export const Icon = styled.Image`
  align-self: center;
  margin-bottom: 8px;
`;

export const OptionButton = styled.TouchableOpacity`
  justify-content: center;
  align-items: center;
  align-self: center;
  /* width: max-content; */
  background-color: ${({isFocused}) =>
    isFocused ? 'rgba(89, 84, 249, 0.08)' : 'transparent'};
  border-radius: 19px;
  flex: 5;
`;

export const UnreadPin = styled.View`
  border: 1px solid white;
  width: 8px;
  height: 8px;
  border-radius: 7px;
  background-color: red;
  position: absolute;
  top: 2px;
  right: 23px;
`;

export const OptionText = styled.Text`
  color: #1b041e;
  font-family: Roboto;
  font-size: 10px;
  text-align: center;
`;
