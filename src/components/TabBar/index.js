import React from 'react';
import {Keyboard} from 'react-native';

import _ from 'lodash';
import HomeIcon from '../../assets/icons/home-2-line.svg';
import CalendarIcon from '../../assets/icons/calendar-2-line.svg';
import ChatIcon from '../../assets/icons/chaht-2-line.svg';
import GiftIcon from '../../assets/icons/gift-2-line.svg';
// import MoreIcon from '../../assets/icons/more-2-line.svg';
import MoreIcon from '../../assets/icons/heart-grey.svg';
import MoreIconFill from '../../assets/icons/heart-01.svg';

import * as S from './styled';
import {connect} from 'react-redux';
import http from '../../services/http';

const config = [
  {
    name: 'Главная',
    icon: <HomeIcon fill="#c9ccd5" />,
    activeIcon: <HomeIcon fill="#5954f9" />,
    route: 'MainScreen',
    id: 0,
  },
  {
    name: 'События',
    icon: <CalendarIcon fill="#c9ccd5" />,
    activeIcon: <CalendarIcon fill="#5954f9" />,
    route: 'AllEventsScreen',
    id: 1,
  },
  {
    name: 'Плюшки',
    icon: <GiftIcon fill="#c9ccd5" />,
    activeIcon: <GiftIcon fill="#5954f9" />,
    route: 'PiesScreen',
    id: 2,
  },
  {
    name: 'Чат',
    icon: <ChatIcon fill="#c9ccd5" />,
    activeIcon: <ChatIcon fill="#5954f9" />,
    route: 'DialogsScreen',
    id: 3,
  },
  {
    name: 'Избранное',
    icon: <MoreIcon fill="#c9ccd5" />,
    activeIcon: <MoreIconFill />,
    route: 'FavoritesScreen',
    id: 4,
  },
];

const TabBar = props => {
  const [keyboardIsShow, setKeyboardShow] = React.useState(false);

  React.useEffect(() => {
    Keyboard.addListener('keyboardDidShow', () => {
      setKeyboardShow(true);
    });
    Keyboard.addListener('keyboardDidHide', () => {
      setKeyboardShow(false);
    });
  });

  async function onRoutePress(route) {
    if (route.route === 'PiesScreen') {
      const {data} = await http.get(
        `/pie/default/index?access-token=${props.token}`,
      );
      const categories = data.data.entities
        .map(pie => {
          if (pie.categories.length) {
            return pie.categories;
          }
        })
        .filter(c => c !== undefined)
        .flat();
      const uniqCategories = _.uniqBy(categories, c => c.id);
      props.navigation.navigate('PiesScreen', {
        items: data.data.entities,
        categories: uniqCategories,
      });
      return;
    }
    props.navigation.navigate(route.route);
  }
  const isUnread = route => {
    if (route === 'DialogsScreen' && props.unread) {
      const unread = props.unread.update
        .filter(room => room.t === 'd')
        .some(room => room.unread > 0);
      return unread;
    }
    return false;
  };
  return (
    <>
      {!keyboardIsShow ? (
        <S.Wrapper>
          <S.Container>
            {config.map((route, index) => (
              <S.OptionButton
                isUnread={isUnread(route.route)}
                onPress={() => onRoutePress(route)}
                isFocused={props.state.index === index}>
                {props.state.index === index ? route.activeIcon : route.icon}
                <S.OptionText>{route.name}</S.OptionText>
                {isUnread(route.route) ? <S.UnreadPin /> : null}
              </S.OptionButton>
            ))}
          </S.Container>
        </S.Wrapper>
      ) : null}
    </>
  );
};

const mapStateToProps = ({auth, chat}) => ({
  token: auth.token,
  unread: chat.unread,
});

export default connect(
  mapStateToProps,
  null,
)(TabBar);
// export default TabBar;
