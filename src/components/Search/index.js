import React from 'react';

import SearchIcon from '../../assets/icons/search.svg';
import Filter from '../../assets/icons/filter.svg';
import ResetIcon from '../../assets/icons/close-circle-fill.svg';

import * as S from './styled';

const Search = ({
  searchProps: {
    filterText,
    onResetPress,
    onInputChange,
    inputPlaceholder,
    withFilter,
    onFilterPress,
    isFiltered,
    setInputFocused = () => {},
    setInputBlured = () => {},
    onSubmit = () => {},
    setRef = () => {},
  },
}) => {
  return (
    <S.Container>
      {!withFilter ? (
        <>
          {filterText ? (
            <S.ResetButton onPress={onResetPress}>
              <ResetIcon />
            </S.ResetButton>
          ) : (
            <S.SearchIconContainer>
              <SearchIcon fill="#5954f9" />
            </S.SearchIconContainer>
          )}
          <S.Input
            onChangeText={text => onInputChange(text.toLocaleLowerCase())}
            placeholder={inputPlaceholder}
            value={filterText}
            onFocus={setInputFocused}
            onBlur={setInputBlured}
            onSubmitEditing={onSubmit}
            ref={r => setRef(r)}
          />
        </>
      ) : (
        <>
          <S.SearchIconContainerLeft>
            <SearchIcon fill="#c9ccd5" />
          </S.SearchIconContainerLeft>
          {!filterText ? (
            <S.FilterIcon onPress={onFilterPress}>
              <Filter fill={isFiltered() ? '#5954f9' : '#c9ccd5'} />
            </S.FilterIcon>
          ) : (
            <S.ResetButton onPress={onResetPress}>
              <ResetIcon />
            </S.ResetButton>
          )}
          <S.Input
            withFilter={withFilter}
            onChangeText={text => onInputChange(text.toLocaleLowerCase())}
            placeholder="Поиск"
            placeholderTextColor="rgba(139, 141, 148, 0.38)"
            value={filterText}
          />
        </>
      )}
    </S.Container>
  );
};

export default Search;
