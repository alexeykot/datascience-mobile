import styled from 'styled-components';

export const Container = styled.View`
  position: absolute;
  align-self: center;
  width: 300px;
  height: 40px;
  top: -25px;
  elevation: 5;
  border-radius: 20px;
  background-color: white;
  justify-content: center;
  align-items: center;
`;

export const ResetButton = styled.TouchableOpacity`
  width: 50px;
  height: 40px;
  justify-content: center;
  align-items: center;
  position: absolute;
  right: 0px;
  z-index: 1111;
`;

export const SearchIconContainer = styled.View`
  position: absolute;
  right: 14px;
`;

export const SearchIconContainerLeft = styled.View`
  position: absolute;
  left: 16px;
  z-index: 1111;
  opacity: 0.5;
`;

export const Input = styled.TextInput`
  width: 100%;
  height: 100%;
  padding-left: 15px;
  padding-left: ${({withFilter}) => (withFilter ? 39 : 15)}px;
  border-radius: 20px;
  opacity: 0.38;
`;

export const FilterIcon = styled.TouchableOpacity`
  position: absolute;
  right: 0px;
  border-top-right-radius: 30px;
  padding-top: 5px;
  border-bottom-right-radius: 30px;
  height: 100%;
  width: 50px;
  justify-content: center;
  background-color: white;
  align-items: center;
  z-index: 1111;
`;
