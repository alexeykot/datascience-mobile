import styled from 'styled-components';

export const Container = styled.View`
  background-color: #5954f9;
  flex: 1;
`;

export const ScrollableOptionContainer = styled.ScrollView`
  width: 100%;
  height: 700px;
  border-top-left-radius: 30px;
  border-top-right-radius: 30px;
  background-color: #ffffff;
`;

export const ViewOptionsContainer = styled.View`
  width: 100%;
  height: 710px;
  border-top-left-radius: 30px;
  border-top-right-radius: 30px;
  background-color: white;
  margin-top: 20px;
  padding: ${({containerPadding}) => containerPadding};
`;
