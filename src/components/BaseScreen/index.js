import React from 'react';
import {StatusBar} from 'react-native';

import Header from '../Header';
import Search from '../Search';
import Tabs from '../Tabs';

import * as S from './styled';

const BaseScreen = ({
  scrollableInsideContainer,
  scrollableOutsideContainer,
  title,
  withSearch,
  searchProps,
  withTabs,
  tabsProps,
  withoutRightButton = true,
  onRightPress,
  rightIcon,
  content,
  containerPadding = '0 15px',
}) => (
  <S.Container>
    <StatusBar barStyle="light-content" backgroundColor="#5954f9" />
    <Header
      title={title}
      withoutRightButton={withoutRightButton}
      onRightPress={onRightPress}
      rightIcon={rightIcon}
    />
    {scrollableInsideContainer ? (
      <S.ScrollableOptionContainer>{content}</S.ScrollableOptionContainer>
    ) : null}
    {scrollableOutsideContainer ? (
      <S.ViewOptionsContainer containerPadding={containerPadding}>
        {withSearch ? <Search searchProps={searchProps} /> : null}
        {withTabs ? <Tabs tabsProps={tabsProps} /> : null}
        {content}
      </S.ViewOptionsContainer>
    ) : null}
  </S.Container>
);

export default BaseScreen;
