import moment from 'moment';

export const formatDate = date => moment(date).format('D MMMM');
export const isSameMonth = ev =>
  moment(ev.beginDate).isSame(ev.finishDate, 'month');
export const isSameDay = ev =>
  moment(ev.beginDate).isSame(ev.finishDate, 'date');
export const getTime = ev => {
  if (ev.finishDate && ev.beginDate) {
    if (isSameDay(ev)) {
      return formatDate(ev.beginDate);
    }
    if (isSameMonth(ev)) {
      return `${moment(ev.beginDate).format('D')} - ${moment(
        ev.finishDate,
      ).format('D MMMM')}`;
    }
    return `${formatDate(ev.beginDate)} - ${formatDate(ev.finishDate)}`;
  }
  if (ev.beginDate) {
    return formatDate(ev.beginDate);
  }
  return '';
};
