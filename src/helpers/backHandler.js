import {BackHandler, Alert} from 'react-native';
/**
 * Attaches an event listener that handles the android-only hardware
 * back button
 * @param  {Function} callback The function to call on click
 */
const handleAndroidBackButton = callback => {
  BackHandler.addEventListener('hardwareBackPress', () => {
    callback();
    return true;
  });
};
/**
 * Removes the event listener in order not to add a new one
 * every time the view component re-mounts
 */
const removeAndroidBackButtonHandler = () => {
  console.log('removed');
  BackHandler.removeEventListener('hardwareBackPress', () => {});
};

const exitAlert = () => {
  Alert.alert(
    'Подтверждение выхода',
    'Вы уверены, что хотите выйти из приложения?',
    [
      {
        text: 'Отменить',
        style: 'cancel',
      },
      {text: 'Да, уверен', onPress: () => BackHandler.exitApp()},
    ],
  );
};
export {handleAndroidBackButton, removeAndroidBackButtonHandler, exitAlert};
